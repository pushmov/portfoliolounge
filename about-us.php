<div class="container">
	<div class="content" style="background-image:url(img/banner/detroit3.jpg);background-repeat:no-repeat;background-size:100%;padding-top:400px;">
		<div class="inner">
			<div id="about">
		
				<h1 class="strategic">Create a portfolio website.</h1>
				<h2>About PortfolioLounge.com</h2>
				<p>Founded in iconic Detroit, Michigan and boasting more than ten thousand members, PortfolioLounge has become a creative hosting powerhouse. Beginning with its inception in February 2013, and continuing today, design has been the main focus of PortfolioLounge and usability the first priority. </p>
				<p>Behind the design of our site is the understanding of how time-consuming it can be to build an online portfolio from scratch. We designed PortfolioLounge to help users learn to create the most aesthetically pleasing portfolios with easy-to-choose options that don't require any programming or HTML knowledge. We've included features to make portfolio creation quick and easy, with various subscription levels and several different hosting options. With the portfolio hosting and creation tools we offer, expanding the reach and scope of your work is both simple and easy. Your work can be beautifully showcased and always accessible in a unique, customized setting of your choosing. We've made it easy to have a professional, highly personalized, custom domain for your work, be it sculpture, painting, writing, photography or other media.</p>		
				<p>
				 At PortfolioLounge.com, we offer simple, effective options to surround your work with your own flair and 
				
				style. There are easily customized layout options, unique fonts and lots of storage space so that you have 
				
				plenty of room to add to your portfolio as you create new works. Feedback options are available, so that you 
				
				can keep a finger on the pulse of your portfolio and find out what people are thinking about your work.</p>
				<p>
				
				New tools and offerings are being added on a regular basis, and the content of your portfolio can be fully 
				
				integrated, no matter where it comes from. Your portfolio can be password protected so that only those 
				
				you select may view it, and there are no bandwidth restrictions; your work is always available, regardless of 
				
				how much traffic you attract; you can even track your traffic using Google Analytics. With PortfolioLounge, 
				
				everything is handled for you. We designed it to be that way, to help you have more time to work on your 
				
				passion.</p>
				
			</div>
			<style>
				#about {
					line-height:1.5em;
					font-size:12px;	
					padding:50px 84px;
				}
				#about p {
					margin:20px 0 20px;	
				}
				#about h1 {
					margin:20px 0 80px;	
					font-size:50px;
				}
			</style>
		</div>
	</div>
</div>