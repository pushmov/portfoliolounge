<!--
<div class="container">
<h1>
	Wait, what is Portfoliolounge?
</h1>
<p>
	As our name suggests, PortfolioLounge is all about a relaxed and comfortable approach to showcasing your work, so you can focus on creating, instead of on portfolio management. 
</p>
<p>
	PortfolioLounge.com was created by a developer whose passion for the visual arts led him to create an intuitive, efficient, attention-getting way for professionals to show the world what they can do.
</p>
<p>
	User-friendly and easy to navigate, PortfolioLounge gives you a professional-looking portfolio in minutes. Just pick a layout theme that reflects your personal style, enter your contact information, upload your best work, describe your projects with our editing tools, and let your creative output shine for potential clients and employers.
</p>
<p>
	There’s no time like right now to get started. Head over to our subscription page and select the package that’s right for you. Or take a free test drive with our tryout version of PortfolioLounge.
</p>




</div>
-->


<!--
-->

<div class="container">
	<div class="content" style="background-image:url(img/banner/detroit2.jpg);background-repeat:no-repeat;background-size:100%;padding-top:400px;padding-bottom:60px;margin-bottom:100px;">
		<div class="inner">
			<div id="about_page">
				<h2>Awesome Online Portfolios quickly and easily.</h2>
				<hr />
				<p>With Portfolio Lounge, you've got an awesome platform for your work.  From creation to management, your portfolio comes alive with Portfolio Lounge.  All the hosting, coding and optimization for search engines is taken care of; you only need worry about uploading great content and customizing your portfolio to accurately reflect your style and personality.</p>
				<p>With our user-friendly interface, your portfolio can be up and running in no time, meaning that you're spending less time working on how your portfolio looks and more on the content you're putting in it.  With our reputation for safety, stability and reliability, you can rest easy knowing that your portfolio is both safe and visible.</p>
				<p>With the tools available from Portfolio Lounge, your portfolio will look professional and unique.  We never impose any restrictions on your creativity, offering customization options to perfect the look of your portfolio.  You have the option of uploading any logos you want to display, creating a specific domain that is unique to you and more.</p>
				
				<h2>Subscription Options</h2>
		<p>In order to ensure that you're impressed with how simple our services are to use, we offer the option of trying them for free.  There are some limits when you're using the free subscription option, but there is more than enough access to determine whether Portfolio Lounge can help you create a stunning portfolio; you can even upload your own custom logo.  Your portfolio may also be featured on our home page if it contains works that have been top-rated or if the portfolio itself is top-rated.  Visitors can browse your custom domain and have the option to rate your works and comment on them.  The free version of our services is available to you for as long as you'd like to use it; upgrade if you find that you need more space or access to expanded features.  All features in the free subscription are included in paid subscriptions with the following modifications:</p>
		
				<h3>Pro Subscription - $12.00 per month (our most popular plan)</h3>
				<ul>
					<li>Upload as many as 1000 items</li>
					<li>Hosting for 80 original projects</li>
					<li>Choose and customize design themes for showcasing your portfolio</li>
					<li>Use custom logos</li>
					<li>Track visitors using Google Analytics</li>
				</ul>
				
				<h3>Max Subscription - $24.00 per month</h3>
				<ul>
					<li>Upload as many as 10,000 items</li>
					<li>Hosting for 1000 original projects</li>
					<li>Choose and customize design themes for showcasing your portfolio</li>
					<li>Use custom logos</li>
					<li>Track visitors using Google Analytics</li>
				</ul>
				
				
				<h2>Registering for an Account</h2>
		After deciding which subscription option you'd like to try, you'll be taken to the "sign up" page.  Our registration form is brief and should only require a few minutes to fill out.  You'll be asked to choose a username which will become a portion of the URL for your portfolio.  Keep in mind that your username cannot be changed once the registration process is complete; you may choose to create a unique, mapped domain name at a later time by accessing your settings.  If you've chosen to create a free account, it will be created after the sign up form is submitted.  For paid subscriptions, you'll be directed to PayPal in order to complete payment and create your account.  We'll get in touch with you via email with a confirmation link that will activate your account and can either be clicked or copied and pasted into the browser.  Once you've completed the activation, you're ready to start building a portfolio that showcases your work beautifully.
		
				<!--
				
				-->
			</div>
		</div>
	</div>
</div>