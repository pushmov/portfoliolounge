<div style="background:#000;height:100%;width:100%;position:fixed;z-index:-1;top:0;">

	<div class="frame home-bg" style="height:100%;width:100%;position:fixed;background:#000;top:0;opacity:.3">
		<div class="canvas"
			data-source="img/photos/portfolio-photo-studio.jpg"
			data-source="img/photos/corporate-architecture.jpg">
		</div>
	</div>

</div>

<div style="font-size:20px;color:#fff;margin:100px auto;max-width:800px;line-height:1.8em;">
	<h1>
		Welcome to your new portfolio.
	</h1>
	
<p>Welcome to PortfolioLounge—where you and your work can relax and introduce yourselves to people effectively and easily.</p>
<p>Create a portfolio that reflects your unique perspective. 
PortfolioLounge.com was created by a developer whose passion for the visual arts led him to create an intuitive, efficient, attention-getting way for professionals to show the world what they can do. As our name suggests, we’re all about a relaxed, relatable and uncomplicated approach to showcasing your work, so you can focus on creating, instead of on portfolio management. It’s a place where you can surround your work with an atmosphere that reflects your personality as a creative professional.</p>
<p>
	
	</p>

Get professional-grade results in minutes.
PortfolioLounge is user-friendly throughout, guiding you through an easy-to-navigate process with easy-to-understand language every step of the way (no geek-speak). Simply pick the portfolio layout theme that best suits your style, enter your contact information, upload your best work, describe your projects with our editing tools, and let your creative output shine. 

Make the most of any size screen. 
Responsive design means PortfolioLounge resizes your work on the fly, helping it achieve maximum impact on whatever device your potential client or employer is using to view it: on the run via smartphone, relaxing at home with a tablet, or sitting at a full-size, fully-calibrated studio or office screen.

Don’t sweat the technical stuff. That’s our job.
Hosting, coding, search engine optimization—we’ve got it all covered. All you have to do is focus on getting great work into your portfolio. We’ll make sure the people who are looking for it can find it.

Ready to get started? Or set up a free tryout portfolio?
Head over to our subscription page to choose a $12 per month Pro package, a $24 per month Max package, or to try out a scaled-down version of PortfolioLounge absolutely free (no strings attached; you don’t even have to provide future billing information). The tryout version won’t give you everything the Pro and Max packages will, but you’ll get enough to show you how easy it is to create and maintain an attention-getting record of your work.

	<p>
		As our name suggests, PortfolioLounge is all about a relaxed and comfortable approach to showcasing your work, so you can focus on creating, instead of on portfolio management. 
	</p>
	<p>
		PortfolioLounge.com was created by a developer whose passion for the visual arts led him to create an intuitive, efficient, attention-getting way for professionals to show the world what they can do.
	</p>
	<p>
		User-friendly and easy to navigate, PortfolioLounge gives you a professional-looking portfolio in minutes. Just pick a layout theme that reflects your personal style, enter your contact information, upload your best work, describe your projects with our editing tools, and let your creative output shine for potential clients and employers.
	</p>
	<p>
		There’s no time like right now to get started. Head over to our subscription page and select the package that’s right for you. Or take a free test drive with our tryout version of PortfolioLounge.
	</p>

</div>
<!--
<div class="container">
<h1>
	Wait, what is Portfoliolounge?
</h1>
<p>
	As our name suggests, PortfolioLounge is all about a relaxed and comfortable approach to showcasing your work, so you can focus on creating, instead of on portfolio management. 
</p>
<p>
	PortfolioLounge.com was created by a developer whose passion for the visual arts led him to create an intuitive, efficient, attention-getting way for professionals to show the world what they can do.
</p>
<p>
	User-friendly and easy to navigate, PortfolioLounge gives you a professional-looking portfolio in minutes. Just pick a layout theme that reflects your personal style, enter your contact information, upload your best work, describe your projects with our editing tools, and let your creative output shine for potential clients and employers.
</p>
<p>
	There’s no time like right now to get started. Head over to our subscription page and select the package that’s right for you. Or take a free test drive with our tryout version of PortfolioLounge.
</p>




</div>
-->


<!--
Welcome to PortfolioLounge—where you and your work can relax and introduce yourselves to people effectively and easily.

Create a portfolio that reflects your unique perspective. 
PortfolioLounge.com was created by a developer whose passion for the visual arts led him to create an intuitive, efficient, attention-getting way for professionals to show the world what they can do. As our name suggests, we’re all about a relaxed, relatable and uncomplicated approach to showcasing your work, so you can focus on creating, instead of on portfolio management. It’s a place where you can surround your work with an atmosphere that reflects your personality as a creative professional.

Get professional-grade results in minutes.
PortfolioLounge is user-friendly throughout, guiding you through an easy-to-navigate process with easy-to-understand language every step of the way (no geek-speak). Simply pick the portfolio layout theme that best suits your style, enter your contact information, upload your best work, describe your projects with our editing tools, and let your creative output shine. 

Make the most of any size screen. 
Responsive design means PortfolioLounge resizes your work on the fly, helping it achieve maximum impact on whatever device your potential client or employer is using to view it: on the run via smartphone, relaxing at home with a tablet, or sitting at a full-size, fully-calibrated studio or office screen.

Don’t sweat the technical stuff. That’s our job.
Hosting, coding, search engine optimization—we’ve got it all covered. All you have to do is focus on getting great work into your portfolio. We’ll make sure the people who are looking for it can find it.

Ready to get started? Or set up a free tryout portfolio?
Head over to our subscription page to choose a $12 per month Pro package, a $24 per month Max package, or to try out a scaled-down version of PortfolioLounge absolutely free (no strings attached; you don’t even have to provide future billing information). The tryout version won’t give you everything the Pro and Max packages will, but you’ll get enough to show you how easy it is to create and maintain an attention-getting record of your work.

-->




<script>
	$("#about img").height(window.innerHeight-154);
</script>
<style>
	
.box {
	background:#000;
	background:rgba(0,0,0,.7);
	width:300px;
	padding:30px;
	color:#fff;	
	position:absolute;
	top:20%;
	left:10%;
	line-height:1.5em;
	color:rgba(255,255,255,.8);
}
.box h2 {
	margin:0 0 10px;
	color:#fff;
}
#about img {
	display:block;	
}

</style>