<div class="container">
<div class="left-nav">
	<ul>		
		<li><a class="<?php if($x=='profile'){echo'cur';}?>" href="admin/account/profile"><img src="img/icon/person.png">Profile<p>Logo, Title, Photo, Name...</p></a></li>
		<li><a class="<?php if($x=='identity'){echo'cur';}?>" href="admin/account/identity"><img src="img/icon/storefront.png">Settings <p>Username, Password, etc.</p></a></li>
		<li><a class="<?php if($x=='advanced'){echo'cur';}?>" href="admin/account/advanced"><img src="img/icon/dash.png">Advanced<p>Domain and analytics</p></a></li>
		 <?php if($expload_url[0]=='portfoliolounge'){?>
		<li><a class="<?php if($x=='share'){echo'cur';}?>" href="admin/account/share"><img src="img/icon/globe.png">Marketing<p>Get more visitors!</p></a></li>
		<li><a class="<?php if($x=='rewards'){echo'cur';}?>" href="admin/account/rewards"><img src="img/icon/ribbon.png">Rewards<p>Earn more items!</p></a></li>
    <?php } ?>
		<?php if($member['account_id'] > 1){?>
		<li><a class="<?php if($x=='payment'){echo'cur';}?>" href="admin/account/payment"><img src="img/icon/creditcard.png">Payment<p>Update payment info, etc</p></a></li>
		<?php } ?>
	</ul>
</div>
<div class="settings-wrapper" ng-controller="memberData">
	<?php if($x=='rewards'){?>
	<div class="content center">
		<div class="inner">			
			<h2>Get Rewards</h2>
            <div class="reward">
            	<h1>+5 Items</h1>
                <h3>Share Portfoliolounge.com</h3>
                <a class="promobtn fb_share_url" data-href="<?=$protocol.SITE_URL?>/click/fb_share_<?=$memberid?>">Share</a>
       		</div>
            <div class="reward">
	            <h1>+2 Items</h1>
                <h3>Like our Facebook Page</h3>
                <div class="fb-like" data-href="https://facebook.com/portfoliolounge" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="false"></div>
            </div>
            <div class="reward">
            	<h1>+2 Items</h1>
                <h3>Like Portfoliolounge.com</h3>
                <div class="fb-like" data-href="<?=$protocol.SITE_URL?>" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="false"></div>
            </div>
		</div>
	</div>        
	<?php /* if($member['earned_items']==0){?>
    <a class="font btn" id="post-to-fb" href="/">
        <p>Share our link for <strong>+15 items</strong></p>
        <img class="fb-loading" src="portfolio/images/load-dots.gif">
        <div class="fb-success">
            <h3>You are Awesome!</h3>
            <p>You can now upload <?php echo $account['item_limit']+$member['earned_items'];?> items.</p>
        </div>
    </a>
    <?php } */ ?>
	<?php } else if($x=='share'){?>
	<div class="content">
		<div class="inner" style="line-height:1.8em">
			<h2>Market yourself!</h2>
			<p>This link can help get anyone to your portfolio! Share this link with your friends, family, clients by emails, twitter, instagram, facebook to help promote your work.</p><br />
			<a class="font copyMe" style="
                font-size: 15px;
                font-weight: 700;
                background: #fff;
                padding: 22px 18px;
                display: block;
                border-radius: 3px;
                margin: 0px 0 18px 0px;
                text-align: center;
                border: 1px dotted #ddd;" target="_blank"><?=$member['username'] ?>.portfoliolounge.com</a>
            <script>  
				function copyToClipboard(text) {
					window.prompt("Copy this and share :)", text);
				}
				$('.copyMe').click(function() {
					copyToClipboard('https://<?=$member['username'] ?>.portfoliolounge.com')
				});
            </script>  
			<p>If you would like a real domain, perhaps <?=$member['username']?>.com, you can upgrade to pro and we will help get the right domain for you. <a class="b tn ri ght" href="admin/upgrade">Upgrade Account &raquo;</a></p>
		</div>
	</div>
	<div class="content">
		<div class="inner">
			<h2>Share your portfolio on facebook</h2>
			<!-- Load Facebook SDK for JavaScript 
			<div id="fb-root"></div>
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1";
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>
		
			<h3 class="left" style="margin-right:20px">It's easy! Click share &raquo;</h3>
			<div class="fb-share-button left" 
				data-href="https://<?=$member['username']?>.portfoliolounge.com" 
				data-layout="button_count">
			</div>
			<div class="clr"></div>-->
            <div id="fb-root"></div>
            <script>(function(d, s, id) {
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) return;
              js = d.createElement(s); js.id = id;
              js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=247254998759584";
              fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
            <div class="fb-like" data-href="https://<?=$member['username']?>.portfoliolounge.com" data-layout="standard" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
		</div>
	</div>		
	<?php } else if($x=='identity'){?>
	<div class="content">
		<div class="inner">
			<h2>Login Credentials</h2>
			<form class="smart_form" id="username">
				<label>Change Username:<span>{{username}}.portfoliolounge.com</span></label>
				<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
				<input class="input" name="username" placeholder="username" ng-model="username" data-original="<?php echo $member['username'];?>" />
				<input class="btn smart_btn" type="submit" value="Save">
			</form>
			<p class="caption">Your username is used in the beginning of your portfolio's URL.<br />
			<strong>Note:</strong> Changing your username will also affect your login.</p>
			<form class="smart_form" id="password">
				<label>Change Password</label>
				<input class="input password" name="password" type="password" placeholder="New Password" data-original="" />
				<input class="input cpassword" name="cpassword" type="password" placeholder="Verify New Password" data-original="" />
				<input class="btn smart_btn" type="submit" value="Save">
			</form>
		</div>
	</div>

	
	<div class="content">
		<div class="inner">
			<h2>Text Editor Display</h2>
			<?php if($member['account_id'] > 1): ?>
			<form class="smart_form" id="te_display">
				<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
				<select name="te_display" class="input">
					<option value="0" <?= ($member['te_editor'] == '0') ? 'selected="selected"' : '';?>>Use Basic Editor</option>
					<option value="1" <?= ($member['te_editor'] == '1') ? 'selected="selected"' : '';?>>Use HTML Editor</option>
				</select>
				<input class="btn smart_btn" type="submit" value="Save" id="te_editor_submit">
			</form>
			<?php else : ?>
			<p>Professional or Maximum Package only. <a href="/admin/upgrade">Upgrade now</a></p>
			<?php endif;?>
		</div>
	</div>
	

    <!-- Delete my account -->
	<div class="content">
		<div class="inner">
			<h2>Don't need your account?</h2>
			<a ng-show="!startDelete" class="btn gray" ng-click="startDelete=true">I wish to delete my account.</a>
			<form class="classy nice" ng-show="startDelete" id="username" action="function/delete-account.php" method="post">
				<!-- Final Feedback from User
                <label>Before you go, please let us know why. Where do you think we can improve?</label>
				<textarea style="height:100px;" class="input" name="feedback" ng-model="feedback" data-original=""></textarea>
                -->
                <div>
                    <label>Please let us know why (anonymously).</label><br>
                    <small class="red-txt">If this is a paid account please head over to your <a href="/admin/account/payment">Payment page</a>  to stop payments before deleting account.</small>
                    <br>
                    <textarea name="message"></textarea>
                    <br>
                    <input class="btn red" type="submit" value="Delete account now.">
                    <a style="color:#aaa;" ng-click="startDelete=false"><small>Cancel</small></a>
                </div>
                <div class="clr"></div>
			</form>
		</div>
	</div>
	<?php } else if($x=='profile'){?>
	<div class="content">
		<div class="inner">
			<h2>Your Picture <p style="font-size:11px;margin:6px 0 0;color:#999;">Usually shows on your about page.</p></h2>
			<div class="upload-area" style="margin-top:20px;">
				<div class="frame profile_img <?php if(!$member['profile_img']){echo ' empty';}?>" style="width:100%;">
					<?php if($member['profile_img']){?>
						<img src="<?=$protocol.SITE_URL?>/uploads/<?php echo $memberid.'/small/'.$member['profile_img'];?>" />
					<?php } ?>
				</div>
				<a id="upload-profile" class="btn" style="margin:0 auto;" href="javascript:void(0)"><input class="fileupload" type="file" name="files[]" data-url="function/fileupload-profile.php">Upload a Profile Picture</a>
				<?php if($member['profile_img']){?><a class="btn red" onclick="deleteProfileImg(<?php echo $memberid;?>)">Delete</a><?php } ?>
			</div>
			<div class="clr"></div>
		</div>
	</div>
	<div class="content">
		<div class="inner">
			<h2>Your Info <p style="font-size:11px;margin:6px 0 0;color:#999;">Displays on About page.</p></h2>
			<form class="smart_form" id="name">
				<label>Your Name</label>
				<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
				<input class="input short" name="first_name" placeholder="first" ng-model="first_name" data-original="<?php echo $member['first_name'];?>" />
				<input class="input short" name="last_name" placeholder="last" ng-model="last_name" data-original="<?php echo $member['last_name'];?>" />
				<input class="btn smart_btn" type="submit" value="Save">
			</form>
			<form class="smart_form" id="email">
				<label>Email</label>
				<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
				<input class="input email" name="email" ng-model="email" data-original="<?php echo $member['email'];?>" />
				<input class="btn smart_btn" type="submit" value="Save">
			</form>
			<form class="smart_form" id="phone">
				<label>Phone</label>
				<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
				<input class="input" maxlength="20" name="phone" ng-model="phone" data-original="<?php echo $member['phone'];?>" />
				<input class="btn smart_btn" type="submit" value="Save">
			</form>	
			<form class="smart_form" id="location">
				<label>Where are you from?</label>
				<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
				<input class="input" name="location" ng-model="location" data-original="<?php echo $member['location'];?>" />
				<input class="btn smart_btn" type="submit" value="Save">
			</form>
			<form class="smart_form" id="school">
				<label>School</label>
				<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
				<input class="input" name="school" ng-model="school" data-original="<?php echo $member['school'];?>" />
				<input class="btn smart_btn" type="submit" value="Save">
			</form>
			<form class="smart_form" id="about">
				<label>Tell us about yourself. <span>Displayed on about page.</span></label>
				<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
				<textarea class="input tall<?=$member['account_id'] > 1 && $member['te_editor'] == '1' ? ' jqte': '';?>" name="about" ng-model="about" data-original="<?php echo $member['about'];?>"></textarea>
				<input class="btn smart_btn" type="submit" value="Save">
			</form>
		</div>
	</div>
	<div class="content">
		<div class="inner">
			<h2>Your Skills</h2>
			<form class="smart_form" id="skills">
				<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
				<ul class="checkbox-wrapper">
					<?php 
					$get_categories = $db->query("SELECT * FROM categories ORDER BY category_name");
					foreach($get_categories->fetchAll() as $categories){?>
						<li><input type="checkbox" name="category[]" value="<?php echo $categories['category_id'];?>"/> <span><?php echo $categories['category_name'];?></span></li>
					<?php } ?>
				</ul>
				<div class="uncheck-wrapper">
				<a class="uncheck">Uncheck All</a>
				</div>
				<input class="btn smart_btn" type="submit" value="Save Skills">
			</form>
		</div>
	</div>
	<div class="content">
		<div class="inner">		
			<h2>Your Logo <p style="font-size:11px;margin:6px 0 0;color:#999;"></p></h2>
			<div class="upload-area">
				<div class="frame logo_img <?php if(!$member['logo_img']){echo ' empty';}?>" style="width:100%;">
					<?php if($member['logo_img']){?>
					<div class="logo-container" style="background-image:url(<?=$protocol.SITE_URL?>/uploads/<?php echo $memberid.'/'.$member['logo_img'];?>);"></div>
					<?php } ?>
				</div>
				<a id="upload-profile" class="btn" href="javascript:void(0)"><input class="fileupload" type="file" name="files[]" data-url="function/fileupload-logo.php"><?php if($member['logo_img']){echo 'Update Your Logo';} else {echo 'Upload Your Logo';}?></a>
				<?php if($member['logo_img']){?><a class="btn red" onclick="deleteLogoImg(<?php echo $memberid;?>)">Delete</a><?php } ?>
                <!--
                <a href="http://tailorbrands.com" title="Our friends at Tailorbrands provide awesome, hand-made logos. Check them out!" class="btn gray right">Get a Custom Logo</a>
                -->
			</div>
			<hr>	
			<form class="smart_form" id="site_title">
				<label>Site Title: <span>Your website's main title.</span></label>
				<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
				<input class="input" name="site_title" ng-model="site_title" data-original="<?php echo $member['site_title'];?>" placeholder="Site Title" />
				<input class="btn smart_btn" type="submit" value="Save">
			</form>
			<p class="caption">Your site title is shown on all pages of your portfolio. It can be replaced<br /> by a custom logo, but is still good to have for search engine results.</p>
			<form class="smart_form" id="tagline">
				<label>Tagline <span>A brief intro message for your website.</span></label>
				<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
				<textarea class="input" name="tagline" ng-model="tagline" data-original="<?php echo $member['tagline'];?>"></textarea>
				<input class="btn smart_btn" type="submit" value="Save">
			</form>
		</div>
	</div>
	<div class="content">
		<div class="inner">
			<h2>Your Links</h2>
			<ul class="member-links">
				<?php //$get_links = mysql_query("SELECT * FROM member_links WHERE member_id = $memberid ORDER BY link_title");
				$get_links = $db->prepare("SELECT * FROM member_links WHERE member_id = :memberid ORDER BY link_title");
				$get_links->bindValue(':memberid', $memberid);
				$get_links->execute();
				$all_links = $get_links->fetchAll();
				foreach($all_links as $link){?>
					<li>
						<a class="deleteLink right" data-source="<?php echo $link['link_id'];?>">-</a>
						<a target="_blank" href="<?php echo $link['link_url'];?>"><?php echo $link['link_title'];?></a>
					</li>
				<?php }?>
			</ul>
			<form id="add_link" class="smart_form">
				<label>Add a link</label>
				<select class="input" name="fixed_link" id="fixed_links_select">
					<option value="Linkedin">LinkedIn</option>
					<option value="Facebook">Facebook</option>
					<option value="Twitter">Twitter</option>
					<option value="Dribbble">Dribbble</option>
					<option value="Vimeo">Vimeo</option>
					<option value="Flickr">Flickr</option>
					<option value="Instagram">Instagram</option>
					<option value="other">Other</option>
				</select>
				<input class="input" name="link_title" placeholder="My Website" id="link_title_input" />
				<input class="input" name="link_url" placeholder="http://www.linkedin.com/mypage" />
				<input class="btn smart_btn" type="submit" value="Add Link" />
			</form>
		</div>
	</div>
	<?php } else if($x=='payment'){?>
	<script type="text/javascript" src="js/signup.js"></script>
		<div class="content">
			<div class="inner">
				<?php if($member['account_profile_status']=='Suspended'){?>
				<p style="padding:20px;background:#FF9494;border:1px solid #C33;color:#fff;font-size:14px;border-radius:4px;">
					<strong>Your last payment did not process.</strong><br /><small>Please update credit card so that we can re-activate your portfolio.</small>
				</p>
				<?php } ?> 
				<h2>Update Credit Card</h2>
				<div id="checkout" class="update">
					<div id="payment-details-view" class="upgrade-view">
						<form id="update-creditcard-form" method="post">
						<div class="row">
							<div class="cc-num">
								<label>Card Number</label>
								<input name="cc_number" value="" />
							</div>
							<div class="ccv">
								<label>Security Code</label>
								<input maxlength="4" name="cvv" value="" />
							</div>		  
						</div>
						<div class="row">
							<div class="cc-container">
								<label>Select Card</label>
								<a class="cc_type visa" href="javascript:void(0)">Visa</a>
								<a class="cc_type amex" href="javascript:void(0)">Amex</a>
								<a class="cc_type mastercard" href="javascript:void(0)">Mastercard</a>
								<a class="cc_type discover" href="javascript:void(0)">Discover</a>
								<input class="cc_type" type="hidden" name="cc_type" value="" />
								<script type="text/javascript">
									$(".cc_type").click(function(){
										thisType = $(this).text();
										$('input.cc_type').val(thisType);
										$('.cc_type').addClass('disabled');
										$(this).removeClass('disabled');
									});
								</script>
							</div>
							<div class="expiration-container">
								<label>Expiration Date</label>
								<ul class="expiration">
									<li>
										<span class="month">Month</span>
										<select name="exp_month" class="month">
											<option value="">Month</option>
											<option value="01">1 - January</option>
											<option value="02">2 - February</option>
											<option value="03">3 - March</option>
											<option value="04">4 - April</option>
											<option value="05">5 - May</option>
											<option value="06">6 - June</option>
											<option value="07">7 - July</option>
											<option value="08">8 - August</option>
											<option value="09">9 - September</option>
											<option value="10">10 - October</option>
											<option value="11">11 - November</option>
											<option value="12">12 - December</option>
										</select>
									</li>
									<li class="year">
										<span class="year">Year</span>
										<select name="exp_year" class="year">
											<option value="">Year</option>
											<option value="2013">2013</option>
											<option value="2014">2014</option>
											<option value="2015">2015</option>
											<option value="2016">2016</option>
											<option value="2017">2017</option>
											<option value="2018">2018</option>
											<option value="2019">2019</option>
											<option value="2020">2020</option>
											<option value="2021">2021</option>
											<option value="2022">2022</option>
											<option value="2023">2023</option>
										</select>
									</li>
									<script type="text/javascript">
										$(".expiration select").change(function(){
											thisSelect = $(this);
											thisSelect.prev().html(thisSelect.val());
										});
									</script>
								</ul>
							</div>
						</div>
						<div class="submit-area">
							<input class="promobtn" type="submit" value="Save Credit Card" />
						</div>
					</form>
				</div>
			</div>
			<div id="payment-processing-view" class="upgrade-view" style="display:none;">
				<h2>Please wait,</h2>
				<p>While we save your new card.</p>
				<img src="images/icon-unchecked.gif" />
			</div>
			<div id="payment-result-view" class="upgrade-view" style="display:none;">
			</div>
		</div>
	</div>
	<div class="content">
		<div class="inner">
			<h2>Downgrade Account</h2>
			<a class="downgrade" onclick="lightbox('downgrade')">Downgrade Account</a>
		</div>
	</div>
	<?php } else if($x=='advanced'){?>
	<div class="content">
		<div class="inner">
			<h2>Analytics</h2>
			<form class="smart_form" id="tracking">
				<input name="get-member-id" type="hidden" value="<?php echo $memberid;?>" />
				<ul class="steps">
					<li>Setup your google analytics account for your portfolio URL.</li>
					<li>Enter your <a target="_blank" href="http://google.com/analytics">Google Analytics</a> Tracking ID.</li>
				</ul>
				<label>Enter Tracking ID:</label>
				<input class="input" id="tracking-id" name="tracking-id" maxlength="20" placeholder="UA-########-#" value="<?php echo $member['tracking_id'];?>" />
				<input type="submit" class="btn smart_btn" value="Save" />
			</form>
		</div>
	</div>
	<!--
	<div class="content">
		<div class="inner">
			
			<h2>Get a domain! </h2>
			
			<form class="smart_form" id="custom_domain">
				<input name="get-member-id" type="hidden" value="<?php echo $memberid;?>" />
				<p style="line-height:1.7em;font-size:11px;">If you have a domain name (maybe something like <?php echo $member['username']?>.com), this will help you pair your Portfoliolounge portfolio with your domain name. If you do not have a domain name don't worry! Contact info@portfoliolounge.com with the domain you would like to use!</p>
				<hr />
				<ul class="steps">
					<li>Sign in to your DNS provider.</li>
					<li>Find your DNS Settings for your domain.</li>
					<li><strong>Create a CNAME record</strong> pointing <strong>www</strong> to <strong>portfoliolounge.com</strong></li>
					<li>Enter your domain below.</li>
				</ul>
				<label>Enter Your Domain (Beginning with <strong>www</strong>):</label>
				<input class="input" name="custom-domain" value="<?php echo $member['custom_domain'];?>" placeholder="www.example.com" />
				<input type="submit" class="btn smart_btn" value="Save" />
			</form>	
			
		</div>
	</div>
	-->
	<div class="content">
		<div class="inner">	
			<h2>Want to use your domain?</h2>
			<form class="smart_form" id="custom_domain">
				<input name="get-member-id" type="hidden" value="<?php echo $memberid;?>" />
				<p style="line-height:1.7em;font-size:11px;">If you have a domain name (maybe something like '<?php echo $member['username']?>.com'), we can use that for your portfolio! Go to where you purchased the domain and follow the steps below. If you do not have a domain name don't worry! Contact info@portfoliolounge.com with the domain you would like to use!</p>
				<hr />
				<ul class="steps">
					<li>Sign in to your DNS provider.</li>
					<li>Find your DNS Settings for your domain.</li>
					<li><strong>Create a CNAME record</strong> pointing <strong>www</strong> to <strong>portfoliolounge.com</strong></li>
					<li>Enter your domain below.</li>
				</ul>
				<label>Enter Your Domain (Beginning with <strong>www</strong>):</label>
				<input class="input" name="custom-domain" value="<?php echo $member['custom_domain'];?>" placeholder="www.example.com" />
				<input type="submit" class="btn smart_btn" value="Save" />
			</form>
            <hr />
			<p>If you would set up a real domain, perhaps <?=$member['username']?>.com, you can upgrade to pro and we will help take care of everything. <a class="b tn ri ght" href="admin/upgrade">Upgrade Account &raquo;</a></p>
		</div>
	</div>	
	<?php } ?>
</div><!-- ng -->
</div><!-- container -->
<?php
$get_member_skills = $db->prepare("SELECT member_id, category_id FROM member_skills WHERE member_id = :memberid");
$get_member_skills->bindValue(':memberid', $memberid);
$get_member_skills->execute();
$all_memberskills = $get_member_skills->fetchAll();
?>
<script type="text/javascript">
/* Member Data
-----------------------------------*/
function memberData($scope) {	
	$scope.memberid = "<?php echo str_replace('"',"'", $member['member_id']);?>";
	$scope.username = "<?php echo str_replace('"',"'", $member['username']);?>";
	$scope.first_name = "<?php echo str_replace('"',"'", $member['first_name']);?>";
	$scope.last_name = "<?php echo str_replace('"',"'", $member['last_name']);?>";
	<?php
		if($member['te_editor'] == 0) {
			$breaks = array("<br />","<br>","<br/>");
		    $about = str_ireplace($breaks, "\r\n", strip_tags($member['about'], '<br />'));
		} else {
			$about = $member['about'];
		}
	?>
	$scope.about = <?php echo json_encode($about)?>;
	$scope.tagline = "<?php echo str_replace('"',"'", $member['tagline']);?>";
	$scope.site_title = "<?php echo str_replace('"',"'", $member['site_title']);?>";
	$scope.email = "<?php echo str_replace('"',"'", $member['email']);?>";
	$scope.location = "<?php echo str_replace('"',"'", $member['location']);?>";
	$scope.phone = "<?php echo str_replace('"',"'", $member['phone']);?>";
	$scope.school = "<?php echo str_replace('"',"'", $member['school']);?>";
}
/* Check Member Skills
-----------------------------------*/
<?php foreach($all_memberskills as $member_skills){?>
$("#skills input[value='<?php echo $member_skills['category_id'];?>']").prop('checked', true);
$("#skills .uncheck").show();
<?php }?>
</script>
<?php if($member['account_id'] > 1 && $member['te_editor'] == '1') : ?>
<link href="css/jquery-te-1.4.0.css" rel="stylesheet">
<link href="css/jquery-te.custom.css" rel="stylesheet">
<script type="text/javascript" src="js/libs/jquery-te-1.4.0.min.js"></script>
<script>
$(document).ready(function(){
	if($('.jqte').length > 0) {
		var val = $('.jqte').val().replace("\n", "<br />", "g");
		$('.jqte').jqte();
		$('.jqte').jqteVal(val);
		$('.jqte').on('change', function(){
			$(this).closest('form').find('.smart_btn').css({
				'display' : 'inline',
				'bottom': '-50px'
			});
		});
	}
});
</script>
<?php endif; ?>
<script type="text/javascript" src="js/account.js"></script>