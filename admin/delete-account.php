<div class="container">
	<div class="content inner" style="text-align:center;">
		<h1><?=$memberid;?></h1>
		<br>
		<br>
		<h2>Are you sure you want to delete your account?</h2>
		<form action="admin/delete-account" method="post">
			<br>
			<input type="submit" class="btn red" value="Yes, I would like to delete my account now." />
		</form>
		<br>
		<br>
	</div>
</div>
<?php if($_POST){ ?>
	<h5>Your account is deleted. Thanks for trying us out!</h5>
<?php 

	$member_upload_path = 'uploads/'.$memberid;
	rmdir($member_upload_path);	
	function rrmdir($dir) {
		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
			  if ($object != "." && $object != "..") {
				if (filetype($dir."/".$object) == "dir") 
				   rrmdir($dir."/".$object); 
				else unlink   ($dir."/".$object);
			  }
			}
			reset($objects);
			rmdir($dir);
		}
	}
} ?> 