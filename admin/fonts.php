<div class="center-title-block">
	<h2>Fonts &amp; Colors</h2>
	<h3>Don't worry, more on the way!</h3>
</div>
<div class="container">	
	<div class="content">
		<div class="inner">
			<div class="option layout-options <?php if($member['template_id']!=0){echo "hide";}?>">		
				<h2 class='option-title'>Project Layout</h2>
				<div id="layouts">				
					<div class="layout-preview" id="list">
						<div class="check <?php if($member['layout_option']=="list"){echo "checked";}?>"></div>
						<h2>List</h2>
						<div class="design-thumb"><img class="thumb" src="images/thumbs/layout-list.png" /></div>
						<input id="layout-id" type="hidden" value="list" />
					</div>				
					<div class="layout-preview" id="slideshow">
						<div class="check <?php if($member['layout_option']=="slideshow"){echo "checked";}?>"></div>
						<h2>Slideshow</h2>
						<div class="design-thumb"><img class="thumb" src="images/thumbs/layout-slideshow.png" /></div>
						<input id="layout-id" type="hidden" value="slideshow" />
					</div>
				</div>
			</div>
			<!-- Colors -->
			<div class="option color-selector" >
				<h2 class='option-title'>Color Scheme</h2>
				<div class="color-schemes">
					<?php 
					$get_color_schemes = $db->prepare("SELECT * FROM colors");
					$get_color_schemes->execute();
					$colors = $get_color_schemes->fetchAll();
					foreach($colors as $color_scheme){?>
						<div class="color-selection" style="background:<?php echo $color_scheme['background'];?>;color:<?php echo $color_scheme['text_main'];?>;">
							<div class="inner">
								<div class="check <?php if($member['color_scheme_id']==$color_scheme['color_scheme_id']){echo "checked";}?>"></div>
								<h2 style="color:<?php echo $color_scheme['color_scheme_txt_color_a'];?>"><?php echo $color_scheme['color_scheme_title'];?></h2>
								<input id="color-scheme-id" type="hidden" value="<?php echo $color_scheme['color_scheme_id'];?>" />
							</div>
						</div>
					<?php } ?>
					<div class="clr"></div>
				</div>
			</div>
			<!-- Fonts -->
			<div class="option font-selector">		
				<h2 class='option-title'>Choose Font</h2>
				<?php 
				$get_font = $db->prepare("SELECT * FROM fonts");
				$get_font->execute();
				$fonts = $get_font->fetchAll();
				foreach($fonts as $font){?>
					<div class="font_preview" id="<?php echo $font['font_id'];?>">
						<link href='<?=$protocol?><?php echo $font['url'];?>' rel='stylesheet' type='text/css'>
						<style>.webfont<?php echo $font['font_id'];?> {font-family:'<?php echo $font['name'];?>'}</style>
						<div class="check <?php if($font['font_id']==$member['font_id']){echo "checked";}?>"></div>
						<h3 class="webfont<?php echo $font['font_id'];?>"><?php echo $font['name'];?></h3>
					</div>
				<?php } ?>
			</div>
				<p class="disclaimer">Fonts provided by <a href="http://www.google.com/fonts">Google Fonts</a>.</p>
		</div>
	</div>
	<hr>
</div>
<script type="text/javascript" src="js/template.js"></script>