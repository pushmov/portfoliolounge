<?php 
// Member Data
$result = $db->prepare("SELECT * FROM members WHERE member_id=:memberid");
$result->bindValue(':memberid', $memberid);
$result->execute();
$member = $result->fetch();

// Get Total Items
$get_total_items = $db->prepare("SELECT * FROM items WHERE member_id = :memberid");
$get_total_items->bindValue(':memberid', $memberid);
$get_total_items->execute();
$total_itemss = $get_total_items->fetchAll();
$total_items = count($total_itemss);

// Get Total Projects
$get_total_items = $db->prepare("SELECT * FROM project WHERE member_id=:memberid");
$get_total_items->bindValue(':memberid', $memberid);
$get_total_items->execute();
$total_projectss = $get_total_items->fetchAll();
$total_projects = count($total_projectss);

// Get Account Limitations
$get_account = $db->prepare("SELECT * FROM accounts WHERE account_id = :accountid");
$get_account->bindValue(':accountid', $member['account_id']);
$get_account->execute();
$account = $get_account->fetch();

// Calculate Remaining Uploads
$available_uploads = $account['item_limit']-$total_items;

// Get Percentages for Status Bars
$item_percentage = $member['item_count'] / ($account['item_limit'] + $member['earned_items']) * 100;
$project_percentage = $member['project_count'] / ($account['project_limit']+ $member['earned_projects']) * 100;

?>

<?php // Sign Up - Conversion Script
if ($i=="welcome" && $member['login_count']==0){
	$updateMember = $db->prepare("UPDATE members SET login_count=:logincount WHERE member_id=:memberid");
	$updateMember->bindValue(':memberid', $memberid);
	$updateMember->execute();
	?>
	<!-- Google Code for Sign Up Conversion Page -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 1026484131;
	var google_conversion_language = "en";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "0LojCOOx3Q0Qo8-76QM";
	var google_conversion_value = 0;
	/* ]]> */
	</script>
	<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1026484131/?value=0&amp;label=0LojCOOx3Q0Qo8-76QM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
<?php } ?>
<?php include 'lightboxes.php';?>
<div id="admin-header" class="grad-a">
	<?php if($member['admin']==1){?>
	<?php include "owner-nav.php"; ?>
	<?php } ?>
	<div class="inner">	
		<div class="admin-nav">	
			<ul class="right">
				<li><a target="_blank" href="http://<?php echo $username;?>.portfoliolounge.com"><img height="24" width="30" src="images/icon-macbook.png" />View Portfolio</a></li>
				<li><a <?php if($i=="preview"){?>class="cur"<?php }?> href="admin/preview"><img width="27" height="24" src="images/icon-devices.png" />Mobile</a></li>
				<li><a <?php if($i=="account"){?>class="cur"<?php }?> href="admin/account/profile"><img width="30" height="30" src="images/icon-folder.png" />Account</a></li>
				<li><a <?php if($i=="templates"){?>class="cur"<?php }?> href="admin/templates"><img height="24" width="30" src="images/icon-brush.png" />Templates &amp; Styles</a></li>
				<li><a <?php if(!$i||$i=="projects"||$i=="project"||$i=="item"){?>class="cur"<?php }?> href="admin/projects"><img height="24" width="30" src="images/icon-pic.png" />Projects</a></li>
			</ul>
		</div>
		<div id="announcements">
			<div id="status-bars">
				<h4>Projects</h4>
				<div class="status-bar"><div class="status-fill" style="width:<?php echo $project_percentage;?>%;"></div></div>	
				<span><?php echo $member['project_count'];?> / <?php echo $account['project_limit'];?></span>
				<h4>Items</h4>
				<div class="status-bar"><div class="status-fill <?php if($item_percentage>=80){echo 'warning';}?>" style="width:<?php echo $item_percentage;?>%;"></div></div>
				<span><?php echo $member['item_count'];?> / <?php echo $account['item_limit'] + $member['earned_items'];?></span>
			</div>	
			<?php if($member['earned_items']==0){?>	
				<a class="font" id="post-to-fb" href="/">
					<h3>Earn 15 more items!</h3>
					<p>Share our link &raquo;</p>
					<img class="fb-loading" src="portfolio/images/load-dots.gif">
					<div class="fb-success">
						<h3>You are Awesome!</h3>
						<p>You can now upload <?php echo $account['item_limit']+15;?> items.</p>
					</div>
				</a>
			<?php } ?>
			<?php if($member['account_id']==1 && $item_percentage > 40){?>	
			<a href="admin/upgrade">
				<h3><strong>Upgrade to Pro!</strong></h3>
				<p>Get more items and projects! <strong>$7</strong></p>
			</a>
			<?php } ?>	
			<div id="fb-like">
				<div id="iframe-wrap">
					<iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com%2Fportfoliolounge&amp;send=false&amp;layout=button_count&amp;width=150&amp;show_faces=true&amp;font&amp;colorscheme=dark&amp;action=like&amp;height=21" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:450px; height:21px;" allowTransparency="true"></iframe>
				</div>
				<h3>Show us some love!</h3>
			</div>
		</div>
		<script>
			$('#announcements').cycle({
				fx : 'scrollDown',
				timeout : 7000,
				speed: 1000,
				easing : 'easeOutQuart',
				random:  1 
			});
			$("#status-bars").on('click',function(){
				window.location.href = 'admin/upgrade';
			});
		</script>
	</div>	
</div>
<div style="height:74px;"></div>
			<?php /*if($member['earned_items']==0 && $x!='rewards' && $member['account_id']==1){?>			
			<div class="slide">
				<div id="fb_earn_btn">
					
					<a class="font" id="post-to-fb btn" href="/" style="font-size:11px;">
						<h3>Earn 15 items!</h3>
						<p>Share our link &raquo;</p>
					</a>
					<img class="loading" src="images/loader.gif">
					<div class="success">
						<h3>You are Awesome!</h3>
						<p>You can now upload up to <?php echo $account['item_limit']+10;?> items.</p>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if($member['account_id']==1 && $i!='upgrade'){?>
				<div class="slide">
					<a href="checkout/professional">
						<h3>Upgrade to Pro!</h3>
						<p>500 items, 50 projects, $8 a month</p>
						<p>Buy &raquo;</a></p>
					</a>
				</div>
			<?php }?>
		</div>
		*/?>

	<?php // Administrative Areas	
	if (!$i || $i=="welcome" || $i=="projects") {
		include 'admin/projects.php';
	
	} else if ($i=="project"){
		include 'admin/items.php';
	
	} else if ($i=="item"){
		include 'admin/single.php';
		
	} else if($i=="account") {
		include 'admin/account.php';
		
	} else if ($i=="templates") {
		include 'admin/templates.php';
		
	} else if ($i=="upgrade") { 
		include 'upgrade.php';
	
	} else if ($i=="preview") { 
		include 'admin/preview.php';
	
	}?>
<script type="text/javascript" src="js/fb-app.js"></script>
<script type="text/javascript" src="js/libs/jquery-ui-1.10.1.custom.min.js"></script>
<script type="text/javascript" src="js/admin.js"></script>