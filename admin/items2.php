<?php

session_start();
// Store Project Id in Session
$_SESSION['PROJECT_ID'] = $x;

// Get Project
$get_project = $db->prepare("SELECT * FROM projects WHERE project_id = :pid LIMIT 1");
$get_project->bindValue(':pid', $x);
$get_project->execute();
$project = $get_project->fetch();

// Get Project Items
$get_items = $db->prepare("SELECT * FROM items WHERE project_id = :pid ORDER BY item_order");
$get_items->bindValue(':pid', $x);
$get_items->execute();
$item_count_all = $get_items->fetchAll();
$item_count = count($item_count_all);

// Get First Item for default cover
$get_first_item = $db->prepare("SELECT * FROM items WHERE project_id = :pid ORDER BY item_id LIMIT 1");
$get_first_item->bindValue(':pid', $x);
$get_first_item->execute();
$first_item = $get_first_item->fetch();

// Specify Project Cover
if ($project['project_type']=='video'){
	$project_cover = "images/add-cover-photo.png";
} else {	
	if ($project['cover_id']!=null) {
		// Choice Cover
		$get_cover = $db->prepare("SELECT item_filename FROM items WHERE item_id = :itemid LIMIT 1");
		$get_cover->bindValue(':itemid', $project['cover_id']);
		$get_cover->execute();
		$item_cover = $get_cover->fetch();
		$project_cover = $protocol.SITE_URL."/uploads/".$memberid."/medium/".$item_cover['item_filename'];
	} else if($item_count!=0){
		// Cover Default
		$project_cover = $protocol.SITE_URL."/uploads/".$memberid."/medium/".$first_item['item_filename'];
	} else {
		// No Cover
		$project_cover = "images/no-cover-item.jpg";
		$project_cover_class = "blank";	
	} 
}

// Get Total Items
$get_total_items = $db->prepare("SELECT * FROM items WHERE member_id = :memberid");
$get_total_items->bindValue(':memberid', $memberid);
$get_total_items->execute();
$total_items_all = $get_total_items->fetchAll();
$total_items = count($total_items_all);

// Get Account Limitations
$get_account = $db->prepare("SELECT * FROM accounts WHERE account_id = :accountid LIMIT 1");
$get_account->bindValue(':accountid', $member['account_id']);
$get_account->execute();
$account = $get_account->fetch();

// Calculate Remaining Uploads
$available_uploads = $account['item_limit'] - $total_items + $member['earned_items'];?>
<style>
	.project-container {
		width:960px;
		margin:80px auto;
	}
.project-form {
	float:left;
}
.item-grid-container  {
	width:651px;
	float:right;
}

.project_cover_edit {
	margin:0px 0 30px;
	height:200px;
	background:rgba(0,0,0,.15);
	border:1px solid rgba(0,0,0,.05);
	border-radius:2px;
	display:none;
}
.project-form form {

}
.form input,
.form textarea {
	width:240px;
	background:#fff;
	padding:8px;
	border:none;
	line-height:1.4em;
	height:1.4em;
	font-size:13px;
	margin:0 0 10px;
	font-family:"Open Sans";
	border-radius:2px;
}
.form textarea {
height:200px;	
}
.form input.large {

}
.form label {
	font-size:11px;
	padding:4px 0;
	display:block;
	color:#888;
	font-family:"Open Sans";

}
</style>
<div class="project-container">
<div class="project-form">
	<div class="project_cover_edit"></div>
	<form ng-controller="projectData">
		<div class="form">
			<p>
				<label>Project Title</label>
				<input class="large" name="title" ng-model="title">
			</p>
			<p>
				<label>Description</label>
				<textarea name="description" ng-model="description"></textarea></p>
			<p>
				<label>Client</label>
				<input name="client" ng-model="client">
			</p>
			<p>
				<label>Related Link</label>
				<input name="website" ng-model="website" placeholder="ex. http://www.yourclient.com">
			</p>
		</div>
		<p>
				<input type="hidden" name="project_id" value="<?php echo $x;?>">
				<input class="btn rig ht" type="submit" value="Save"> 
				<a class="lazyBtns cancel ri ght btn gray" style="margin:0 4px;" href="javascript:void(0)">Cancel</a>
		</p>
	</form>
</div>
<div class="item-grid-container">
	<div class="inner">	

		<div id="item_grid">
			<?php // Project does not exist
			if(!$project['project_id']){?> 
				<div style="text-align:center;color:#ccc;line-height:2.5em;padding:60px 0;">
					<h2>This project doesn't exist anymore!</h2>
					<p>Sorry, try heading back to the <a href="admin">Projects Page.</a></p>
				</div>
				
			<?php // No Items Yet
			} else if(!$item_count){?>
				<div style="text-align:center;color:#ccc;line-height:2.5em;padding:60px 0;">
					<h2>Here is your new project!</h2>
					<p>Add some items, and they will be displayed here.</p>
				</div>
				
			<?php  // Project Items
			} else {?>
			
			
				<?php //mysql_data_seek($get_items,0);
				foreach($item_count_all as $item){
					$item_filename = $protocol.SITE_URL.'/uploads/'.$memberid.'/medium/'.$item['item_filename'];?>
				
					<div class="item_thumb" id="<?php echo $item['item_id'];?>">
						<a href="admin/item/<?php echo $item['item_id'];?>">
							<div class="cover" id="cover_<?php echo $item['item_id'];?>"></div>
						</a>				
						<div class="thumb_bar2">
	                        <a class="btn green img_only sort_handle" title="Edit item" href="admin/item/<?php echo $item['item_id'];?>"><img width="14" src="images/icon-pencil-white.png" /></a>
							<a class="btn img_only  sort_handle" title="Click and Drag" href="javascript:void(0)"><img width="17" src="images/icon-cross-hair.png" /></a>
                            <a class="btn img_only gray" title="Delete" onclick="lightbox('deleteItem',<?php echo $item['item_id'];?>);"><img width="10" src="images/icon-trash.png" /></a>
						</div>
					</div>
					
					<script type="text/javascript">								
						
						$("<img />").attr("src", "<?php echo $item_filename;?>").load(function() {	
							var thumbWidth = $('.item_thumb .cover').width();	
							var thumbHeight = $('.item_thumb .cover').height();							
							var gridRatio = thumbWidth / thumbHeight;
							var imgRatio = this.width / this.height;
							
							if (imgRatio < gridRatio){
								var imgWidthAttr = thumbWidth;
								var imgHeightAttr = this.height * (thumbWidth / this.width);
							} else {
								var imgWidthAttr = this.width * (thumbHeight / this.height);
								var imgHeightAttr = thumbHeight;
							}
							
							var marginTop = imgHeightAttr / 2 * -1 + thumbHeight / 2;
							var marginLeft = imgWidthAttr / 2 * -1 + thumbWidth / 2;
							
							$('#cover_<?php echo $item['item_id'];?>').html('<img width="'+imgWidthAttr+'" height="'+imgHeightAttr+'" style="margin:'+marginTop+'px 0 0 '+marginLeft+'px;" src="<?php echo $item_filename;?>" />').children('img').fadeIn();
						});
					</script>
					
				<?php } ?>
				
			<?php } ?>
		</div>
	
		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>
</div><!-- container -->
<script type="text/javascript">
/* Project Ang Model
-----------------------------------------------------------*/	
function projectData($scope) {
	$scope.title = <?=json_encode($project['project_title'])?>;
	$scope.description = <?=json_encode($project['project_desc'])?>;
	$scope.website = <?=json_encode($project['project_website'])?>;
}
/* Item Uploader
-----------------------------------------------------------*/	
var availableUploads=<?php echo $available_uploads;?>;
var totalFiles=<?php echo $total_items;?>;
//var maxFiles=1000;
$('#fileupload').fileupload({
	dataType: 'json',
	submit: function (event, files) {
		
		// How many files are you being uploaded
		var fileCount = files.originalFiles.length;
		
		// Too many files
		if (fileCount > availableUploads && totalFiles+fileCount < 500) {
			lightbox('itemLimit', availableUploads);
			return false; 
			
		} else if (fileCount > availableUploads && totalFiles+fileCount > 500) {
			lightbox('itemLimitMax', availableUploads);
			return false; 
		}
		
		// Check File Type
		for (var i = 0; i < files.originalFiles.length; i++) {			
			var fileType = files.originalFiles[i].type;
			if(fileType=="image/jpeg"||fileType=="image/png"||fileType=="image/gif"){
				// Acceptable
			} else {
				lightbox('error', 'fileType');
				return false; 	
			}
		}
	},
	done: function (e, data) {
		//console.log("done");
	},
	progressall: function (e, data) {
		var progress = parseInt(data.loaded / data.total * 100, 10);
		$('#progress .bar').css('width', progress + '%');
		lightbox('newItems');
	},
	stop: function (e, data) {
		location.reload();
	}
});

</script>