<link href="css/datatable.custom.css" rel="stylesheet">
<div class="container">
	<div class="settings-wrapper" style="float:none">
		<div class="content" style="width: 100%">
			<div class="inner">
				<h2>My Job Listing</h2>
				<table id="my_jobs" class="dataTable stripe display compact hover cell-border">
					<thead>
						<tr>
							<th>Ref. ID</th>
							<th>Title</th>
							<th>Company Name</th>
							<th>Type</th>
							<th>Remote</th>
							<th>Yrs of Exp</th>
							<th>Portfolio Items</th>
							<th>Publish</th>
							<th>Activity</th>
							<th>Edit</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script src="js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="js/admin_jobs.js" type="text/javascript"></script>
<div id="confirm_delete" class="lightbox admin-lightbox"></div>