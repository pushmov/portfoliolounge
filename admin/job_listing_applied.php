<?php
	require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobs.php';
	if (!isset($_SESSION)) session_start();
	$db = $jobs->db_instance();
	$getrow = $db->prepare("SELECT * FROM jobs WHERE reference_id=:r AND member_id=:id");
	$getrow->bindValue(':r', $e);
	$getrow->bindValue(':id', $_SESSION['SESS_MEMBER_ID']);
	$getrow->execute();
	$job = $getrow->fetch();
	if(!is_array($job)) {
		header("Location:/admin/my-job-listing");
	}
?>
<link href="css/datatable.custom.css" rel="stylesheet">
<div class="container">
	<div class="settings-wrapper" style="float:none">
		<div class="content" style="width: 100%">
			<div class="inner">
				<h2 id="ref_id" data-value="<?=$job['reference_id']?>">List of Applicants : <?=$job['title']?></h2>
				<table id="applied" class="dataTable stripe display compact hover cell-border">
					<thead>
						<tr>
							<th>Email</th>
							<th>Username</th>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Account Type</th>
							<th>Last Login</th>
							<th>Date Applied</th>
							<th>Portfolio Items</th>
							<th>Project Count</th>
							<th>External URL</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div id="confirm_reject" class="lightbox admin-lightbox"></div>
<script src="js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="js/admin_jobs.js" type="text/javascript"></script>