<?php
	require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobs.php';
	require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobcategory.php';
	
	$data = $jobs->get_by_reference_id($e);
	if(!is_array($data)) {
		header("Location:/admin/my-job-listing");
		exit();
	}

	if (!isset($_SESSION)) session_start();
	if ($data['member_id'] != $_SESSION['SESS_MEMBER_ID'])
		header("Location:/admin/my-job-listing");

	$page['title'] = 'Edit Job Data';
	$page['button'] = 'Update';
	$categories = $job_category->get_job_categories($data['job_id']);
	$cats = array();
	foreach($categories as $row) {
		$cats[] = $row['category_id'];
	}
	$data['categories'] = $cats;
	$page['action'] = 'modify';
	$page['hidden'] = '<input type="hidden" name="data[reference_id]" value="'.$data['reference_id'].'" id="reference_id">';
	$page['submit'] = '<input type="submit" class="btn" value="Update" id="update">';

	require_once $_SERVER['DOCUMENT_ROOT'] . '/jobs_form.php';