<div id="devices">
	<div class="center-title-block">
		<h2>Your Mobile Preview</h2>
		<h3>Check out these mobile versions of your portfolio.</h3>
	</div>
	<div class="device" id="macbook-air">
		<iframe src="https://<?=$username?>.portfoliolounge.com/"></iframe>
	</div>
	<div class="device" id="ipad-mini">
		<iframe src="https://<?=$username?>.portfoliolounge.com/"></iframe>
	</div>
	<div class="device" id="iphone">
		<iframe src="https://<?=$username?>.portfoliolounge.com/"></iframe>
	</div>
	<div class='clr'></div>
</div>		