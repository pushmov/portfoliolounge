<?php
// Get Projects
$get_projects = $db->prepare("SELECT * FROM projects WHERE member_id=:memberid ORDER BY project_order");
$get_projects->bindValue(':memberid', $memberid);
$get_projects->execute();
$project_count_all = $get_projects->fetchAll();
$project_count = count($project_count_all);

// Get Items
$get_items = $db->prepare("SELECT * FROM items WHERE member_id = :memberid ORDER BY item_id DESC");
$get_items->bindValue(':memberid', $memberid);
$get_items->execute();
$item_count_all = $get_items->fetchAll();
$item_count = count($item_count_all);

$get_account = $db->prepare("SELECT * FROM accounts WHERE account_id = :account_id LIMIT 1");
$get_account->bindValue(':account_id', $member['account_id']);
$get_account->execute();
$account = $get_account->fetch();

$item_percentage = $member['item_count'] / $account['item_limit'] * 100;
$project_percentage = $member['project_count'] / $account['project_limit'] * 100;

// Get Total Items
$get_total_projects = $db->prepare("SELECT project_id FROM projects WHERE member_id = :memberid");
$get_total_projects->bindValue(':memberid', $memberid);
$get_total_projects->execute();
$total_projects_all = $get_total_projects->fetchAll();
$total_projects = count($total_projects_all);

// Calculate Remaining Uploads
$available_projects = $account['project_limit'] - $total_projects; ?>

<style>

.blue-bg {	
  background-image: radial-gradient(center bottom, ellipse cover, #276fb7, #054b8d);
  background-image: -o-radial-gradient(center bottom, ellipse cover, #276fb7, #054b8d);
  background-image: -ms-radial-gradient(center bottom, ellipse cover, #276fb7, #054b8d);
  background-image: -moz-radial-gradient(center bottom, ellipse cover, #276fb7, #054b8d);
  background-image: -webkit-radial-gradient(center bottom, ellipse cover, #276fb7, #054b8d);
	background:#eee;

}
</style>
<script type="text/javascript" src="js/fb-app.js"></script>
	
<?php if($i=='welcome'){?>
	
<div id="welcome" class="blue-bg font" style="position:fixed;top:200px;padding:20px 20px 24px;z-index:99;width:160px;color:#888;font-size:12px;line-height:1.5em;margin:0;">
	<h2>
		Welcome!
	</h2>
	<p>
		We're looking forward to seeing your work!
	</p>
</div>
	
<?php }?>

<div class="container responsive">
<!-- Portfolio Header -->
<div class="portfolio_header">
	<h3>Your Portfolio has <strong><?php echo $project_count;?></strong> projects and <strong><?php echo $item_count;?></strong> total items</h3>
    <?php if($project_count <= 0){?>
	<a class="btn img newProject" href="javascript:void(0)"><strong style="margin-left:-10px;display:inline-block;">+</strong> &nbsp;<strong>Create your first project.</strong></a><br />
    <?php } else { ?>
	<a class="btn img newProject" href="javascript:void(0)"><strong style="margin-left:-10px;display:inline-block;">+</strong> &nbsp;<strong>Add a Project</strong></a>
    <?php } ?>
</div>
<div class="clear"></div>

<!-- Projects -->
<div class="content" style="margin-bottom:140px">
	<div class="inner">			
		<div id="project_grid">
			<?php foreach($project_count_all as $projects){
				if ($projects['cover_id']) {						
					$get_cover = $db->prepare("SELECT item_filename FROM items WHERE item_id = :itemid LIMIT 1");
					$get_cover->bindValue(':itemid', $projects['cover_id']);
					$get_cover->execute();
					$item_cover = $get_cover->fetch();
					$project_cover = $protocol.SITE_URL."/uploads/".$memberid."/medium/".$item_cover['item_filename'];
				
				} else {	
					$get_item = $db->prepare("SELECT item_filename FROM items WHERE project_id = :pid LIMIT 1");
					$get_item->bindValue(':pid', $projects['project_id']);
					$get_item->execute();
					$item = $get_item->fetch();
					if($item){
						$project_cover = $protocol.SITE_URL."/uploads/".$memberid."/medium/".$item['item_filename'];
					} else {
						$project_cover = "images/no-photo.png";	
					}
				}?>
				
				<div class="project_thumb" id="<?php echo $projects["project_id"];?>">
					<a href="admin/project/<?php echo $projects['project_id'];?>">
						<div class="cover" id="cover_<?php echo $projects["project_id"];?>"></div>
					</a>
					<div class="thumb_bar">
						<h2><?php echo $projects['project_title'];if($projects['privacy_option']==1){?> <img width="9" src="images/icon-lock.png"><?php } ?></h2>
						<a class="sort_handle btn img_only" title="Click and drag to change ordering" href="javascript:void(0)"><img width="17" src="images/icon-cross-hair.png" /></a>
						<a class="btn gray" title="Select cover photo" onclick="lightbox('changeCover', <?php echo $projects["project_id"];?>);" href="javascript:void(0)">Change Cover</a>
					</div>
				</div>
				
				<script type="text/javascript">					
					var thumbWidth = $('.project_thumb .cover').width();	
					var thumbHeight = $('.project_thumb .cover').height();					
					
					$("<img />").attr("src", "<?php echo $project_cover;?>").load(function() {						
						var gridRatio = thumbWidth / thumbHeight;
						var imgRatio = this.width / this.height;
						
						if (imgRatio < gridRatio){
							var imgWidthAttr = thumbWidth;
							var imgHeightAttr = this.height * (thumbWidth / this.width);
						} else {
							var imgWidthAttr = this.width * (thumbHeight / this.height);
							var imgHeightAttr = thumbHeight;
						}
						
						var marginTop = imgHeightAttr / 2 * -1 + thumbHeight / 2;
						var marginLeft = imgWidthAttr / 2 * -1 + thumbWidth / 2;
						
						$('#cover_<?php echo $projects['project_id'];?>').html('<img width="'+imgWidthAttr+'" height="'+imgHeightAttr+'" style="margin:'+marginTop+'px 0 0 '+marginLeft+'px;" src="<?php echo $project_cover;?>" />').children('img').fadeIn();
					});
				</script>
				
				
			<?php }?>
			
		
			<?php if(!$project_count){?>
			<div style="text-align:center;color:#ccc;line-height:2.5em;padding:0px 0;">
				<a class="blank-thumb newProject"></a>
				<a class="blank-thumb newProject"></a>
				<a class="blank-thumb newProject"></a>
				<a class="blank-thumb newProject"></a>
				<a class="blank-thumb newProject"></a>
				<style>
					.blank-thumb {
						display:block;
						height:200px;
						width:266px;
						border:1px dashed #ccc;
						float:left;
						margin:10px;
					}
				</style>
                <!--
                <h2>Here is where your projects are stored!</h2>
				<p>Create new projects, and they will be displayed here.</p>
               
                <h2>Create a project and it will be shown here.</h2>
				<p>Create many projects to show case your work.</p>

 -->
			</div>
			<?php } ?>
		
		</div><!-- project_grid -->
		<div class="clear"></div>
	</div>
</div>
</div><!-- container -->
<script>
/* Item Uploader
-----------------------------------------------------------*/	
var availableProjects=<?php echo $available_projects;?>;
var totalProjects=<?php echo $total_projects;?>;
$('.newProject').click(function(){
	if (availableProjects <= 0) {
		lightbox('projectLimit', availableProjects);
	} else {
		lightbox('newProject');
	}
});
</script>