<?php
// Get Item
$get_this_item = $db->prepare("SELECT * FROM items WHERE item_id=:itemid LIMIT 1");
$get_this_item->bindValue(':itemid', $x);
$get_this_item->execute();
$this_item = $get_this_item->fetch();

// Get Text
$item_project = $this_item['project_id'];
$item_title = $this_item['item_title'];
$item_desc = $this_item['item_desc'];
$item_project = $this_item['project_id'];
$item_price = $this_item['item_price'];
$video_id = $this_item['video_id'];
$video_type = $this_item['video_type'];

// Next Item in Project
$get_next_item = $db->prepare("SELECT item_id FROM items WHERE project_id = :pid AND item_id > :itemid ORDER BY item_id ASC LIMIT 1");
$get_next_item->bindValue(':pid', $item_project);
$get_next_item->bindValue(':itemid', $x);
$get_next_item->execute();
$next_item = $get_next_item->fetch();
$next = $next_item['item_id'];

// Prev Item in Project
$get_prev_item = $db->prepare("SELECT item_id FROM items WHERE project_id = :pid AND item_id < :itemid ORDER BY item_id DESC LIMIT 1");
$get_prev_item->bindValue(':pid', $item_project);
$get_prev_item->bindValue(':itemid', $x);
$get_prev_item->execute();
$prev_item = $get_prev_item->fetch();
$prev = $prev_item['item_id']; ?>

<script type="text/javascript">
//<![CDATA[
    // Project Model
    function singleData($scope) {
        $scope.title = "<?php echo $item_title?>";
        $scope.description = "<?php echo $item_desc;?>";
        $scope.price = "<?php echo $item_price;?>";
        $scope.video_id = "<?php echo $video_id;?>";
        $scope.video_type = "<?php echo $video_type;?>";
        $scope.video_url_or_id = $scope.video_id;

        $scope.$watch('video_url_or_id', function(value) {
            if(value.match(/^https?/)) {
                // url has been inserted
                var found;
                switch ($scope.video_type) {
                    case 'youtube':
                        // https://www.youtube.com/watch?v=qRv7G7WpOoU
                        // find id section from youtube url
                        // first check if url is in short form e.g. "https://youtu.be/qRv7G7WpOoU" then check full form
                        found = value.match(/youtu.be\/([^\?]+)/) || value.match(/[\?&]v=([^&]+)/);
                        if(found) {
                            $scope.video_id = found[1];
                        }
                        break;
                    case 'vimeo':
                        // https://vimeo.com/62184299
                        found = value.match(/vimeo.com\/([\d]+)/);
                        if(found) {
                            $scope.video_id = found[1];
                        }
                        break;
                    case 'soundcloud':
                        // https://soundcloud.com/imrealugly/bitch
                        found = value.match(/soundcloud.com\/(.+)/);
                        if(found) {
                            $scope.video_id = found[1];
                        }
                        break;
                }
            } else {
                $scope.video_id = value;
            }
        });

        if($scope.video_id){
            $scope.video=true;  
        }
    }
//]]>
</script>
<div class="" style="text-align:center;">
    <a class="gray btn" style="margin:40px 0 0 40px;" href="/admin/project/<?=$this_item['project_id']?>">&laquo; &nbsp;<small>Back to Project</small></a>
</div>    
    <?php if ($next){?><a class="next arrow right" href="admin/item/<?php echo $next_item['item_id'];?>"><span></span></a><?php } ?>           
    <?php if ($prev){?><a class="prev arrow left" href="admin/item/<?php echo $prev_item['item_id'];?>"><span></span></a><?php } ?>
    <div class="content" ng-controller="singleData" id="editor" style="width:720px">
        <div class="inner">            
            <img class="display" width="400" src="<?=$protocol.SITE_URL?>/uploads/<?php echo $memberid;?>/medium/<?php echo $this_item["item_filename"];?>" />
            <form method="post" id="single-data">
                <div class="inputs">
                    <label>Title</label>
                    <input name="title" ng-model="title" placeholder="Item Title" /><br />
                    <label>Description</label>
                    <textarea name="description" ng-model="description" placeholder="Item Description"></textarea><br />
                    <a ng-show="!video" ng-click="video=!video" class="">Link a Video</a>
                    <div ng-show="video">
                        <div class="row">
                            <div class="center-title-block">
                                <img width="50" src="images/icon-video.png" />
                                <h3>Add Media</h3>
                            </div>
                                                        <label>Video Type (vimeo, youtube, or soundcloud)</label>
                                                        <select name="video_type" ng-model="video_type" ng-init="video_type='vimeo'">
                                                                <option value="vimeo">Vimeo</option>
                                                                <option value="youtube">Youtube</option>
                                                        </select>
                                                        <div ng-show="video_type">
                                                                <label>Media ID</label>
                                                                <input ng-model="video_url_or_id" placeholder="Enter {{video_type}} ID">
                                                                <input type="hidden" name="video_id" value="{{video_id}}">
                                                        </div>
                            <div class="videoTips">
                                <p ng-show="video_type=='youtube'">ex. https://www.youtube.com/watch?v=<strong>qRv7G7WpOoU</strong></p>
                                <p ng-show="video_type=='vimeo'">ex. https://vimeo.com/<strong>62184299</strong></p>
                                <p ng-show="video_type=='soundcloud'">ex. https://soundcloud.com/<strong>?</strong></p>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="item_id" value="<?php echo $x;?>" />
                </div>
                <br><br><br>
                <input class="btn large" type="submit" value="Save Info">
            </form>
            <div class="clear"></div>
        </div>
    </div>
<div class="" style="text-align:center;">
    <a class="gray btn" style="margin:40px 0 40px 40px;" href="/admin/project/<?=$this_item['project_id']?>">&laquo; &nbsp;<small>Back to Project</small></a>
</div>