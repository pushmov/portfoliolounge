<div class="center-title-block" data-file="new-templates">
	<h2>Choose a Template</h2>
	<h3>Then customize your template below.</h3>
</div>
<div id="templates">
	<div class="inner">
		<?php
		$get_templates = $db->query("SELECT * FROM templates ORDER BY template_id");
		$templates_all = $get_templates->fetchAll();
		foreach($templates_all as $template){?>
			<div class="template <?php if($rowodd==true){echo 'odd';} if($template['premium'] && $member['account_id']=='1'){echo 'paidOnly';} ?>" id="<?php echo $template['template_slug'];?>">
				<div class="check <?php if($member['template_id']==$template['template_id']){echo "checked";}?>"></div>				
				<img width="600" class="thumb" src="images/thumbs/template-<?php echo $template['template_slug'];?>.jpg" />
				<div class="info">
					<h2><?php echo $template['template_name'];?><?php if($template['status']){?><img class="new" src="images/icons/new-pin.png" /><?php } ?></h2>
					<?php if( $template['premium']){?><small style="padding:0;"><a style="color:#888" href="admin/upgrade">Premium Template</a></small><?php } ?> 
					<div class="clr">
						<br>
					</div>
					<p><?php echo $template['template_desc'];?></p>
					<a target="_blank" class="btn gray" href="http://<?=$username?>.v.<?=$template['template_id']+1?>.portfoliolounge.com">Preview</a>
					<?php if( $template['mobile'] == 1){?><p style="padding:14px 0 0 24px;font-size:11px;"><img style="position:absolute;margin:-4px 0 0 -25px;width:18px;" src="images/icon-large-phone.png" />Mobile Version Included</p><?php } ?> 
				</div>
				<input id="template-id" type="hidden" value="<?php echo $template['template_id'];?>" />
			</div>
		<?php } ?>
	</div>
	<div class="clr"></div>
</div>

<div class="container">
	<hr>
	<div class="center-title-block">
		<h2>Customize.</h2>
		<h3>Options vary from template to template.</h3>
	</div>
	<div class="content">
		<div class="inner">
			<div class="option layout-options <?php if($member['template_id']!=0){echo "hide";}?>">		
				<h2 class='option-title'>Project Layout</h2>
				<div id="layouts">				
					<div class="layout-preview" id="list">
						<div class="check <?php if($member['layout_option']=="list"){echo "checked";}?>"></div>
						<h2>List</h2>
						<div class="design-thumb"><img class="thumb" src="images/thumbs/layout-list.png" /></div>
						<input id="layout-id" type="hidden" value="list" />
					</div>				
					<div class="layout-preview" id="slideshow">
						<div class="check <?php if($member['layout_option']=="slideshow"){echo "checked";}?>"></div>
						<h2>Slideshow</h2>
						<div class="design-thumb"><img class="thumb" src="images/thumbs/layout-slideshow.png" /></div>
						<input id="layout-id" type="hidden" value="slideshow" />
					</div>
				
				</div>
			</div>
			<!-- Colors -->
			<div class="option color-selector" >
				<h2 class='option-title'>Color Scheme</h2>
				<div class="color-schemes">
					<?php 
					$get_color_schemes = $db->query("SELECT * FROM colors");
					$color_schemes_all = $get_color_schemes->fetchAll();
					foreach($color_schemes_all as $color_scheme){ ?>
						<div class="color-selection <?=$scheme_class?>" style="background:<?=$color_scheme['background']?>;color:<?=$color_scheme['main_text']?>;">
							<div class="inner">
								<div class="check <?php if($member['color_scheme_id']==$color_scheme['color_scheme_id']){echo "checked";}?>"></div>
								<h2 style="color:<?=$color_scheme['sub_text']?>"><?=$color_scheme['color_scheme_title']?></h2>
								<input id="color-scheme-id" type="hidden" value="<?=$color_scheme['color_scheme_id']?>" />
							</div>
						</div>
					<?php } ?>
					<div class="clr"></div>
				</div>
			</div>
		
			<!-- Fonts -->
			<div class="option font-selector">		
				<h2 class='option-title'>Choose Font</h2>		
				<?php 
				$get_font = $db->query("SELECT * FROM fonts");
				$fonts_all = $get_font->fetchAll();
				foreach($fonts_all as $font){?>
					<div class="font_preview" id="<?php echo $font['font_id'];?>">
						<link href='<?=$protocol?><?php echo $font['url'];?>' rel='stylesheet' type='text/css'>
						<style>.webfont<?php echo $font['font_id'];?> {font-family:'<?php echo $font['name'];?>'}</style>
						<div class="check <?php if($font['font_id']==$member['font_id']){echo "checked";}?>"></div>
						<h3 class="webfont<?php echo $font['font_id'];?>"><?php echo $font['name'];?></h3>
					</div>
				<?php } ?>
			</div>
			<p class="disclaimer">Fonts provided by <a href="http://www.google.com/fonts">Google Fonts</a>.</p>
			
			<!-- Banner 
			<div class="option color-selector" >
				<h2 class='option-title'>Banner</h2>
				<p class="centered">
					<h2 class="left"><div class="check <?php if($member['banner']==0){echo "checked";}?>"></div>I like banners</h2>
					<h2 class="right"><div class="check <?php if($member['banner']==1){echo "checked";}?>"></div>I don't like banners</h2>
				</p>
			</div>
		-->
		</div>
	</div>
	<hr>
</div>
<script type="text/javascript" src="js/template.js"></script>