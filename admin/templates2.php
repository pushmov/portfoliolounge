<script type="text/javascript" src="js/template2.js"></script>

<?php if($x=='templates'||!$x){ /* -------------------- */?>
<div class="center-title-block" data-file="new-templates-2">
	<h2>
		Choose a Template
	</h2>	
	<h3>
		Your portfolio will look great!
	</h3>
</div>
<div class="template-container" ng-controller="templateCtrl" ng-show="templates">
	<div ng-init="currentTemplate=<?=$member['template_id']?>"></div>
	<div class="artist" ng-repeat="template in templates" ng-class-even="'flip'">
		<div class="macbook">
			<h3 class="circle-check" ng-class="{'cur': currentTemplate==template.template_id}"><img style="margin:36px 0 0 -2px" width="80" src="images/icon-large-checkmark.png"></h3>
			<div class="stage">
				<img class="display" ng-src="img/templates/{{template.template_slug}}-desktop.jpg" />
			</div>
		</div>
		<div class="iphone7">
			<div class="stage">
				<img class="display" ng-src="img/templates/{{template.template_slug}}-mobile.jpg" />
			</div>
		</div>
		<div class="text">
			<h2>{{template.template_name}}</h2>
			<p>{{template.template_desc}}</p>
			<ul class="btns">
				<li><a ng-class="{'cur': currentTemplate==template.template_id}" class="font gray" ng-click="setTemplate(template.template_id)">Activate</a></li> 
				<li><a class="font" target="_blank" ng-href="http://<?=$member['username']?>.v.{{template.template_id}}.portfoliolounge.com">Preview</a></li>
			</ul>
		</div>
	</div>
	<div class="clr"></div>
</div>
<?php } ?>
	

	
	
<?php if($x=='background'){ /* -------------------- */?>
<div class="template-container" ng-controller="templateCtrl" ng-cloak>
	<div style="margin:0 0 40px">
		<h1>Choose a Background</h1>
		<div style="height:40px">
			<a ng-show="currentBg" ng-click="setBackground()">Remove background</a>
		</div>
	</div>
	<div ng-init="currentBg='<?=$member['background']?>'" class="backgrounds" style="width:600px;float:right;">
		<a class="frame" style="width:300px;height:200px;" ng-repeat="background in backgrounds" ng-click="setBackground(background.slug)">
			<div class="canvas" data-source="img/photos/{{background.slug}}.jpg">
				<div style="position:absolute;" ng-show="currentBg==background.slug">
					<h3 class="circle-check">
						<img src="images/icon-large-checkmark.png">
					</h3>
				</h3>
				</div>
			</div>
		</a>
	</div>
</div>
<?php } ?>


<?php if($x=='fonts'){ /* -------------------- */?>
<div class="center-title-block">
	<h2>
		Choose a Font
	</h2>	
	<h3>
		Try them out!
	</h3>
</div>
<div class="container" ng-controller="templateCtrl">
	<div class="font-display" ng-init="currentFont=<?=$member['font_id']?>">
			<li class="font_option" ng-class="{'cur' : font.font_id==currentFont}" ng-repeat="font in fonts">
				<link ng-href='<?=$protocol?>{{font.url}}' rel='stylesheet' type='text/css'>
				<!--<div ng-click="setFont(font.font_id)" class="check" ng-class="{'checked' : font.font_id==currentFont}"></div>-->
				<a ng-click="setFont(font.font_id)" style="font-family:'{{font.name}}'">{{font.name}}</a>
			</li>
			<div class="clr"></div>	
			<p ng-show="fonts" style="text-align:center;font-size:11px;margin-top:100px;">Fonts provided by <a href="https://google.com/fonts">Google Fonts.</a></p>																																												
	</div>
</div>
<?php } ?>




<?php if($x=='colors'){ /* -------------------- */?>
	<!-- Colors -->
<div class="center-title-block">
	<h2>
		Choose a color theme.
	</h2>	
	<h3>
		Choose between dark and light.
	</h3>
</div>
<div class="container" ng-controller="templateCtrl">
	<div class="option color-selector" ng-init="currentColor=<?=$member['color_scheme_id']?>">

		<div class="color-schemes">


				<div ng-repeat="color in colors" class="color-selection" style="background:{{color.background}}" ng-click="setColor(color.color_scheme_id)">
					<div class="inner">
						<div class="check" ng-class="{'checked': currentColor==color.color_scheme_id}"></div>
						<h2 style="color:{{color.sub_text}}">{{color.color_scheme_title}}</h2>
					</div>
				</div>
			<div class="clr"></div>
		</div>
	</div>
</div>
<?php } ?>
