<div class="container">
	<div class="content" style="background-image:url(img/banner/detroit3.jpg);background-repeat:no-repeat;background-size:100%;padding-top:400px;">
		<div class="inner">
			<div id="about">
		
				<h2>Advertising with Portfoliolounge</h2>
				<p>If your business is interested in advertising with Portfoliolounge, contact us at info@portfoliolounge.com. We can happily answer any questions you might have. Our pricing packages are very reasonable and your business will be impressed upon thousands of creatives and executive minds alike. We have millions of page loads every year and would love to have you as part of the Portfoliolounge team. <br /><br /><small>Matt Visk ~ Founder at Portfoliolounge ~ info@portfoliolounge.com</small></p>
				
			</div>
			<style>
				#about {
					line-height:1.5em;
					font-size:12px;	
					padding:50px 84px;
				}
				#about p {
					margin:20px 0 20px;	
				}
				#about h1 {
					margin:20px 0 80px;	
					font-size:50px;
				}
			</style>
		</div>
	</div>
</div>
