<!DOCTYPE html>
<html ng-app>
<head>
<meta charset="UTF-8">
<title>Portfolio Manager</title>
<link rel="icon" type="image/png" href="/images/portfoliolounge_favicon_1.png" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/styles_v2.css" />
<script src="js/libs/angular.min.js"></script>
<script src="js/libs/jquery-1.8.3.min.js"></script>
<script src="affiliates/js/ctrl.js"></script>
<script src="affiliates/js/signup-affiliate.js"></script>
</head>
<body ng-controller="affiliateCtrl">
        <div class="affiliate-container">
            <a href="http://sxdmodelsandartists.com"><img alt="sxdmodelsandartists.com" width="200" src="https://static1.squarespace.com/static/57ed4446b3db2b3f433cd40f/t/58928291a5790a5ec3842c87/1485996689447/SXD+M%26A+Web+Logo2.png?format=2500w"></a>
            
            <h3>Portfolio Manager</h3>
            
            <!-- Login into affiliate account -->
            <div ng-show="!signup">
                <div class="inner">
                    <form method="post" action="function/login.php">
                        <?php if($i=='error'){?>
                            <p class="error show" style="text-align:center">Incorrect username and password combination. Please try again.</p>
                            <br />
                        <?php }?>
            
                        <div class="inputs">
                            <label>Username <span></span></label>
                            <input class="username" id="username" name="username" placeholder="Username" />
                            <label>Password</label>
                            <input class="password" name="password" type="password" placeholder="Password" />
                        </div>
                        <br>
                        <input class="btn" type="submit" value="Login" />
                    </form>
                </div>
                
                <div class="inner">
                    <hr>
                    <a class="btn gray" href="signup">Create New Account</a>
                    <p>Want to create your portfolio? Let's get started!</p>
                </div>
            </div>
        </div>
</body>
</html>