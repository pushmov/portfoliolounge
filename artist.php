<?php 

$get_portfolio = $db->prepare("SELECT * FROM members WHERE username = :username LIMIT 1");
$get_portfolio->bindValue(':username', $i);
$get_portfolio->execute();
$portfolio = $get_portfolio->fetch();

$get_projects = $db->prepare("SELECT * FROM projects WHERE member_id = :member_id ");
$get_projects->bindValue(':member_id', $portfolio['member_id']);
$get_projects->execute();
$projects = $get_projects->fetchAll();

?>
<style>
	.snap_container {
		margin:100px auto;
		width:960px;
	}
	#snap_sidebar {
		width:250px;
		float:left;
		background:#fff;
		border-radius:3px;
		border:1px solid #ddd;
		padding:20px;
		font-size:11px;
		color:#888;
		line-height:1.7em;
	}
	#snap_sidebar h1 {
		font-size:12px;
	}
	.portfolio {
		width:630px;
		margin:0 auto;
		float:right;
	}
	.snapshot_project {
		margin:0 0 50px 0;
	}
	.snapshot_project .frame {
		height:210px;
		width:210px;
		float:left;
		margin:0;
	}
	.snapshot_project h1 {
		background:#222;
		padding:20px;
		font-size:14px;
		color:#fff;
		margin:0;
	}
	.snapshot_project p {
		padding:20px;
		background:#eee;
		font-size:11px;
		line-height:1.4em;
		color:#888;
	}
	.portfolio p.frame.desc {
		background:rgba(255,255,255,.25);
		font-size:11px;
		padding:10px 30px 10px 10px;
		width:160px;
		height:180px;
	}
	.frame.profile_img {
		width:100%;
		height:200px;
		margin-bottom:10px;
	}
</style>

<div class="snap_container">
	<a class="btn right" title="Portfolio Website of <?=$portfolio['username']?>" target="_blank" href="https://<?=$portfolio['username']?>.portfoliolounge.com">View Portfolio</a>
	<h2 style="font-size:60px">
		<?=$portfolio['site_title']?> 
		<?php if(!$portfolio['site_title']) echo $portfolio['username'];?>
	</h2>
	<h3>
		<?php if($portfolio['site_title']) echo $portfolio['username'];?>
	</h3>
	<hr>
	<div id="snap_sidebar">
		<?php if($portfolio['profile_img']){?>
			<div class="frame profile_img">
				<div class="canvas" data-source="<?php echo $protocol.SITE_URL."/uploads/".$portfolio['member_id']."/".$portfolio['profile_img'] ?>"></div>
			</div>
		<?php } ?>
		<p>
			<?=$portfolio['tagline']?>
		</p>
		<p>
			<?=$portfolio['about']?>
		</p>
		<p>
			<?=$portfolio['location']?>
		</p>
	</div>


	<div class="portfolio">
		<?php if(!empty($projects)) : //if not empty ?>
		<?php foreach($projects as $project) : ?>
		<div class="snapshot_project">
			<?php if($project['project_title']){?><h1><?=$project['project_title']?></h1><?php } ?>
			<?php if($project['project_desc']){?><p><?=$project['project_desc']?></p><?php } ?>
			<?php if(isset($project['project_url']) && $project['project_url']){?><p><?=$project['project_url']?></p><?php } ?>
			<?php 
				$get_items = $db->prepare("SELECT * FROM items WHERE project_id = :project_id AND member_id = :member_id");
				$get_items->bindValue(':project_id', $project['project_id']);
				$get_items->bindValue(':member_id', $project['member_id']);
				$get_items->execute();
				$items = $get_items->fetchAll();
				if (!empty($items)) :
					foreach($items as $item) :
			?>
				<div class="frame">
					<div class="canvas" data-source="uploads/<?=$portfolio['member_id']?>/medium/<?=$item["item_filename"]?>"></div>	

				</div>
				<?php if($item['item_title']||$item['item_desc']) :?>
					<p class="frame desc"> &laquo;
						<?=$item['item_title']?> 
						<?=$item['item_desc']?>
					</p>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
			<div class="clr"></div>
		</div>
		<?php endforeach; ?>
		<?php endif; ?>

	</div>
</div>