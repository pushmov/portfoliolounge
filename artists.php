<style>

.artist-container {
	width:960px;
	background:#fff;
}	

.artist {
	background:#fff;
	max-width:960px;	
	position:relative;
	height:300px;
	width:680px;
	margin:20px;
	float:left;
}
.artist.flip {
	float:right;
}
.artist .iphone7 {
	background:url(../img/iphone7.png);
	background-size:100%;
	height:210px;
	width:123px;
	position:absolute;
	margin:80px 0 0 400px;
}
.artist .macbook {
	background:url(../img/macbook.png);	
	background-size:100%;
	height:306px;
	width:507px;
	position:absolute;
	margin:20px 0 0 0;
}
.artist .iphone7 img {
	width: 80px;
    margin: 19px 0 0 20px;
    height: 152px;
}
.artist .macbook img {
	width:337px;
	margin:11px 0 0 83px;
}
.artist .text {
	position:absolute;
	margin:18px 0 0 450px;
}
.artist .text h2 {	
	font-size:18px;
}
.artist .text h3 {	
	font-weight:700;
	font-size:18px;
}

.artist ul.btns {
	margin:40px 0 0 55px;
	padding:0;
}
.artist ul.btns li {
	list-style:none;
	margin:0;
	padding:0;
}
.artist .btns a {
	background:#eee;
	display:inline-block;
	padding:1em;
	padding-right:2em;
	font-size:14px;
	color:#888;
	margin:0 0 2px;
	-webkit-transition: padding 200ms;
    -moz-transition: padding 200ms;
    -o-transition: padding 200ms;
	border-radius:2px;
}
.artist .btns a.gray {
	background:#e2e2e2;
}
.artist .btns a:hover {
	padding-right:2.2em;
}

</style>
      
        
<script>
    function artistCtrl($scope, $http) {
        
		$scope.artists = [
		
					
				{name:'Matt', location:'Detroit', url:'matt', path:"01"},
				{name:'Matt', location:'Detroit', url:'matt', path:"01"},
				{name:'Matt', location:'Detroit', url:'matt', path:"01"},
				{name:'Matt', location:'Detroit', url:'matt', path:"01"},
				{name:'Matt', location:'Detroit', url:'matt', path:"01"},
				{name:'Matt', location:'Detroit', url:'matt', path:"01"}
				
		];
		
    }
</script>

<div class="artist-cont ainer" ng-controller="artistCtrl">
    
    <div class="artist" ng-repeat="artist in artists" ng-class-even="'flip'">
        <div class="text">
            <h2>{{artist.name}}</h2>
            <h3>{{artist.location}}</h3>
            <ul class="btns">
                <li><a class="font" target="_blank" ng-href="https://{{artist.url}}.portfoliolounge.com">View Portfolio.</a></li>
                <li><a class="font gray" href="signup">Try it out!</a></li>
            </ul>
        </div>
        
        <div class="macbook">
            <img ng-src="img/screenshots/macbook-{{artist.path}}.jpg" />
        </div>
        <div class="iphone7">
            <img ng-src="img/screenshots/iphone7-{{artist.path}}.jpg" />
        </div>
    </div>
    <div class="clr"></div>
</div>