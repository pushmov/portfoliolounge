<?php

// Get Categories
$get_categories = $db->query("SELECT * FROM categories ORDER BY category_name");
$category_arr = $get_categories->fetchAll();

// Pagination Values
$page = $x ? $x:0;
$show_per_page = 16;
$limit_start = $page * $show_per_page;

// Set i to 0 if undefined
// Set browse type
$browse_type = $i ? $i : 'editor-picked-portfolios'; 

// Remove '-portfolios' from category name
$category_url = str_replace('create-your-','',$i);
$category_url = str_replace('-portfolio-website','',$category_url);

/* Get editors' Picks
-------------------------------------*/
$dailyRandomOrder = date("dmY");

if($browse_type=='editor-picked-portfolios'){
	
	$get_members = $db->prepare("SELECT member_id, username, about, location, first_name, last_name, logo_img, account_profile_status FROM members WHERE (top_pick > 0 OR editors_pick > 0) AND item_count > 0 ORDER BY top_pick DESC, 
		RAND(".$dailyRandomOrder.") LIMIT :limit_start, :show_per_page");
	$get_members->bindValue(':limit_start', $limit_start);
	$get_members->bindValue(':show_per_page', $show_per_page);
	$get_members->execute();
	$members = $get_members->fetchAll();
	$member_count = count($members);
	$member_arr = $members;
}

/* Get Recent Members
-------------------------------------*/
else if($browse_type=='recent-portfolios'){
	
	$get_members = $db->prepare("SELECT member_id, username, about, location, first_name, last_name, account_profile_status FROM members WHERE (item_count > 1 AND project_count > 0) 
		ORDER BY member_id DESC LIMIT :limit_start, :show_per_page");
	$get_members->bindValue(':limit_start', $limit_start);
	$get_members->bindValue(':show_per_page', $show_per_page);
	$get_members->execute();
	$members = $get_members->fetchAll();
	$member_count = count($get_members);
	$member_arr = $members;
}

/* Get Members by Category
-------------------------------------*/
else if($browse_type){
	$member_arr = array();
	
	// Determine Category ID
	$get_category = $db->prepare("SELECT * FROM categories WHERE category_url = :category_url");
	$get_category->bindValue(':category_url', $category_url);
	$get_category->execute();
	$category = $get_category->fetch();

	$category_id = $category['category_id'];
	$category_name = $category['category_name'];
	// Get Members with Skills = to Category ID
	$get_member_skills = $db->prepare("SELECT member_id FROM member_skills WHERE category_id = :category_id ORDER BY member_id DESC");
	$get_member_skills->bindValue(':category_id', $category_id);
	$get_member_skills->execute();
	$member_arr = array();
	foreach($get_member_skills->fetchAll() as $member_skill){
		
		$get_members = $db->prepare("SELECT member_id, username, about, location, first_name, last_name,logo_img, account_profile_status FROM members
			WHERE member_id = :member_id AND item_count > :icount AND project_count > :pcount");
		$get_members->bindValue(':member_id', $member_skill['member_id']);
		$get_members->bindValue(':icount', 1);
		$get_members->bindValue(':pcount', 0);
		$get_members->execute();
		$members_fetch = $get_members->fetchAll();
		if (count($members_fetch) > 0) {
			$member_arr[] = $members_fetch[0];
		}
	}
	$member_arr = array_slice($member_arr, $limit_start, $show_per_page);
	$member_count = count($member_arr);

}

$last_page = $member_count < $show_per_page ? true : false;
?>

	
		<div class="strategic" style="text-align:center;margin:20px 0 0;">
		<?php if($browse_type=='editor-picked-portfolios'){?>
		<h1>Browse Portfolios</h1>
		<?php } else if ($browse_type=='recent-portfolios'){?>
		<h1>Browse Recent Portfolios</h1>
		<?php } else { ?>
		<h1><?php echo $category_name;?> Portfolios</h1>
		<?php } ?>
		</div>
			
    <?php if(!$memberid){?>  
	<div class="browse_head">
    	<h2>Personal Portfolio Websites</h2>
      <p>Portfolio Lounge helps artist in many different categories.  Whether your creations are in the arena of Photography, Architecture, Interior Design, Graphic Design, Illustration or any of a whole host of others, there is a category for you and your work. Portfolio Lounge enables you to reach a wider audience, get valuable feedback on your work and create a name for yourself, all without worrying about coding, hosting, search engine optimization or any of the other headaches commonly associated with creating an outstanding online portfolio that showcases your work to its best advantage.  With a portfolio hosted by Portfolio Lounge, you can make sure your stunning creations are given an equally stunning online setting.</p>
        <a href="signup">Create your portfolio</a>
	</div>
    <?php 	} ?>
    
    <div class="clr"></div>
	
    
    
	<div id="browse_nav">
		<ul>
			<li><a <?php if($browse_type=='editor-picked-portfolios'){echo 'class="cur"';}?> href="browse-portfolios">Editors' Pick ★ </a></li>
		</ul>
		<ul>
			<?php foreach($category_arr as $category){?>
				<li><a <?php if($category_url==$category['category_url']){echo 'class="cur"';}?> href="browse-portfolios/create-your-<?php echo $category['category_url'];?>-portfolio-website"><?php echo $category['category_name'];?></a></li>
			<?php } ?>
		</ul>
		<ul>
			<li><a <?php if($browse_type=='recent-portfolios'){echo 'class="cur"';}?> href="browse-portfolios/recent-portfolios">Recent Portfolios</a></li>
		</ul>
	</div>
	
	
	<div id="browse_grid">
	
    
		<?php if(!$memberid){ ?>
			<div class="strategic">
				<a href="signup" class="btn right">Create a portfolio</a>
				<h1>Create your <?=$category_name?> Portfolio!</h1>
				<hr>
			</div>
		<?php } ?>
		<?php /* Loop Out Members
		-------------------------------------*/
		foreach($member_arr as $member){
			
			$get_cover = $db->prepare("SELECT cover_id FROM projects WHERE member_id = :member_id AND cover_id != :c ORDER BY RAND() LIMIT 1");
			$get_cover->bindValue(':member_id', $member['member_id']);
			$get_cover->bindValue(':c', '');
			$get_cover->execute();
			$cover = $get_cover->fetch();
			$cover_id = $cover['cover_id'];
					
			
			if(!$member['logo_img']){
							
				if($cover_id){					
					
					$get_cover = $db->prepare("SELECT item_filename FROM items WHERE item_id = :item_id");
					$get_cover->bindValue(':item_id', $cover_id);
					$get_cover->execute();
					$cover = $get_cover->fetch();
				} else {				
					
					$get_cover = $db->prepare("SELECT item_filename FROM items WHERE member_id = :member_id ORDER BY RAND() LIMIT 1");
					$get_cover->bindValue(':member_id', $member['member_id']);
					$get_cover->execute();
					$cover = $get_cover->fetch();
				}		
				if($cover['item_filename']){
					$cover_path = $protocol."portfoliolounge.com/uploads/".$member['member_id']."/medium/".$cover['item_filename'];			
				} else {
					$cover_path = '';
				}
				
				
			
			} else {
			
				$cover_path = $protocol."portfoliolounge.com/uploads/".$member['member_id']."/".$member['logo_img'];	
			
			
			}?>	
			
			<?php if($member['account_profile_status']!='Suspended'){?>
			<!--<a class="browse_thumb frame" target="_blank" href="http://<?php echo $member['username'];?>.portfoliolounge.com">-->
			<a class="browse_thumb frame" href="portfolio/<?=$member['username']?>">
				<div class="canvas<?php if($member['logo_img']){echo "Fit";}?>" data-source="<?php echo $cover_path;?>"></div>
				<div class="info">
					<div class="inner">
						<?php if($member['first_name']){?><strong><?php echo $member['first_name']."'s Portfolio Website";?></strong><?php } else {?><strong><?php echo $member['username']."'s Portfolio Website";?></strong><?php } ?>
						<?php if($member['location']){?><p class="location"><?php echo $member['location'];?></p><?php } ?>
						<?php if($member['about']){?><p class="about"><?php echo htmlspecialchars($member['about']);?></p><?php } ?>
					</div>
				</div>
				<div class="thumb_overlay"></div>
			</a>
            <?php } ?>
			
					
		<?php } ?>
				
				
				
	</div><!-- browse grid -->
	
				
					
		<div id="browse_pagination_wrapper">
			<div id="browse_pagination">
					<?php if($page > 0){?>
					<a class="left" href="/browse-portfolios/<?php echo $browse_type;?>/<?php echo $page-1;?>"></a>
					<?php } ?>
					
					<?php if(!$last_page){?>
					<a class="right" href="/browse-portfolios/<?php echo $browse_type;?>/<?php echo $page+1;?>"></a>
					<?php } ?>
		
			</div>
		</div>