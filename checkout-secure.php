<?php 
// if Logged In
if ($memberid){
    // Get Member
    
    $result = $db->prepare("SELECT * FROM members WHERE member_id = :member_id LIMIT 1");
    $result->bindValue(':member_id', $memberid);
    $result->execute();
    $member = $result->fetch();
}

$secure_path = 'http://portfoliolounge.com/';


if($i=='professional') {
    $account_name = "Pro";
    $account_price = "12";
    $account_id = 2;
}
if($i=='maximum') {
    $account_name = "Max";
    $account_price = "24";
    $account_id = 3;
}?>

<?php if($x=='success'){ ?>

<div class="container bg-image" style="min-height:500px;">

    <div class="center-title-block">
        <h2>Awesome!</h2>
        <h3>You have successfully upgraded your account.</h3>
        <a class="promobtn" href="admin/account/profile">Go to my account</a>
    </div>

</div>


<?php } else { ?>

<script type="text/javascript" src="js/signup.js"></script>
<div class="frame" style="top:0;height:100%;width:100%;position:fixed;z-index:-1;background:rgba(0,0,0,.75)"></div>
<div class="frame" style="top:0;height:100%;width:100%;position:fixed;z-index:-2;">
    <div class="canvas" data-source="img/photos/create-a-portfolio-jeff-sheldon.jpg">
        
    </div>
</div>
<div class="container">

    <div class="center-title-block" style="color:#fff">
        <h2><?php echo $account_name;?> Checkout</h2>
        <h3 style="color:#eee;">Quick checkout and you're ready to go!</h3>
    </div>
    
    <div id="checkout">
    
        <!-- PAYMENT DETAILS -->
        <div id="payment-details-view" class="upgrade-view">
            <form id="checkout-form" method="post" action="function/paypal-subscribe.php">
                <div class="row">
                    <div class="cc-num">
                        <label>Card Number</label>
                        <input name="cc_number" value="" />
                    </div>
                    <div class="ccv">
                        <label>Security Code</label>
                        <input maxlength="4" name="cvv" value="" />
                    </div>                
                </div>
                <div class="row">
                    <div class="cc-container">
                        <label>Select Your Card</label>
                        <a class="cc_type visa" href="javascript:void(0)">Visa</a>
                        <a class="cc_type amex" href="javascript:void(0)">Amex</a>
                        <a class="cc_type mastercard" href="javascript:void(0)">Mastercard</a>
                        <a class="cc_type discover" href="javascript:void(0)">Discover</a>
                        <input class="cc_type" type="hidden" name="cc_type" value="" />
                        <script type="text/javascript">
                            $(".cc_type").click(function(){
                                thisType = $(this).text();
                                $('input.cc_type').val(thisType);
                                $('.cc_type').addClass('disabled');
                                $(this).removeClass('disabled');
                            });
                        </script>
                    </div>
                    <div class="expiration-container">
                        <label>Expiration Date</label>
                        <ul class="expiration">
                            <li>
                                <span class="month">Month</span>
                                <select name="exp_month" class="month">
                                    <option value="">Month</option>
                                    <option value="01">1 - January</option>
                                    <option value="02">2 - February</option>
                                    <option value="03">3 - March</option>
                                    <option value="04">4 - April</option>
                                    <option value="05">5 - May</option>
                                    <option value="06">6 - June</option>
                                    <option value="07">7 - July</option>
                                    <option value="08">8 - August</option>
                                    <option value="09">9 - September</option>
                                    <option value="10">10 - October</option>
                                    <option value="11">11 - November</option>
                                    <option value="12">12 - December</option>
                                </select>
                            </li>
                            <li class="year">
                                <span class="year">Year</span>
                                <select name="exp_year" class="year">
                                    <option value="">Year</option>
                                    <option value="2013">2013</option>
                                    <option value="2014">2014</option>
                                    <option value="2015">2015</option>
                                    <option value="2016">2016</option>
                                    <option value="2017">2017</option>
                                    <option value="2018">2018</option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                </select>
                            </li>
                            <script type="text/javascript">
                                $(".expiration select").change(function(){
                                    thisSelect = $(this);
                                    thisSelect.prev().html(thisSelect.val());
                                });
                            </script>
                        </ul>
                    </div>
                </div>
                <div class="submit-area">
                    <input class="account-id" type="hidden" name="account" value="<?php echo $account_id;?>" />
                    <input class="promobtn" type="submit" value="Purchase" />
                    <p class="summary">You will be charged $<?php echo $account_price;?> now<br /> 
                    Your monthly payment will be $<?php echo $account_price;?><br />
                    You may cancel any time.</p>
                </div>
                
                
                
            </form>
            
        </div>
        
        <!-- PROCESSING -->
        <div id="payment-processing-view" class="upgrade-view" style="display:none;">
            <h2>Please wait,</h2>
            <p>While we process your payment.</p>
            <img src="images/icon-unchecked.gif" />
        </div>
        
        <!-- RESULT -->
        <div id="payment-result-view" class="upgrade-view" style="display:none;">
            <!-- content found in subscribe function -->
        </div>
        
        <div class="paypalBtn" style="text-align:center;padding:60px 0;margin-top:40px;border:1px dashed rgba(255,255,255,.2);">
            <!-- $0.01 Test Btn
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="hosted_button_id" value="3KKWSD6AJA9RU">
            <input type="image" src="http://portfoliolounge.com/img/paypal2.jpg" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
            </form>
             -->
            
            
            <!-- 7.99 Pro Btn-->
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="UYSCDAU8HDM78">
<input type="image" src="http://portfoliolounge.com/img/paypal2.jpg" width="330" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>


            <!-- 7.99 Pro Btn 1 CENT
            <h3>Testing Btn</h3>
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="3KKWSD6AJA9RU">
<input type="image" src="http://portfoliolounge.com/img/paypal2.jpg" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
-->


            


        </div>
        <div class="clr"></div>

</div>

<?php } ?>