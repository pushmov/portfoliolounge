<?php

class Html {

	public static function dropdown($options = array(), $name = null, $value = null, $attr = array()) {
		$name = ($name == null) ? '' : $name;
		$attrs = '';
		if (!empty($attr)) {
			foreach ($attr as $k => $v) {
				$attrs .= ' ' . $k . '="' . $v . '"';
			}
		}
		$html = '<select name="' . $name . '"'.$attrs.'>';
		foreach ($options as $val => $label) {
			if ($value == $val) {
				$html .= '<option value="' . $val . '" selected="selected">' . $label . '</option>';
			} else {
				$html .= '<option value="' . $val . '">' . $label . '</option>';
			}
		}
		$html .= '<select>';
		return $html;
	}

	public static function anchor($target = null, $label = '', $attr = array()){
		$html = '<a';
		if ($target === null)
			$html .= ' href="#"';
		else
			$html .= ' href="' . $target . '"';
		if (!empty($attr)) {
			foreach ($attr as $k => $v) {
				$html .= ' ' . $k . '="' . $v . '"';
			}
		}
		$html .= '>';
		$html .= $label;
		$html .= '</a>';
		return $html;
	}

}