<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/country.php';
class Location {

	public static function to_string($arr_location=array()) {
		$result = array();
		if (isset($arr_location['city'])) {
			$result['city'] = $arr_location['city'];
		}
		if (isset($arr_location[2])) {
			$result['city'] = $arr_location[2];
		}
		if (isset($arr_location['prov_state'])) {
			$result['prov_state'] = $arr_location['prov_state'];
		}
		if (isset($arr_location[1])) {
			$result['prov_state'] = $arr_location[1];
		}
		if (isset($arr_location['country_code'])) {
			$result['country'] = \Country::name($arr_location['country_code']);
		}
		if (isset($arr_location[0])) {
			$result['country'] = \Country::name($arr_location[0]);
		}
		return implode(', ', $result);
	}

	public static function to_array($location=null) {
		$result = array();
		list($countryname, $prov_state, $city) = array_map('trim', explode(',', $location));
		if ($countryname != '' && strlen($countryname) == 2) {
			$result['country'] = \Country::name($countryname);
		}
		if ($countryname != '' && strlen($countryname) > 2) {
			$result['country'] = $countryname;
		}
		if ($prov_state != '') {
			$result['prov_state'] = $prov_state;
		}
		if ($city != '') {
			$result['city'] = $city;
		}
		return $result;
	}

}