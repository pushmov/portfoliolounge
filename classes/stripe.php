<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/function/stripe.php';
use \Stripe\Stripe as StripeLib;

class Stripe {

	protected $_public;
	protected $_secret;
	protected $_config;
	protected $_type = array('mastercard' => 'Mastercard', 'visa' => 'Visa', 'discover' => 'Discover',
		'dinersclub' => 'Diners Club', 'jcb' => 'JCB', 'amex' => 'American Express');
	
	public function __construct(){
		global $stripe;
		$this->_secret =& $stripe['secret'];
		$this->_public =& $stripe['public'];
		$this->_config =& $stripe['config'];
		StripeLib::setApiKey($this->_secret);
	}

	public function get_public_key() {
		return $this->_public;
	}

	public function get_secret_key() {
		return $this->_secret;
	}

	public function load_config() {
		return $this->_config;
	}

	public function get_card_type($type=null){
		if($type === null) {
			return $this->_type;
		}
		return (array_key_exists($type, $this->_type)) ? $this->_type[$type] : $this->_type;
	}

	public function get_month(){
		$data = array(
			'01' => '01', '02' => '02', '03' => '03',
			'04' => '04', '05' => '05', '06' => '06',
			'07' => '07', '08' => '08', '09' => '09',
			'10' => '10', '11' => '11', '12' => '12' );
		return $data;
	}

	public function get_year(){
		$data = array();
		$curr_year = date_create()->format('Y');
		for($i = $curr_year;$i<=$curr_year+10 ; $i++) {
			$data[$i] = $i;
		}
		return $data;
	}

	public function charge($data){
		$response = array();
		try {
			$result = \Stripe\Charge::create($data);
			$response = $result;
			$response['status'] = 'OK';
			$response['message'] = 'Payment successfully accepted.';
		} catch(\Stripe\Error\Card $e) {
			$response['status'] = $e->getHttpStatus();
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$response['message'] = $err['message'];
		} catch(\Stripe\Error\RateLimit $e) {
			$body = $e->getJsonBody();
  			$err  = $body['error'];
  			$response['status'] = 'ERR1 - Rate limit error';
			$response['message'] = $err['message'];
		} catch(\Stripe\Error\InvalidRequest $e) {
			$body = $e->getJsonBody();
  			$err  = $body['error'];
  			$response['status'] = 'ERR2 - Invalid request error';
			$response['message'] = $err['message'];
		} catch(\Stripe\Error\Authentication $e) {
			$body = $e->getJsonBody();
  			$err  = $body['error'];
  			$response['status'] = 'ERR3 - Authentication error';
			$response['message'] = $err['message'];
		} catch(\Stripe\Error\ApiConnection $e) {
			$body = $e->getJsonBody();
  			$err  = $body['error'];
  			$response['status'] = 'ERR4 - Connection error';
			$response['message'] = $err['message'];
		} catch(\Stripe\Error\Base $e) {
			$body = $e->getJsonBody();
  			$err  = $body['error'];
  			$response['status'] = 'ERR5 - Base error';
			$response['message'] = $err['message'];
		} catch(Exception $e) {
  			$response['status'] = 'ERR1 - Unknown Error';
			$response['message'] = $e->getMessage();
		}
		return $response;
	}
}