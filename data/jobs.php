<?php

//test data for jobs
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/country.php';
use Cocur\Slugify\Slugify;
$tiny = new \ZackKitzmiller\Tiny('5SX0TEjkR1mLOw8Gvq2VyJxIFhgCAYidrclDWaM3so9bfzZpuUenKtP74QNH6B');
$slugify = new Slugify();
$data = array();

$refid = $tiny->to('27311'.time());
$data[0] = array(
	'job_id' => 1,
	'member_id' => '27313',
	'reference_id' => $refid,
	'title' => 'GRAPHIC DESIGNER',
	'slug' => $slugify->slugify('GRAPHIC DESIGNER '.$refid),
	'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
	'email' => 'rizki.a.aprilian@gmail.com',
	'cell' => '709-234-6601',
	'country' => \Country::code('Canada'),
	'prov_state' => 'Newfoundland and Labrador',
	'city' => 'Gander',
	'postal_code' => 'A1V 2M7',
	'salary' => '50000/yrs',
	'employment_type' => 1,
	'company_name' => 'PT Dwi Cermat Indonesia',
	'remoteable' => 0,
	'years_of_exp' => 6,
	'created_at' => date_create('-2 days')->format('Y-m-d H:i:s'),
	'updated_at' => date_create()->format('Y-m-d H:i:s'),
	'expired_at' => date_create('+30 days')->format('Y-m-d H:i:s'),
	'stripe_charge_id' => 'ch_1AIahxBYx8wR208PqjMWXz65',
	'publish' => 1,
);

$refid = $tiny->to('27312'.time());
$data[1] = array(
	'job_id' => 1,
	'member_id' => '27313',
	'reference_id' => $refid,
	'title' => 'SITE SUPERVISOR INTERIOR',
	'slug' => $slugify->slugify('SITE SUPERVISOR INTERIOR '.$refid),
	'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
	'email' => 'rizki.a.aprilian@gmail.com',
	'cell' => '709-234-6601',
	'country' => \Country::code('Canada'),
	'prov_state' => 'Newfoundland and Labrador',
	'city' => 'Gander',
	'postal_code' => 'A1V 2M7',
	'salary' => '50000/yrs',
	'employment_type' => 1,
	'company_name' => 'DUTA NICHIRINDO PRATAMA, PT',
	'remoteable' => 0,
	'years_of_exp' => 6,
	'created_at' => date_create('-2 days')->format('Y-m-d H:i:s'),
	'updated_at' => date_create()->format('Y-m-d H:i:s'),
	'expired_at' => date_create('+30 days')->format('Y-m-d H:i:s'),
	'stripe_charge_id' => 'ch_1AIahxBYx8wR208PqjMWXz65',
	'publish' => 1,
);

$refid = $tiny->to('27313'.time());
$data[2] = array(
	'job_id' => 1,
	'member_id' => '27313',
	'reference_id' => $refid,
	'title' => 'PRODUCT DEVELOPMENT',
	'slug' => $slugify->slugify('PRODUCT DEVELOPMENT '.$refid),
	'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
	'email' => 'rizki.a.aprilian@gmail.com',
	'cell' => '709-234-6601',
	'country' => \Country::code('Canada'),
	'prov_state' => 'Newfoundland and Labrador',
	'city' => 'Gander',
	'postal_code' => 'A1V 2M7',
	'salary' => '50000/yrs',
	'employment_type' => 1,
	'company_name' => 'PT Dwi Cermat Indonesia',
	'remoteable' => 0,
	'years_of_exp' => 6,
	'created_at' => date_create('-2 days')->format('Y-m-d H:i:s'),
	'updated_at' => date_create()->format('Y-m-d H:i:s'),
	'expired_at' => date_create('+30 days')->format('Y-m-d H:i:s'),
	'stripe_charge_id' => 'ch_1AIahxBYx8wR208PqjMWXz65',
	'publish' => 1,
);

$refid = $tiny->to('27314'.time());
$data[3] = array(
	'job_id' => 1,
	'member_id' => '27313',
	'reference_id' => $refid,
	'title' => 'KOORDINATOR LAPANGAN',
	'slug' => $slugify->slugify('KOORDINATOR LAPANGAN '.$refid),
	'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
	'email' => 'rizki.a.aprilian@gmail.com',
	'cell' => '709-234-6601',
	'country' => \Country::code('Canada'),
	'prov_state' => 'Newfoundland and Labrador',
	'city' => 'Gander',
	'postal_code' => 'A1V 2M7',
	'salary' => '50000/yrs',
	'employment_type' => 1,
	'company_name' => 'SUPER POLY INDUSTRY, PT',
	'remoteable' => 0,
	'years_of_exp' => 6,
	'created_at' => date_create('-2 days')->format('Y-m-d H:i:s'),
	'updated_at' => date_create()->format('Y-m-d H:i:s'),
	'expired_at' => date_create('+30 days')->format('Y-m-d H:i:s'),
	'stripe_charge_id' => 'ch_1AIahxBYx8wR208PqjMWXz65',
	'publish' => 1,
);

$refid = $tiny->to('27315'.time());
$data[4] = array(
	'job_id' => 1,
	'member_id' => '27313',
	'reference_id' => $refid,
	'title' => 'STAFF DESIGN ACCESSORIES BATTERY',
	'slug' => $slugify->slugify('STAFF DESIGN ACCESSORIES BATTERY '.$refid),
	'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
	'email' => 'rizki.a.aprilian@gmail.com',
	'cell' => '709-234-6601',
	'country' => \Country::code('Canada'),
	'prov_state' => 'Newfoundland and Labrador',
	'city' => 'Gander',
	'postal_code' => 'A1V 2M7',
	'salary' => '50000/yrs',
	'employment_type' => 1,
	'company_name' => 'TRIMITRA BATERAI PRAKASA, PT',
	'remoteable' => 0,
	'years_of_exp' => 6,
	'created_at' => date_create('-2 days')->format('Y-m-d H:i:s'),
	'updated_at' => date_create()->format('Y-m-d H:i:s'),
	'expired_at' => date_create('+30 days')->format('Y-m-d H:i:s'),
	'stripe_charge_id' => 'ch_1AIahxBYx8wR208PqjMWXz65',
	'publish' => 1,
);

