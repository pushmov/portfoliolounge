<div class="dialog-header">
	<h2>Confirm</h2>
</div>
<div class="dialog-body">
	<form class="text-center" method="POST" action="/function/jobs.php?action=apply" name="apply_job">
		<input type="hidden" name="ref" value="<?=$data['reference_id']?>">
		<div class="text-left">
			<p>You are about to apply job : "<strong><?=$data['title']?></strong>".</p>
			<p>Are you sure want to continue?</p>
		</div>
		<button type="button" class="btn" id="submit_apply">Apply</button>
		<button type="button" class="btn red btn-close-dialog">Cancel</button>
	</form>
</div>