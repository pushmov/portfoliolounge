<div class="dialog-header">
	<h2>Warning</h2>
</div>
<div class="dialog-body">
	<form class="text-center" method="POST" action="function/adminjobs.php?action=delete" name="delete_job">
		<input type="hidden" name="data[reference_id]" value="<?=$data['reference_id']?>">
		<div class="text-left">
			<p>Are you sure want to delete this record? This action cannot be undone.</p>
			<p>All applicants will be rejected as well.</p>
		</div>
		<button type="button" class="btn red" id="delete_job">Delete</button>
		<button type="button" class="btn btn-close-dialog">Cancel</button>
	</form>
</div>