<div class="dialog-header">
	<h2>Reject</h2>
</div>
<div class="dialog-body">
	<form class="text-center" method="POST" action="/function/adminjobs.php?action=reject" name="apply_job">
		<input type="hidden" name="value" value="<?=$data['value']?>">
		<div class="text-left">
			<p>You are about to reject <strong><em><?=$data['username']?></em></strong> apply.</p>
			<p>Are you sure want to continue?</p>
		</div>
		<button type="button" class="btn red" id="submit_reject">Continue</button>
		<button type="button" class="btn btn-close-dialog">Cancel</button>
	</form>
</div>