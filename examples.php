<?php include 'services/services.php';?>
<div class="center-title-block">
	<h2>Favorite Portfolios, 2014</h2>
	<h3 style="font-size:22px;">
		 These member-created portfolios are judged on overall completion, presentation, and style.
	</h3>
</div>
<div class="inner" style="background:#fff;">
	<!--
	<div id="ipad-mini" class="device">
		<iframe src="http://mistermissus.v.6.portfoliolounge.com/"></iframe>
	</div>
	<iframe id="iphone" src="http://mistermissus.portfoliolounge.com/"></iframe>
	-->
	<div id="imacThumbs">
		<?=getFavorites('top_portfolios')?>
	</div>
	<div class='clr'></div>
	<br>
	<br>
</div>
<script>
		$('.frame').click(function(){
			event.preventDefault();
			var loadUrl = $(this).attr('href');
			$('iframe').attr('src',loadUrl);
		})
	</script>