<?php

/**
* all jobs admin data source function process going here 
*/
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobs.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobs.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobapply.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/transactions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/members.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/html.php';
use \Valitron\Validator;

if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) ||
    strcasecmp($_SERVER['HTTP_X_REQUESTED_WITH'], 'xmlhttprequest') != 0) {
	die('Error: Direct script access not allowed');
}
$response = array();
$allowed_actions = array(
	'my_jobs',
	'applied',
	'delete',
	'reject',
	'owner_trans'
);
if (!isset($_GET['action'])) {
	die('Error: Malformed request');
}
if (!isset($_SESSION)) session_start();
$action = $_GET['action'];
if (!in_array($action, $allowed_actions)) {
	die('Error: No actions defined');
}

if ($action == 'applied') {
	$params = $_GET;
	$job = $jobs->get_by_reference_id($params['ref_id']);
	$params['job_id'] = $job['job_id'];
	$response = $job_apply->dtt_applied($params);
}

if ($action == 'my_jobs') {
	$params = $_GET;
	$params['member_id'] = $_SESSION['SESS_MEMBER_ID'];
	$response = $jobs->dtt_my_jobs($params);
}

if ($action == 'delete') {
	$data = $_POST['data'];
	$validation = new Validator($data);
	$validation->rule('required', array('reference_id'));
	if (!$validation->validate()) {
		$result = array('status' => false, 'error' => $validation->errors());
		echo json_encode($result);
		exit();
	}
	$response = $jobs->delete($data);
}

if ($action == 'reject') {
	$response = $job_apply->reject_apply($_POST['value']);
}

if ($action == 'owner_trans') {
	if (!isset($_SESSION['SESS_MEMBER_ID'])) {
		header('HTTP/1.0 403 Forbidden');
    	die('Access Forbidden');
	}
	$member = $members->load($_SESSION['SESS_MEMBER_ID']);
	if (!is_array($member) || $member['admin'] != \Members::UT_ADMIN) {
		header('HTTP/1.0 403 Forbidden');
    	die('Access Forbidden');
	}
	$response = $transactions->dtt_all($_GET);
}

echo json_encode($response);
exit();