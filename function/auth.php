<?php
	//Start session
	if(!isset($_SESSION)) session_start();
	// Set redirect for oops
	$redirect = $_SERVER['REQUEST_URI'];
	$_SESSION['LOGIN_REDIRECT'] = $redirect;
	//Check whether the session variable SESS_MEMBER_ID is present or not
	if(!isset($_SESSION['SESS_MEMBER_ID']) || trim($_SESSION['SESS_MEMBER_ID']) == '') {
		header("location: /login");
		exit();
	}
?>