<?php

$site_url = $_SERVER['HTTP_HOST'];	
date_default_timezone_set("UTC");

if(strpos($site_url,'portfoliolounge.com') !== false) {

	define('ENV', 'PRODUCTION');
	// Production
	define('DB_HOST', 'localhost');
    define('DB_USER', 'mattvisk_pladmin');
    define('DB_PASSWORD', '9Jzqdq2zfGS4aFrb');
    define('DB_DATABASE', 'mattvisk_portfoliolounge');
	define('SITE_URL', 'portfoliolounge.com');

} elseif (strpos($site_url,'portfoliolounge.dev') !== false || strpos($site_url,'portfoliolounge.co') !== false) {
	define('ENV', 'DEVELOPMENT');
	// Local Environment
	
    if(strpos($site_url, 'portfoliolounge.dev') !== false) {
    	define('SITE_URL', 'portfoliolounge.dev');
    	define('DB_HOST', 'localhost');
	    define('DB_USER', 'root');
	    define('DB_PASSWORD', 'root1');
	    define('DB_DATABASE', 'mattvisk_portfoliolounge');
	    error_reporting(E_ALL);
		ini_set('display_errors', 1);
    } else {
    	define('SITE_URL', 'portfoliolounge.co');
    	define('DB_HOST', 'localhost');
	    define('DB_USER', 'mattvisk_pladmin');
	    define('DB_PASSWORD', '9Jzqdq2zfGS4aFrb');
	    define('DB_DATABASE', 'mattvisk_portfoliolounge');
    }
    	
} else {
	define('ENV', 'PRODUCTION');
	// Production
	define('DB_HOST', 'localhost');
    define('DB_USER', 'mattvisk_pladmin');
    define('DB_PASSWORD', '9Jzqdq2zfGS4aFrb');
    define('DB_DATABASE', 'mattvisk_portfoliolounge');
	define('SITE_URL', 'portfoliolounge.com');
}

if (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$protocol = 'https://';
} else {
	$protocol = 'http://';
}

$db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_DATABASE.';charset=utf8mb4', DB_USER, DB_PASSWORD);
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

?>