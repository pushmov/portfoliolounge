<?php 

// FYI - copy a table in mysql: CREATE TABLE foo SELECT * FROM bar LIMIT 0
require_once('config.php');

session_start();
$memberid = $_SESSION['SESS_MEMBER_ID'];

//$message = $_POST['message'];

$message = $_POST['message'];

/*
// Copy Member to Deleted Table
$db->prepare("INSERT INTO members_cancelled SELECT * FROM members WHERE member_id = :id");
$db->bindValue(':id', $memberid);
$db->execute();

// Delete Member from Member Table
$db->prepare("DELETE FROM members WHERE member_id = :id");
$db->bindValue(':id', $memberid);
$db->execute();
*/

$del = $db->prepare("UPDATE members SET disabled=:disabled, delete_date=:delete_date WHERE member_id=:member_id");
$del->bindValue(':disabled', 1);
$del->bindValue(':delete_date', time());
$del->bindValue(':member_id', $memberid);
$del->execute();

$_SESSION['SESS_MEMBER_ID'] = null;

echo "<div style='font-size:18px;width:300px;margin:0 auto;text-align:center;top:30px;'>Your account has been deleted. Thank you for trying out my project. Please let me know if there's anything I can help with in the future  - Matt Visk, Portfoliolounge.com</div>";

//$_SESSION['SESS_MEMBER_ID'] = '';
$to = "info@portfoliolounge.com";
$subject = "Account Cancelled";
$from = $member['email'];
$headers = "From:" .$member_data['email'];
mail($to,$subject,$message,$headers);

?>