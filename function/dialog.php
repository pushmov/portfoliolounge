<?php

/**
* this function to emulate the ajax dialog
* all function should return in HTML format
*/

if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) ||
    strcasecmp($_SERVER['HTTP_X_REQUESTED_WITH'], 'xmlhttprequest') != 0) {
	die('Error: Direct script access not allowed');
}

require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobs.php';
$allowed_actions = array(
	'confirm_delete_job', 
	'confirm_apply_job',
	'confirm_reject'
);
if (!isset($_GET['action'])) {
	die('Error: Malformed request');
}
if (!isset($_SESSION)) session_start();
$action = $_GET['action'];
if (!in_array($action, $allowed_actions)) {
	die('Error: No actions defined');
}

if ($action == 'confirm_delete_job') {
	$data['reference_id'] = $_GET['ref'];
	include $_SERVER['DOCUMENT_ROOT'] . '/dialog/confirm_delete_job.php';
}

if ($action == 'confirm_apply_job') {
	$db = $jobs->db_instance();
	$getdata = $db->prepare("SELECT * FROM jobs WHERE reference_id = :refid LIMIT 1");
	$getdata->bindValue(':refid', $_GET['ref']);
	$getdata->execute();
	$job = $getdata->fetch();
	if (!is_array($job)) {
		exit();
	}
	$data['reference_id'] = $_GET['ref'];
	$data['title'] = $job['title'];
	include $_SERVER['DOCUMENT_ROOT'] . '/dialog/confirm_apply_job.php';
}

if ($action == 'confirm_reject') {
	$db = $jobs->db_instance();
	$getmembers = $db->prepare("SELECT * FROM job_apply INNER JOIN members ON members.member_id = job_apply.member_id WHERE job_apply.id = :id LIMIT 1");
	$getmembers->bindValue(':id', $_GET['value']);
	$getmembers->execute();
	$data = $getmembers->fetch();
	$data['value'] = $_GET['value'];
	include $_SERVER['DOCUMENT_ROOT'] . '/dialog/confirm_reject.php';
}
exit();