<?php

$a = $_GET["a"];

//Include database connection details
require_once('config.php');


$member_id = isset($_POST['member_id']) ? $_POST['member_id'] : '';

if($a=="username"){
	$username = htmlspecialchars(trim($_POST['username']));
	$username = strtolower($username);
	$username = str_replace(' ', '', $username);
	//Check for duplicate username ID
	if($username != '') {
		$result = $db->prepare("SELECT * FROM members WHERE username=:u");
		$result->bindValue(':u', $username);
		$executed = $result->execute();
		if($executed) {
			$user = $result->fetchAll();
			if(count($result) > 0) {
				//$errmsg_arr[] = 'Username is taken, try a different one!';
				//$errflag = true;
				echo "Username is taken, try a different one!";
			} else {
				$addClient = $db->prepare("UPDATE members SET username=:u WHERE member_id=:memberid");
				$addClient->bindValue(':u', $username);
				$addClient->bindValue(':memberid', $member_id);
				$addClient->execute();
				// Set new session username
				session_start();
				$_SESSION['SESS_USER_NAME'] = $username;	
			}
		} else {
			die("Query failed 1");
		}
	}
}
else if($a=="name"){
	$first_name = trim($_POST['first_name']);
	$last_name = trim($_POST['last_name']);
	$first_name = addslashes($first_name);
	$last_name = addslashes($last_name);
	$addClient = $db->prepare("UPDATE members SET first_name=:fname, last_name=:lname WHERE member_id=:memberid");
	$addClient->bindValue(':fname', $first_name);
	$addClient->bindValue(':lname', $last_name);
	$addClient->bindValue(':memberid', $member_id);
	$addClient->execute();
}
else if($a=="help"){	
	$email = trim($_POST['email']);
	$get_member = $db->prepare("SELECT username FROM members WHERE email = :email LIMIT 1");
	$get_member->bindValue(':email', $email);
	$get_member->execute();
	$member = $get_member->fetch();
	
	$updatepass = $db->prepare("UPDATE members SET password=:pass WHERE email=:email");
	$updatepass->bindValue(':pass', md5('Summer21'));
	$updatepass->bindValue(':email', $email);
	$updatepass->execute();
	
	// Email Administrators
	$message = "Hey There! \n\nYour username is: ".$member['username']." \nWe reset your password to: Summer21 \n\nYou can change your password to something better, here: www.portfoliolounge.com/admin/account/profile \n\nI hope this helps! \nPortfolioLounge Team";
	$to = $email;
	$subject = "Login Helper";
	$from = "info@portfoliolounge.com";
	$headers = "From: info@portfoliolounge.com";
	mail($to,$subject,$message,$headers);
}
else if($a=="phone"){
	$phone = trim($_POST['phone']);
	$phone = addslashes($phone);
	$addClient = $db->prepare("UPDATE members SET phone=:p WHERE member_id=:id");
	$addClient->bindValue(':p', $phone);
	$addClient->bindValue(':id', $member_id);
	$addClient->execute();
}
else if($a=="location"){
	$location = trim($_POST['location']);
	$location = addslashes($location);
	$addClient = $db->prepare("UPDATE members SET location=:loc WHERE member_id=:id");
	$addClient->bindValue(':loc', $location);
	$addClient->bindValue(':id', $member_id);
	$addClient->execute();
}
else if($a=="school"){
	$school = trim($_POST['school']);
	$school = addslashes($school);
	$addClient = $db->prepare("UPDATE members SET school=:sch WHERE member_id=:id");
	$addClient->bindValue(':sch', $school);
	$addClient->bindValue(':id', $member_id);
	$addClient->execute();
}
else if($a=="about"){
	$about = trim($_POST['about']);
	$about = addslashes($about);
	$about = nl2br($about);
	$addClient = $db->prepare("UPDATE members SET about=:about WHERE member_id=:id");
	$addClient->bindValue(':about', $about);
	$addClient->bindValue(':id', $member_id);
	$addClient->execute();
}
else if($a=="site_title"){
	$site_title = trim($_POST['site_title']);
	$site_title = addslashes($site_title);
	$addClient = $db->prepare("UPDATE members SET site_title=:site WHERE member_id=:id");
	$addClient->bindValue(':site', $site_title);
	$addClient->bindValue(':id', $member_id);
	$addClient->execute();
}
else if($a=="tagline"){
	$tagline = trim($_POST['tagline']);
	$tagline = addslashes($tagline);
	$addClient = $db->prepare("UPDATE members SET tagline = :tagline WHERE member_id = :id");
	$addClient->bindValue(':tagline', $tagline);
	$addClient->bindValue(':id', $member_id);
	$executed = $addClient->execute();
}
else if($a=="add_link"){
	session_start();
	$memberid = $_SESSION['SESS_MEMBER_ID'];
	$fixed_link = trim($_POST['fixed_link']);
	if($fixed_link == 'other'){
		$link_title = trim($_POST['link_title']);
	} else {
		$link_title = $fixed_link;
	}
	$link_url = trim($_POST['link_url']);
	$addClient = $db->prepare("INSERT INTO member_links(member_id, link_url, link_title)
		VALUES(:id, :link_url, :link_title)");
	$addClient->bindValue(':id', $memberid);
	$addClient->bindValue(':link_url', $link_url);
	$addClient->bindValue(':link_title', $link_title);
	$addClient->execute();
}
else if($a=="delete_link"){	
	session_start();
	$memberid = $_SESSION['SESS_MEMBER_ID'];
	$b = $_GET["b"];
	$addClient = $db->prepare("DELETE FROM member_links WHERE link_id=:linkid AND member_id=:id");
	$addClient->bindValue(':linkid', $b);
	$addClient->bindValue(':id', $memberid);
	$addClient->execute();
}
else if($a=="email"){
	$email = trim($_POST['email']);
	$addClient = $db->prepare("UPDATE members SET email=:email WHERE member_id=:id");
	$addClient->bindValue(':email', $email);
	$addClient->bindValue(':id', $member_id);
	$addClient->execute();
}
else if($a=="skills"){
	$addClient = $db->prepare("DELETE FROM member_skills WHERE member_id=:id");
	$addClient->bindValue(':id', $member_id);
	$addClient->execute();
	if(!empty($_POST['category'])) {
		foreach($_POST['category'] as $category_id) {
			$addClient = $db->prepare("INSERT INTO member_skills(member_id, category_id) VALUES (:id, :catid)");
			$addClient->bindValue(':id', $member_id);
			$addClient->bindValue(':catid', $category_id);
			$addClient->execute();
		}
	}
}
else if ($a=="tracking") {
	$tracking_id = $_POST['tracking-id'];
	$member_id = $_POST['get-member-id'];
	$addClient = $db->prepare("UPDATE members SET tracking_id=:tid WHERE member_id=:id");
	$addClient->bindValue(':tid', $tracking_id);
	$addClient->bindValue(':id', $member_id);
	$addClient->execute();
}
else if ($a=="custom_domain") {
	$domain = $_POST['custom-domain'];
	$domain = strtolower($domain);
	$domain = str_replace(' ', '', $domain);
	$member_id = $_POST['get-member-id'];
	//Check for duplicate username ID
	if($domain != '') {
		$result = $db->prepare("SELECT * FROM members WHERE custom_domain=:cdomain");
		$result->bindValue(':cdomain', $domain);
		$executed = $result->execute();
		if($executed) {
			$result_all = $result->fetchAll();
			if(count($result_all) > 0) {
				die("Already taken.");
			} else {
				$addClient = $db->prepare("UPDATE members SET custom_domain=:domain WHERE member_id=:memberid");
				$addClient->bindValue(':domain', $domain);
				$addClient->bindValue(':memberid', $member_id);
				$addClient->execute();
			}
		} else {
			die("Query failed 1");
		}
	}
}
else if($a=="delete_logo_img"){
	$member_id = $_GET['member_id'];
	$get_member = $db->prepare("SELECT logo_img FROM members WHERE member_id=:id LIMIT 1");
	$get_member->bindValue(':id', $member_id);
	$get_member->execute();
	$member = $get_member->fetch();
	$img = "../uploads/".$member_id."/".$member['logo_img'];
	if (file_exists($img)) {
		unlink($img);
	}
	$thumb = "../uploads/".$member_id."/thumbnail/".$member['logo_img'];
	if (file_exists($thumb)) {
		unlink($thumb);
	}
	
	$addClient = $db->prepare("UPDATE members SET logo_img=:img WHERE member_id=:id");
	$addClient->bindValue(':id', $member_id);
	$addClient->bindValue(':img', '');
	$addClient->execute();
}

else if($a=="delete_profile_img"){
	$member_id = $_GET['member_id'];
	$get_member = $db->prepare("SELECT profile_img FROM members WHERE member_id=:id LIMIT 1");
	$get_member->bindValue(':id', $member_id);
	$get_member->execute();
	$member = $get_member->fetch();
	$img = "../uploads/".$member_id."/".$member['profile_img'];
	if (file_exists($img)) {
		unlink($img);
	}
	$thumb = "../uploads/".$member_id."/thumbnail/".$member['profile_img'];
	if (file_exists($thumb)) {
		unlink($thumb);
	}
	$addClient = $db->prepare("UPDATE members SET profile_img=:pimg WHERE member_id=:id");
	$addClient->bindValue(':pimg', '');
	$addClient->bindValue(':id', $member_id);
	$addClient->execute();
}
else if($a=="password"){
	session_start();
	$memberid = $_SESSION['SESS_MEMBER_ID'];
	$password = $_POST['password'];
	$cpassword = $_POST['cpassword'];
	if($password==$cpassword){
		$updatePassword = $db->prepare("UPDATE members SET password=:pass WHERE member_id=:id");
		$updatePassword->bindValue(':pass', md5($password));
		$updatePassword->bindValue(':id', $memberid);
		$updatePassword->execute();
	} else {
		echo "Passwords do not match";	
	}
}
else if($a=="password"){	
	session_start();
	$memberid = $_SESSION['SESS_MEMBER_ID'];
	$password = $_POST['password'];
	$cpassword = $_POST['cpassword'];
	if($password==$cpassword){
		$updatePassword = $db->prepare("UPDATE members SET password=:pass WHERE member_id=:id");
		$updatePassword->bindValue(':pass', md5($password));
		$updatePassword->bindValue(':id', $memberid);
		$updatePassword->execute();
	} else {
		echo "Passwords do not match";	
	}
}
else if($a == 'te_display') {
	$getmember = $db->prepare("SELECT * FROM members WHERE member_id = :id LIMIT 1");
	$getmember->bindValue(':id', trim($_POST['member_id']));
	$getmember->execute();
	$member = $getmember->fetch();

	if($member['account_id'] > 1) {
		$update = $db->prepare("UPDATE members SET te_editor = :editor WHERE member_id = :id");
		$update->bindValue(':editor', intval(trim($_POST['te_display'])));
		$update->bindValue(':id', $member['member_id']);
		$update->execute();
	}
	exit();
}
else if($a=="facebook" || "pinterest" || "instagram" || "twitter" || "google-plus" || "youtube" || "vimeo" || "dribble" || "vine"){	
	$link_url = trim($_POST['link_url']);
	$link_url = strtolower($link_url);	
	$link_slug = $a;
	$get_link_id = $db->prepare("SELECT link_id FROM links WHERE link_slug=:slug LIMIT 1");
	$get_link_id->bindValue(':slug', $link_slug);
	$get_link_id->execute();
	$link_id = $get_link_id->fetch();
	$link_id = $link_id['link_id'];
	$addClient = $db->prepare("DELETE FROM member_links WHERE member_id=:id AND link_id=:link_id");
	$addClient->bindValue(':id', $member_id);
	$addClient->bindValue(':link_id', $link_id);
	$addClient->execute();
	$addClient = $db->prepare("INSERT INTO member_links(member_id, link_url, link_id) 
		VALUES (:id, :url, :linkid)");
	$addClient->bindValue(':id', $member_id);
	$addClient->bindValue(':url', $link_url);
	$addClient->bindValue(':linkid', $link_id);
	$addClient->execute();
}

?>