<?php 
session_start();
require_once('config.php');
$member_id = $_POST['member_id'];
$a = $_GET["a"];
$i = $_GET["i"];
$x = $_GET["x"];
if($a=="delete"){
	// Post
	$item_id = $_POST['item_id'];
	$project_id = $_POST['project_id'];
	// Get Filename
	$get_items = $db->prepare("SELECT item_filename FROM items WHERE item_id = :itemid LIMIT 1");
	$get_items->bindValue(':itemid', $item_id);
	$get_items->execute();
	$item = $get_items->fetch();
	// Item Count -1
	$db->query("UPDATE members SET item_count = item_count-1 WHERE member_id = ".$_SESSION['SESS_MEMBER_ID']);
	// Delete Files
	$delete_file = "../uploads/".$member_id."/".$item['item_filename'];
	if (file_exists($delete_file)) {
		unlink($delete_file);	
	}
	$delete_file = "../uploads/".$member_id."/large/".$item['item_filename'];
	if (file_exists($delete_file)) {
		unlink($delete_file);	
	}
	$delete_file = "../uploads/".$member_id."/medium/".$item['item_filename'];
	if (file_exists($delete_file)) {
		unlink($delete_file);	
	}
	$delete_file = "../uploads/".$member_id."/small/".$item['item_filename'];
	if (file_exists($delete_file)) {
		unlink($delete_file);	
	}
	$delete_file = "../uploads/".$member_id."/thumbnail/".$item['item_filename'];
	if (file_exists($delete_file)) {
		unlink($delete_file);	
	}
	// Delete Database Entry
	$delete = $db->prepare("DELETE FROM items WHERE item_id = :itemid");
	$delete->bindValue(':itemid', $item_id);
	$delete->execute();
	// Redirect back to Project
	header("location: /admin/project/$project_id");
}
if($a=="add-video"){
	$video_title = htmlspecialchars(trim($_POST['video_title']));
	$video_description = htmlspecialchars(trim($_POST['video_description']));
	$video_type = htmlspecialchars(trim($_POST['video_type']));
	$video_id = htmlspecialchars(trim($_POST['video_id']));
	$project_id = htmlspecialchars(trim($_POST['project_id']));
	$memberid = htmlspecialchars(trim($_POST['member_id']));
	$addClient = $db->prepare("INSERT INTO items(project_id, member_id, item_type, item_title, item_desc, video_id, item_time)
		VALUES (:pid, :memberid, :video, :video_title, :video_description, :video_id, :time)");
	$addClient->bindValue(':pid', $project_id);
	$addClient->bindValue(':memberid', $memberid);
	$addClient->bindValue(':video', 'video');
	$addClient->bindValue(':video_title', $video_title);
	$addClient->bindValue(':video_description', $video_description);
	$addClient->bindValue(':video_id', $video_id);
	$addClient->bindValue(':time', time());
	$addClient->execute();
	header("location: /admin/project/$project_id");
}
if($a=="edit-video"){	
	$item_id = $_POST['item_id'];
	$title = htmlspecialchars(trim($_POST["title"]),ENT_QUOTES);
	$description = htmlspecialchars(trim($_POST["desc"]),ENT_QUOTES);
	$project_id = htmlspecialchars(trim($_POST["project_id"]));
	$addClient = $db->prepare("UPDATE items SET item_title=:title, item_desc=:description WHERE item_id=:item_id");
	$addClient->bindValue(':title', $title);
	$addClient->bindValue(':description', $description);
	$addClient->bindValue(':item_id', $item_id);
	$addClient->execute();
	header("location: /admin/project/$project_id");
	echo json_encode("");
}
if($a=="item-info"){	
	$item_id = $_POST['item_id'];
	$title = htmlspecialchars(trim($_POST['title']));
	$video_type = htmlspecialchars(trim($_POST['video_type']));
	$video_id = htmlspecialchars(trim($_POST['video_id']));
	$description = htmlspecialchars(trim($_POST['description']));
	$title = addslashes($title);
	$description = addslashes($description);
	$price = $_POST['price'];
	$addClient = $db->prepare("UPDATE items SET item_title=:title, item_desc=:description, 
		item_price=:price, video_id=:video_id, video_type=:video_type 
		WHERE item_id=:item_id");
	$addClient->bindValue(':title', $title);
	$addClient->bindValue(':description', $description);
	$addClient->bindValue(':price', $price);
	$addClient->bindValue(':video_id', $video_id);
	$addClient->bindValue(':video_type', $video_type);
	$addClient->bindValue(':item_id', $item_id);
	$addClient->execute();
	echo json_encode("");
}
if($a=="order"){
	$data = json_decode(stripslashes($_POST['data']));
	$i = 0;
	foreach($data as $item_id){
		$addClient = $db->prepare("UPDATE items SET item_order=:i WHERE item_id=:item_id");
		$addClient->bindValue(':i', $i);
		$addClient->bindValue(':item_id', $item_id);
		$addClient->execute();		
		$i++;
	}
}
?>