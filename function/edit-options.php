<?php
$a = $_GET["a"];
$b = $_GET["b"];
$c = $_GET["c"];
require_once('config.php');
session_start();
$memberid = $_SESSION['SESS_MEMBER_ID'];
if($a=="scheme"){
	$addClient = $db->prepare("UPDATE members SET color_scheme_id=:color WHERE member_id=:member_id");
	$addClient->bindValue(':color', $b);
	$addClient->bindValue(':member_id', $memberid);
	$addClient->execute();
}
else if ($a=="layout") {	
	$addClient = $db->prepare("UPDATE members SET layout_option=:layout WHERE member_id=:member_id");
	$addClient->bindValue(':layout', $b);
	$addClient->bindValue(':member_id', $memberid);
	$addClient->execute();
}
else if ($a=="template") {
	$addClient = $db->prepare("UPDATE members SET template_id=:template WHERE member_id=:memberid");
	$addClient->bindValue(':template', $b);
	$addClient->bindValue(':memberid', $memberid);
	$addClient->execute();
}
else if ($a=="title") {
	$addClient = $db->prepare("UPDATE members SET title_option=:title WHERE member_id=:member_id");
	$addClient->bindValue(':title', $b);
	$addClient->bindValue(':member_id', $memberid);
	$addClient->execute();
}
else if ($a=="font") {
	$addClient = $db->prepare("UPDATE members SET font_id=:font WHERE member_id=:member_id");
	$addClient->bindValue(':font', $b);
	$addClient->bindValue(':member_id', $memberid);
	$addClient->execute();
}