<?php
$a = $_GET["a"];
$cover_id = $_GET["cover_id"];
$project_id = $_GET["project_id"];
$e = $_GET["e"];
$page_view = $_GET["page_view"];
// Connect to DB
require_once('config.php');
session_start();
$member_id = $_SESSION['SESS_MEMBER_ID'];
if($a=="add"){
	// Get Title
	$project_title = addslashes(trim($_POST['project_title']));
	$project_desc = addslashes(trim($_POST['project_desc']));
	$project_type = isset($_POST['project_type']) ? addslashes(trim($_POST['project_type'])) : '';
	$category_id = trim($_POST['category_id']);
	// Create Slug		
	function to_slug($string){
		return str_replace('---','-', strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string))));
	}
	$project_slug = to_slug($project_title);
	$project_slug = trim($project_slug,'-');
	// If Slug does not work - use something else (for other languages usually)
	if(!$project_slug){
		$project_slug = 'project-';	
		$digits = 7;
		$project_slug .= rand(pow(10, $digits-1), pow(10, $digits)-1);
	}
	// Add Project
	$addClient = $db->prepare("INSERT INTO projects
		(member_id, project_title, project_slug, project_order, project_desc, project_type, project_time, category_id) 
		VALUES (:member_id, :title, :slug, :porder, :description, :type, :time, :catid) ");
	$addClient->bindValue(':member_id', $member_id);
	$addClient->bindValue(':title', $project_title);
	$addClient->bindValue(':slug', $project_slug);
	$addClient->bindValue(':porder', 9999);
	$addClient->bindValue(':description', $project_desc);
	$addClient->bindValue(':type', $project_type);
	$addClient->bindValue(':time', time());
	$addClient->bindValue(':catid', $category_id);
	$addClient->execute();
	$backup_slug = $db->lastInsertId();
	//Get this project's ID
	$get_projects = $db->prepare("SELECT * FROM projects WHERE member_id=:memberid ORDER BY project_id DESC LIMIT 1");
	$get_projects->bindValue(':memberid', $member_id);
	$get_projects->execute();
	$project = $get_projects->fetch();
	// Project Count +1
	$db->query("UPDATE members SET project_count = project_count+1 WHERE member_id = $member_id");
	// Redirect
	header("location: /admin/project/".$project['project_id']);
} else if($a=="make-private-project"){
	$addClient = $db->prepare("UPDATE projects SET privacy_password=:pass, privacy_option=:opt WHERE project_id=:pid AND member_id=:member_id");
	$addClient->bindValue(':pass', md5($_POST['password2']));
	$addClient->bindValue(':opt', '1');
	$addClient->bindValue(':pid', $project_id);
	$addClient->bindValue(':member_id', $member_id);
	$addClient->execute();
	// Redirect
	header("location: /admin/project/".$project_id);	
} else if($a=="make-project-public"){
	$addClient = $db->prepare("UPDATE projects SET privacy_password=:pass, privacy_option=:opt WHERE
		project_id=:pid AND member_id=:member_id");
	$addClient->bindValue(':pass', '');
	$addClient->bindValue(':opt', '0');
	$addClient->bindValue(':pid', $project_id);
	$addClient->bindValue(':member_id', $member_id);
	$addClient->execute();
	// Redirect
	header("location: /admin/project/".$project_id);
} else if ($a=="title") {
	$title = addslashes(trim($_POST['title']));
	$description = addslashes(trim($_POST['description']));
	$website = addslashes(trim($_POST['website']));
	$project_id = trim($_POST['project_id']);
	$category_id = trim($_POST['category_id']);
	$website = str_replace("https://", "", $website);
	$website = str_replace("http://", "", $website);
	// Create Slug		
	function to_slug($string){
		return str_replace('---','-', strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string))));
	}
	$project_slug = to_slug($title);
	$project_slug = trim($project_slug,'-');
	$addClient = $db->prepare("UPDATE projects SET project_slug=:slug, project_title=:title,
		 project_desc=:description, project_website=:website, category_id=:catid
		 WHERE project_id=:pid AND member_id=:member_id");
	$addClient->bindValue(':slug', $project_slug);
	$addClient->bindValue(':title', $title);
	$addClient->bindValue(':description', $description);
	$addClient->bindValue(':website', $website);
	$addClient->bindValue(':catid', $category_id);
	$addClient->bindValue(':pid', $project_id);
	$addClient->bindValue(':member_id', $member_id);
	$addClient->execute();
	echo json_encode("");
} else if ($a=="cover"){
	$addClient = $db->prepare("UPDATE projects SET cover_id=:cover WHERE project_id=:pid");
	$addClient->bindValue(':cover', $cover_id);
	$addClient->bindValue(':pid', $project_id);
	$addClient->execute();
	// Redirect
	if($page_view){
		header("location: /admin/$page_view/$e");
	} else {
		header("location: /admin");
	}
} else if($a=="delete"){
	// Get Project
	$project_id = htmlspecialchars(trim($_POST['project_id']));
	// Delete Project 
	$addClient = $db->prepare("DELETE FROM projects WHERE project_id=:pid");
	$addClient->bindValue(':pid', $project_id);
	$addClient->execute();
	// Project Count -1
	$db->query("UPDATE members SET project_count = project_count-1 WHERE member_id = ".$_SESSION['SESS_MEMBER_ID']);
	// Get Items in Project
	$get_items = $db->prepare("SELECT * FROM items WHERE project_id = :pid");
	$get_items->bindValue(':pid', $project_id);
	$get_items->execute();
	$all_items = $get_items->fetchAll();
	foreach($all_items as $item){
		// Delete Items and Files
		$addClient = $db->prepare("DELETE FROM items WHERE item_id=:itemid");
		$addClient->bindValue(':itemid', $item['item_id']);
		$addClient->execute();
		// Item Count -1
		$db->query("UPDATE members SET item_count = item_count-1 WHERE member_id = ".$_SESSION['SESS_MEMBER_ID']);
		// Delete Files
		$delete_file = "../uploads/".$member_id."/".$item['item_filename'];
		if (file_exists($delete_file)) {
			unlink($delete_file);	
		}
		$delete_file = "../uploads/".$member_id."/large/".$item['item_filename'];
		if (file_exists($delete_file)) {
			unlink($delete_file);	
		}
		$delete_file = "../uploads/".$member_id."/medium/".$item['item_filename'];
		if (file_exists($delete_file)) {
			unlink($delete_file);	
		}
		$delete_file = "../uploads/".$member_id."/small/".$item['item_filename'];
		if (file_exists($delete_file)) {
			unlink($delete_file);	
		}
		$delete_file = "../uploads/".$member_id."/thumbnail/".$item['item_filename'];
		if (file_exists($delete_file)) {
			unlink($delete_file);	
		}
	}
	// Redirect
	header("location: /admin");
} else if($a=="order"){
	$data = json_decode(stripslashes($_POST['data']));
	$i = 0;
	foreach($data as $project_id){
		$addClient = $db->prepare("UPDATE projects SET project_order=:porder WHERE project_id=:pid");
		$addClient->bindValue(':porder', $i);
		$addClient->bindValue(':pid', $project_id);
		$addClient->execute();
		$i++;
	}
}
?>