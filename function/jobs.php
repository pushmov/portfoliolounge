<?php

/**
* jobs function process going here 
* act like controller to pass data to model
* credit to https://github.com/vlucas/valitron for the validation library
*/

if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) ||
    strcasecmp($_SERVER['HTTP_X_REQUESTED_WITH'], 'xmlhttprequest') != 0) {
	die('Error: Direct script access not allowed');
}

require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobs.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobcategory.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/transactions.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/payment.php';

use \Valitron\Validator;

$allowed_actions = array(
	'add', 'modify', 'apply', 'payment_dialog', 'payment',
	'payment_success', 'check_username', 'check_email', 'check_email_other', 'more'
);
if (!isset($_GET['action'])) {
	die('Error: Malformed request');
}
if (!isset($_SESSION)) session_start();
$action = $_GET['action'];
if (!in_array($action, $allowed_actions)) {
	die('Error: No actions defined');
}

if ($action == 'check_username') {
	$db = $jobs->db_instance();
	$user = $db->prepare("SELECT COUNT(member_id) AS total FROM members WHERE username = :u AND username <> '' LIMIT 1");
	$user->bindValue(':u', $_GET['u']);
	$user->execute();
	$countrow = $user->fetch();
	if($countrow['total'] > 0) {
		$response = array('status' => 'ERROR', 'message' => 'Username \''.$_GET['u'].'\' already exist. Please try another one');
	} else {
		$response = array('status' => 'OK');
	}
	echo json_encode($response);
	exit();
}

if ($action == 'check_email') {
	$db = $jobs->db_instance();
	$user = $db->prepare("SELECT COUNT(member_id) AS total FROM members WHERE email = :e AND email <> '' LIMIT 1");
	$user->bindValue(':e', $_GET['e']);
	$user->execute();
	$countrow = $user->fetch();
	if($countrow['total'] > 0) {
		$response = array('status' => 'ERROR', 'message' => 'Email \''.$_GET['e'].'\' already exist. Please try another one');
	} else {
		$response = array('status' => 'OK');
	}
	echo json_encode($response);
	exit();
}

if ($action == 'check_email_other') {
	$db = $jobs->db_instance();
	$getjob = $db->prepare("SELECT email FROM jobs WHERE reference_id = :r LIMIT 1");
	$getjob->bindValue(':r', $_GET['refid']);
	$getjob->execute();
	$job = $getjob->fetch();

	$user = $db->prepare("SELECT COUNT(member_id) AS total FROM members WHERE email = :e AND email <> '' AND email <> :j LIMIT 1");
	$user->bindValue(':e', $_GET['e']);
	$user->bindValue(':j', $job['email']);
	$user->execute();
	$countrow = $user->fetch();
	if($countrow['total'] > 0) {
		$response = array('status' => 'ERROR', 'message' => 'Email \''.$_GET['e'].'\' already exist. Please try another one');
	} else {
		$response = array('status' => 'OK');
	}
	echo json_encode($response);
	exit();
}

if ($action == 'add') {
	//new member
	if(isset($_POST['register'])) {
		$validation = new Validator($_POST['register']);
		$validation->rule('required', array('email', 'username', 'password'));
		$validation->rule('email', array('email'));
		$validation->rule('equals', 'cpassword', 'password')->message('Confirm password must be the same as \'Password\'');
		if (!$validation->validate()) {
			$result = array('status' => 'ERROR', 'error' => $validation->errors());
			echo json_encode($result);
			exit();
		} else {
			$_SESSION['register'] = $_POST['register'];
		}
	}
	$validation = new Validator($_POST['data']);
	$data = $_POST['data'];
	$data['member_id'] = isset($_SESSION['SESS_MEMBER_ID']) ? $_SESSION['SESS_MEMBER_ID'] : '';
	$validation->rule('required', array('title', 'description', 'email', 'country', 'company_name'));
	$validation->rule('email', array('email'));
	//$validation->rule('accepted', array('remoteable'));
	$validation->rule('integer', array('years_of_exp', 'portfolio_items', 'employment_type'));
	if ($validation->validate()) {
		//$jobs->insert($data);
		$_SESSION['jobs'] = $data;
		$result = array('status' => 'OK');
	} else {
		$result = array('status' => 'ERROR', 'error' => $validation->errors());
	}
	echo json_encode($result);
	exit();
}

if ($action == 'modify') {
	if (!isset($_SESSION)) session_start();
	$validation = new Validator($_POST['data']);
	$data = $_POST['data'];
	$data['member_id'] = isset($_SESSION['SESS_MEMBER_ID']) ? $_SESSION['SESS_MEMBER_ID'] : '';
	$validation->rule('required', array('title', 'description', 'email', 'country', 'company_name'));
	$validation->rule('email', array('email'));
	$validation->rule('integer', array('years_of_exp', 'portfolio_items', 'employment_type'));
	if ($validation->validate()) {
		$jobs->update($_POST['data']);
		$result = array('status' => 'OK');
		$categories = $_POST['data']['categories'];
		if (count($categories) > 0) {
			$db = $jobs->db_instance();
			$jobrow = $db->prepare("SELECT * FROM jobs WHERE reference_id = :refid LIMIT 1");
			$jobrow->bindValue(':refid', $data['reference_id']);
			$jobrow->execute();
			$job = $jobrow->fetch();
			$job_category->clean_categories($job['job_id']);
			foreach ($categories as $cat) {
				$job_category->insert_categories(array('jid' => $job['job_id'], 'cid' => $cat));
			}
		}
		$result = array('status' => 'OK');
	} else {
		$result = array('status' => 'ERROR', 'error' => $validation->errors());
	}
	echo json_encode($result);
	exit();
}

if ($action == 'apply') {
	if (!isset($_SESSION)) session_start();

	if (!isset($_SESSION['SESS_MEMBER_ID']) || $_SESSION['SESS_MEMBER_ID'] == '') {
		$response = array('status' => 'login');
		echo json_encode($response);
		exit();
	}

	$db = $jobs->db_instance();
	$getjob = $db->prepare("SELECT * FROM jobs WHERE reference_id = :refid");
	$getjob->bindValue(':refid', trim($_POST['ref']));
	$getjob->execute();
	$job = $getjob->fetch();

	$insert = $db->prepare("INSERT INTO job_apply (jobs_id, member_id, date_applied) VALUES(:jobid, :memid, :date_applied)");
	$insert->bindValue(':jobid', $job['job_id']);
	$insert->bindValue(':memid', $_SESSION['SESS_MEMBER_ID']);
	$insert->bindValue(':date_applied', date_create()->format('Y-m-d H:i:s'));
	$insert->execute();

	echo json_encode(array('status' => 'OK'));
	exit();
	
}

if ($action == 'more') {
	$data = $jobs->load_more(intval($_GET['seq']), intval($_GET['catid']));
	if (ENV == 'DEVELOPMENT') {
		if(!isset($_SESSION)) session_start();
		$memberid = (isset($_SESSION['SESS_MEMBER_ID'])) ? $_SESSION['SESS_MEMBER_ID'] : null;
	}
	$seq = $_GET['seq'] + 1;
	if (!empty($data)) {
		$showing = ($seq - 1) * \Jobs::PER_SEGMENT;
		if($showing > $jobs->total()) {
			$showing = $jobs->total();
		}
	}

	include $_SERVER['DOCUMENT_ROOT'] . '/jobs_more.php';
}

if ($action == 'payment_dialog') {
	$config = $payment->load_config();
	include $_SERVER['DOCUMENT_ROOT'] . '/jobs_form_payment.php';
}

if ($action == 'payment_success') {
	include $_SERVER['DOCUMENT_ROOT'] . '/jobs_payment_success.php';
}

if ($action == 'payment') {
	if (!isset($_SESSION)) session_start();
	$card_types = $payment->get_card_type();
	$validation = new Validator($_POST['card']);
	//$validation->rule('creditCard', 'number', $_POST['card']['type'],array_keys($card_types));
	$validation->rule('required', array('name', 'number', 'cvv'));
	$validation->rule('length', array('cvv'), 3);
	$validation->rule('length', array('exp_month'), 2);
	$validation->rule('length', array('exp_year'), 4);
	if($validation->validate()) {
		//process payment to stripe.com
		$card = $_POST['card'];
		$stripe_config = $payment->load_config();
		if(isset($_SESSION['SESS_MEMBER_ID']) && $_SESSION['SESS_MEMBER_ID'] != '') {
			$memberq = $db->prepare("SELECT * FROM members WHERE member_id = :id LIMIT 1");
			$memberq->bindValue(':id', $_SESSION['SESS_MEMBER_ID']);
			$memberq->execute();
			$member = $memberq->fetch();
			$receipt_email = $member['email'];
		} else {
			$receipt_email = $_SESSION['register']['email'];
		}
		$data = array(
			'amount' => $stripe_config['post_job_fee'] * 100,
			'currency' => $stripe_config['currency'],
			'description' => 'Charge for post a job at portfoliolounge.com from : '.$receipt_email. ' (ref: '.$_SESSION['jobs']['reference_id'].')',
			'receipt_email' => $receipt_email,
			'source' => array(
				'exp_month' => $card['exp_month'],
				'exp_year' => $card['exp_year'],
				'number' => $card['number'],
				'object' => 'card',
				'cvc' => $card['cvv'],
				'name' => $card['name']
			)
		);
		$response = $payment->charge($data);
		if ($response['status'] == \Payment::STATUS_OK) {
			//payment completed. process to next step. save jobs data to database,
			//register new member if data.register provided
			if(isset($_SESSION['register']) && is_array($_SESSION['register'])) {
				$register = $_SESSION['register'];
				$result = $db->prepare("INSERT INTO members (email,username,password,time, background, delete_date, disabled, under_constructionj) 
					VALUES(:email,:username,:pass,:time, :background, :delete_date,:disabled,:under_constructionj)");
				$result->bindValue(':email', $register['email']);
				$result->bindValue(':username', $register['username']);
				$result->bindValue(':pass', md5($register['password']));
				$result->bindValue(':time', time());
				$result->bindValue(':background', '');
				$result->bindValue(':delete_date', 0);
				$result->bindValue(':disabled', 0);
				$result->bindValue(':under_constructionj', 0);
				$exec = $result->execute();
				$member_id = $db->lastInsertId();
				unset($_SESSION['register']);
			} else {
				$member_id = $_SESSION['SESS_MEMBER_ID'];
			}

			$jobs_data = $_SESSION['jobs'];
			$jobs_data['member_id'] = $member_id;
			$job_id = $jobs->insert($jobs_data);

			//insert category jobs
			$categories = $_SESSION['jobs']['categories'];
			if (count($categories) > 0) {
				foreach ($categories as $cat) {
					$job_category->insert_categories(array('jid' => $job_id, 'cid' => $cat));
				}
			}

			unset($_SESSION['jobs']);

			//create transaction record
			$params = array();
			$params['mid'] = $member_id;
			$params['tx'] = $response['id'];
			$params['method'] = \Transactions::METHOD_STRIPE;
			$params['amount'] = $stripe_config['post_job_fee'];
			$params['currency'] = $stripe_config['currency'];
			$params['description'] = $data['description'];
			$params['tx_ref_id'] = $jobs_data['reference_id'];
			$transactions->create($params);
		}
		
	}
	else{
		$response = array('status' => 'ERROR', 'error' => $validation->errors());
	}
	echo json_encode($response);
	exit();
}