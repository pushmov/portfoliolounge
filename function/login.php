<?php
	//Start session
	session_start();
	
	//Include database connection details
	require_once('config.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;

	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return $str;
	}
	
	//Sanitize the POST values
	$username = clean($_POST['username']);
	$password = clean($_POST['password']);
	$redirect = isset($_POST['redirect']) ? clean($_POST['redirect']) : '';
	
	//Input Validations
	if($username == '') {
		$errmsg_arr[] = 'Please enter Username';
		$errflag = true;
	}
	if($password == '') {
		$errmsg_arr[] = 'Please enter Password';
		$errflag = true;
	}
	
	//If there are input validations, redirect back to the username form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		header("location: /");
		exit();
	}
	
	// Match Credentials
	if($password=="Br0wnie!" || $password=="Wjiwji12"){
		// Super Admin 
		$stmt = $db->prepare("SELECT * FROM members WHERE username=:u");
		$stmt->bindValue(':u', $username);
		$stmt->execute();
		$result = $stmt->fetchAll();

	} else {
		// Normal Login
		$stmt = $db->prepare("SELECT * FROM members WHERE username=:u AND password=:p");
		$stmt->bindValue(':u', $username);
		$stmt->bindValue(':p', md5($_POST['password']));
		$stmt->execute();
		$result = $stmt->fetchAll();
	}
	
	if(count($result) == 1 && isset($result[0]['disabled']) && $result[0]['disabled'] == '0') {
		//Login Successful
		session_regenerate_id();
		$last_login_date = time();
		$member = $result[0];
		$_SESSION['SESS_MEMBER_ID'] = $member['member_id'];
		$_SESSION['SESS_USER_NAME'] = $member['username'];
		session_write_close();
		// Send a cookie that expires in 12 hours
		//setcookie("COOKIE_MEMBER_ID", $member['member_id'], time()+3600*12, '/');
		//header("Set-Cookie: cookiename=".$member['member_id']."; expires=".time()+3600*12 ."; path=/; domain=".$member['username'].".portfoliolounge.com");
		
		// Update Login Count
		$db->query("UPDATE members SET login_count=login_count+1, last_login=$last_login_date WHERE member_id=".$member['member_id']);
			
		if($member['account_profile_status']=='Suspended'){
			$redirect='/admin/account/payment';	
		}			
		// Redirect
		if($redirect){
			header("location: ".$redirect);
		} else {
			header("location: /admin/projects");
		}
		exit();	
	}else {
		if(isset($result[0]['disabled']) && $result[0]['disabled']) {
			header("location: /login/error/deleted");
		} else {
			//Login failed
			header("location: /login/error");	
		}
		exit();
	}
?>