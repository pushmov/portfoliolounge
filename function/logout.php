<?php
	//Start session
	session_start();
	
	//Unset the variables stored in session
	unset($_SESSION['SESS_MEMBER_ID']);
	unset($_SESSION['SESS_USER_NAME']);
	
	unset($_COOKIE['COOKIE_MEMBER_ID']);
	
	header("location: ../");
?>
