<?php
$a = $_GET["a"];
$b = $_GET["b"];
//Include database connection details
require_once('config.php');
session_start();
$memberid = $_SESSION['SESS_MEMBER_ID'];
if($a=="add_editors_pick"){
	$username = trim($_POST['username']);
	$addClient = $db->prepare("UPDATE members SET editors_pick=:ep WHERE username=:u");
	$addClient->bindValue(':ep', 1);
	$addClient->bindValue(':u', $username);
	$addClient->execute();
	$get_user = $db->prepare("SELECT email FROM members WHERE username=:u LIMIT 1");
	$get_user->bindValue(':u', $username);
	$get_user->execute();
	$user = $get_user->fetch();
	$message = "Hello! \n\nWe added your portfolio to our list of favorites on our browse page! Keep up the great work! \n\nSincerly, \nMatt Visk \nFounder at PortfolioLounge \r\n";
	$to = $user['email'];
	$subject = "Your portfolio is awesome!";
	$from = 'matt.visk@portfoliolounge.com';
	$headers = "From: matt.visk@portfoliolounge.com";
	mail($to,$subject,$message,$headers);
	header("location:  /owner/top-pick");
}
if($a=="add_top_pick"){
	$username = trim($_POST['username']);
	$addClient = $db->prepare("UPDATE members SET top_pick=:top WHERE username=:u");
	$addClient->bindValue(':top', '1');
	$addClient->bindValue(':u', $username);
	$addClient->execute();
	header("location:  /owner/top-pick");
}
if($a=="remove_editors_pick"){	
	$username = trim($_POST['username']);
	$addClient = $db->prepare("UPDATE members SET editors_pick=:ep WHERE member_id=:memberid");
	$addClient->bindValue(':ep', '');
	$addClient->bindValue(':memberid', $b);
	$addClient->execute();
	header("location:  /owner/top-pick");
}
if($a=="suspend_acct"){	
	$suspended_member_id = trim($_POST['suspended_member_id']);
	if($suspended_member_id){
		$update = $db->prepare("UPDATE members SET account_profile_status = :status WHERE account_profile_id = :id");
		$update->bindValue(':status', 'Suspended');
		$update->bindValue(':id', $suspended_member_id);
		$update->execute();

		$get_member = $db->prepare("SELECT email FROM members WHERE account_profile_id=:id");
		$get_member->bindValue(':id', $suspended_member_id);
		$get_member->execute();
		$member = $get_member->fetch();

		$message = "Hello! \n Your credit card on your Pro Account seems to have expired. Please login and update your credit card info to reactivate your account. \n http://portfoliolounge.com/admin/account/payment \n We are placing a temporary alert over top of your portfolio (you will still have access to the admin). \n Thank you! \n PortfolioLounge Billing Team \r\n";
		$to = $member['email'];
		$subject = "Your Portfolio Needs Attention!";
		$from = 'billing@portfoliolounge.com';
		$headers = "From: billing@portfoliolounge.com";
		mail($to,$subject,$message,$headers);
	
	header("location:  /owner/billing");	
	}	
}
?>