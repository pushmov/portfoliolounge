<?php
/*
* Script to receive paypal variables from paypal button subscription purchase
* this script will receive the tx variable from paypal, then adds it to a curl post along with
* the token for PL's account and submits it automatically. The returned values from that post
* get sent back with all the details of the transaction: customer's email, etc...
*/

session_start();
$member_id = $_SESSION['SESS_MEMBER_ID'];
$a = 2;

require_once('config.php');
require_once('paypal-config.php');

//this script will only handle an upgrade for the pro (not max) account. there
//will be another script to handle that account update.

if($member_id){

	$get_member = $db->prepare("SELECT * FROM members WHERE member_id = :member_id LIMIT 1");
	$get_member->bindValue(':member_id', $member_id);
	$get_member->execute();
	$member = $get_member->fetch();
	$current_account = $member['account_id'];
	
	if ($current_account == 1) {
		
		$start_date = time();
		$txrecieved=$_GET['tx'];
		//$txrecieved="7J344678FP6437806";
		$pltoken="bCbaYHg64lo__ICQIFTVRV8wTcmqUWD5HKFq4O287PxiQISleXmQLnlakfu";	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.paypal.com/cgi-bin/webscr");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, true);
		$data = array(
			'cmd' => '_notify-synch',
			'tx' => $txrecieved,
			'at' => $pltoken
		);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$output = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
		//$output contains all details of transaction. (LISTED OUT BELOW as $outputstatic)
		$valarr=explode("\n", $output);
		$ppvarname=array();
		$ppvarvalue=array();
		for($i=0;$i<count($valarr);$i++){
			if($valarr[$i]){
				$temparr=explode("=",$valarr[$i]);
				$ppvarname[$i]=$temparr[0];
				$ppvarvalue[$i]=urldecode($temparr[1]);
				//echo $valarr[$i]."<br/><br/>";
			}
		}
		//$PROFILEID=$txrecieved;
		$PROFILESTATUS="ActiveProfile";
		for($i=0;$i<count($ppvarname);$i++){
			//if($ppvarname[$i]=="profile_status"){
				//$PROFILESTATUS=$ppvarvalue[$i];
				//echo "profile status: ".$ppvarvalue[$i]."<br/>";
			//}
			//recurring_payment_id
			if($ppvarname[$i]=="subscr_id"){
				$PROFILEID=$ppvarvalue[$i];
				//echo "profile id: ".$ppvarvalue[$i]."<br/>";
			}
			
			//if($ppvarname[$i]=="first_name"){
				//echo "Customer's First Name: ".$ppvarvalue[$i]."<br/>";
			//}
			//if($ppvarname[$i]=="last_name"){
				//echo "Customer's Last Name: ".$ppvarvalue[$i]."<br/>";
			//}	
		}
	
		// Updata Member Data
		//if ($valarr[1] == "SUCCESS"){
		/* RESPONSE FROM PAYPAL
		
		echo "<br/><br/>";
		echo $output;
		echo "<br/><br/>";
		
		*/
		
		
		if(stristr($output, "SUCCESS")){
		//if ($ACK == "Success"){
			
			$addClient = $db->prepare("UPDATE members SET 
				account_id=:a,
				account_profile_status=:profile_status,
				upgrade_method=:pp,
				account_profile_id=:profile_id,
				account_start_date=:start_date
				WHERE member_id=:member_id");
			$addClient->bindValue(':a', $a);
			$addClient->bindValue(':profile_status', $PROFILESTATUS);
			$addClient->bindValue(':start_date', $start_date);
			$addClient->bindValue(':pp', 'paypal');
			$addClient->bindValue(':profile_id', $PROFILEID);
			$addClient->bindValue(':member_id', $member_id);
			$addClient->execute();
			
			if($a==2){// Upgraded to PROFESSIONAL
			
			
				?>
                
                <!--
				<h2>You have upgraded to Professional!</h2>
				<img height="80" width="80" src="images/icon-large-checkmark.png" />
				<p>Your account has been updated and you are now ready to add more work to your portfolio. We cannot wait to see it!</p>
				<a class="btn green" href="/admin">Continue</a>
                
                -->
                <div style="text-align:center;padding:100px 0;">
                <h4>You have upgraded to Professional!</h4>
                 <a class="btn green" href="/admin">Continue</a>
                </div>
				<?php 
				
				$message = "A member has upgraded to a Professional account. \r\n\r\n Member ID:".$member['member_id']."\r\n Portfolio: http://".$member['username'].".portfoliolounge.com \r\n Profile ID: ".$PROFILEID;
				$to = "upgrades@portfoliolounge.com";
				$subject = "Pro Upgrade! (via paypal)";
				$from = $member['email'];
				$headers = "From:" .$member['email'];
				mail($to,$subject,$message,$headers);
				
				header('Location: /checkout/professional/success');
				
				
				
			} else if ($a==3) {// Upgraded to MAXIMUM
				
				/*
				?>
				<h2>You have upgraded to Maximum!</h2>
				<img height="80" width="80" src="images/icon-large-checkmark.png" />
				<p>Your account has been updated and you are now ready to add more work to your portfolio. We cannot wait to see it!</p>
				<a class="btn green" href="/admin">Continue</a>
				<?
				*/
				
				$message = "A member has upgraded to a Maximum account. \r\n\r\n Member ID:".$member['member_id']."\r\n Portfolio: http://".$member['username'].".portfoliolounge.com \r\n Profile ID: ".$PROFILEID;
				$to = "upgrades@portfoliolounge.com";
				$subject = "Max Upgrade!";
				$from = $member['email'];
				$headers = "From:" .$member['email'];
				mail($to,$subject,$message,$headers);
			}
			
		} else {
			
			/*
			?>
			<h2>We're sorry.</h2>
			<p>We could not process your paypal payment.</p>
			<?php if($a==2){?>
				<a class="btn" href="/checkout/professional">Try Again</a>
			<?php } else if($a==3){?>
				<a class="btn" href="/checkout/maximum">Try Again</a>
			<?php } ?>
			<script>
			console.log("<?php echo $output;?>");
			</script>
			<?
			*/
			
		}
		
	} else {
			
			?>
			<h2>Oops.</h2>
			<p>You already have an upgraded account. Please contact billing@portfoliolounge.com</p>
			<?
				
	}

}
?>