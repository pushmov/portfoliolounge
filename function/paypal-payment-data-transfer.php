<?php
/*
* Script to receive paypal variables from paypal button subscription purchase
* this script will receive the tx variable from paypal, then adds it to a curl post along with
* the token for PL's account and submits it automatically. The returned values from that post
* get sent back with all the details of the transaction: customer's email, etc...
*/

session_start();
$member_id = $_SESSION['SESS_MEMBER_ID'];
//$a = $_POST['account'];
$a = 2;

require_once('config.php');
require_once('paypal-config.php');

//this script will only handle an upgrade for the pro (not max) account. there
//will be another script to handle that account update.
echo "mid: ".$member_id."<br/>";
$get_member = $db->prepare("SELECT * FROM members WHERE member_id=:member_id");
$get_member->bindValue(':member_id', $member_id);
$get_member->execute();
$member = $get_member->fetch();
$current_account = $member['account_id'];

if ($current_account == 1) {
	
	$start_date = time();
	$txrecieved=$_GET['tx'];
	//$txrecieved="7J344678FP6437806";
	$pltoken="bCbaYHg64lo__ICQIFTVRV8wTcmqUWD5HKFq4O287PxiQISleXmQLnlakfu";	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://www.paypal.com/cgi-bin/webscr");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, true);
	$data = array(
		'cmd' => '_notify-synch',
		'tx' => $txrecieved,
		'at' => $pltoken
	);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	$output = curl_exec($ch);
	$info = curl_getinfo($ch);
	curl_close($ch);
	//$output contains all details of transaction. (LISTED OUT BELOW as $outputstatic)
	$valarr=explode("\n", $output);
	$ppvarname=array();
	$ppvarvalue=array();
	for($i=0;$i<count($valarr);$i++){
		if($valarr[$i]){
			$temparr=explode("=",$valarr[$i]);
			$ppvarname[$i]=$temparr[0];
			$ppvarvalue[$i]=urldecode($temparr[1]);
			//echo $valarr[$i]."<br/><br/>";
		}
	}
	//$PROFILEID=$txrecieved;
	$PROFILESTATUS="ActiveProfile";
	for($i=0;$i<count($ppvarname);$i++){
		//if($ppvarname[$i]=="profile_status"){
			//$PROFILESTATUS=$ppvarvalue[$i];
			//echo "profile status: ".$ppvarvalue[$i]."<br/>";
		//}
		//recurring_payment_id
		if($ppvarname[$i]=="subscr_id"){
			$PROFILEID=$ppvarvalue[$i];
			echo "profile id: ".$ppvarvalue[$i]."<br/>";
		}
		
		//if($ppvarname[$i]=="first_name"){
			//echo "Customer's First Name: ".$ppvarvalue[$i]."<br/>";
		//}
		//if($ppvarname[$i]=="last_name"){
			//echo "Customer's Last Name: ".$ppvarvalue[$i]."<br/>";
		//}	
	}

	// Updata Member Data
	//if ($valarr[1] == "SUCCESS"){
	echo "<br/><br/>";
	echo $output;
	echo "<br/><br/>";
	if(stristr($output, "SUCCESS")){
	//if ($ACK == "Success"){
		
		$addClient = $db->prepare("UPDATE members SET account_id=:a,
			account_profile_status=:profile_status,
			account_profile_id=:profile_id,
			account_start_date=:start_date 
			WHERE member_id=:member_id");
		$addClient->bindValue(':a', $a);
		$addClient->bindValue(':profile_status', $PROFILESTATUS);
		$addClient->bindValue(':profile_id', $PROFILEID);
		$addClient->bindValue(':start_date', $start_date);
		$addClient->bindValue(':member_id', $member_id);
		$addClient->execute();
		
		if($a==2){// Upgraded to PROFESSIONAL
		
			?>
			<h2>You have upgraded to Professional!</h2>
			<img height="80" width="80" src="images/icon-large-checkmark.png" />
			<p>Your account has been updated and you are now ready to add more work to your portfolio. We cannot wait to see it!</p>
			<a class="btn green" href="/admin">Continue</a>
			<?
			
			$message = "A member has upgraded to a Professional account. \r\n\r\n Member ID:".$member['member_id']."\r\n Portfolio: http://".$member['username'].".portfoliolounge.com \r\n Profile ID: ".$PROFILEID;
			$to = "upgrades@portfoliolounge.com";
			$subject = "Pro Upgrade!";
			$from = $member['email'];
			$headers = "From:" .$member['email'];
			mail($to,$subject,$message,$headers);
			
		} else if ($a==3) {// Upgraded to MAXIMUM
			?>
			<h2>You have upgraded to Maximum!</h2>
			<img height="80" width="80" src="images/icon-large-checkmark.png" />
			<p>Your account has been updated and you are now ready to add more work to your portfolio. We cannot wait to see it!</p>
			<a class="btn green" href="/admin">Continue</a>
			<?
			
			$message = "A member has upgraded to a Maximum account. \r\n\r\n Member ID:".$member['member_id']."\r\n Portfolio: http://".$member['username'].".portfoliolounge.com \r\n Profile ID: ".$PROFILEID;
			$to = "upgrades@portfoliolounge.com";
			$subject = "Max Upgrade!";
			$from = $member['email'];
			$headers = "From:" .$member['email'];
			mail($to,$subject,$message,$headers);
		}
		
	} else {
		
		?>
        <h2>We're sorry.</h2>
		<p>We could not process your paypal payment.</p>
		<?php if($a==2){?>
    	    <a class="btn" href="/checkout/professional">Try Again</a>
		<?php } else if($a==3){?>
	        <a class="btn" href="/checkout/maximum">Try Again</a>
		<?php } ?>
        <script>
		console.log("<?php echo $output;?>");
		</script>
		<?
		
	}
	
} else {
		
		?>
        <h2>Oops.</h2>
		<p>You already have an upgraded account. Please contact billing@portfoliolounge.com</p>
		<?
			
}



/*
$outputstatic="
SUCCESS
mc_gross=7.99
period_type=+Regular
outstanding_balance=0.00
next_payment_date=03%3A00%3A00+Jun+22%2C+2016+PDT
protection_eligibility=Ineligible
payment_cycle=Monthly
tax=0.00
payer_id=ZJUTJBSTUP458
payment_date=09%3A17%3A13+May+22%2C+2016+PDT
payment_status=Completed
product_name=PortfolioLounge+Pro
charset=windows-1252
recurring_payment_id=I-RKMGP9H83GXM
first_name=19317+Teresa
mc_fee=0.50
amount_per_cycle=7.99
payer_status=unverified
currency_code=USD
business=billing%40portfoliolounge.com
payer_email=tbtne%40hotmail.com
initial_payment_amount=0.00
profile_status=Active
amount=7.99
txn_id=7J344678FP6437806
payment_type=instant
last_name=Tira
receiver_email=billing%40portfoliolounge.com
payment_fee=0.50
receiver_id=J6A9NDUDR8RES
txn_type=recurring_payment
mc_currency=USD
residence_country=US
receipt_id=4195-9412-5724-2752
transaction_subject=
payment_gross=7.99
shipping=0.00
product_type=1
time_created=08%3A51%3A48+May+22%2C+2016+PDT
";
*/
/*
//$txrecieved=$_GET['tx'];
$txrecieved="7J344678FP6437806";
//$txrecieved="22436258YS470743Y";
$pltoken="bCbaYHg64lo__ICQIFTVRV8wTcmqUWD5HKFq4O287PxiQISleXmQLnlakfu";
//write these out into a form and submit once the page loads:
$to_post['cmd']="_notify-synch";
$to_post['tx'] = $txrecieved;
$to_post['at'] = $pltoken;

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://www.paypal.com/cgi-bin/webscr");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, true);

$data = array(
    'cmd' => '_notify-synch',
    'tx' => '7J344678FP6437806',
    'at' => 'bCbaYHg64lo__ICQIFTVRV8wTcmqUWD5HKFq4O287PxiQISleXmQLnlakfu'
);

curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
$output = curl_exec($ch);
$info = curl_getinfo($ch);
curl_close($ch);
*/
/*
$output contains all details of transaction. The payer_email
*/
/*
$valarr=explode("\n", $output);
$ppvarname=array();
$ppvarvalue=array();
for($i=0;$i<count($valarr);$i++){
	if($valarr[$i]){
		$temparr=explode("=",$valarr[$i]);
		$ppvarname[$i]=$temparr[0];
		$ppvarvalue[$i]=urldecode($temparr[1]);
		//echo $valarr[$i]."<br/><br/>";
		
	}
}
echo "<br/>";
for($i=0;$i<count($ppvarname);$i++){
	if($ppvarname[$i]=="payer_email"){
		echo "Customer's Email: ".$ppvarvalue[$i]."<br/>";
	}
	if($ppvarname[$i]=="first_name"){
		echo "Customer's First Name: ".$ppvarvalue[$i]."<br/>";
	}
	if($ppvarname[$i]=="last_name"){
		echo "Customer's Last Name: ".$ppvarvalue[$i]."<br/>";
	}
	
}
*/
?>
<!--
<form id="ppf" name="ppf" method=post action="https://www.paypal.com/cgi-bin/webscr">
  <input type="hidden" name="cmd" value="_notify-synch">
  <input type="hidden" name="tx" value="<?php echo $txrecieved; ?>">
  <input type="hidden" name="at" value="<?php echo $pltoken; ?>">
  <input type="submit" value="PDT">
</form>

<script type="text/javascript">
document.getElementById("ppf").submit();
</script>
-->
