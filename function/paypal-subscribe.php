<?php 
session_start();
$member_id = $_SESSION['SESS_MEMBER_ID'];
$a = $_POST['account'];

require_once('config.php');
require_once('paypal-config.php');

$get_member = $db->prepare("SELECT * FROM members WHERE member_id=:member_id LIMIT 1");
$get_member->bindValue(':member_id', $member_id);
$get_member->execute();
$member = $get_member->fetch();
$current_account = $member['account_id'];

if ($current_account == 1) {
	
	$cc_number = $_POST['cc_number'];
	$cc_type = $_POST['cc_type'];
	$cvv = $_POST['cvv'];
	$expiration = $_POST['exp_month'].$_POST['exp_year'];
	$start_date = time();
	
	if($a==2){// Upgrade to PROFESSIONAL
		
		$request_params = array(
			'METHOD' => 'CreateRecurringPaymentsProfile', 
			'USER' => $api_username, 
			'PWD' => $api_password, 
			'SIGNATURE' => $api_signature, 
			'VERSION' => $api_version, 
			'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
			'EMAIL' => $member['email'],
			'FIRSTNAME' => $member['member_id'].' '.$member['first_name'],
			'LASTNAME' => $member['last_name'],
			'CREDITCARDTYPE' => $cc_type, 
			'ACCT' => $cc_number,					
			'EXPDATE' => $expiration, 	
			'CVV2' => $cvv,
			'AMT' => '12.00', 
			'CURRENCYCODE' => 'USD', 
			'DESC' => 'PortfolioLounge Pro',
			'BILLINGPERIOD' => 'Month',
			'BILLINGFREQUENCY' => '1',
			'PROFILESTARTDATE' => gmdate("Y-m-d")."T".gmdate("H:i:s"),
			'L_PAYMENTREQUEST_0_NAME0' => 'PortfolioLounge Pro',
			'L_PAYMENTREQUEST_0_AMT0' => '12.00',
			'L_PAYMENTREQUEST_0_QTY0' => '1',
			'MAXFAILEDPAYMENTS' => '2'
		);
	
	} else if($a==3) { // Upgrade to MAXIMUM
		
		$request_params = array(
			'METHOD' => 'CreateRecurringPaymentsProfile', 
			'USER' => $api_username, 
			'PWD' => $api_password, 
			'SIGNATURE' => $api_signature, 
			'VERSION' => $api_version, 
			'IPADDRESS' => $_SERVER['REMOTE_ADDR'],
			'EMAIL' => $member['email'],
			'FIRSTNAME' => $member['member_id'].' '.$member['first_name'],
			'LASTNAME' => $member['last_name'],
			'CREDITCARDTYPE' => 'Visa', 
			'ACCT' => $cc_number,					
			'EXPDATE' => $expiration, 	
			'CVV2' => $expiration,
			'AMT' => '24.00', 
			'CURRENCYCODE' => 'USD', 
			'DESC' => 'Professional Maximum',
			'BILLINGPERIOD' => 'Month',
			'BILLINGFREQUENCY' => '1',
			'PROFILESTARTDATE' => gmdate("Y-m-d")."T".gmdate("H:i:s"),
			'L_PAYMENTREQUEST_0_NAME0' => 'Professional Maximum',
			'L_PAYMENTREQUEST_0_AMT0' => '24.00',
			'L_PAYMENTREQUEST_0_QTY0' => '1',
			'MAXFAILEDPAYMENTS' => '2'
		);
	}
						
	// Loop through $request_params array to generate the NVP string.
	$nvp_string = '';
	foreach($request_params as $var=>$val) {
		$nvp_string .= '&'.$var.'='.urlencode($val);	
	}
	
	// Send NVP string to PayPal and store response
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_VERBOSE, 1);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($curl, CURLOPT_TIMEOUT, 30);
	curl_setopt($curl, CURLOPT_URL, $api_endpoint);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);
	
	// Get Result
	$result = curl_exec($curl);
	curl_close($curl);
	//echo $result;
	
	// Create Variables
	parse_str($result);
	
	// Updata Member Data
	if ($ACK == "Success"){
		
		$addClient = $db->prepare("UPDATE members SET account_id=:a,
			account_profile_status=:profile_status,
			account_profile_id=:profile_id,
			account_start_date=:start_date WHERE member_id=:member_id");
		$addClient->bindValue(':a', $a);
		$addClient->bindValue(':profile_status', $PROFILESTATUS);
		$addClient->bindValue(':profile_id', $PROFILEID);
		$addClient->bindValue(':start_date', $start_date);
		$addClient->bindValue(':member_id', $member_id);
		$addClient->execute();
		
		if($a==2){// Upgraded to PROFESSIONAL
		
			?>
			<h2>You have upgraded to Professional!</h2>
			<img height="80" width="80" src="images/icon-large-checkmark.png" />
			<p>Your account has been updated and you are now ready to add more work to your portfolio. We cannot wait to see it!</p>
			<a class="btn green" href="admin/projects">Continue</a>
			<?
			
			$message = "A member has upgraded to a Professional account. \r\n\r\n Member ID:".$member['member_id']."\r\n Portfolio: http://".$member['username'].".portfoliolounge.com \r\n Profile ID: ".$PROFILEID;
			$to = "upgrades@portfoliolounge.com";
			$subject = "Pro Upgrade!";
			$from = $member['email'];
			$headers = "From:" .$member['email'];
			mail($to,$subject,$message,$headers);
			
		} else if ($a==3) {// Upgraded to MAXIMUM
			?>
			<h2>You have upgraded to Maximum!</h2>
			<img height="80" width="80" src="images/icon-large-checkmark.png" />
			<p>Your account has been updated and you are now ready to add more work to your portfolio. We cannot wait to see it!</p>
			<a class="btn green" href="admin/projects">Continue</a>
			<?
			
			$message = "A member has upgraded to a Maximum account. \r\n\r\n Member ID:".$member['member_id']."\r\n Portfolio: http://".$member['username'].".portfoliolounge.com \r\n Profile ID: ".$PROFILEID;
			$to = "upgrades@portfoliolounge.com";
			$subject = "Max Upgrade!";
			$from = $member['email'];
			$headers = "From:" .$member['email'];
			mail($to,$subject,$message,$headers);
		}
		
	} else {
		
		?>
        <h2>We're sorry.</h2>
		<p>We could not process your credit card.</p>
		<?php if($a==2){?>
    	    <a class="btn" href="/checkout/professional">Try Again</a>
		<?php } else if($a==3){?>
	        <a class="btn" href="/checkout/maximum">Try Again</a>
		<?php } ?>
        <script>
		console.log("<?php echo $result;?>");
		</script>
		<?
		
	}
	
} else {
		
		?>
        <h2>Oops.</h2>
		<p>You already have an upgraded account. Please contact billing@portfoliolounge.com</p>
		<?
			
}
	

?>