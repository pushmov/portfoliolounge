<?php 
session_start();
$member_id = $_SESSION['SESS_MEMBER_ID'];

require_once('config.php');
require_once('paypal-config.php');

$get_member_data = $db->prepare("SELECT * FROM members WHERE member_id=:member_id LIMIT 1");
$get_member_data->bindValue(':member_id', $member_id);
$get_member_data->execute();
$member_data = $get_member_data->fetch();

$cc_number = $_POST['cc_number'];
$cc_type = $_POST['cc_type'];
$cvv = $_POST['cvv'];
$expiration = $_POST['exp_month'].$_POST['exp_year'];

if(!$cc_number || !$cc_type || !$cvv || !$expiration){
	?>
	<h3>We're sorry.</h3>
	<p>We could not update your credit card. Please try a different one.</p>
	<a class="btn" href="/admin/account/payment">Try Again</a>
	<script>
	console.log("<?php echo $result;?>");
	</script>
	<?
	exit();
}



// Store request params in an array
$request_params = array(
	'METHOD' => 'UpdateRecurringPaymentsProfile', 
	'USER' => $api_username, 
	'PWD' => $api_password,
	'SIGNATURE' => $api_signature, 
	'VERSION' => $api_version,
	'PROFILEID' => $member_data['account_profile_id'], 
	'CREDITCARDTYPE' => $cc_type, 
	'ACCT' => $cc_number,			
	'EXPDATE' => $expiration, 	
	'CVV2' => $cvv,
);
		
// Loop through $request_params array to generate the NVP string.
$nvp_string = '';
foreach($request_params as $var=>$val) {
	$nvp_string .= '&'.$var.'='.urlencode($val);	
}
// Send NVP string to PayPal and store response
$curl = curl_init();
curl_setopt($curl, CURLOPT_VERBOSE, 1);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt($curl, CURLOPT_TIMEOUT, 30);
curl_setopt($curl, CURLOPT_URL, $api_endpoint);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);

// Get Result
$result = curl_exec($curl);
curl_close($curl);
parse_str($result);

// CANCEL ACCOUNT
if ($ACK == "Success"){
	
	$addClient = $db->prepare("UPDATE members SET account_profile_status=:profile_status WHERE 
		member_id=:member_id");
	$addClient->bindValue(':profile_status', 'ActiveProfile');
	$addClient->bindValue(':member_id', $member_id);
	$addClient->execute();
	?>
	
	<h2>Your credit card has been updated.</h2>
	<p>Your account has been reactivated! Keep up the great work!</p>
	<a class="btn green" href="/admin/settings/account">Continue</a>
	
	<?
	$message = "Credit Card Updated. \r\n\r\n Member ID:".$member_data['member_id']."\r\n Portfolio: http://".$member['username'].".portfoliolounge.com \r\n Profile ID: ".$member_data['account_profile_id'];
	$to = "upgrades@portfoliolounge.com";
	$subject = "Credit Card Updated!";
	$from = $member_data['email'];
	$headers = "From:" .$member_data['email'];
	mail($to,$subject,$message,$headers);
	
} else {

	?>
	<h3>We're sorry.</h3>
	<p>We could not update your credit card. Please try a different one.</p>
	<a class="btn" href="/admin/account/payment">Try Again</a>
	<script>
	console.log("<?php echo $result;?>");
	</script>
	<?
	
}
