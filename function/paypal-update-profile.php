<?php 
session_start();
$member_id = $_SESSION['SESS_MEMBER_ID'];

require_once('config.php');
require_once('paypal-config.php');

$get_member_data = $db->prepare("SELECT * FROM members WHERE member_id=:member_id LIMIT 1");
$get_member_data->bindValue(':member_id', $member_id);
$get_member_data->execute();
$member_data = $get_member_data->fetch();

$get_account_data = $db->prepare("SELECT * FROM accounts WHERE account_id=:account_id LIMIT 1");
$get_account_data->bindValue(':account_id', 1);
$get_account_data->execute();
$account_data = $get_account_data->fetch();

$get_total_items = $db->prepare("SELECT * FROM items WHERE member_id=:member_id");
$get_total_items->bindValue(':member_id', $member_id);
$get_total_items->execute();
$total_items_all = $get_total_items->fetchAll();
$total_items = count($total_items_all);

$get_total_items = $db->prepare("SELECT * FROM projects WHERE member_id=:member_id");
$get_total_items->bindValue(':member_id', $member_id);
$get_total_items->execute();
$total_projects_all = $get_total_items->fetchAll();
$total_projects = count($total_projects_all);

// If items are within free limits
if ($total_items <= $account_data['item_limit']+$member_data['earned_items']){
	
	
	// Store request params in an array
	$request_params = array(
		'METHOD' => 'ManageRecurringPaymentsProfileStatus', 
		'USER' => $api_username, 
		'PWD' => $api_password,
		'SIGNATURE' => $api_signature, 
		'VERSION' => $api_version,
		'PROFILEID' => $member_data['account_profile_id'], 
		"ACTION" => "cancel"
	);
			
	// Loop through $request_params array to generate the NVP string.
	$nvp_string = '';
	foreach($request_params as $var=>$val) {
		$nvp_string .= '&'.$var.'='.urlencode($val);	
	}
	// Send NVP string to PayPal and store response
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_VERBOSE, 1);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($curl, CURLOPT_TIMEOUT, 30);
	curl_setopt($curl, CURLOPT_URL, $api_endpoint);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string);
	
	// Get Result
	$result = curl_exec($curl);
	curl_close($curl);
	parse_str($result);
	
	// CANCEL ACCOUNT
	if ($ACK == "Success"){
		
		$addClient = $db->prepare("UPDATE members SET account_id=:account_id,
			account_profile_status=:status,
			account_profile_id=:profile_id,
			account_start_date=:start_date 
			WHERE member_id=:member_id");
		$addClient->bindValue(':account_id', 1);
		$addClient->bindValue(':status', 'Cancelled');
		$addClient->bindValue(':profile_id', '');
		$addClient->bindValue(':start_date', '');
		$addClient->bindValue(':member_id', $member_id);
		$addClient->execute();
		
		?>
		<h2>Your account has been downgraded.</h2>
		<p>Your account is now free and you will not be charged again.</p>
		<a class="btn green" href="/admin/settings/account">Continue</a>
		<?
		
			
		$message = "Account cancellation. \r\n\r\n Member ID:".$member_data['member_id']."\r\n Portfolio: http://".$member['username'].".portfoliolounge.com \r\n Profile ID: ".$member_data['account_profile_id'];
		$to = "upgrades@portfoliolounge.com";
		$subject = "Account Cancelled :(";
		$from = $member_data['email'];
		$headers = "From:" .$member_data['email'];
		mail($to,$subject,$message,$headers);
		
	} else {
	
		?>
		<h2>We're sorry.</h2>
		<p>We could not cancel your account. Please contact billing@portfoliolounge.com for help.</p>
		<a class="btn" href="/admin">Try Again</a>
		<?
		
	}
} else {
	
	?>
    <h2>We're sorry.</h2>
    <p>You have <?php echo $total_items; ?> items, but you'll need less than <?php echo $account_data['item_limit']+$member_data['earned_items'];?> to downgrade.</p>
    <p>You'll need to delete at least <?php echo $total_items - ($account_data['item_limit']+$member_data['earned_items']);?> items before you can downgrade.</p>
    <a class="btn" href="/admin">Go to Projects</a>
	<?
	
}?>