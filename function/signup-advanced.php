<?php
	//Start session
	session_start();
	
	//Include database connection details
	require_once('config.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	
	//Validation error flag
	$errflag = false;
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return $str;
	}
	
	//Sanitize the POST values
	$username = clean($_POST['username']);
	$email = clean($_POST['email']);
	$genre = clean($_POST['genre']);
	$password = clean($_POST['password']);
	$cpassword = clean($_POST['cpassword']);
	
	//Input Validations
	if($username == '') {
		//$errmsg_arr[] = 'Please enter Username';
		$errflag = true;
	}
	if($password == '') {
		//$errmsg_arr[] = 'Please enter Password';
		$errflag = true;
	}
	if($email == '') {
		//$errmsg_arr[] = 'Please enter Email Address';
		$errflag = true;
	}
	if($cpassword == '') {
		//$errmsg_arr[] = 'Oops, please verify password';
		$errflag = true;
	}
	if( strcmp($password, $cpassword) != 0 ) {
		//$errmsg_arr[] = 'Passwords do not match, please try again';
		$error_key = "password";
		$errflag = true;
	}
	
	//Check for duplicate username ID
	if($username != '') {
		$qry = "SELECT * FROM members WHERE username='$username'";
		$result = mysql_query($qry);

		$stmt = $db->prepare("SELECT * FROM members WHERE username=:u");
		$stmt->bindValue(':u', $username);
		$stmt->execute();
		$result = $stmt->fetchAll();
		if(count($result) > 0) {
			//$errmsg_arr[] = 'Username is taken, try a different one!';
			//header ("location: /signup/error");
			$error_key = "username";
			$errflag = true;
		}
	}
	
	//If there are input validations, redirect back to the registration form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		
		if ($error_key=="username"){
			header("location: /signup/username");
		}
		else if ($error_key=="password"){
			header("location: /signup/password");
		}
		exit();
	}
	
	//Sanitize the POST values
	$email = clean($_POST['email']);
	$username = clean($_POST['username']);
	$password = clean($_POST['password']);
	$cpassword = clean($_POST['cpassword']);


	//Create INSERT query
	$result = $db->prepare("INSERT INTO members (email,username,password,time) 
		VALUES(:email,:username,:pass,:time)");
	$result->bindValue(':email', $email);
	$result->bindValue(':username', $username);
	$result->bindValue(':pass', md5($password));
	$result->bindValue(':time', time());
	$exec = $result->execute();
	
	//Check whether the query was successful or not
	if($exec) {
		
		/* Login on successful signup */
		
		//Create query
		$stmt = $db->prepare("SELECT * FROM members WHERE username=:u AND password=:p");
		$stmt->bindValue(':u', $username);
		$stmt->bindValue(':p', md5($password));
		$stmt->execute();
		$result = $stmt->fetchAll();
		
		if(count($result) == 1) {
			
			
			//Login Successful!
			session_regenerate_id();
			$member = $result[0];
			$_SESSION['SESS_MEMBER_ID'] = $member['member_id'];
			$_SESSION['SESS_USER_NAME'] = $member['username'];
			session_write_close();
			
			// Go to Welcome
			header("location: /admin/welcome");
			
			// Email Administrators
			$message = "PortfolioLounge just got another new member! \r\n $username.portfoliolounge.com";
			$to = "members@portfoliolounge.com";
			$subject = "Portfolio Lounge New Member! ($username)";
			$from = $email;
			$headers = "From:" . $email;
			mail($to,$subject,$message,$headers);
			
			exit();
		}
		
		
	}else {
		die("Query failed 2");
	}
?>