<?php
	$first_name = clean($_POST['first_name']);
	$last_name = clean($_POST['last_name']);
	
	if($first_name || $last_name){
		die("Oh no! We have reason to believe you are a robot. If you are not, we are very sorry! feel free to try again. - Portfoliolounge Team");
	}

	session_start();
	require_once('config.php');
	
	//Array to store validation errors
	$errmsg_arr = array();
	$errflag = false;
	
	//Function to sanitize values received from the form. Prevents SQL injection
	function clean($str) {
		$str = @trim($str);
		if(get_magic_quotes_gpc()) {
			$str = stripslashes($str);
		}
		return $str;
	}
	
	//Sanitize the POST values
	$username = clean($_POST['username']);
	$email = clean($_POST['email']);
	$genre = clean($_POST['genre']);
	$password = clean($_POST['password']);
	$cpassword = clean($_POST['cpassword']);
	$account = clean($_POST['account']);
	
	//Input Validations
	if($username == '') {
		//$errmsg_arr[] = 'Please enter Username';
		$errflag = true;
	}
	if($password == '') {
		//$errmsg_arr[] = 'Please enter Password';
		$errflag = true;
	}
	if($email == '') {
		//$errmsg_arr[] = 'Please enter Email Address';
		$errflag = true;
	}
	if($cpassword == '') {
		//$errmsg_arr[] = 'Oops, please verify password';
		$errflag = true;
	}
	if( strcmp($password, $cpassword) != 0 ) {
		//$errmsg_arr[] = 'Passwords do not match, please try again';
		$error_key = "password";
		$errflag = true;
	}
	
	//Check for duplicate username ID
	if($username != '') {
		$stmt = $db->prepare("SELECT * FROM members WHERE username=:u");
		$stmt->bindValue(':u', $username);
		$stmt->execute();
		$result = $stmt->fetchAll();
		if(count($result) > 0) {
			$error_key = "username";
			$errflag = true;
		}
	}
	
	//If there are input validations, redirect back to the registration form
	if($errflag) {
		$_SESSION['ERRMSG_ARR'] = $errmsg_arr;
		session_write_close();
		
		if ($error_key=="username"){
			header("location: /signup/username");
		}
		else if ($error_key=="password"){
			header("location: /signup/password");
		}
		exit();
	}
	
	//Sanitize the POST values
	$email = clean($_POST['email']);
	$username = clean($_POST['username']);
	$password = clean($_POST['password']);
	$cpassword = clean($_POST['cpassword']);


	$firstname = clean($_POST['firstlast_name']);
	//if($firstname==''){
		
		if(isset($_COOKIE['pl_campaign_id'])){
	
			$campaign_id = $_COOKIE['pl_campaign_id'];
	
		} else {
			$campaign_id = '';
		}
	
	
		//Create INSERT query
		$result = $db->prepare("INSERT INTO members (email,username,password,time,campaign_id) 
			VALUES(:email,:username,:pass,:time,:campaign_id)");
		$result->bindValue(':email', $email);
		$result->bindValue(':username', $username);
		$result->bindValue(':pass', md5($password));
		$result->bindValue(':time', time());
		$result->bindValue(':campaign_id', $campaign_id);
		$exec = $result->execute();
		
		//Check whether the query was successful or not
		if($exec) {
			
			/* Login on successful signup */
			
			//Create query
			$stmt = $db->prepare("SELECT * FROM members WHERE username=:u AND password=:p");
			$stmt->bindValue(':u', $username);
			$stmt->bindValue(':p', md5($password));
			$stmt->execute();
			$result = $stmt->fetchAll();
			
			if(count($result) == 1) {
				
				
				//Login Successful!
				session_regenerate_id();
				$member = $result[0];
				$_SESSION['SESS_MEMBER_ID'] = $member['member_id'];
				$_SESSION['SESS_USER_NAME'] = $member['username'];
				session_write_close();
				
				// Go to Welcome
				if($account==1)	header("location: /admin/welcome");
				if($account==2)	header("location: /checkout/professional");
				if($account==3)	header("location: /checkout/maximum");
				
				// Email Administrators
				$message = "PortfolioLounge just got another new member! \r\n $username.portfoliolounge.com";
				$to = "members@portfoliolounge.com";
				$subject = "Portfolio Lounge New Member! ($username)";
				$from = $email;
				$headers = "From:" . $email;
				mail($to,$subject,$message,$headers);
				
				exit();
			}
			
			
		}else {
			die("ERROR: The email address you chose, is already in use.");
		}
	
/*
	} else {	

		// Email Administrators
		$message = "Robot Signup Block!";
		$to = "info@portfoliolounge.com";
		$subject = "Robot Signup Block!";
		$from = $email;
		$headers = "From:" . $email;
		mail($to,$subject,$message,$headers);
				
	 	echo 'hi robot!';
		
		
		
	}
	*/
?>