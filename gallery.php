<?php
$page = $x ? $x:0;
$show_per_page = 12;
$limit_start = $page * $show_per_page;
$dailyRandomOrder = date("dmY");
if($p=='gallery') $show_per_page = 1000;

$get_members = $db->prepare("SELECT * FROM items WHERE featured = :featured ORDER BY RAND() LIMIT :limit_start, :show_per_page");
$get_members->bindValue(':featured', 1);
$get_members->bindValue(':limit_start', $limit_start);
$get_members->bindValue(':show_per_page', $show_per_page);
$get_members->execute();
$members = $get_members->fetchAll();
$member_count = count($members);
$item_arr = $members
?>
<script src="js/gallery.js"></script>
<div class="gallery" ng-controller="ownerCtrl">
<?php // HOME Content
	if($p!='gallery'){ ?>
	<div class="headline">
		<h1 style="font-weight:700;">Join the Pros</h1>
		<h3>25,811 <small><a href="browse-portfolios">Portfolios</a></small> &nbsp; 30,750 <small>Projects</small> &nbsp; 104,163 <small><a href="gallery">Items</a></small></h3>
		<br />
		<a title="View our favorite work by artists like you." href="signup" class="promobtn">Create your portfolio website &raquo;</a>
	</div>
<?php } ?>
	<?php foreach($item_arr as $item){

		$get_member = $db->prepare("SELECT first_name, last_name, username FROM members WHERE member_id = :member_id LIMIT 1");
		$get_member->bindValue(':member_id', $item['member_id']);
		$get_member->execute();
		$item_member = $get_member->fetch();?>

		<a class="home_thumb frame" <?php if(isset($member) && !$member['admin']){?>href="portfolio/<?=$item_member['username']?>"<?php }?> style="position:relative;">
			<?php if(isset($member) && $member['admin'] && $p=='gallery'){?>
					<div style="position:absolute;top:0;z-index:9999" class="check <?php if($item['featured']) {echo 'checked';}?>" ng-click="markItem(<?=$item['item_id'];?>,$event)"></div>
					<div style="position:absolute;top:0;left:30px;z-index:9999" class="check <?php if($item['featured_home']) {echo 'checked';}?>" ng-click="markItemHome(<?=$item['item_id'];?>,$event)"></div>
				<?php }  ?>
			<div class="awesome-title">
				<h2>
					<?=$item_member['first_name']?>
					<?=$item_member['last_name']?>
				</h2>
				<h3>
					<?=$item_member['username']?>
				</h3>
				<p>
					<?=$item['item_title']?>
					<?=$item['item_desc']?>
				</p>
			</div>
			<div class="canvas" data-source="https://portfoliolounge.com/uploads/<?php echo $item['member_id'].'/medium/'.$item['item_filename'];?>"></div>
		</a>
	<?php } ?>
	<div class="clear"></div>
</div>       
       
	<style>
		.headline h3 a {
			color:#fff;
			text-decoration:underline;
		}
		a.boxbtn {
			font-size:20px;
			color:#fff;
			border:1px solid rgba(255,255,255,.3);
			padding:0;	
			display:inline-block;
			position:absolute;
			bottom:0px;
			right:0px;
			-webkit-transition: border-color 200ms, padding 200ms;
			-moz-transition:  border-color 200ms, padding 200ms;
			-o-transition:  border-color 200ms, padding 200ms;
			transition:  border-color 200ms, padding 200ms;
			background:rgba(0,0,0,.55);
			line-height:210px;
			width:210px;
		}
		a.boxbtn:hover {
			/*padding:2em 4.2em;*/
			border-color:rgba(255,255,255,.5);	
		}
		.awesome-title {
			position:absolute;
			z-index:4;
			font-size:12px;
			color:#fff;
			left:1%;
			bottom:1%;
		}
		.awesome-title h2,
		.awesome-title h3,
		.awesome-title p {
			font-size:9px;
			text-transform:uppercase;
			color:#eee;
		}
	</style>