<?php 
// Determine Https or Http
if($prod){
	$protocol = 'https://';
}
// Get Member Info if Authenticated.
if ($memberid){
	
	$result = $db->prepare("SELECT * FROM members WHERE member_id = :memberid LIMIT 1");
	$result->bindValue(':memberid', $memberid);
	$result->execute();
	$member = $result->fetch();
	$profile_thumb_url = $member['profile_img'] ? $protocol.SITE_URL."/uploads/".$member['member_id']."/thumbnail/".$member['profile_img'] : "images/icon-profile-thumb.jpg";
}

?>
<!doctype html>
<html ng-app>
<head>
<base href="<?php echo $protocol . $host?>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="google-site-verification" content="vbePibOHwkxFxTpAcYC9Pg18hOUUt0Qu1x4neF9hN6Q" />
<meta name="description" content="<?php if(isset($page_desc)) {echo $page_desc;} else {echo "Creating and managing your free portfolio website has never been easier. Portfolio Lounge has helped many illustrators, graphic designers, photographers, and more create free  portfolio websites.";}?>" />
<title><?php if(isset($page_title)) {echo $page_title;} else {echo "Create an online portfolio.";}?></title>
<link rel="icon" type="image/png" href="/images/portfoliolounge_favicon_1.png" />
<link href='<?=$protocol?>fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/styles_v2.css" />

<meta property="og:url" content="http://portfoliolounge.com" />
<meta property="og:type" content="product" />
<meta property="og:title" content="Make a Portfolio Website" />
<meta property="og:description" content="Stress free." />
<meta property="og:image" content="http://portfoliolounge.com/images/portfoliolounge-banner.jpg"/>

<script src="js/libs/angular.min.js"></script>
<script src="js/libs/jquery-1.8.3.min.js"></script>
<script src="js/fb-app.js"></script>
<script src="js/libs/jquery.ui.widget.js"></script>
<script src="js/libs/jquery.cycle.all.min.js"></script>
<script src="js/libs/jquery.easing.1.3.js"></script>
<script src="js/libs/jquery.fileupload.js"></script>
<?php if(isset($member) && !$member['admin']){?><script src="js/libs/analytics.js"></script><?php } ?>
<!--
<script src="js/libs/analytics-only-pl.js"></script>
-->
<script src="js/global.js"></script>


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '1401288196554348');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1401288196554348&ev=PageView&noscript=1"
/></noscript>
</head>
<body>
<?php if(isset($member) && $member['admin']){include 'todo/todo.php';}?>
<div id="fb-root"></div>

	<?php if(isset($member) && $member['admin']==1){?>
	<?php include "owner-nav.php"; ?>
	<?php } ?>
	
<div id="wrapper">
<div id="inner-wrapper">
<header>
	<nav>
	<?php if($expload_url[0]=='portfoliolounge'){?>
		<a id="logo" title="Create Your Free Online Portfolio!" href="/"><img width="198" alt="Create Your Free Online Portfolio!" src="images/logo-retina.png" /></a>
	<?php } else { ?>
		<a id="logo" href="/"><img width="160" alt="Create Your Free Online Portfolio!" src="affiliates/images/doner.png" /></a>
		<h2 style="float:left;font-size:9px;color:#888;text-transform:uppercase;letter-spacing:1px;position:absolute;top:30px;left:210px;">Portfolio Manager</h2>
	<?php } ?>
	<?php if($expload_url[0]=='portfoliolounge'){?>
	<ul class="nav left">
		<li><a <?php if($p=="browse-portfolios"){echo 'class=cur';}?>  href="browse-portfolios">Browse Portfolios</a></li>
		<li><a <?php if($p=="gallery"){echo 'class=cur';}?>  href="gallery">Featured Work</a></li>
		<?php if(!$memberid){?>
		<li><a class="signuplink <?php if($p=="signup"){echo 'cur';}?>" href="signup">Create a Portfolio</a></li>
		<?php } ?>
	</ul>
	<?php } ?>
	<?php if ($memberid){?>
		<a id="member-thumb" title="<?php echo $member['username'];?>" href="admin/account/profile" style="background-image:url(<?php echo $profile_thumb_url;?>);"></a>
		<ul class="nav right">
			<li><a <?php if($p=="admin"){echo 'class=cur';}?> title="Make changes to your online portfolio." href="admin/account/profile"><?php echo $member['username'];?></a></li>
			<li><a class="logout" href="/function/logout.php">Logout</a></li>
		</ul>
	<?php } else { ?>
		<ul class="nav right">
			<li>
				<form id="login" method="post" action="function/login.php">
					<input name="username" placeholder="Username" />
					<input name="password" placeholder="Password" type="password" />
					<input class="send" type="submit" value="Login"/>
				</form>
			</li>
		</ul>
	<?php } ?>
	</nav>
</header>