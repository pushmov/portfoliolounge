<?php

if($prod){
	$protocol = 'https://';		
}
if ($memberid){
	
	$result = $db->prepare("SELECT * FROM members WHERE member_id = :memberid LIMIT 1");
	$result->bindValue(':memberid', $memberid);
	$result->execute();
	$member = $result->fetch();
	$profile_thumb_url = $member['profile_img'] ? $protocol."portfoliolounge.com/uploads/".$member['member_id']."/thumbnail/".$member['profile_img'] : "images/icon-profile-thumb.jpg";
}

?>
<!doctype html>
<html ng-app>
<head>
<base href="<?php echo $protocol . $host?>">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="google-site-verification" content="vbePibOHwkxFxTpAcYC9Pg18hOUUt0Qu1x4neF9hN6Q" />
<meta name="description" content="<?php if($page_desc) {echo $page_desc;} else {echo "Creating and managing your free portfolio website has never been easier. Portfolio Lounge has helped many illustrators, graphic designers, photographers, and more create free  portfolio websites.";}?>" />
<title><?php if($page_title) {echo $page_title;} else {echo "Create an online portfolio.";}?></title>
<link rel="icon" type="image/png" href="/images/portfoliolounge_favicon_1.png" />
<link href='<?=$protocol?>fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="css/reset.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="css/styles.css" />
<link rel="image_src" href="portfolio/images/facebook-thumb-portfolio.jpg" />
<meta property="og:image" content="portfolio/images/facebook-thumb-portfolio.jpg"/>
<script src="js/libs/angular.min.js"></script>
<script src="js/libs/jquery-1.8.3.min.js"></script>
<script src="js/libs/jquery.ui.widget.js"></script>
<script src="js/libs/jquery.cycle.all.min.js"></script>
<script src="js/libs/jquery.easing.1.3.js"></script>
<script src="js/libs/jquery.fileupload.js"></script>
<script src="js/libs/analytics.js"></script>
<script src="js/libs/analytics-only-pl.js"></script>
<script src="js/global.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1401288196554348');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1401288196554348&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>

<div id="fb-root"></div>

	<?php if($member['admin']==1){?>
	<?php include "owner-nav.php"; ?>
	<?php } ?>
	
<div id="wrapper">
<div id="inner-wrapper">

	
	<div class="strategic">
		<h1>
			Create a portfolio website.
		</h1>
		<h2>
			Build your online portfolio quickly and easily.
		</h2>
	</div>



<header>
	<nav>

	<a id="logo" title="Create Your Free Online Portfolio!" href="/"><img alt="Create Your Free Online Portfolio!" src="images/logo2.png" /></a>
	
	
	<ul class="nav left">
		<li><a <?php if($p=="browse-portfolios"){echo 'class=cur';}?>  href="browse-portfolios">Browse Portfolios</a></li>
		<!--<li><a title="Featured portfolios websites" <?php if($p=="featured-portfolios"){echo 'class=cur';}?>  href="featured-portfolios">Favorite Portfolios</a></li>-->
		<!--<li><a title="Featured portfolios websites" <?php if($p=="about-portfolio-lounge"){echo 'class=cur';}?>  href="about-portfolio-lounge">About Our Product</a></li>-->
		<!--
		<li><a <?php if($p=="about-portfolio-lounge"){echo 'class=cur';}?>  href="about-portfolio-lounge">Lear</a></li>
		<li><a <?php if($p=="about-us"){echo 'class=cur';}?>  href="about-us">Our Co.</a></li>
		-->
		<?php if(!$memberid){?>
		<li><a class="signuplink <?php if($p=="signup"){echo 'cur';}?>" href="signup">Create a Portfolio</a></li>
		<?php } ?>
	</ul>
	
	
	<?php if ($memberid){?>		
	
		<a id="member-thumb" title="<?php echo $member['username'];?>" href="admin/account/profile" style="background-image:url(<?php echo $profile_thumb_url;?>);"></a>
		<ul class="nav right">
			<li><a <?php if($p=="admin"){echo 'class=cur';}?> title="Make changes to your online portfolio." href="admin/account/profile"><?php echo $member['username'];?></a></li>
			<li><a class="logout" href="/function/logout.php">Logout</a></li>
		</ul>	
	
	<?php } else { ?>
	
		<ul class="nav right">
			<li>
				<form id="login" method="post" action="function/login.php">
					<input name="username" placeholder="Username" />
					<input name="password" placeholder="Password" type="password" />
					<input class="send" type="submit" value="Login"/>
				</form>
			</li>
		</ul>
		
	<?php } ?>
	
	</nav>
</header>