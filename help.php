<div class="center-title-block">
    <h2>Forgot your stuff?</h2>
    <h3>Don't worry we can help.</h3>
</div>
<div class="container">
	<div class="content">
		<div class="inner">
			<form class="smart_form" id="help">
				<label>What is your email address?</label>
				<input class="input email" name="email" ng-model="email" data-original="<?php echo $member['email'];?>" />
				<input class="btn smart_btn" type="submit" value="Send me my info!">
			</form>
		</div>
	</div>
</div>