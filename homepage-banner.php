<link rel="stylesheet" type="text/css" href="css/phone-banner.css" />
 <!-- portfolio begins -->
    <section id="page-portfolio">
      <div class="container">
        <div class="row">
          <div class="col-md-4 col-xs-4" id="iphone-row">
            <div id="iphone-container">
              <img src="img/iphone.png" id="iphone">
              <div id="carousel-a" class="carousel slide carousel-sync">
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="images/iphone/1.jpg">
                  </div>
                  <div class="item">
                    <img src="images/iphone/2.jpg">
                  </div>
                  <div class="item">
                    <img src="images/iphone/3.jpg">
                  </div>
                  <div class="item">
                    <img src="images/iphone/4.jpg">
                  </div>
                  <div class="item">
                    <img src="images/iphone/5.jpg">
                  </div>
                  <div class="item">
                    <img src="images/iphone/6.jpg">
                  </div>
                </div>

                <a class="left carousel-control" href="#carousel-a" data-slide="prev">
                  <img src="img/chevron-left.png" />
                </a>
                <a class="right carousel-control" href="#carousel-a" data-slide="next">
                  <img src="img/chevron-right.png" />
                </a>
              </div>
            </div>
          </div>
          <div class="col-md-8 col-xs-8" id="portfolio-right-column">
            <h3>Check out some of<br /> our favorites!</h3>
            <div id="slider1">
              <span class="left buttons prev" href="#" id="chevron-left">
                <img src="img/chevron-left.png" />
              </span>
              <span class="right buttons next" href="#" id="chevron-right" >
                <img src="img/chevron-right.png" />
              </span>
              <div class="viewport">
                <ul class="overview">
                  <li data-order="6"><img src="img/portfolio/thumbs/p6.jpg" /></li>
                  <li data-order="1" class="active"><img src="img/portfolio/thumbs/p1.jpg" /></li>
                  <li data-order="2"><img src="img/portfolio/thumbs/p2.jpg" /></li>
                  <li data-order="3"><img src="img/portfolio/thumbs/p3.jpg" /></li>
                  <li data-order="4"><img src="img/portfolio/thumbs/p4.jpg" /></li>
                  <li data-order="5"><img src="img/portfolio/thumbs/p5.jpg" /></li>
                </ul>
              </div>
            </div>
            <div id="portfolio-get-started">
              <a href="<?=$protocol.SITE_URL?>/browse-portfolios" class="promobtn"><span data-hover="Get Started ">See our favorites »</span></a>
            </div>

          </div>
        </div>
      </div>
    </section>
    <!-- portfolio ends -->
  
  <script>
  
  $('#page-portfolio').delay(800).animate({opacity:1},400);
  
  </script>
  
  
  <div class="clr"></div>
  
    <script src="js/libs/bootstrap.min.js"></script>
    <script src="js/libs/jquery.tinycarousel.js"></script>
  <script type="text/javascript" src="js/homepage.js"></script>