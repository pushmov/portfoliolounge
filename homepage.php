<style>
header {
  position:absolute;
    background:rgba(0,0,0,.67);
    z-index:999;
    width:100%;
}
#vbanner {
    top:0;
    z-index:9;
    background:#111;
    position:relative;  
    overflow:hidden;
    min-height:900px;
}
#vbanner video {
    //background:#000;
    min-height:900px;
    position:absolute;
    left:50%;
    margin-left:-800px;
    -moz-transition: height 0.55s ease-in-out, width 0.55s ease-in-out;
    -o-transition: height 0.55s ease-in-out, width 0.55s ease-in-out;
    -webkit-transition: height 0.55s ease-in-out, width 0.55s ease-in-out;
    transition: height 0.55s ease-in-out, width 0.55s ease-in-out;
}
#vbanner video.focus {
    width:800px;
    //left:10%;
    margin-left:-400px;
    bottom:180px;
    z-index:999999;
    min-height:initial;
    opacity:0;
}
#vbanner .overlay {
    position:absolute;
    bottom:0;
    right:0;
    top:0;
    left:0;
    background:rgba(0,0,0,0.4); 
}
#vbanner p.credits {
    position:absolute;
    bottom:10px;
    right:10px;
    left:10px;
    text-align:center;
    color:#777;
    color:rgba(255,255,255,.5);
    font-size:9px;
    text-transform: uppercase;
    z-index: 999;
    margin:0;
}
#vbanner p.credits a {
    color:#aaa;
    color:rgba(255,255,255,.3);
}
#vbanner p.credits a:hover {
    color:#aaa;
    color:rgba(255,255,255,.8);
}
/* Content
------------------------*/
.contents {
    position:absolute;
    width:100%;
    top:150px;
    height:460px;
    text-align:center;
    color:#fff;
    font-size:28px;
}
.contents h1 {
    font-size:38px;
    line-height: 1.3em;
        margin:5px;
}
.contents h2 {
    font-size:30px;
    line-height: 1.4em;
    color:rgba(255,255,255,.8);
        margin:0;
        font-size:24px;
}
.textBox .btn {
    border-radius: 2px;
    padding: 14px 0 16px 0;
    margin: 30px 0 0;
    background: rgba(26, 171, 255, 0.97);
    font-size: 22px;
    color: #fff;
    text-shadow:0 1px 13px rgba(0,0,0,.1);
    width:340px;
    padding-left:10px;
}
.textBox .btn:hover {
    opacity:.9;
}
/* Displays
------------------------*/
.contents .displays {
    width:700px;
    height:430px;
    margin:30px auto 0;
    position:relative;
}
    
/* Apple Products
------------------------*/
.contents .macbook {
    position:absolute;
    width:700px;
    left:50%;
    margin:0 0 0 -380px;
}
.macbook-screen {
    position: absolute;
    background: #000;
    width: 523px;
    height: 338px;
    /*left: 87px;
    top: 32px;*/
    left: 50%;
    top: 32px;
    margin-left:-293px;
    overflow:hidden;
}
.contents .iphone {
    position:absolute;
    width:160px;
    left:50%;
    margin:128px 0 0 200px;
    z-index:99;
}
.iphone-screen {
    position:absolute;
    background:#000;
    width: 118px;
    height: 208px;
    left:50%;
    margin:168px 0 0 220px;
    z-index:999;
    overflow:hidden;
}
.macbook-screen img,
.iphone-screen img {
    max-width:100%;
}
/* Slide Info
------------------------*/
.info {
    position:absolute;
    margin:0;
    bottom:60px;
    right:145px;
    background:rgba(0,0,0,.9);
    padding:2em 2em 1.4em 4em;
    color:#fff;
    line-height:1.8em;
    text-align: right;
}
.info li {
    list-style:none;
}
.info li a { 
    color:#888;
}
</style>
<div id="vbanner" class="co ntainer">
    
    
    <?php /* if($i){?>
    <video autoplay loop muted>
        <source src="img/andyluce-painting_2_1.mp4" type="video/mp4">
        <!--<source src="img/andyluce-painting.ogg" type="video/ogg">-->
    </video>
    <p class="credits"><a id="muteBtn">Show Video</a><!-- &nbsp; - &nbsp; <a target="_blank" href="http://andyluce.com">Artist: Andyluce.com</a>--></p>

     <?php } */ 
    
    $home_bg_stmt = $db->prepare("SELECT * FROM items WHERE featured_home = :featured ORDER BY RAND() LIMIT 1");
    $home_bg_stmt->bindValue(':featured', 1);
    $home_bg_stmt->execute();
    $home_bg = $home_bg_stmt->fetch();
    
    $home_bg_member_stmt = $db->prepare("SELECT * FROM members WHERE member_id = :member_id LIMIT 1");
    $home_bg_member_stmt->bindValue(':member_id', $home_bg['member_id']);
    $home_bg_member_stmt->execute();
    $home_bg_member = $home_bg_member_stmt->fetch();
    
    ?>
    
    
    <div class="frame home-bg" style="height:100%;width:100%;position:absolute;background:#222">
        <div class="canvas"
            data-source="uploads/<?=$home_bg["member_id"]?>/<?=$home_bg["item_filename"]?>"
            data-source="img/photos/portfolio-photo-studio.jpg"
            data-source="img/photos/corporate-architecture.jpg">
                <div class="in fo" style="position:absolute;bottom:0;left:0;border-radius:0;text-transform:uppe rcase;color:rgba(255,255,255,.8);z-index:1;font-size:11px;">
                     
                    <a title="<?=$home_bg_member['about']?>" target="_blank" href="https://<?=$home_bg_member['username']?>.portfoliolounge.com" style="
                        color: rgba(255, 255, 255, 0.5);
                        padding: 10px;
                        display: inline-block;
                        text-transform: uppercase;
                        letter-spacing: 1px;
                        font-size: 8px;">
                        Featured work by:
                        <?=$home_bg_member['first_name']?>
                        <?=$home_bg_member['last_name']?> 
                        <?php if(!$home_bg_member['first_name']&&!$home_bg_member['last_name']){
                            echo $home_bg_member['username'];
                         } ?>
                    </a>
                </div>
    <div class="overlay"></div>
        </div>
    </div>
    
  
    <div class="contents">
                <?php if($i=='architecture'){?>
                    <h1>Create an architecture portfolio.</h1>
                <?php } else if($i=='interior-design') { ?>
                    <h1>Create an interior design portfolio.</h1>
                <?php } else { ?>
        <h1>Create a portfolio website.</h1>
                <?php } ?>
        <h2>Quickly and Beautifully.</h2>
        <div class="displays">
            <img class="macbook" src="images/devices/macbook.png" />
            <img class="iphone" src="images/devices/iphone.png" />
            
            <!-------
            --------- MacBook
            -------->
            <div class="macbook-screen">
                <!-- wrapping divs for js cycle -->
                <a href="browse-portfolios"><img src="img/banner/v2_mistermissus.jpg"></a>
                <!--<a href="browse-portfolios"><img src="img/banner/v2_nestormora27.jpg"></a>-->
                <a href="browse-portfolios"><img src="img/banner/v2_crisneves.jpg"></a>
                <a href="browse-portfolios"><img src="img/banner/v2_cor.jpg"></a>
                <!--
                <a href="browse-portfolios"><img src="img/banner/v2_mhill.jpg"></a>
                -->
            </div>
            
            <!-------
            --------- iPhone
            -------->
            <div class="iphone-screen">
                <!-- wrapping divs for js cycle -->
                <a href="browse-portfolios"><img src="img/banner/v2_mistermissus_mobile.jpg"></a>
                <!--<a href="browse-portfolios"><img src="img/banner/v2_nestormora27_mobile.jpg"></a>-->
                <a href="browse-portfolios"><img src="img/banner/v2_crisneves_mobile.jpg"></a>
                <a href="browse-portfolios"><img src="img/banner/v2_cor_mobile.jpg"></a>
                <!--
                <a href="browse-portfolios"><img src="img/banner/v2_mhill_mobile.jpg"></a>
                -->
            </div>
        </div>
        <div class="textBox">
            <a href="signup" class="btn">Create your portfolio &raquo;</a>
        </div>
    </div>  
</div>
<script>
    
$('.macbook-screen').cycle({
    fx: 'scrollUp',
    speed:600,
    timeout:4000,
    easing: 'easeOutExpo',
    //random:1
});
    
$('.iphone-screen').cycle({
    fx: 'scrollUp',
    speed:600,
    timeout:4000,
    easing: 'easeOutExpo',
    //delay: 300
    //random:1
});
   
    
$("#muteBtn").click( function (){
    if( $("video").prop('muted')){
        //$(".contents").fadeOut();
        //$(".overlay").fadeOut();
        $("#vbanner video").stop().css({opacity:0});
        $("#vbanner video").addClass("focus");
        $("#vbanner video").stop().animate({opacity:1},700);
        //$("#vbanner video").animate({maxWidth:'80%',left:'10%',marginLeft:0});
        $("video").prop('muted', false);
    }
    else {
        //$(".contents").fadeIn();
        //$(".overlay").fadeIn();
        $("#vbanner video").stop().css({opacity:0});
        $("#vbanner video").removeClass("focus");
        $("#vbanner video").stop().animate({opacity:1},1000);
        //$("#vbanner video").animate({maxWidth:'10000px',left:'50%',marginLeft:-600});
        $("video").prop('muted', true);
    }
});
    
    
</script>

<div id="testimonials">
    <a title="Check out the featured portfolios!" href="featured-portfolios"><img height="80" alt="award winning portfolio website" src="images/large-certificate.png" /></a>
    <h2>Show your work to the world. It's easy!</h2>
    <!--
    <small style="color:#777;display:inline-block" class="pad-sm"><a style="color:#aaa;" title="Check em' out!" href="http://www.instagram.com/mattvisk"><img style="position:absolute;margin:-4px 0 0 -20px;" width="16" src="img/instagram-icon.png" />Matt Visk</a> ~ Founder</small>-->
    <ul class="testimonial-list">
        <li>"Portfolio Lounge is awesome! It so easy to use. I've searched and searched for something like this for quite some time. Thanks Portfolio Lounge!"</li>
        <li>"An awesome way to show off your stuff. I'm so glad I found Portfolio Lounge. My online portfolio has never looked this good!"</li>
        <li>"Great website! Easy to use and very clean and professional portfolios. I've never thought it would be this easy to create a portfolio for my graphic design work."</li>
        <li>"Portfolio Lounge is a really user-friendly and well designed. An awesome portfolio service your going to want to take advantage of."</li>
        <li>"Best portfolio website yet! It's perfect for cranking out an awesome portfolio. I cannot wait to show off my stuff."</li>
        <li>"The most beautiful online portfolio I've ever laid my eyes on. Keep up the good work guys!"</li>
        <li>"I have no coding or website experience and I was able to create my online portfolio quickly and beautifully just like they said!"</li>
        <li>"Best portfolio website I've ever setup. Thanks PL for making my professional life a little better."</li>
        <li>"What a product! Quick editing, fantastic UI and has all the tools to let you create and market a beautiful portfolio."</li>
        <li>"Portfolio Lounge is really easy to use. I can't believe I haven't heard of this until now. My html portfolio was aweful, thanks portfolio lounge!"</li>
        <li>"I'd like to say that portfolio lounge really is a one of a kind service. I've tried countless portfolio sites, and this is the best so far."</li>
        <li>"My photography portfolio has never been better! Great work on the administrative section it's so easy to manage and the outcome is spectacular!"</li>
        <li>"I can't wait to share my portfolio. It looks super clean and professional. Thanks portfolio lounge!"</li>
        <li>"Super nice solution for building an online portfolio. Helped me start my career!"</li>
        <li>"I love portfolio lounge. Thank you so much and keep up the great work! Looking forward to more updates as well!"</li>
        <li>"Made my life much easier. I am a photographer who was in need of a simple, clean portfolio. I found it and then some. Great work guys!"</li>
        <li>"Fantastic portfolio building website! Keep up the good work portfolio lounge team!"</li>
        <li>"Very easy to work with! I'm so glad my friend told me about portfolio lounge. It really helped me out."</li>
        <li>"Portfolio lounge rocks! I have been looking for a portfolio website to help me show off my illustrations. I'm really happy with your website!"</li>
        <li>"It took me a few minutes to get an awesome portfolio and I dont regret one second of it. Great application portfolio lounge team!"</li>
        <li>"Super nice site! Made it pretty painless to create my online portfolio."</li>
        <li>"I suggest portfolio lounge to all my creative friends. They love it as well. This portfolio website is amazing!"</li>
        <li>"I started graphic design a few years ago, and portfolio lounge really helped get my work online. It's so much easier than I thought to have my own website."</li>
        <li>"Definately love portfolio lounge. Super clean design, and an overall amazing portfolio experience."</li>
        <li>"Greatest portfolio website I have come across! It really made my day. It's so easy to use, my grandma could probably whip up a nice portfolio website!"</li>
        <li>"Portfolio lounge is super simple. My portfolio is looking fantastic. Thanks portfolio lounge team!!!"</li>
        <li>"Awesome job on the site! Really made it simple to create portfolios quickly and make them look super professional."</li>
        <li>"My portfolio rocks! Thanks portfolio lounge. You guys made it almost too easy! Looks great too!"</li>
        <li>"I've been trying to put together a decent portfolio for years. Finally, something saved me... Portfolio lounge is a very nice site."</li>
        <li>"I wanted to create portfolios like this on my own, but I'm not sure how to code them and they were difficult. Thanks portfolio lounge!!!"</li>
        <li>"Fabulous site! We are loving portfolio lounge! Our photography business was in need of a great portfolio website, and we got one! Thank you portfolio lounge!"</li>
    </ul>
    <!--
    <small>&#8210; Thanks to our members</small>  -->
    
</div>


<?php include 'gallery.php';?>






<!--
<div style="    background: #eee;
    padding:4px 0 0 0;
    line-height:50px;
    text-align: center;
    font-size: 18px;
    position: absolute;
    border-radius: 20px 20px 0 0;
    margin: -54px 0 0 -180px;
    width: 360px;
    left: 50%;" class="font">
    <strong>15,517</strong> portfolios and counting!
</div>
-->



<div class="press" style="
    background:#fff;
    text-align:center;
    color:#bbb;
    padding:100px 3% 100px;">
    
    
    <h3 style="text-transform:uppercase;font-size:9px 0;margin:0;letter-spacing:2px;">Featured on</h3>
    <a style="width:192px;background-position:-892px 0;" href="http://mashable.com"></a>
    <a style="width:94px;background-position:-34px 0;" href="http://toolkits.io"></a>
    <a style="width:54px;background-position:-1128px 0;" href="http://crazyleafdesign.com"></a>
    <a style="width:206px;background-position:-156px 0;" href="http://designinstruct.com"></a>
    <a style="width:192px;background-position:-400px 0;" href="http://inspirationfeed.com"></a>
    <a style="width:200px;background-position:-645px 0;" href="http://detroit-photography-school.com"></a>
    
</div>
<style>
    
    .press a {
        display:inline-block;
        margin:0 20px;
        background:url(img/press-icons.png);
        height:100px;
    }
    .press a:hover {
        opacity:.9; 
    }
</style>


<div class="container" id="home">
    <div id="stories">
        <article>
            <img height="50" src="images/icon-large-compass.png" />
            <h2>User-Friendly Portfolio Builder</h2>
            <p>Building your portfolio website should be just as fun as creating the work you put into it. We made it easier than ever to create your portfolio with Portfolio Lounge. Simply upload your work, and you'll be ready to show off your new portfolio website to the world!
        </article>
        
        <article>
            <img height="50" src="images/icon-large-customize.png" />
            <h2>Customize Your Portfolio</h2>
            <p>Portfolio Lounge lets you customize your portfolio website in many ways. It's simple! All it takes is a few clicks, and you will be customizing your portfolio's fonts, colors, logo and more. Create your portfolio website to be as unique as you are! <a title="Customize your portfolio website" href="signup">Get started!</a></p>
        </article>
        
        <article>
            <img height="50" src="images/icon-large-macbook.png" />
            <h2>Choose Portfolio Layout</h2>
            <p>Try out Portfolio Lounge's variety of templates and page layouts on your portfolio website. You don't have to worry about resizing your work to fit new templates. We make sure each template works seamlessly with your work. <a href="signup">Sign up and choose your own layout.</a></p>
        </article>
        
        <article>
            <img height="50" src="images/icon-large-lightbulb.png" />
            <h2>No HTML Needed</h2>
            <p>You don't need to be a web developer or designer to create your portfolio. We made our portfolio building process extremely user friendly without any coding necessary. Simply upload and arrange your work to your portfolio, and you're done! <a title="Create your portfolio easily, with portfolio lounge." href="signup">Create your clean, simple portfolio with ease.</a></p>
        </article>
        
        <article>
            <img height="50" src="images/icon-large-stats.png" />
            <h2>Track Portfolio Analytics</h2>
            <p>If you're wondering how many people are viewing your online portfolio, we have the answer! Your portfolio is tied into your Google analytics account which gives a ton of useful information about the traffic that reaches your portfolio. <a href="signup">Create your portfolio</a> and start tracking your portfolio's analytics today.</p>
        </article>
        
        <article>
            <img height="50" src="images/icon-large-www.png" />
            <h2>Custom Portfolio Domain</h2>
            <p>Your domain is what people use to get to your portfolio directly. When you create a portfolio with Portfolio Lounge, you get a custom subdomain for free, such as <em>john.portfoliolounge.com</em>. You can also map your domain to your online portfolio in just a few simple steps. <a href="signup">Sign up for free.</a></p>
        </article>
        
        <article>
            <img height="50" src="images/icon-large-piechart.png" />
            <h2>SEO Optimized Templates</h2>
            <p>All of our portfolio templates are SEO optimized to make sure your portfolio has a great chance at ranking well in major search engines like Google, Yahoo and Bing. The Portfolio Lounge team has been working to enhance your portfolio. Take advantage of our portfolio service and <a href="signup">create one today.</a></p>
        </article>
        
        <article>
            <img height="50" src="images/icon-large-envelope.png" />
            <h2>Friendly Support</h2>
            <p>If you ever have any questions, or need help with your portfolio, our portfolio designers and developers are always available to help you with your online portfolio. Feel free to contact the Portfolio Lounge team at any time. We will do our best to keep you headed in the "perfect portfolio" direction.</p>
        </article>
        
        <!--
        <article>
            <img height="50" src="images/icon-large-group.png" />
            <h2>Join the Club</h2>
            <p>We have thousands of portfolio lounge members. Feel free to check out their work. Our diverse users come from far and wide to create their portfolios here. Join them and get featured on the homepage at your request! Can't wait to see your work. <br /><a href="browse-portfolios">Check out the portfolios.</a></p>
        </article>
        -->
        
        <article>
            <img height="50" src="images/icon-video.png" />
            <h2>Add Videos!</h2>
            <p>Easily add videos from sites like Youtube &amp; Vimeo to your portfolio website. Your users will see an image of your choosing with an elegant play button overlaying. Once they click, we load a video player smoothly into the center of the screen. Your viewers will be fully immersed in your video content!<br /><a title="Create your portfolio easily, with portfolio lounge." href="signup">Create your portfolio!</a></p>
        </article>
        
        <div class="clr"></div>
    </div>
</div>


<!-- Portfolio Lounge Display -->
<div class="graphic-display" style="
    background:#fff url(img/portfolio-lounge-display.jpg) bottom right no-repeat;
    background-size:80%;                                                    
    padding:0 0 0 0;
    height:700px;">
    
    <div class="text" style="
                                                
    padding:100px 10%;">

        <h2 style="font-size:3.2em;">
            We make things easy.
        </h2>
        <h3>
            Create your portfolio here.
        </h3>
        <br>
        <a title="Get started on your portfolio!" href="signup" class="btn gray">Get started &raquo;</a>
    </div>
</div>

<style>
    @media (min-width:700px){
        .graphic-display {
            
        }
    }


</style>


<?php //  include 'homepage-banner.php';?>


<div class="container">
    <div id="skinny">
        <article>
            <h2>Create your portfolio website with Portfolio Lounge.</h2>
            <img src="images/story-create-portfolio-easily.jpg" />
            <p>Portfolio Lounge helps you create, manage, and perfect your online portfolio website. Creating your online portfolio will be easier than ever. Our team has provided one of the best portfolio builder experiences online. Portfolio Lounge has a user-friendly approach when it comes to creating your portfolio. Your portfolio will look clean and professional within minutes without having to write any HTML or other programming languages.</p>
        </article>
        <article>   
            <img class="left" src="images/story-beautifully-designed-portfolios.jpg" />
            <h2 class="left">We've helped thousands build their online portfolio.</h2>
            <p class="left">Our portfolios are used by many different types of creatives.  It doesn't matter if you're a designer, illustrator, photographer, interior designer, or an architect. Portfolio Lounge is by far, the easiest way to create an amazing portfolio. <a href="signup">Signup today</a> to create your free portfolio website and start customizing your portfolio to the way you want it. We can't wait to see your personal portfolio!</p>
            
        </article>
        <article>
            <h2>Your online portfolio, made easy.</h2>
            <img src="images/story-no-coding-for-your-portfolio.jpg" />
            <p>Portfolio Lounge is a great tool to help you show off your work. We take care of all the hosting and make sure our portfolios are kept safe and reliable. There's no need to worry about the nerdy work. We've taken care of everything for you! Your portfolio will turn out to be very professional with only a small amount of effort, so you can work on the things that really matter - your work! Try out Portfolio Lounge, and see your work hit the web with full force! <a href="signup">Create your online portfolio.</a></p>
        </article>
        <div class="clr" style="text-align:center;">
        
        <a href="signup" class="promobtn">Get Started On Your Portfolio Website &raquo;</a>
        <br /><br /><br /><br /><br /><br />
        </div>
        
        
        
        <br />

            <div class="full_content">

                <h3>Creating Unique Online Portfolios with Portfolio Lounge</h3>
                <p>Portfolio Lounge is your one-stop, easy solution for all of your online portfolio needs.  With our user-friendly interface, Portfolio Lounge offers the tools you need to create a stunning online portfolio, even if you lack any HTML programming skill.</p>
                
                
                <h3>Online Portfolios without the Hassle</h3>
                <p>Regardless of what you create, Portfolio Lounge offers the best and simplest option for managing your work.  As a designer or artist of any type, it's important to create your online portfolio with individual flair.  Design your portfolio to your own standards; create your own domain to showcase your talent in a simple, elegant, unique way.</p>
                
                
                <h3>Our Services</h3>
                <p><strong>Unique Design</strong> Easily customizable options for layouts, fonts and everything in between mean that your portfolio will be as unique as your work. Reinvent your portfolio using fully-customizable, professionally-designed templates that are easy to use and don't distract from the power and quality of your work.</p>
                <p><strong>No-Cost Portfolio Options</strong> We offer a free version of our services so that you can try our offerings for yourself; some limitations apply.</p>
                <p><strong>Ample Storage Space</strong> High-resolution images and source files can take up a lot of space; your portfolio has plenty of room to store it all.</p>
                <p><strong>Customized Domain</strong> Choose your own web address using our format, or create a unique map for your own domain and create a portfolio free of any outside branding.</p>
                <p><strong>Mobile and Retina Display Optimization</strong> When viewed on a mobile or Retina display, your portfolio will look its absolute best.  Devices and displays are automatically detected and your portfolio is optimized for whatever device it's viewed on, from iPhone to Android and more.</p>
                
            </div>
            <div class="full_content small">
                
                <h3>The Right Tools</h3>
                <p>Portfolio Lounge provides every tool you need to create a stunning online portfolio. With our editing options and fast-growing set of features, your work can be published quickly and without hassle.</p>
                
                
                
                <h3>User-Friendly Options</h3>
                <p>Creating your portfolio is as easy as clicking a few options and uploading your content; everything else is handled for you.</p>
                
                
                
                
                
                <h3>Analytics</h3>
                <p>Google Analytics can be added to your portfolio so that you can keep track of where your visitors are coming from and who they are.</p>
                
                
                
                
                <h3>No Bandwidth Restrictions</h3>
                <p>No matter how much attention your portfolio attracts, you'll never be saddled with penalties or limits on bandwidth.  Your portfolio will always be displayed in an exemplary manner.</p>
                
                
                <h3>Search Engine Optimization (SEO)</h3>
                <p>If your portfolio doesn't turn up on search engines, no one is going to know it's there.  Our templates are optimized for visibility on search engines such as Google, Bing and others.</p>
                
                
                <h3>No-Hassle Hosting</h3>
                <p>Top-tier dedication and enterprise-grade encrypted servers provide the best protection against outside threats.  Your works, and our servers, are always safe.</p>
                
                <h3>Unmatched Support</h3>
                <p>We provide unmatched support and a friendly, knowledgeable staff to assist you, regardless of your needs.  You can be assured of fast, friendly service.<br /><br />When you're looking to showcase your work in a professional and unique manner, you can count on Portfolio Lounge to meet your needs.  Our services do not require any downloads or installations and you'll never have the hassle of dealing with programming glitches, hosting issues, optimizing your portfolio for search engines or any of the other common problems with online portfolios.  Our secure, reliable services allow you to focus on displaying your work in a way that is simple, easy and completely unique.  There's no reason to wait; start your online portfolio today!</p>
                
            </div>
            <div class="clr"></div>
        </div>
    </div>
    
</div>




<!-- end testimonials -->
<script>
$('#screenshots').cycle({
    fx: 'fade',
    speed:800,
    timeout:3000
});
$('#testimonials ul').cycle({
    fx: 'scrollUp',
    speed:800,
    timeout:4000,
    easing: 'easeOutExpo',
    random:1
});
</script>






<!--

<div id="floating_header">
    <div id="logo_new">
        <img width="200" style="position:absolute;top:20px;" src="images/logo-retina.png">
    </div>
</div>


<div id="slideshow">
    
    <article>
        <h2>
            Create your portfolio website.
        </h2>
        <h3>
            Personal portfolios up to full scale business designs.
        </h3>
        <a class="btn">Upload your photos and begin &raquo;</a>
    </article>
    <img src="http://khoov.com/photos/services/studio-girl.jpg">
    
</div>
-->
