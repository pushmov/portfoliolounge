

<div id="masthead" class="container">
	<div id="banner">
		<div id="banner-titles">
			<h1>Create a portfolio website.<br /><strong>Stress free.</strong></h1>
		</div>
		<div id="screenshots">
			<a href="signup"><img alt="View Katie's Online Portfolio" src="img/banner/mistermissus.png" /></a>
			<a href="signup"><img alt="View Cris's Online Portfolio" src="img/banner/crisneves.png" /></a>
			<a href="signup"><img alt="View Kahlilangeles's Online Portfolio" src="img/banner/justanpeterson.png" /></a>
			<a href="signup"><img alt="View jacksonemills's Online Portfolio" src="img/banner/jacksonemills.png" /></a>
			<a href="signup"><img alt="View Kahlilangeles's Online Portfolio" src="img/banner/kahlilangeles.png" /></a>
			<a href="signup"><img alt="View Enrique's Online Portfolio" src="img/banner/enrique.png" /></a><!--
			<a target="_blank" rel="nofollow" href="http://www.crisneves.portfoliolounge.com"><img alt="View Cris Neves' Online Portfolio" src="images/masthead-screenshot-crisneves.png" /></a>
			<a target="_blank" rel="nofollow" href="http://www.enrique.portfoliolounge.com"><img alt="View Enrique's Online Portfolio" src="images/masthead-screenshot-enrique.png" /></a>
			<a target="_blank" rel="nofollow" href="http://www.jtat.portfoliolounge.com"><img alt="View Jtat's Online Portfolio" src="images/masthead-screenshot-jtat.png" /></a>-->
		</div>
		<div class="cta">
			<a title="Create an online portfolio!" class="promobtn" href="signup">Create your portfolio &raquo;</a>
		</div>
	</div>
</div>






<div id="testimonials">
	<a title="Check out the featured portfolios!" href="featured-portfolios"><img alt="award winning portfolio website" src="images/large-certificate.png" /></a>
    <h2>The easiest way to create and manage your portfolio website.</h2>
	<!--
	<small style="color:#777;display:inline-block" class="pad-sm"><a style="color:#aaa;" title="Check em' out!" href="http://www.instagram.com/mattvisk"><img style="position:absolute;margin:-4px 0 0 -20px;" width="16" src="img/instagram-icon.png" />Matt Visk</a> ~ Founder</small>-->
    <ul class="testimonial-list">
        <li>"Portfolio Lounge is awesome! It so easy to use. I've searched and searched for something like this for quite some time. Thanks Portfolio Lounge!"</li>
        <li>"An awesome way to show off your stuff. I'm so glad I found Portfolio Lounge. My online portfolio has never looked this good!"</li>
        <li>"Great website! Easy to use and very clean and professional portfolios. I've never thought it would be this easy to create a portfolio for my graphic design work."</li>
        <li>"Portfolio Lounge is a really user-friendly and well designed. An awesome portfolio service your going to want to take advantage of."</li>
        <li>"Best portfolio website yet! It's perfect for cranking out an awesome portfolio. I cannot wait to show off my stuff."</li>
        <li>"The most beautiful online portfolio I've ever laid my eyes on. Keep up the good work guys!"</li>
        <li>"I have no coding or website experience and I was able to create my online portfolio quickly and beautifully just like they said!"</li>
        <li>"Best portfolio website on the web! Thanks PL for making my life a little better."</li>
        <li>"What a product, quick editing, fantastic UI and has all the tools to let you create a beautiful portfolio with ease."</li>
        <li>"Portfolio Lounge is really easy to use. I can't believe I haven't heard of this until now. My html portfolio was aweful, thanks portfolio lounge!"</li>
        <li>"I'd like to say that portfolio lounge really is a one of a kind service. I've tried countless portfolio sites, and this is the best so far."</li>
        <li>"My photography portfolio has never been better! Great work on the administrative section it's so easy to manage and the outcome is spectacular!"</li>
        <li>"I can't wait to share my portfolio. It looks super clean and professional. Thanks portfolio lounge!"</li>
        <li>"Super nice solution for building an online portfolio. Helped me start my career!"</li>
        <li>"I love portfolio lounge. Thank you so much and keep up the great work! Looking forward to more updates as well!"</li>
        <li>"Made my life much easier. I am a photographer who was in need of a simple, clean portfolio. I found it and then some. Great work guys!"</li>
        <li>"Fantastic portfolio building website! Keep up the good work portfolio lounge team!"</li>
        <li>"Very easy to work with! I'm so glad my friend told me about portfolio lounge. It really helped me out."</li>
        <li>"Portfolio lounge rocks! I have been looking for a portfolio website to help me show off my illustrations. I'm really happy with your website!"</li>
        <li>"It took me a few minutes to get an awesome portfolio and I dont regret one second of it. Great application portfolio lounge team!"</li>
        <li>"Super nice site! Made it pretty painless to create my online portfolio."</li>
        <li>"I suggest portfolio lounge to all my creative friends. They love it as well. This portfolio website is amazing!"</li>
        <li>"I started graphic design a few years ago, and portfolio lounge really helped get my work online. It's so much easier than I thought to have my own website."</li>
        <li>"Definately love portfolio lounge. Super clean design, and an overall amazing portfolio experience."</li>
        <li>"Greatest portfolio website I have come across! It really made my day. It's so easy to use, my grandma could probably whip up a nice portfolio website!"</li>
        <li>"Portfolio lounge is super simple. My portfolio is looking fantastic. Thanks portfolio lounge team!!!"</li>
        <li>"Awesome job on the site! Really made it simple to create portfolios quickly and make them look super professional."</li>
        <li>"My portfolio rocks! Thanks portfolio lounge. You guys made it almost too easy! Looks great too!"</li>
        <li>"I've been trying to put together a decent portfolio for years. Finally, something saved me... Portfolio lounge is a very nice site."</li>
        <li>"I wanted to create portfolios like this on my own, but I'm not sure how to code them and they were difficult. Thanks portfolio lounge!!!"</li>
        <li>"Fabulous site! We are loving portfolio lounge! Our photography business was in need of a great portfolio website, and we got one! Thank you portfolio lounge!"</li>
    </ul>
	<!--
	<small>&#8210; Thanks to our members</small>  -->
	
</div>

<!--
<div style="    background: #eee;
    padding:4px 0 0 0;
	line-height:50px;
    text-align: center;
    font-size: 18px;
    position: absolute;
    border-radius: 20px 20px 0 0;
    margin: -54px 0 0 -180px;
    width: 360px;
    left: 50%;" class="font">
	<strong>15,517</strong> portfolios and counting!
</div>
-->


<div class="container" id="home">
	<div id="stories">
		<article>
			<img height="50" src="images/icon-large-compass.png" />
			<h2>User-Friendly Portfolio Builder</h2>
			<p>Building your portfolio website should be just as fun as creating the work you put into it. We made it easier than ever to create your portfolio with Portfolio Lounge. Simply upload your work, and you'll be ready to show off your new portfolio website to everybody!
		</article>
		
		<article>
			<img height="50" src="images/icon-large-customize.png" />
			<h2>Customize Your Portfolio</h2>
			<p>Portfolio Lounge lets you customize your portfolio website in many ways. It's simple! All it takes is a few clicks, and you will be customizing your portfolio's fonts, colors, logo and more. Create your portfolio website as unique as you are! <a title="Customize your portfolio website" href="signup">Get started!</a></p>
		</article>
		
		<article>
			<img height="50" src="images/icon-large-macbook.png" />
			<h2>Choose Portfolio Layout</h2>
			<p>Try out Portfolio Lounge's variety of templates and page layouts on your portfolio website. You don't have to worry about resizing your work to fit new templates. We make sure each template works seamlessly with your work. <a href="signup">Sign up and choose your own layout.</a></p>
		</article>
		
		<article>
			<img height="50" src="images/icon-large-lightbulb.png" />
			<h2>No HTML Needed</h2>
			<p>You do not need to be a web developer or designer to create your portfolio. We made our portfolio building process extremely user friendly without any coding necessary. Simply upload and arrange your work to your portfolio, and your done! Clean simple portfolio with ease. <a title="Create your portfolio easily, with portfolio lounge." href="signup">Create your portfolio with ease.</a></p>
		</article>
		
		<article>
			<img height="50" src="images/icon-large-stats.png" />
			<h2>Track Portfolio Analytics</h2>
			<p>If your wondering how many people are viewing your online portfolio, we have the answer! Your portfolio is tied into your google analytics account which gives a ton of useful information about the traffic that reaches your portfolio. <a href="signup">Create your portfolio</a> and start tracking your portfolio's analytics today.</p>
		</article>
		
		<article>
			<img height="50" src="images/icon-large-www.png" />
			<h2>Custom Portfolio Domain</h2>
			<p>Your domain is what people use to get to your portfolio directly. When you create a portfolio with Portfolio Lounge, you get a custom subdomain for free, such as <em>john.portfoliolounge.com</em>. You can also map your domain to your online portfolio in just a few simple steps. <a href="signup">Sign up for free.</a></p>
		</article>
		
		<article>
			<img height="50" src="images/icon-large-piechart.png" />
			<h2>SEO Optimized Templates</h2>
			<p>All of our portfolio templates are SEO optimized to make sure your portfolio has a great chance at ranking well in major search engines like Google, Yahoo and Bing. The Portfolio Lounge team has been working to enhance your portfolio. Take advantage of our portfolio service and <a href="signup">create one today.</a></p>
		</article>
		
		<article>
			<img height="50" src="images/icon-large-envelope.png" />
			<h2>Friendly Support</h2>
			<p>If you ever have any questions, or need help with your portfolio. Our portfolio designers and developers are always available to help you with your online portfolio. Feel free to contact the Portfolio Lounge team at any time. We will do our best to keep you headed in the "perfect portfolio" direction.</p>
		</article>
		
		<article>
			<img height="50" src="images/icon-large-group.png" />
			<h2>Join the Club</h2>
			<p>We have thousands of portfolio lounge members. Feel free to check out their work. Our diverse users come from far and wide to create their portfolios here. Join them and get featured on the homepage at your request! Can't wait to see your work. <br /><a href="browse-portfolios">Check out the portfolios.</a></p>
		</article>
		
		<div class="clr"></div>
	</div>
</div>



<?php include 'homepage-banner.php';?>






<div class="container">
	<hr />
	<div id="skinny">
		<article>
			<h1>Create your portfolio website with Portfolio Lounge.</h1>
			<img src="images/story-create-portfolio-easily.jpg" />
			<p>Portfolio Lounge helps you create, manage, and perfect your online portfolio website. Creating your online portfolio will be easier than ever. Our team has provided one of the best portfolio builder experiences online. Portfolio Lounge has a user-friendly approach when it comes to creating your portfolio. Your portfolio will look clean and professional within minutes without having to write any HTML or other programming languages.</p>
		</article>
		<article>	
			<img class="left" src="images/story-beautifully-designed-portfolios.jpg" />
			<h1 class="left">We've helped thousands build their online portfolio.</h1>
			<p class="left">Our portfolios are used by many different types of creatives.  It doesn't matter if you're a designer, illustrator, photographer, interior designer, or an architect. Portfolio Lounge is by far, the easiest way to create an amazing portfolio. <a href="signup">Signup today</a> to create your free portfolio website and start customizing your portfolio to the way you want it. We can't wait to see your personal portfolio!</p>
			
		</article>
		<article>
			<h1>Your online portfolio, made easy.</h1>
			<img src="images/story-no-coding-for-your-portfolio.jpg" />
			<p>Portfolio Lounge is a great tool to help you show off your work. We take care of all the hosting and make sure our portfolios are kept safe and reliable. There's no need to worry about the nerdy work. We've taken care of everything for you! Your portfolio will turn out to be very professional with only a small amount of effort, so you can work on the things that really matter - your work! Try out Portfolio Lounge, and see your work hit the web with full force! <a href="signup">Create your online portfolio.</a></p>
		</article>
		<div class="clr" style="text-align:center;">
		
		<a href="signup" class="promobtn">Get Started On Yours &raquo;</a>
		<br /><br /><br /><br /><br /><br />
		</div>
		
		
		
		<br />
		<br />
		<br />

			<div class="full_content">

				<h2>Creating Unique Online Portfolios with Portfolio Lounge</h2>
				<p>Portfolio Lounge is your one-stop, easy solution for all of your online portfolio needs.  With our user-friendly interface, Portfolio Lounge offers the tools you need to create a stunning online portfolio, even if you lack any HTML programming skill.</p>
				
				
				<h2>Online Portfolios without the Hassle</h2>
				<p>Regardless of what you create, Portfolio Lounge offers the best and simplest option for managing your work.  As a designer or artist of any type, it's important to create your online portfolio with individual flair.  Design your portfolio to your own standards; create your own domain to showcase your talent in a simple, elegant, unique way.</p>
				
				
				<h2>Our Services</h2>
				<p><strong>Unique Design</strong> Easily customizable options for layouts, fonts and everything in between mean that your portfolio will be as unique as your work. Reinvent your portfolio using fully-customizable, professionally-designed templates that are easy to use and don't distract from the power and quality of your work.</p>
				<p><strong>No-Cost Portfolio Options</strong> We offer a free version of our services so that you can try our offerings for yourself; some limitations apply.</p>
				<p><strong>Ample Storage Space</strong> High-resolution images and source files can take up a lot of space; your portfolio has plenty of room to store it all.</p>
				<p><strong>Customized Domain</strong> Choose your own web address using our format, or create a unique map for your own domain and create a portfolio free of any outside branding.</p>
				<p><strong>Mobile and Retina Display Optimization</strong> When viewed on a mobile or Retina display, your portfolio will look its absolute best.  Devices and displays are automatically detected and your portfolio is optimized for whatever device it's viewed on, from iPhone to Android and more.</p>
				
			</div>
			<div class="full_content small">
				
				<h2>The Right Tools</h2>
				<p>Portfolio Lounge provides every tool you need to create a stunning online portfolio. With our editing options and fast-growing set of features, your work can be published quickly and without hassle.</p>
				
				
				
				<h2>User-Friendly Options</h2>
				<p>Creating your portfolio is as easy as clicking a few options and uploading your content; everything else is handled for you.</p>
				
				
				
				
				
				<h2>Analytics</h2>
				<p>Google Analytics can be added to your portfolio so that you can keep track of where your visitors are coming from and who they are.</p>
				
				
				
				
				<h2>No Bandwidth Restrictions</h2>
				<p>No matter how much attention your portfolio attracts, you'll never be saddled with penalties or limits on bandwidth.  Your portfolio will always be displayed in an exemplary manner.</p>
				
				
				<h2>Search Engine Optimization (SEO)</h2>
				<p>If your portfolio doesn't turn up on search engines, no one is going to know it's there.  Our templates are optimized for visibility on search engines such as Google, Bing and others.</p>
				
				
				<h2>No-Hassle Hosting</h2>
				<p>Top-tier dedication and enterprise-grade encrypted servers provide the best protection against outside threats.  Your works, and our servers, are always safe.</p>
				
				<h2>Unmatched Support</h2>
				<p>We provide unmatched support and a friendly, knowledgeable staff to assist you, regardless of your needs.  You can be assured of fast, friendly service.<br /><br />When you're looking to showcase your work in a professional and unique manner, you can count on Portfolio Lounge to meet your needs.  Our services do not require any downloads or installations and you'll never have the hassle of dealing with programming glitches, hosting issues, optimizing your portfolio for search engines or any of the other common problems with online portfolios.  Our secure, reliable services allow you to focus on displaying your work in a way that is simple, easy and completely unique.  There's no reason to wait; start your online portfolio today!</p>
				
			</div>
			<div class="clr"></div>
            <div style="text-align:center;margin:0 0 80px 0;">
			<a href="signup" class="promobtn">Create your portfolio website &raquo;</a>
            </div>
		</div>
	</div>
	
</div>




<!-- end testimonials -->
<script>
$('#screenshots').cycle({
	fx: 'fade',
	speed:800,
	timeout:3000
});
$('#testimonials ul').cycle({
	fx: 'scrollUp',
	speed:800,
	timeout:4000,
	easing: 'easeOutExpo',
	random:1
});
</script>
