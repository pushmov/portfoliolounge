<?php session_start();

$memberid = $_SESSION['SESS_MEMBER_ID'];
$username = $_SESSION['SESS_USER_NAME'];
$expload_url = explode(".",$_SERVER['HTTP_HOST']);

// Retailspot.com or Retailspot.mac:8888
$host = $_SERVER['HTTP_HOST'];
$prod = strpos($host,'portfoliolounge.com')!==false ? true : false; 

if (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$protocol = 'https://';
} else {
	$protocol = 'http://';
}



$p = $_GET['p'];
$i = $_GET['i'];
$x = $_GET['x'];
$e = $_GET['e'];
$l = $_GET['l'];

require_once('function/config.php');


// ---------------------- //
//       Services         //
// ---------------------- //
if ($p=="services"){
	include 'services/'.$i.'.php';
	break;
}

// ---------------------- //
//       Affiliates       //
// ---------------------- //
else if ($expload_url[0]=="portfolio" && !$p){
	$page_title="Portfoliolounge: Create an awesome portfolio website";
	$page_desc="Portfolio Builder";
	include 'affiliates/affiliates.php';
}

// ---------------------- //
//       Portfolios       //
// ---------------------- //
else if ($expload_url[0]!="portfoliolounge" && $expload_url[0]!="portfolio"){
	include 'portfolio/index.php';

// Member ID Redirect
} else if (is_numeric($p)){
	include 'portfolio/redirect.php';
		
} else if ($p=="your-portfolio"){
	include 'portfolio/index.php';
	

// ---------------------- //
//        PL Pages        //
// ---------------------- //
} else if (!$p || $p=='ad'|| $p=='click'){
	if(isset($_SERVER['HTTPS']) && $_SERVER["HTTPS"] != "on")
	{
			header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
			exit();
	}
	// If user lands on ad page (store ad ID in cookie and store on signup)
	if(($p=='ad' || $p=='click') && $i){
		setcookie("pl_campaign_id",$i,time()+86400*365, "/");	
	}
	$page_title="Portfoliolounge: Create an awesome portfolio website";
	$page_desc="A simple way to create and manage your portfolio website. Whether you are a graphic designer or photographer, we have a solution for you. Enjoy!";
	include 'head.php';
	include 'homepage.php';
	include 'footer.php';
	
} else if ($p=="home2"){
	$page_title="Portfoliolounge: Create an awesome portfolio website";
	$page_desc="A simple way to create and manage your portfolio website. Whether you are a graphic designer or photographer, we have a solution for you. Enjoy!";
	include 'head.php';		
	include 'homepageOLD.php';
	include 'footer.php';
	
} else if ($p=="pitch"){
	$page_title="Portfoliolounge: Pitch Deck";
	$page_desc="Thank you.";	
	include 'pitch.php';
	
} else if ($p=="portfolio"){
	$page_title="Portfoliolounge: Create an awesome portfolio website";
	$page_desc="A simple way to create and manage your portfolio website. Whether you are a graphic designer or photographer, we have a solution for you. Enjoy!";
	include 'head.php';		
	include 'artist.php';
	include 'footer.php';
	
} else if ($p=="artist"){
	$page_title="Portfoliolounge: Create an awesome portfolio website";
	$page_desc="A simple way to create and manage your portfolio website. Whether you are a graphic designer or photographer, we have a solution for you. Enjoy!";
	include 'head.php';		
	include 'artist.php';
	include 'footer.php';
	
	
} else if ($p=="photography"){
	$page_title="Create your portfolio website | PortfolioLounge";
	$page_desc="We created a simple way to build and manage your online portfolio website. We have online portfolios for everyone from graphic designers to photographers. Enjoy!";
	include 'head_new.php';
	include 'homepage2.php';
	include 'footer.php';
	
} else if ($p=="admin"){
	$page_title="Manage Your Portfolio";
	$page_desc="Managing your online portfolio has never been easier. Enjoy!";
	include_once("function/auth.php");
	include 'head.php';
	include 'admin/admin.php';
	include 'footer.php';
	
} else if ($p=="featured-portfolios"){
	$page_title="Portfolios - The best portfolio websites";
	$page_desc="A hand selected group of members based on item quality and overall portfolio website completeness!";
	include 'head.php';
	include 'examples.php';
	include 'footer.php';
		
} else if ($p=="browse-portfolios"){
	$page_title="Portfoliolounge: Check out the Portfolio Lounge Portfolio Websites!";
	$page_desc="A hand selected group of members based on quality of work and portfolio website completion.";
	include 'head.php';
	include 'browse.php';
	include 'footer.php';
		
} else if ($p=="gallery"){
	$page_title="Portfoliolounge: Check out the Portfolio Lounge Portfolio Websites!";
	$page_desc="A hand selected group of members based on quality of work and portfolio website completion.";
	include 'head.php';
	include 'gallery.php';
	include 'footer.php';
		
} else if ($p=="signup"){
	$page_title="Portfoliolounge: Create Your Portfolio Website with Portfolio Lounge";
	include 'head.php';
	include 'signup.php';
	include 'footer.php';
		
} else if ($p=="signup-angular"){
	$page_title="Create Your Portfolio with Portfolio Lounge";
	include 'head.php';
	include 'signup.php';
	include 'footer.php';
	
} else if ($p=="checkout2"){
	include_once("function/auth.php");
	include 'head.php';
	include 'checkout.php';
	include 'footer.php';
	
} else if ($p=="checkout"){
	include_once("function/auth.php");
	include 'head.php';
	include 'checkout-secure.php';
	include 'footer.php';
	
} else if ($p=="checkout-new"){
	include_once("function/auth.php");
	include 'head.php';
	include 'checkout-secure-new.php';
	include 'footer.php';
	
} else if ($p=="about"){
	include 'head.php';
	include 'about2.php';
	include 'footer.php';
	
} else if ($p=="about-portfolio-lounge"){
	include 'head.php';
	include 'about.php';
	include 'footer.php';
	
} else if ($p=="about-us"){
	include 'head.php';
	include 'about-us.php';
	include 'footer.php';
	
} else if ($p=="advertise"){
	include 'head.php';
	include 'advertise.php';
	include 'footer.php';
	
} else if ($p=="questions-about-your-portfolio"){
	include 'head.php';
	include 'help.php';
	include 'footer.php';
	
} else if ($p=="login"){
	$page_title="Portfoliolounge: Login to Manage Your Portfolio Lounge Portfolio";
	include 'head.php';
	include 'login.php';
	include 'footer.php';
	
} else if ($p=="login-reset-password"){
	$page_title="Portfoliolounge: Login to Manage Your Portfolio Lounge Portfolio";
	include 'head.php';
	include 'login.php';
	include 'footer.php';
	
} else if ($p=="help"){
	$page_title="Log in - We can help!";
	include 'head.php';
	include 'help.php';
	include 'footer.php';
	
} else if ($p=="jobs"){
	$page_title="Post a job to all creatives, Free.";
	//if (( !isset($_SERVER['PHP_AUTH_USER'] ) || ( $_SERVER['PHP_AUTH_USER'] != "dev" ) ) ||
    //( !isset($_SERVER['PHP_AUTH_PW'] ) || ( $_SERVER['PHP_AUTH_PW'] != "tester1" )) )
    //{
    //    header("WWW-Authenticate: " .
    //        "Basic realm=\"Protected Area\"");
    //    header("HTTP/1.0 401 Unauthorized");
    //    print("This page is protected by HTTP Authentication.");
    //    exit();
    //}
	include 'head.php';
	include 'jobs.php';
	include 'footer.php';
	
} else if ($p=="owner"){
	include_once("function/auth.php");
	include 'head.php';
	include 'owner.php';
	include 'footer.php';
	
} else if ($p=="webhook"){
	include 'head.php';
	include 'webhook.php';
	include 'footer.php';
	
} else if ($p=="contest"){
	$page_title="Win an iPad with your new portfolio!";
	include 'head.php';
	include 'contest.php';
	include 'footer.php';
	
} else if ($p=="post"){
	$page_title="Win an iPad with your new portfolio!";
	include_once("function/auth.php");
	include 'head.php';
	include 'function/post.php';
	include 'footer.php';

} else if ($p=="browse-jobs") {
	$page_title="Post a job to all creatives, Free.";
	include 'head.php';
	include 'job_category.php';
	include 'footer.php';
} else if ($p=="post-a-job"){
	$page_title="Post a job to all creatives, Free.";
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
	include 'head.php';
	include 'job_post.php';
	include 'footer.php';
} else {
	header ("Location: http://www.portfoliolounge.com");	
}?>