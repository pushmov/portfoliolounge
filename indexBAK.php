<?php session_start();

$memberid = $_SESSION['SESS_MEMBER_ID'];
$username = $_SESSION['SESS_USER_NAME'];
$expload_url = explode(".",$_SERVER['HTTP_HOST']);

$p = $_GET['p'];
$i = $_GET['i'];
$x = $_GET['x'];
$e = $_GET['e'];
$l = $_GET['l'];

require_once('function/config.php');



// ---------------------- //
//       Portfolios       //
// ---------------------- //

// Subdomain Portfolios
if ($expload_url[0]!="portfoliolounge"){
	include 'portfolio/index.php';

// Member ID Redirect
} else if (is_numeric($p)){
	include 'portfolio/redirect.php';
		
} else if ($p=="your-portfolio"){
	include 'portfolio/index.php';
	

// ---------------------- //
//        PL Pages        //
// ---------------------- //
} else if (!$p){
	$page_title="Portfoliolounge: Create an awesome portfolio website";
	$page_desc="A simple way to create and manage your portfolio website. Whether you are a graphic designer or photographer, we have a solution for you. Enjoy!";
	include 'head.php';
	include 'homepage.php';
	include 'footer.php';
	
} else if ($p=="photography"){
	$page_title="Create your portfolio website | PortfolioLounge";
	$page_desc="We created a simple way to build and manage your online portfolio website. We have online portfolios for everyone from graphic designers to photographers. Enjoy!";
	include 'head_new.php';
	include 'homepage2.php';
	include 'footer.php';
	
} else if ($p=="admin"){
	include_once("function/auth.php");
	include 'head.php';
	//include 'admin/index.php';
	include 'admin/admin.php';
	include 'footer.php';
	
} else if ($p=="featured-portfolios"){
	$page_title="Portfolios - Portfolio Lounge Favorites";
	include 'head.php';
	include 'examples.php';
	include 'footer.php';
		
} else if ($p=="browse-portfolios"){
	$page_title="Portfolios - Browse Portfolio Lounge Portfolios";
	include 'head.php';
	include 'browse.php';
	include 'footer.php';
		
} else if ($p=="signup"){
	$page_title="Sign Up! Create Your Portfolio with Portfolio Lounge";
	include 'head.php';
	include 'signup.php';
	include 'footer.php';
		
} else if ($p=="email"){
	include 'services/emails.php';
	
} else if ($p=="checkout2"){
	include_once("function/auth.php");
	include 'head.php';
	include 'checkout.php';
	include 'footer.php';
	
} else if ($p=="checkout"){
	include_once("function/auth.php");
	include 'head.php';
	include 'checkout-secure.php';
	include 'footer.php';
	
} else if ($p=="about-portfolio-lounge"){
	include 'head.php';
	include 'about.php';
	include 'footer.php';
	
} else if ($p=="questions-about-your-portfolio"){
	include 'head.php';
	include 'help.php';
	include 'footer.php';
	
} else if ($p=="login"){
	$page_title="Login to Manage Your Portfolio Lounge Portfolio";
	include 'head.php';
	include 'login.php';
	include 'footer.php';
	
} else if ($p=="help"){
	$page_title="Log in - We can help!";
	include 'head.php';
	include 'help.php';
	include 'footer.php';
	
} else if ($p=="owner"){
	include_once("function/auth.php");
	include 'head.php';
	include 'owner.php';
	include 'footer.php';
	
} else if ($p=="contest"){
	$page_title="Win an iPad with your new portfolio!";
	include 'head.php';
	include 'contest.php';
	include 'footer.php';
	
} else if ($p=="post"){
	$page_title="Win an iPad with your new portfolio!";
	include_once("function/auth.php");
	include 'head.php';
	include 'function/post.php';
	include 'footer.php';

} else {
	header ("Location: http://www.portfoliolounge.com");	
}?>