<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobs.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/categories.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobcategory.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobapply.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/payment.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/location.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/html.php';

if (ENV == 'DEVELOPMENT') {
    if(!isset($_SESSION)) session_start();

    $memberid = (isset($_SESSION['SESS_MEMBER_ID'])) ? $_SESSION['SESS_MEMBER_ID'] : '';
    $username = (isset($_SESSION['SESS_USER_NAME'])) ? $_SESSION['SESS_USER_NAME'] : '';
    $p = 'jobs';
    $page_title="Post a job to all creatives, Free.";
    $host = $_SERVER['HTTP_HOST'];
    $prod = strpos($host,'portfoliolounge.com')!==false ? true : false; 
    $expload_url = explode(".",$_SERVER['HTTP_HOST']);
    $memberid = (isset($_SESSION['SESS_MEMBER_ID'])) ? $_SESSION['SESS_MEMBER_ID'] : null;
    
}

$category_number = $categories->get_category_number(str_replace('-jobs', '', $i));
$alljobs = $jobs->get_all_by_category(null, null, $category_number['category_id']);
$stripe = $payment->load_config();
$side_nav = '';
$jobs_total = $jobs->total_by_category($category_number['category_id']);

include $_SERVER['DOCUMENT_ROOT'] . '/job_list.php';
?>