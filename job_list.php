<div class="browse_head" style="background:rgba(44,62,80,0.35)">
    <h2>Find jobs</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    <a href="post-a-job">Post a job for $<?=$stripe['post_job_fee']?></a>
</div>
<div class="clr"></div>
<div id="browse_nav">
    <ul>
        <li><a<?=$side_nav=='all'? ' class="cur"' : '';?> href="jobs">All Categories</a></li>
    </ul>
    <ul>
        <?php foreach($categories->get_all() as $category) :?>
        <li><a<?=$category_number['category_url']==$category['category_url'] ? ' class="cur"' : '';?> href="browse-jobs/<?=$category['category_url']?>-jobs"><?=$category['category_name']?></a></li>
        <?php endforeach; ?>
    </ul>
</div>
<div id="browse_grid" class="job-list" data-cat-id="<?=$category_number['category_id']?>">
    <div class="job-inner">
        <div>
            <div class="inner">
                <?php if(count($alljobs) > 0) :?>
                <h5>Showing <span id="showing"><?=count($alljobs);?></span> from <span id="total"><?=$jobs_total;?></span> jobs</h5>
                <?php 
                
                    foreach($alljobs as $job) : ?>
                <div class="job" data-id="<?=$job['reference_id']?>">
                    
                    <div class="clr"></div>
                    <h3><a href="/job/<?=$job['slug']?>/<??>"><?=$job['title']?></a></h3>
                    <span class="date-add">Date Posted : <?=date_create($job['created_at'])->format('F d, Y')?></span>
                    <?php if($job['expired_at'] != '') : ?>
                    <span class="date-add date-expired">Date Expired : <?=date_create($job['expired_at'])->format('F d, Y')?></span>
                    <?php endif; ?>

                    <?php
                        $categories = $job_category->get_job_categories($job['job_id']);
                        if (count($categories) > 0) :

                    ?>
                        <ul class="job-categories">
                            <?php foreach($categories as $c) : ?>
                            <li><a<?=$category_number['category_url']==$c['category_url']? ' class="highlight"' : '';?> href="<?='/browse-jobs/'.$c['category_url'].'-jobs'?>"><?=$c['category_name']?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                    <div class="clr"></div>
                    <div class="description"><?=$job['description']?></div>
                    <div class="info">
                        <ul class="list-unstyled">
                            <li>Type : <?=$jobs->get_empltype_label($job['employment_type']);?></li>
                            <li>Company : <?=($job['company_website'] == '') ? $job['company_name'] : \Html::anchor($job['company_website'], $job['company_name'], array('target' => '_blank'))?></li>
                            <li>Salary : <?=$job['salary'] == '' ? 'Negotiable' : $job['salary']?></li>
                            <li>Location : <?=\Location::to_string(array($job['country'], $job['prov_state'], $job['city']));?></li>
                        </ul>
                    </div>
                    <div class="btn">
                        <!--button type="button" class="button btn-view">View Detail</button-->

                        <?php if ($_SESSION['SESS_MEMBER_ID'] != $job['member_id']) : ?>
                            <?php if($job_apply->is_applied($job['reference_id'], $memberid) === false) : ?>
                            <button type="button" class="button btn-apply btn-apply-submit" data-value="<?=$job['reference_id']?>">Apply</button>
                            <?php else :?>
                                <?php if($job_apply->is_rejected($job['reference_id'], $memberid)) : ?>
                                <button type="button" class="button btn-rejected">Rejected at <?php echo $job_apply->get_rejected_date($job['reference_id'], $memberid);?></button>
                                <?php else :?>
                                <button type="button" class="button btn-applied">Applied</button>
                                <?php endif;?>
                            
                            <?php endif; ?>
                        <?php else : ?>
                            <button type="button" class="button btn-direct-edit" data-ref="<?=$job['reference_id']?>" style="background:#e74c3c">Edit Data</button>
                        <?php endif; ?>

                    </div>
                </div>
                    <?php endforeach; 
                else : ?>
                <div>No jobs found</div>
                <?php endif; ?>
            </div>

            <?php if(count($alljobs) > 0): ?>
            <div class="text-center" id="more_result"><button type="button" data-seq="2" class="button btn-load-more">Load more jobs</button></div>
            <?php endif; ?>
            
        </div>
    </div>
</div>
<script type="text/javascript" src="js/jobs.js"></script>
<div id="overlay" style="display:none"></div>
<div id="confirm_apply" class="lightbox admin-lightbox"></div>