<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
//require_once 'model/country.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobs.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/categories.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobcategory.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobapply.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/payment.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/location.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/html.php';
//require_once 'model/jobapply.php';
//require_once 'classes/payment.php';

if (ENV == 'DEVELOPMENT') {
    if(!isset($_SESSION)) session_start();

    $memberid = (isset($_SESSION['SESS_MEMBER_ID'])) ? $_SESSION['SESS_MEMBER_ID'] : '';
    $username = (isset($_SESSION['SESS_USER_NAME'])) ? $_SESSION['SESS_USER_NAME'] : '';
    $p = 'jobs';
    $page_title="Post a job to all creatives, Free.";
    $host = $_SERVER['HTTP_HOST'];
    $prod = strpos($host,'portfoliolounge.com')!==false ? true : false; 
    $expload_url = explode(".",$_SERVER['HTTP_HOST']);
    $memberid = (isset($_SESSION['SESS_MEMBER_ID'])) ? $_SESSION['SESS_MEMBER_ID'] : null;
    
}

$alljobs = $jobs->get_all();
$stripe = $payment->load_config();
$side_nav = 'all';
$jobs_total = $jobs->total();
$category_number['category_url'] = $category_number['category_id'] = '';

?>
<?php if(ENV == 'DEVELOPMENT') : include 'head.php'; endif; ?>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/job_list.php';?>
<?php if(ENV == 'DEVELOPMENT') : include 'footer.php'; endif; ?>