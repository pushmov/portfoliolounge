<?php
require_once 'model/country.php';
require_once 'model/jobs.php';
require_once 'model/categories.php';
require_once 'classes/html.php';
?>
<div class="container">
	<div class="content">
		<div class="inner">
			<form method="post" action="function/jobs.php?action=<?=$page['action']?>" accept-charset="utf-8" class="smart_form job-form" name="job_form">
				<?php if(!isset($_SESSION['SESS_MEMBER_ID']) || $_SESSION['SESS_MEMBER_ID'] == '') : ?>
				<h2>New member register</h2>
				<a href="/login">Already have an account? Click here to login</a>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				<h6><i class="asterix">*</i> is required</h6>
				<div class="input-group">
					<label for="email">
						<span>Your Email <i class="asterix">*</i></span>
					</label>
					<div class="form-input">
						<input type="text" id="email" class="input email" name="register[email]" placeholder="Your Email">
					</div>
				</div>
				<div class="input-group">
					<label for="username">
						<span>Username <i class="asterix">*</i></span>
					</label>
					<div class="form-input">
						<input type="text" class="input username" id="username" name="register[username]" placeholder="Username">
					</div>
				</div>
				<div class="input-group">
					<label for="password">
						<span>Password <i class="asterix">*</i></span>
					</label>
					<div class="form-input">
						<input id="password" class="input password short" name="register[password]" placeholder="Password" type="password">
					</div>
          		</div>
          		<div class="input-group">
          			<label for="password">
          				<span>Confirm Password</span>
          			</label>
          			<div class="form-input">
          				<input id="cpassword" class="input cpassword short last" name="register[cpassword]" placeholder="Verify Password" type="password">
          			</div>
          		</div>
				<?php endif;?>
				<h2><?= isset($page['title']) ? $page['title'] : 'Post a Job!';?></h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				<div class="input-group">
					<label for="title">
						<span>Job Title <i class="asterix">*</i></span>
					</label>
					<div class="form-input">
						<?php echo $page['hidden']; ?>
						<input type="text" class="input" name="data[title]" value="<?=$data['title']?>" placeholder="Job Title" id="title">
					</div>
				</div>
				<div class="input-group">
					<label for="description">
						<span>Job Description <i class="asterix">*</i></span>
					</label>
					<div class="form-input">
						<textarea name="data[description]" class="input" id="description" placeholder="Job Description"><?=$data['description']?></textarea>
					</div>
				</div>
				<div class="input-group">
					<label for="contactemail">
						<span>Contact Email <i class="asterix">*</i></span>
					</label>
					<div class="form-input">
						<input type="text" name="data[email]" id="contactemail" class="input" placeholder="Contact Email" value="<?=$data['email']?>">
					</div>
				</div>
				<div class="input-group">
					<label for="cell">
						<span>Contact Phone/Cell</span>
					</label>
					<div class="form-input">
						<input type="text" name="data[cell]" id="cell" class="input" placeholder="Contact Phone" value="<?=$data['cell']?>">
					</div>
				</div>
				<div class="input-group">
					<label for="country">
						<span>Country <i class="asterix">*</i></span>
					</label>
					<div class="form-input">
						<?php echo \Country::dropdown('data[country]', $data['country'], array('class' => 'input', 'id' => 'country'));?>
					</div>
				</div>
				<div class="input-group">
					<label for="prov_state">
						<span>Province/State</span>
					</label>
					<div class="form-input">
						<input type="text" name="data[prov_state]" class="input" id="prov_state" placeholder="Province / State" value="<?=$data['prov_state']?>">
					</div>
				</div>
				<div class="input-group">
					<label for="city">
						<span>City</span>
					</label>
					<div class="form-input">
						<input type="text" name="data[city]" class="input" id="city" placeholder="City" value="<?=$data['city']?>">
					</div>
				</div>
				<div class="input-group">
					<label for="postal_code">
						<span>ZIP/Postal Code</span>
					</label>
					<div class="form-input">
						<input type="text" name="data[postal_code]" class="input" id="postal_code" placeholder="ZIP/Postal code" value="<?=$data['postal_code']?>">
					</div>
				</div>
				<div class="input-group">
					<label for="salary">
						<span>Salary</span>
					</label>
					<div class="form-input">
						<input type="text" name="data[salary]" class="input" id="salary" placeholder="Salary" value="<?=$data['salary']?>">
					</div>
				</div>
				<div class="input-group">
					<label for="employment_type">
						<span>Type <i class="asterix">*</i></span>
					</label>
					<div class="form-input">
						<?php echo \Html::dropdown($jobs->get_empltype_label(), 'data[employment_type]', $data['employment_type'], array('class' => 'input', 'id' => 'employment_type'))?>
					</div>
				</div>
				<div class="input-group">
					<label for="company_name">
						<span>Company Name <i class="asterix">*</i></span>
					</label>
					<div class="form-input">
						<input type="text" name="data[company_name]" id="company_name" class="input" placeholder="Company Name" value="<?=$data['company_name']?>">
					</div>
				</div>
				<div class="input-group">
					<label for="company_website">
						<span>Company Website</span>
					</label>
					<div class="form-input">
						<input type="text" name="data[company_website]" id="company_website" class="input" placeholder="Company Website" value="<?=$data['company_website']?>">
					</div>
				</div>
				<div class="input-group">
					<label for="remoteable">
						<span>Remoteable <i class="asterix">*</i></span>
					</label>
					<div class="form-input">
						<input type="radio" name="data[remoteable]" <?=$data['remoteable']==\Jobs::REMOTEABLE_YES ? 'checked' : '' ?> value="<?=\Jobs::REMOTEABLE_YES?>"> <?=$jobs->get_remoteable_label(\Jobs::REMOTEABLE_YES);?>
						<input type="radio" name="data[remoteable]" <?=$data['remoteable']==\Jobs::REMOTEABLE_NO ? 'checked' : '' ?> value="<?=\Jobs::REMOTEABLE_NO?>"> <?=$jobs->get_remoteable_label(\Jobs::REMOTEABLE_NO);?>
					</div>
				</div>
				<div class="clr"></div>
				<div class="input-group">
					<label for="years_of_exp">
						<span>Years of Experience (min) <i class="asterix">*</i></span>
					</label>
					<div class="form-input">
						<?php echo \Html::dropdown($jobs->get_years_of_exp_label(null), 'data[years_of_exp]', $data['years_of_exp'], array('class' => 'input', 'id' => 'years_of_exp')); ?>
					</div>
				</div>
				<div class="input-group">
					<label for="portfolio_items">
						<span>Portfolio Items (min) <i class="asterix">*</i></span>
					</label>
					<div class="form-input">
						<?php echo \Html::dropdown($jobs->get_portfolio_items_label(), 'data[portfolio_items]', $data['portfolio_items'], array('class' => 'input', 'id' => 'portfolio_items'));?>
					</div>	
				</div>
				<div class="input-group">
					<label for="expired_at">
						<span>Expired at</span>
					</label>
					<div class="form-input">
						<input type="text" name="data[expired_at]" id="expired_at" class="input" data-toggle="datepicker" value="<?=$data['expired_at']?>">
					</div>
				</div>
				<div class="input-group">
					<label for="expired_at">
						<span>Categories</span>
					</label>
					<div class="form-input">
						<ul class="checkbox-wrapper">
							<?php foreach($categories->get_all() as $cat) : ?>
							<li><input type="checkbox" <?=in_array($cat['category_id'], $data['categories']) ? 'checked' : '';?> name="data[categories][]" value="<?=$cat['category_id']?>"><span><?=$cat['category_name']?></span></li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>

				<div class="input-group">
					<label for="expired_at">
						<span>Publish</span>
					</label>
					<div class="form-input">
						<input type="radio" name="data[publish]" value="<?=\Jobs::PUBLISH_Y?>" <?=$data['publish'] == \Jobs::PUBLISH_Y ? 'checked' : ''?>>  <?=$jobs->get_publish_label(\Jobs::PUBLISH_Y);?>
						<input type="radio" name="data[publish]" value="<?=\Jobs::PUBLISH_N?>" <?=$data['publish'] == \Jobs::PUBLISH_N ? 'checked' : ''?>>  <?=$jobs->get_publish_label(\Jobs::PUBLISH_N);?>
					</div>
				</div>
				<div class="clr"></div>
				<div class="text-center"><?=$page['submit'];?></div>

			</form>
		</div>
	</div>
</div>
<script src="https://use.fontawesome.com/725ae58a3d.js"></script>
<div id="overlay" style="display:none"></div>
<div class="lightbox" style="display:none"><i class="fa fa-spinner fa-4x fa-spin"></i></div>
<link href="css/jquery-te-1.4.0.css" rel="stylesheet">
<link href="css/jquery-te.custom.css" rel="stylesheet">
<link href="css/datepicker.min.css" rel="stylesheet">
<script type="text/javascript" src="js/libs/jquery-te-1.4.0.min.js"></script>
<script type="text/javascript" src="js/jobs.js"></script>
<script type="text/javascript" src="js/libs/datepicker.min.js"></script>
<style>
.jqte{
	border:1px solid #eee;
	box-shadow: none;
	margin-top: 0
}
.jqte_focused{
	border-color: #eee;
	box-shadow: none
}
</style>