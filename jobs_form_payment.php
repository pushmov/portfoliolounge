<?php 
	require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/payment.php';
	require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/html.php';
?>
<div class="payment-dialog">
	<div class="payment-dialog-header">
		<h2>Payment Details</h2>
	</div>
	<form action="function/jobs.php?action=payment" class="smart_form form-horizontal form-payment" method="post" name="payment">
		<label>*required fields</label>
		<div class="clr"></div>
		<div class="payment-error"></div>
		<div class="input-group">
			<label for="card_name">
				<span>Name on Card <i class="asterix">*</i></span>
			</label>
			<div class="form-input">
				<input type="text" class="input" name="card[name]" placeholder="Card's Holder Name" id="card_name">
			</div>
		</div>
		<div class="input-group">
			<label for="card_number">
				<span>Card Number <i class="asterix">*</i></span>
			</label>
			<div class="form-input">
				<input type="text" class="input" name="card[number]" id="card_number" placeholder="Debit/Credit Card Number">
			</div>
		</div>
		<div class="input-group">
			<label for="card_type">
				<span>Card Type</span>
			</label>
			<div class="form-input">
				<?php echo \Html::dropdown($payment->get_card_type(), 'card[type]', null, array('id' => 'card_type', 'class' => 'input')); ?>
			</div>
		</div>
		<div class="input-group">
			<label for="card_exp_month">
				<span>Expiration Date</span>
			</label>
			<div class="form-input">
				<?php echo \Html::dropdown($payment->get_month(), 'card[exp_month]', null, array('id' => 'exp_month', 'class' => 'input exp-month'));?>
				<?php echo \Html::dropdown($payment->get_year(), 'card[exp_year]', null, array('id' => 'exp_year', 'class' => 'input exp-year'));?>
			</div>
		</div>
		<div class="input-group">
			<label for="card_cvv">
				<span>Card CVV <i class="asterix">*</i></span>
			</label>
			<div class="form-input">
				<input type="text" class="input cvv" name="card[cvv]" id="card_cvv" style="width:45px !important" placeholder="CVV">
			</div>
		</div>

		<div class="text-right btn-checkout">
			<h3 class="amount">Amount: $<?=$config['post_job_fee']?></h3>
			<button type="button" class="btn red" id="cancel">Cancel</button>
			<button type="button" class="btn" id="process">Checkout</button>
		</div>
		<div class="text-right card-list">
			<img src="img/card_mastercard.png" width="48" alt="Mastercard" />
			<img src="img/card_visa.png" width="48" alt="Visa">
			<img src="img/card_discover.png" width="48" alt="Discover">
			<img src="img/card_amex.png" width="48" alt="American Express">
			<img src="img/card_jcb.png" width="48" alt="JCB">
			<img src="img/card_dinersclub.png" width="48" alt="Diners Club">
			<div class="text-left">
				<img src="img/powered_by_stripe@2x.png" alt="Powered by Stripe" width="100">
			</div>
		</div>
		<div class="clr"></div>
	</form>
</div>