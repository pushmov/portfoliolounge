<?php 
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobcategory.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobs.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/html.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/location.php';
if(count($data) > 0) :
	foreach($data as $job) : ?>
    <div class="job" style="">                
        <div class="clr"></div>
        <h3><a href="/job/<?=$job['slug']?>/<??>"><?=$job['title']?></a></h3>
        <span class="date-add">Date Posted : <?=date_create($job['created_at'])->format('F d, Y')?></span>
        <?php if($job['expired_at'] != '') : ?>
        <span class="date-add date-expired">Date Expired : <?=date_create($job['expired_at'])->format('F d, Y')?></span>
        <?php endif; ?>
        
        <?php
            $categories = $job_category->get_job_categories($job['job_id']);
            if (count($categories) > 0) :
        ?>
        <ul class="job-categories">
            <?php foreach($categories as $c) : ?>
            <li><a href="<?='/browse-jobs/'.$c['category_url'].'-jobs'?>"><?=$c['category_name']?></a></li>
            <?php endforeach; ?>
        </ul>
        <?php endif; ?>
        
        <div class="clr"></div>
        <div class="description"><?=$job['description']?></div>
        <div class="info">
            <ul class="list-unstyled">
                <li>Type : </li>
                <li>Company : <?=($job['company_website'] == '') ? $job['company_name'] : \Html::anchor($job['company_website'], $job['company_name'], array('target' => '_blank'))?></li>
                <li>Salary : <?=$job['salary'] == '' ? 'Negotiable' : $job['salary']?></li>
                <li>Location : <?=\Location::to_string(array($job['country'], $job['prov_state'], $job['city']));?></li>
            </ul>
        </div>
        <div class="btn">
            <!--button type="button" class="button btn-view">View Detail</button-->
            <?php if($job_apply->is_applied($job['reference_id'], $memberid) === false) : ?>
            <button type="button" class="button btn-apply btn-apply-submit" data-value="<?=$job['reference_id']?>">Apply</button>
            <?php else :?>
            <button type="button" class="button btn-applied">Applied</button>
            <?php endif; ?>
        </div>
    </div>
    <?php endforeach; ?>
    <script>
    $(document).find('#more_result button').attr('data-seq', '<?=$seq?>');
    $(document).find('#showing').html('<?=$showing?>');
    </script>
<?php endif; ?>