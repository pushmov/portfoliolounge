<div class="payment-dialog">
	<div class="payment-dialog-header">
		<h2>Payment Completed</h2>
	</div>
	<p>Now you can manage your job / applicants in your dashboard page. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	<div class="text-right btn-checkout">
		<button type="button" class="btn" id="continue">Continue</button>
	</div>
</div>