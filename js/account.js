
/* Smart Form
-----------------------------------*/

// Show save btn

/*
$('.smart_form input, .smart_form textarea').on('input', function() {
	var showSaveBtn = 0;
	$(this).parent().children('.input').each(function(){
		if($(this).data('original') != $(this).val()){
			showSaveBtn++;
		}
	});	
	showSaveBtn ? $(this).siblings('.smart_btn').fadeIn() : $(this).siblings('.smart_btn').fadeOut();
});
*/

/* Submit smart form //

$('.smart_form').submit(function(e){
	var thisForm = $(this);
	var formId = $(this).attr('id');
	
	
	if(formId=='username'){
		var username = $("input[name='username']").val();
		var alphaNumeric = /^[a-zA-Z_0-9]+$/;
		if (!alphaNumeric.test(username)) {
			alert("Username can only be letters and numbers");
			return false;
		}
	}
	if(formId=='email'){
		var email = $("input[name='email']").val();
		var validEmail = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
		if (!validEmail.test(email)) {
			alert("Please enter a valid email address.");
			return false;
		}
	}
	if(formId=='password'){
		var password = $("input[name='password']").val();
		var cpassword = $("input[name='cpassword']").val();
		if (password!=cpassword) {
			alert("Passwords do not match! Try Again.");
			return false;
		}
	}
	if(formId=='about'||formId=='tagline'){
		var textareaVal = $(this).find("textarea").val();
		textareaVal = textareaVal.replace(/\s+/g, ' ');	
		$(this).find("textarea").val(textareaVal);
	}
	
	$.ajax({
		type: "POST",
		url: "function/edit-bio.php?a="+formId,
		data: $(e.target).serialize(),
		dataType: "json",
		error: function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		},
		success: function(data){
			thisForm.children('.input').each(function(){
				$(this).data('original', $(this).val());
			});
			thisForm.children('.smart_btn').fadeOut();
		}
	});
	return false;
	
});
*/


// Special case for skill checkboxes //

$('#skills input').change(function(){
	$(this).parent().parent().siblings('.smart_btn').fadeIn();
	$("#skills .uncheck").fadeIn();
});

$("#skills .uncheck").on("click", function(){
	$(".checkbox-wrapper input[type='checkbox']").prop('checked', false);
	$(this).parent().siblings('.smart_btn').fadeIn();
	$("#skills .uncheck").fadeOut();	
});





/* Upload Profile Image
-----------------------------------*/
$('.fileupload').fileupload({
	dataType: 'json',
	submit: function (event, files) {
		// Check File Type
		for (var i = 0; i < files.originalFiles.length; i++) {			
			var fileType = files.originalFiles[i].type;
			if(fileType=="image/jpeg"||fileType=="image/png"||fileType=="image/gif"){
				// Acceptable
			} else {
				lightbox('error', 'fileType');
				return false; 	
			}
		}
	},
	done: function (e, data) {
		//console.log("done");
	},
	progressall: function (e, data) {
		var progress = parseInt(data.loaded / data.total * 100, 10);
		$('#progress .bar').css('width', progress + '%');
		lightbox('newItems');
	},
	stop: function (e, data) {
		location.reload();
	}
});





/* Delete Profile Img
-----------------------------------*/
function deleteProfileImg(id){	
	$.ajax({
		type: "POST",
		url: "function/edit-bio.php?a=delete_profile_img&member_id="+id,
		error: function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		},
		success: function(data){
			$(".frame.profile_img img").fadeOut();
			$(".frame.profile_img").addClass("empty");
		}
	});
}


/* Delete Profile Img
-----------------------------------*/
$(".deleteLink").click(function(){
	
	var thisLink = $(this);
	var id = thisLink.attr('data-source');
		
	$.ajax({
		type: "POST",
		url: "function/edit-bio.php?a=delete_link&b="+id,
		error: function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		},
		success: function(data){
			thisLink.parent('li').hide();
		}
	});
	

	
});


/*
*/

$('#fixed_links_select').change(function () {
	var select = $('#fixed_links_select'), title = $('#link_title_input');
	
	if (select.val() == 'other') {
		title.val('');
		title.show();
	} else {
		title.hide();
		value= select.val(); 
		title.val(value);
	}
}).trigger('change'); // added trigger to calculate initial option

$('#add_link').submit(function(e){
	
	var linkUrl = $("input[name=link_url]").val();
	var linkTitle = $("input[name=link_title]").val();
	
	//$("input[name=link_url]").val('');
	//$("input[name=link_title]").val('');
	
	$('.member-links').append('<li><a target="_blank" href="'+linkUrl+'">'+linkTitle+'</a></li>');
	
	
});


/* Delete Logo
-----------------------------------*/
function deleteLogoImg(id){	
	$.ajax({
		type: "POST",
		url: "function/edit-bio.php?a=delete_logo_img&member_id="+id,
		error: function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		},
		success: function(data){
			$(".frame.logo_img .logo-container").fadeOut();
			$(".frame.logo_img").addClass("empty");
		}
	});
}



