/* Edit & Preview Mode
-----------------------------------*/
function editMode(){
	$(this).parents(".data-view").hide().next().fadeIn();
}
function previewMode(field){
	if(field=="all"){
		$(".data-edit").hide();
		$(".data-view").fadeIn();	
	}
	$(this).parents(".data-edit").hide().prev().fadeIn();
}
$(".edit").on("click", editMode);
$(".cancel").on("click", previewMode);




$.fn.followTo = function (pos) {
    var $this = this,
        $window = $(window);

    $window.scroll(function (e) {
        if ($window.scrollTop() < pos) {
            $this.css({
                position: 'absolute',
                top: pos,
				width: '100%'
            });
        } else {
            $this.css({
                position: 'fixed',
				width: '100%',
				zIndex: '99',
                top: 0
            });
        }
    });
};

$('#admin-header').followTo(74);

	


/* Lightboxes
-----------------------------------*/
function lightbox(action, id){
	if(action=="newItems"){
		$("#overlay").fadeIn();		
		$("#upload-items").fadeIn();				
	}
	if(action=="upgrade"){
		$("#overlay").fadeIn();		
		$("#upgrade").fadeIn();
		if(id==2){
			$("#upgrade input.account-id").val(2);
			$("#upgrade span.price").html("7.99");	
			$("#upgrade input.btn").val("Upgrade to Pro");	
		}
		if(id==3){
			$("#upgrade input.account-id").val(3);
			$("#upgrade span.price").html("16.99");	
			$("#upgrade input.btn").val("Upgrade to Max");
		}
	}
	if(action=="downgrade"){
		$("#overlay").fadeIn();		
		$("#downgrade").fadeIn();
	}
	if(action=="projectLimit"){
		$("#overlay").fadeIn();				
		$("#project-limit").fadeIn();
		// Proper english
		uploadTxt = "projects";
		if (id==1){uploadTxt="project";}
		// Upgrade Message
		if(id<=0){
			$("#project-limit h3").html("Your out of projects!");
		} else {
			$("#project-limit h3").html("You only have <strong>"+id+" "+uploadTxt+" left.</strong>");
		}
	}
	if(action=="itemLimit"){
		$("#overlay").fadeIn();				
		$("#item-limit").fadeIn();
		// Proper english
		uploadTxt = "uploads";
		if (id==1){uploadTxt="upload";}
		// Upgrade Message
		if(id<=0){
			$("#item-limit h3").html("Your out of items!");
		} else {
			$("#item-limit h3").html("You only have <strong>"+id+" "+uploadTxt+" left.</strong>");
		}
	}
	if(action=="itemLimitMax"){
		$("#overlay").fadeIn();				
		$("#item-limit.max").fadeIn();
		// Proper english
		uploadTxt = "uploads";
		if (id==1){uploadTxt="upload";}
		// Upgrade Message
		if(id<=0){
			$("#item-limit h3").html("You've reached your limit.");
		} else {
			$("#item-limit h3").html("You only have <strong>"+id+" "+uploadTxt+" left.</strong>");
		}
	}
	if(action=="profileImg"){
		$("#overlay").fadeIn();			
		$("#upload").fadeIn();				
	}
	if(action=="delete"){
		$("#overlay").fadeIn();				
		$("#confirm").fadeIn();
		$("#confirm #delete .project_id").val(id);
	}
	if(action=="tips"){
		$("#overlay").fadeIn();				
		$("#tips").fadeIn();
	}
	if(action=="deleteItem"){
		$("#overlay").fadeIn();				
		$("#deleteItem").fadeIn();
		$("#deleteItem #delete .item_id").val(id);
	}
	if(action=="newProject"){
		$("#overlay").fadeIn();				
		$("#new-project").fadeIn();
	}
	if(action=="privacy"){
		$("#overlay").fadeIn();				
		$("#privacy").fadeIn();
	}
	if(action=="pleaseUpgrade"){
		$("#overlay").fadeIn();				
		$("#pleaseUpgrade").fadeIn();
	}
	if(action=="pleaseUpgradeTemplate"){
		$("#overlay").fadeIn();				
		$("#pleaseUpgradeTemplate").fadeIn();
	}
	if(action=="addVideo"){
		$("#overlay").fadeIn();				
		$("#add-video").fadeIn();
	}
	if(action=="changeCover"){
		$("#overlay").fadeIn();
		$(".cover-thumbs a").css({"display":"none"});			
		$("#change-cover").fadeIn();
		$(".cover-thumbs .cover"+id).fadeIn();
	}
	if(action=="error"){
		if(id=="fileType"){
			$("#error h3").html("We're sorry! We only allow image files.");
			$("#error p").html("You can only upload images that end with these extensions: <strong>.jpg .png and .gif</strong>");
		}
		if(id=="alreadyUpgraded"){
			$("#error h3").html("We're sorry! You already have this upgrade.");
			$("#error p").html("Please see our other plans.");
		}
		$("#overlay").fadeIn();		
		$("#error").fadeIn();	
	}
	if(action=="cancel"){
		$("#overlay").fadeOut();			
		$(".lightbox").fadeOut();	
	}
}
$("#overlay").on({
	click: function() {
		lightbox('cancel');
	}
});


/* Hovers
-----------------------------------*/
$(".item-thumbs").hover(function(){
	$(this).stop().css({'opacity':'.7'});
}, function(){
	$(this).stop().animate({'opacity':'1'},400);
});


/* Marquee
-----------------------------------*/
$('#marquee').cycle({ 
    fx:     'scrollVert', 
    easing: 'easeOutBack',
	speed: 440,
	timeout: 3000,
	nowrap: 1
});


/* Create Project
-----------------------------------*/
$("#add-project").submit(function() {
	var projectTitle = $("input#project_title").val();
	if (!projectTitle) {
		$("#project_title").addClass("red-flag");
		return false;
	}
	$("#project_title").removeClass("red-flag");
	return true;
});


/* Edit Project
-----------------------------------*/
$("#project-title").submit(function(e) {
	var textareaVal = $(this).find("textarea").val();
	textareaVal = textareaVal.replace(/\s+/g, ' ');	
	$(this).find("textarea").val(textareaVal);
	$.ajax({
		type: "POST",
		url: "function/edit-projects.php?a=title",
		data: $(e.target).serialize(),
		dataType: "json",
		error: function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		},
		success: function(data){
			previewMode("all");
		}
	});
	return false;
});





$("#single-data").submit(function(e) {
	var textareaVal = $(this).find("textarea").val();
	textareaVal = textareaVal.replace(/\s+/g, ' ');	
	$(this).find("textarea").val(textareaVal);
	$.ajax({
		type: "POST",
		url: "function/edit-items.php?a=item-info",
		data: $(e.target).serialize(),
		dataType: "json",
		error: function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		},
		success: function(data){
			previewMode("all");
		}
	});
	return false;
});

/* Sortable Projects
-----------------------------------*/
$(function() {
	$( "#project_grid" ).sortable({
		handle: ".sort_handle",
		scroll: true,
		// Do this on order change
		update : function(){
			// Create Order Array
			projectArr = [];
			$('#project_grid .project_thumb').each( function( index ) {
				project_id = $(this).attr("id");
				projectArr.push(project_id);
			});
			// Convert array to jason
			var projectArr = JSON.stringify(projectArr);
			$.ajax({
				type: "POST",
				url: "../function/edit-projects.php?a=order",
				data: {data : projectArr},
				error: function(jqXHR, textStatus, errorThrown){
					console.log("error");
				},
				success: function(data){
					console.log("success");
				}
			});
		}
	});
	$( "#project_grid" ).disableSelection();
});



/* Sortable Items
-----------------------------------*/
$(function() {
	$( "#item_grid" ).sortable({
		handle: ".sort_handle",
		scroll: true,
		// Do this on order change
		update : function(){
			// Create Order Array
			itemArr = [];
			$('#item_grid .item_thumb').each( function( index ) {
				item_id = $(this).attr("id");
				itemArr.push(item_id);
			});
			// Convert array to jason
			itemArr = JSON.stringify(itemArr);
			console.log(itemArr);
			// Post JSON to php
			$.ajax({
				type: "POST",
				url: "function/edit-items.php?a=order",
				data: {data : itemArr},
				error: function(jqXHR, textStatus, errorThrown){
					console.log("error");
				},
				success: function(data){
					console.log("success");
				}
			});
		}
	}); 
	$( "#item_grid" ).disableSelection();
});



/* Upgrade
-----------------------------------*/
$("#cc-payment").submit(function(e) {
	$.ajax({
		type: "POST",
		url: "function/paypal-subscribe.php",
		data: $(e.target).serialize(),
		//dataType: "json",
		beforeSend:function(){
			$(".upgrade-view").hide();
			$("#payment-processing-view").fadeIn();
		},
		error: function(jqXHR, textStatus, errorThrown){
			$(".upgrade-view").hide();
			$("#payment-result-view").html(errorThrown);
			$("#payment-result-view").fadeIn();
		},
		success: function(data){
			$(".upgrade-view").hide();
			$("#payment-result-view").html(data);
			$("#payment-result-view").fadeIn();
		}
	});
	return false;
});


/* Downgrade Account
-----------------------------------*/
function downgradeAccount(e){
	$.ajax({
		type: "POST",
		url: "function/paypal-update-profile.php",
		beforeSend:function(){
			$("#downgrade .view").hide();
			$("#downgrade .processing").show();
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert('There was an error. Please refresh and try again, or contact billing@portfoliolounge.com');
		},
		success: function(data){
			$("#downgrade .view").hide();
			$("#downgrade .result").html(data);
			$("#downgrade .result").show();
		}
	});
}
$("#confirm-downgrade").on("click", downgradeAccount);