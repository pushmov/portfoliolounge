$(document).ready(function(){
	if($('#my_jobs').length > 0) {
		var dtj = $('#my_jobs').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			lengthMenu: [ [5, 10, 25, 50, 100], [5, 10, 25, 50, 100] ],
			ajax: {
				url: '/function/adminjobs.php?action=my_jobs'
			},
			aoColumnDefs: [
				{ 'bSortable': false, 'aTargets': [ 8,9,10 ] }
			]
		});
	}

	if($('#applied').length > 0) {
		var dta = $('#applied').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			lengthMenu: [ [5, 10, 25, 50, 100], [5, 10, 25, 50, 100] ],
			ajax: {
				url: '/function/adminjobs.php?action=applied&ref_id='+$('#ref_id').attr('data-value')
			},
			aoColumnDefs: [
				{ 'bSortable': false, 'aTargets': [ 10 ] }
			]
		});
	}

	$('#my_jobs').on('click', '.delete-job', function(e){
		e.preventDefault();
		$('#overlay').fadeIn();
		$('.admin-lightbox').html('<i class="fa fa-spinner fa-spin"></i>');
		$.get('/function/dialog.php?action=confirm_delete_job', {ref: $(this).attr('data-ref')}, function(html){
			$('.admin-lightbox').html(html);
			$('.admin-lightbox').fadeIn();
			$('.admin-lightbox').on('click', '#delete_job', function(e){
				var btn = $(this);
				btn.attr('disabled', true);
				e.preventDefault();
				var f = $(this).closest('form');
				$.post(f.attr('action'), f.serialize(), function(r){
					var json = $.parseJSON(r);
					dtj.ajax.reload();
					btn.attr('disabled', false);
					$('.admin-lightbox').fadeOut();
					$('.admin-lightbox').html('');
					$('#overlay').fadeOut();
				});
			});
		});
		
	});

	$('#applied').on('click', '.action-reject', function(e){
		e.preventDefault();
		$.get('/function/dialog.php?action=confirm_reject', {value: $(this).attr('data-value')}, function(html){
			$('.admin-lightbox').html(html);
			$('.admin-lightbox').fadeIn();
			$('.admin-lightbox').on('click', '#submit_reject', function(ev){
				ev.preventDefault();
				var btn = $(this);
				btn.prop('disabled', true);
				var form = $(this).closest('form');
				$.post(form.attr('action'), form.serialize(), function(data){
					//var json = $.parseJSON(data);
					btn.prop('disabled', false);
					$('.admin-lightbox').fadeOut();
					$('.admin-lightbox').html('');
					$('#overlay').fadeOut();
					dta.ajax.reload();
				});

			});
		});
		$('#overlay').fadeIn();
	});

	if($('#tb_trans').length > 0) {
		var dtj = $('#tb_trans').DataTable({
			processing: true,
			serverSide: true,
			stateSave: true,
			lengthMenu: [ [5, 10, 25, 50, 100], [5, 10, 25, 50, 100] ],
			ajax: {
				url: '/function/adminjobs.php?action=owner_trans'
			}
		});
	}
	$('.container').css({
		width: '100%',
		padding: 0,
	})

	$('.admin-lightbox').on('click', '.btn-close-dialog', function(e){
		e.preventDefault();
		$('.admin-lightbox').fadeOut();
		$('.admin-lightbox').html('');
		$('#overlay').fadeOut();
	});
});