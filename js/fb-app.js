var fb_share_url;

// Facebook Functionality
// --------------------------------------------
window.fbAsyncInit = function() {
	FB.init({
	  appId      : '247254998759584',
	  xfbml      : true,
	  version    : 'v2.8'
	});
	FB.AppEvents.logPageView();
	
	
	
	
	// Facebook Liking with Callback
	// --------------------------------------------
	var page_like = function(url, html_element) {
		if (url=='https://portfoliolounge.com'){
			url_suffix = 'like_pl_url';	
		} else {
			url_suffix = 'like_fb_page';	
		}
		$.ajax({
		  url: "services/facebook/"+url_suffix,
		  context: document.body
		}).done(function(data) {
		  alert(data);
		});
	}
	
	
	// Facebook Functions
	var page_unlike = function(url, html_element) {
	  //console.log("page_like_or_unlike_callback");
	  console.log("unliked " + url);
	  //console.log(html_element);
	}
	FB.Event.subscribe('edge.create', page_like);
	FB.Event.subscribe('edge.remove', page_unlike);
	
	
	
	// Facebook Sharing with Callback
	// --------------------------------------------
	 fb_share_url = function(url){
		FB.ui({
			method: 'share',
			href: url,
		}, function(response) {
			if (response && !response.error_message) {
				//alert('Posting completed.');
				$.ajax({
				  url: "services/facebook/share_pl_url",
				  context: document.body
				}).done(function(data) {
				  alert(data);
				});
			} else {
				alert('Error while posting.');
			}
		});
	}
	
	

	
};
	
	$( ".fb_share_url" ).on( "click", function() {
		url = $( this ).attr("data-href");
		fb_share_url(url);
	});

	



// Facebook Config
// --------------------------------------------
(function(d, s, id){
 var js, fjs = d.getElementsByTagName(s)[0];
 if (d.getElementById(id)) {return;}
 js = d.createElement(s); js.id = id;
 js.src = "//connect.facebook.net/en_US/sdk.js";
 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


