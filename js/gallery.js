function ownerCtrl($scope, $http) {
    $scope.markItem = function(id, event){
        $http.post("/services/owner/featured-item/"+id).success(function(response) {		
            //$scope.account = angular.copy($scope.settingsCopy);
            $scope.respone = response;
            thisItem = $(event.target);
            if(thisItem.hasClass("checked")){
                thisItem.removeClass("checked")
            } else {
                thisItem.addClass("checked")
            }

        }).error(function(data, status) {
            $scope.data = data || "Request failed";
            $scope.status = status;			
        });
    }
    $scope.markItemHome = function(id, event){
        $http.post("/services/owner/featured-item-home/"+id).success(function(response) {		
            //$scope.account = angular.copy($scope.settingsCopy);
            $scope.respone = response;
            thisItem = $(event.target);
            if(thisItem.hasClass("checked")){
                thisItem.removeClass("checked")
            } else {
                thisItem.addClass("checked")
            }

        }).error(function(data, status) {
            $scope.data = data || "Request failed";
            $scope.status = status;			
        });
    }
}