$(document).ready(function(){
	
	/* Canvas & Frame
	-------------------------------*/	
	function initCanvas(){
		$('.canvas').each(function(){
			
			var canvas = $(this);
			var canvasWidth = canvas.closest('.frame').width();
			var canvasHeight = canvas.closest('.frame').height();
			var path = canvas.attr('data-source');		
			
			
			// Display Image on load
			$("<img />").attr("src",path).load(function() {	
					
					
				// Figure out how to center and resize
				// the new image to fit frame.	
				var gridRatio = canvasWidth / canvasHeight;
				var imgRatio = this.width / this.height;
				
				if (imgRatio < gridRatio){
					var imgWidthAttr = canvasWidth;
					var imgHeightAttr = this.height * (canvasWidth / this.width);
				} else {
					var imgWidthAttr = this.width * (canvasHeight / this.height);
					var imgHeightAttr = canvasHeight;
				}
				
				
				// Position Image and Display
				var marginTop = imgHeightAttr / 2 * -1 + canvasHeight / 2;
				var marginLeft = imgWidthAttr / 2 * -1 + canvasWidth / 2;
				canvas.append('<img width="'+imgWidthAttr+'" height="'+imgHeightAttr+'" style="margin:'+marginTop+'px 0 0 '+marginLeft+'px;" src="'+path+'" />').fadeIn(800);
				
				
				// Animate Image Title
				canvas.children('.info')
					.delay(300)
					.css({'margin-top':-20})
					.animate({
						'marginTop' : 0,
						'opacity' : 1
					}, 800, 'easeOutQuint');
					
				
					
			});
		});
	}
	initCanvas();
	
	
	
	/* Canvas & Frame
	-------------------------------*/	
	function initFitCanvas(){
		$('.canvasFit').each(function(){
			
			var gutter = 24;
			var canvas = $(this);
			var canvasWidth = canvas.closest('.frame').width() - gutter * 2;
			var canvasHeight = canvas.closest('.frame').height() - gutter * 2;
			var path = canvas.attr('data-source');	
			
			
			// Display Image on load
			$("<img />").attr("src",path).load(function() {	
					
					
				// Figure out how to center and resize
				// the new image to fit frame.	
				var gridRatio = canvasWidth / canvasHeight;
				var imgRatio = this.width / this.height;
				var imgWidthAttr, imgHeightAttr;
				
				if (imgRatio > gridRatio){
					imgWidthAttr = canvasWidth;
					imgHeightAttr = this.height * (canvasWidth / this.width);
				} else {
					imgWidthAttr = this.width * (canvasHeight / this.height);
					imgHeightAttr = canvasHeight;
				}
				
				imgHeightAttr = Math.round(imgHeightAttr);
				imgWidthAttr = Math.round(imgWidthAttr);
				
				
				if(imgHeightAttr > this.height){
					imgHeightAttr = this.height;	
				}
				if(imgWidthAttr > this.width){
					imgWidthAttr = this.width;	
				}
				
				
				// Position Image and Display
				var marginTop = imgHeightAttr / 2 * -1 + canvasHeight / 2;
				var marginLeft = imgWidthAttr / 2 * -1 + canvasWidth / 2;
				var marginTop = Math.round(marginTop) + gutter;
				var marginLeft = Math.round(marginLeft) + gutter;
				canvas.append('<img width="'+imgWidthAttr+'" height="'+imgHeightAttr+'" style="margin:'+marginTop+'px 0 0 '+marginLeft+'px;" src="'+path+'" />').fadeIn(800);
				
				canvas.append('<div class="share"></div>');
				
				
				// Animate Image Title
				canvas.children('.info')
					.delay(1000)
					.css({'margin-top':-20})
					.animate({
						'marginTop' : 0,
						'opacity' : 1
					}, 800, 'easeOutQuint');
				
				
				
					
			});
		});
	}
	initFitCanvas();
	
	
	
	
	
	/*
		
	$('.smart_form input, .smart_form textarea').on('input', function() {
		var showSaveBtn = 0;
		$(this).parent().children('.input').each(function(){
			if($(this).data('original') != $(this).val()){
				showSaveBtn++;
			}
		});	
		showSaveBtn ? $(this).siblings('.smart_btn').fadeIn() : $(this).siblings('.smart_btn').fadeOut();
	});

	
	// Submit smart form //
	
	$('.smart_form').submit(function(e){
		var thisForm = $(this);
		var formId = $(this).attr('id');
		
		
		if(formId=='username'){
			var username = $("input[name='username']").val();
			var alphaNumeric = /^[a-zA-Z_0-9]+$/;
			if (!alphaNumeric.test(username)) {
				alert("Username can only be letters and numbers");
				return false;
			}
		}
		if(formId=='email'){
			var email = $("input[name='email']").val();
			var validEmail = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
			if (!validEmail.test(email)) {
				alert("Please enter a valid email address.");
				return false;
			}
		}
		if(formId=='password'){
			var password = $("input[name='password']").val();
			var cpassword = $("input[name='cpassword']").val();
			if (password!=cpassword) {
				alert("Passwords do not match! Try Again.");
				return false;
			}
		}
		if(formId=='about'||formId=='tagline'){
			var textareaVal = $(this).find("textarea").val();
			textareaVal = textareaVal.replace(/\s+/g, ' ');	
			$(this).find("textarea").val(textareaVal);
		}
		
		$.ajax({
			type: "POST",
			url: "function/edit-bio.php?a="+formId,
			data: $(e.target).serialize(),
			dataType: "json",
			error: function(jqXHR, textStatus, errorThrown){
				alert(errorThrown);
			},
			success: function(data){
				thisForm.children('.input').each(function(){
					$(this).data('original', $(this).val());
				});
				thisForm.children('.smart_btn').fadeOut();
			}
		});
		return false;
		
	});
	
	*/
		
	$('.smart_form input, .smart_form textarea, .smart_form select').on('input', function() {
		var showSaveBtn = 0;

		$(this).parent().children('.input').each(function(){
			if($(this).data('original') != $(this).val()){
				showSaveBtn++;
			}
		});	
		showSaveBtn ? $(this).siblings('.smart_btn').fadeIn() : $(this).siblings('.smart_btn').fadeOut();

	});



	
	// Submit smart form //
	
	$('.smart_form').submit(function(e){
		var thisForm = $(this);
		var formId = $(this).attr('id');
		
		
		if(formId=='username'){
			var username = $("input[name='username']").val();
			var alphaNumeric = /^[a-zA-Z_0-9]+$/;
			if (!alphaNumeric.test(username)) {
				alert("Username can only be letters and numbers");
				return false;
			}
		}
		if(formId=='email'||formId=='help'){
			var email = $("input[name='email']").val();
			var validEmail = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
			if (!validEmail.test(email)) {
				alert("Please enter a valid email address.");
				return false;
			}
		}
		if(formId=='password'){
			var password = $("input[name='password']").val();
			var cpassword = $("input[name='cpassword']").val();
			if (password!=cpassword) {
				alert("Passwords do not match! Try Again.");
				return false;
			}
		}
		//if(formId=='about'||formId=='tagline'){
		if(formId=='tagline'){
			var textareaVal = $(this).find("textarea").val();
			textareaVal = textareaVal.replace(/\s+/g, ' ');	
			$(this).find("textarea").val(textareaVal);
		}

		if(formId=='te_display'){

		}
		
		$.ajax({
			type: "POST",
			url: "function/edit-bio.php?a="+formId,
			data: $(e.target).serialize(),
			dataType: "json",
			error: function(jqXHR, textStatus, errorThrown){
				alert(errorThrown);
			},
			success: function(data){
				thisForm.children('.input').each(function(){
					$(this).data('original', $(this).val());
				});
				thisForm.children('.smart_btn').fadeOut();
			}
		});
		return false;
		
	});
	
	
	
	
	
});