
var arrowMovementDisabled = false;
var latestSlide;
var amountOfSlides;
$(document).ready(function(){

/*==========================================================================
Portfolio sliders
========================================================================== */  

  $('#slider1').tinycarousel({
    interval: false,
    intervalTime: 2000,
    animationTime: 575,
    start: 2
  });
  var slider = $("#slider1").data("plugin_tinycarousel");
  latestSlide = 6;
  amountOfSlides = $(".carousel-inner .item").length;
    $("#slider1").bind("before-move", function(a,b)
    {
      $(".buttons").off();
      //Before moving
      //Sync with the iphone carousel
      targetSlideNumber = $(b).data("order");
      diff = Math.abs(latestSlide-targetSlideNumber);
      if((targetSlideNumber > latestSlide && diff == 1) || (targetSlideNumber < latestSlide && diff != 1)){        
        $(".carousel-sync").carousel('next');
      }else{
        $(".carousel-sync").carousel('prev');
      }

      //Store the current slide
      latestSlide = targetSlideNumber;

      //Stop the iphone carousel of having its own rotations
      $(".carousel-sync").carousel('pause');

      realSlideNumber = $(b).data("order") +1;
      if(realSlideNumber > amountOfSlides){
        realSlideNumber = 1;
      }
      $("#slider1 li").removeClass('active');
      $("#slider1 li[data-order=" + realSlideNumber + "]").addClass('active');
    });

    $("#slider1").bind("move", function(a,b)
    {
      //After moving
      slider.reSetEvents();
      arrowMovementDisabled = false; 
    });


    $(document).keydown(function(e){
      if (e.keyCode == 37) {
        if(!arrowMovementDisabled){
          arrowMovementDisabled = true;
          $(".carousel-sync").carousel('prev');
        }
        return false;
      }else if(e.keyCode == 39){
        if(!arrowMovementDisabled){
          arrowMovementDisabled = true;   
          $(".carousel-sync").carousel('next');
        }
        return false;
      }
    });
    $('#slider1 li').on('click', function(e) {
      if($(this).data('order') == latestSlide + 2 || (latestSlide == amountOfSlides && $(this).data('order') == 2) || latestSlide == amountOfSlides -1 && $(this).data('order') == 1){
        if(!arrowMovementDisabled){
          arrowMovementDisabled = true;
          slider.moveNext();
        }
      }else if( $(this).data('order') == latestSlide  || latestSlide == amountOfSlides && $(this).data('order') +1 == 6 ){
        if(!arrowMovementDisabled){
          slider.movePrev();
          arrowMovementDisabled = true;
        }
      }
    });



/*Document ready end*/
});
