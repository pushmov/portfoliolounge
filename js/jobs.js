$(document).ready(function(){
	if($('[data-toggle="datepicker"]').length > 0) {
		$('[data-toggle="datepicker"]').datepicker({
			format: 'yyyy-mm-dd',
			startDate: new Date()
		});
	}

	$('#email').on('blur', function(){
		$('.job-form').find('.email-error').remove();
		$.get('function/jobs.php', {e: $('#email').val(), action: 'check_email'}, function(data){
			var json = $.parseJSON(data);
			if (json.status === 'ERROR') {
				$('#email').after('<div class="form-error email-error">'+json.message+'</div>');
				$('#email').focus();
				$('#email').val('');
			}
		});
	});
	

	$('#username').on('blur', function(){
		$('.job-form').find('.username-error').remove();
		$.get('function/jobs.php', {u: $('#username').val(), action: 'check_username'}, function(data){
			var json = $.parseJSON(data);
			if (json.status === 'ERROR') {
				$('#username').after('<div class="form-error username-error">'+json.message+'</div>');
				$('#username').focus();
				$('#username').val('');
			}
		});
	});
	
	if($('#description').length > 0) {
		$('#description').jqte();
	}
	
	$('#submit').on('click', function(e){
		e.preventDefault();
		var form = $(this).closest('form');
		form.find('.form-error').remove();
		$.post(form.attr('action'), form.serialize(), function(data){
			var json = $.parseJSON(data);
			if (json.status === 'ERROR') {
				$.each(json.error, function(k, messages){
					for(var i=0;i<=messages.length - 1;i++) {
						if (k == 'description') {
							form.find('.jqte_editor').after('<div class="form-error">'+messages[i]+'</div>');
						} else {
							form.find('#' + k).after('<div class="form-error">'+messages[i]+'</div>');
						}
					}
					
				});
			} else if(json.status === 'OK') {

				$('#overlay').fadeIn();
				$('.lightbox').html('<i class="fa fa-spinner fa-spin"></i>');
				$('.lightbox').fadeIn();
				$.get('function/jobs.php', {action: 'payment_dialog'}, function(data){
					$('.lightbox').html(data);
				});
				
			}
		});
	});

	$('#update').on('click', function(e){
		e.preventDefault();
		var form = $(this).closest('form');
		$.post(form.attr('action'), form.serialize(), function(data){
			var json = $.parseJSON(data);
			if (json.status === 'ERROR') {
				$.each(json.error, function(k, messages){
					for(var i=0;i<=messages.length - 1;i++) {
						if (k == 'description') {
							form.find('.jqte_editor').after('<div class="form-error">'+messages[i]+'</div>');
						} else {
							form.find('#' + k).after('<div class="form-error">'+messages[i]+'</div>');
						}
					}
					
				});
			} else if(json.status === 'OK') {
				window.location.href = '/admin/my-job-listing';
			}
		});
	});

	$(document).on('click', '#process', function(e){
		e.preventDefault();
		var form = $(this).closest('form');
		form.find('.payment-error').html('');
		$.post(form.attr('action'), form.serialize(), function(data){
			var json = $.parseJSON(data);
			if (json.status === 'OK') {
				$('.lightbox').html('').fadeOut();
				$('#overlay').fadeOut();
				$.get('function/jobs.php', {action: 'payment_success'}, function(html){
					$('#overlay').fadeIn();
					$('.lightbox').html(html);
					$('.lightbox').fadeIn();
				});
			} else {
				form.find('.payment-error').html(json.message);
			}
		});
	});

	$(document).on('click', '#cancel', function(e){
		e.preventDefault();
		$('.lightbox').html('').fadeOut();
		$('#overlay').fadeOut();
	});

	$(document).on('click', '#continue', function(e){
		e.preventDefault();
		window.location.href='/jobs';
	});

	$('.job-list').on('click', '.btn-apply-submit', function(e){
		e.preventDefault();
		$.get('/function/dialog.php?action=confirm_apply_job', {ref: $(this).attr('data-value')}, function(html){
			$('#overlay').fadeIn();
			$('.lightbox').html(html);
			$('.lightbox').fadeIn();
			$('.lightbox').on('click', '#submit_apply', function(){
				var btn = $(this);
				btn.prop('disabled', true);
				var form = $(this).closest('form');
				$.post(form.attr('action'), form.serialize(), function(data){
					btn.prop('disabled', false);
					var json = $.parseJSON(data);
					if (json.status === 'login') {
						window.location.href="/login";
					} else if(json.status === 'OK') {
						window.location.reload();
					}
				});
			});
		});
	});

	$(document).on('click', '.btn-load-more', function(e){
		var btn = $(this);
		var txt = btn.html();
		btn.prop('disabled', true);
		btn.html('<i class="fa fa-spin fa-spinner"></i> Loading');
		$.get('/function/jobs.php?action=more', {seq: $(this).attr('data-seq'), catid:$('#browse_grid').attr('data-cat-id')}, function(html){
			btn.prop('disabled', false);
			btn.html(txt);
			$(this).remove();
			$(document).find('.inner .job:last-child').after(html);
			//$(html).insertAfter('.inner .job:last-child');
		});
	});

	$('.btn-direct-edit').on('click', function(e){
		e.preventDefault();
		window.location.href = '/admin/my-job-listing/edit/' + $(this).attr('data-ref');
	});

	$('.admin-lightbox').on('click', '.btn-close-dialog', function(e){
		e.preventDefault();
		$('.admin-lightbox').fadeOut();
		$('.admin-lightbox').html('');
		$('#overlay').fadeOut();
	});
});