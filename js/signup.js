/* 
 *
 * Signup.js
 * 
 * Description:
 * Validate signup sheet
 * Ensure username is available before submission.
 *
 */

$(document).ready(function(){
	
	
	// Define each input
	var username = $("input.username");
	var email = $("input.email");
	var password = $("input.password");
	var cpassword = $("input.cpassword");
	
	// Define each error box
	var errorUsername = $(".error.username");
	var errorEmail = $(".error.email");
	var errorPasswords = $(".error.passwords");		

	// Regex 
	var validUsername = /^[a-zA-Z_0-9]+$/;
	var validEmail = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;

	// Validation Status
	var passwordsValidated, emailValidated, usernameValidated, usernameAvail;
	
	
	/* VALIDATE - USERNAME 
	----------------------------------*/
	username.blur(function(){
		if(username.val()){
			// Validate Availability
			$.ajax({
				type: "POST",
				url: "function/validate-signup.php?a=username",
				data: "username=" + username.val(),
				error: function(jqXHR, textStatus, errorThrown){
					console.log(errorThrown);
				},
				success: function(data){
					if(data=='username.available') {
						usernameAvail = true;
					}
					if(data=='username.taken') {
						username.addClass("alert");
						errorUsername.append("<p>This username is already taken. Try another.</p>").fadeIn();
						usernameAvail = false;
					}
				}
			});
			// Validate Regex
			if (!validUsername.test(username.val())) {
				username.addClass('alert');
				if(username.val()) errorUsername.append("<p>Only numbers and letters please!<p>").fadeIn();
				usernameValidated = false;
			} else {
				usernameValidated = true;
			}
		} else {
			usernameAvail = false;
			usernameValidated = false;
		}
	});
	username.focus(function(){
		username.removeClass("alert");
		errorUsername.html("").hide();
	});
	
	
	
	/* VALIDATE - EMAIL ADDRESS 
	----------------------------------*/
	email.blur(function(){
		if(email.val()){
			// Validate Email
			if (!validEmail.test(email.val())) {
				emailValidated = false;
				email.addClass('alert');
				if(email.val()) errorEmail.html("<p>Please enter a valid email address.</p>").fadeIn();
			} else {
				emailValidated = true;	
			}
		} else {
			emailValidated = false;
		}
	});
	email.focus(function(){
		email.removeClass("alert");
		errorEmail.html("").hide();
	});
	
	
	/* VALIDATE - PASSWORDS
	----------------------------------*/
	cpassword.blur(function(){
		if(password.val() != cpassword.val()){
			password.addClass('alert');
			cpassword.addClass('alert');
			errorPasswords.html("Your passwords don't match!").fadeIn();
			passwordsValidated = false;	
		}
		if(password.val() == cpassword.val() && password.val()) passwordsValidated = true;
	});
	password.blur(function(){
		if(password.val() != cpassword.val() && cpassword.val()){
			password.addClass('alert');
			cpassword.addClass('alert');
			errorPasswords.html("Your passwords don't match!").fadeIn();
			passwordsValidated = false;	
		}
		if(password.val() == cpassword.val() && password.val()) passwordsValidated = true;	
	});
	cpassword.focus(function(){
		password.removeClass('alert');
		cpassword.removeClass('alert');
		errorPasswords.html("");
	});
	password.focus(function(){
		password.removeClass('alert');
		cpassword.removeClass('alert');
		errorPasswords.html("");
	});
	
	
	
	
	
	
	/* SUBMIT FORM
	----------------------------------*/
	$("#signup-form").submit(function() {
		
		// Start Validation
		validated = false;
		
		// Check each field
		if (usernameAvail && usernameValidated && emailValidated && passwordsValidated) validated = true;
		
		// Send
		if (validated) return;
		
		// Don't Sent
		return false;
		 
	});
	
	
	
	
	
	/* Upgrade
	-----------------------------------*/
	$("#checkout-form").submit(function(e) {
		$.ajax({
			type: "POST",
			url: "function/paypal-subscribe.php",
			data: $(e.target).serialize(),
			//dataType: "json",
			beforeSend:function(){
				$(".upgrade-view").hide();
				$("#payment-processing-view").fadeIn();
			},
			error: function(jqXHR, textStatus, errorThrown){
				$(".upgrade-view").hide();
				$("#payment-result-view").html(errorThrown);
				$("#payment-result-view").fadeIn();
			},
			success: function(data){
				$(".upgrade-view").hide();
				$("#payment-result-view").html(data);
				$("#payment-result-view").fadeIn();
			}
		});
		return false;
	});
	
	
	
	
	/* Upgrade
	-----------------------------------*/
	$("#update-creditcard-form").submit(function(e) {
		$.ajax({
			type: "POST",
			url: "function/paypal-update-creditcard.php",
			data: $(e.target).serialize(),
			//dataType: "json",
			beforeSend:function(){
				$(".upgrade-view").hide();
				$("#payment-processing-view").fadeIn();
			},
			error: function(jqXHR, textStatus, errorThrown){
				$(".upgrade-view").hide();
				$("#payment-result-view").html(errorThrown);
				$("#payment-result-view").fadeIn();
			},
			success: function(data){
				$(".upgrade-view").hide();
				$("#payment-result-view").html(data);
				$("#payment-result-view").fadeIn();
			}
		});
		return false;
	});
	
	


});