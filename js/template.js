/* Choose Template
-------------------------------*/
function selectTemplate(e){
	var thisPreview = $(this);
	var templateId = thisPreview.find("input#template-id").val(); // username, name, custom, upload
	var memberId = thisPreview.find("input#member-id").val();
	$.ajax({
		type: "POST",
		url: "function/edit-options.php?a=template&b="+templateId+"&c="+memberId,
		beforeSend:function(){
			thisPreview.find(".check").addClass("checking");
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		},
		success: function(data){
			$("#templates .template .check").removeClass("checked");
			$("#templates .template .check").removeClass("checking");
			thisPreview.find(".check").addClass("checked");
			
			if(templateId==0){
				$(".layout-options").removeClass("hide");
				//$(".color-selector").removeClass("hide");
			} else {
				$(".layout-options").addClass("hide");
				//$(".color-selector").addClass("hide");
			}
			
			/*
			if(templateId > 6){
				$(".old-color-scheme").addClass("hide");
				$(".new-color-scheme").removeClass("hide");
				setColorScheme(2);
			} else {
				$(".new-color-scheme").addClass("hide");
				$(".old-color-scheme").removeClass("hide");
				setColorScheme(0);
 			}  
			*/
			
		}
	});
}
$("#templates .template").not(".paidOnly").on("click", selectTemplate);

function openLightBoxMessage(){
	lightbox('pleaseUpgradeTemplate');	
}
$("#templates .paidOnly").on("click", openLightBoxMessage);


/* Choose Layout
-------------------------------*/
function selectLayout(e){
	var thisPreview = $(this);
	var layoutId = thisPreview.find("input#layout-id").val(); // username, name, custom, upload
	var memberId = thisPreview.find("input#member-id").val();
	$.ajax({
		type: "POST",
		url: "function/edit-options.php?a=layout&b="+layoutId+"&c="+memberId,
		beforeSend:function(){
			thisPreview.find(".check").addClass("checking");
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		},
		success: function(data){
			$("#layouts .layout-preview .check").removeClass("checked");
			$("#layouts .layout-preview .check").removeClass("checking");
			thisPreview.find(".check").addClass("checked");
		}
	});
}
$("#layouts .layout-preview").on("click", selectLayout);


/* Choose Color
-------------------------------*/
function selectColorScheme(e){
	var thisScheme = $(this);
	var colorSchemeId = thisScheme.find("input#color-scheme-id").val();
	var memberId = thisScheme.find("input#member-id").val();
	$.ajax({
		type: "POST",
		url: "function/edit-options.php?a=scheme&b="+colorSchemeId+"&c="+memberId,
		beforeSend:function(){
			thisScheme.find(".check").addClass("checking");
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		},
		success: function(data){
			$(".color-selection .check").removeClass("checked");
			$(".color-selection .check").removeClass("checking");
			thisScheme.find(".check").addClass("checked");
		}
	});
}
$(".color-selection").on("click", selectColorScheme);


/* Set Color on template change
-------------------------------*/
function setColorScheme(colorSchemeId){
	$.ajax({
		type: "POST",
		url: "function/edit-options.php?a=scheme&b="+colorSchemeId+"&c=",
		error: function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		},
		success: function(data){
			$(".color-selection .check").removeClass("checked");
			if(colorSchemeId > 1){
				$(".new-color-scheme:first .inner .check").addClass("checked");
			} else {
				$(".old-color-scheme:first .inner .check").addClass("checked");
			}
		}
	});
}
 


/* Choose Font
-------------------------------*/
function selectFont(e){
	var thisFont = $(this);
	var fontId = thisFont.attr('id');
	var memberId = thisFont.find("input#member-id").val();
	$.ajax({
		type: "POST",
		url: "function/edit-options.php?a=font&b="+fontId+"&c="+memberId,
		beforeSend:function(){
			thisFont.find(".check").addClass("checking");
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		},
		success: function(data){
			$(".font_preview .check").removeClass("checked");
			$(".font_preview .check").removeClass("checking");
			thisFont.find(".check").addClass("checked");
		}
	});
}
$(".font_preview").on("click", selectFont);







