function templateCtrl($scope, $http) {

	/* Templates
	------------------------*/
	$scope.getTemplates = function(){
		$http.get("/services/templates/get").success(function(data, response) {
			$scope.templates = data;
		},
    function(data) {
        alert("Something went wrong!");
    });
	};
	$scope.getTemplates();

	$scope.setTemplate = function(id){
		$scope.currentTemplate=id;
		$http.get("/services/templates/set/"+id).success(function(data, response) {

		},
    function(data) {
        alert("Something went wrong!");
    });
	};

	/* Set Background
	------------------------*/
	$scope.setBackground = function(id){
		$scope.currentBg = id;
		$http.get("/services/background/set/"+id).success(function(data, response) {
		},
    function(data) {
        alert("Something went wrong!");
    });	
	};

	$scope.backgrounds = [
		{slug:'forest'},
		{slug:'wood'},
		{slug:'desktop'},
		{slug:'rocks'},
		{slug:'mountain'}
	]


	$scope.currentFont;
	
	/* Fonts
	------------------------*/
	$scope.getFonts = function(){
		$http.get("/services/fonts/get").success(function(data, response) {
			$scope.fonts = data;
		});
	}
	$scope.getFonts();
	
	$scope.setFont = function(id){
		$scope.currentFont = id;
		$http.get("/services/fonts/set/"+id).success(function(data, response) {
		},
    function(data) {
        alert("Something went wrong!");
    });	
	};
	
	
	
	
	/* Colors
	------------------------*/
	$scope.getColors = function(){
		$http.get("/services/colors/get").success(function(data, response) {
			$scope.colors = data;
		});
	}
	$scope.getColors();
	
	$scope.setColor = function(id){
		$scope.currentColor = id;
		$http.get("/services/colors/set/"+id).success(function(data, response) {
		},
    function(data) {
        alert("Something went wrong!");
    });	
	};
	
	
	
}




