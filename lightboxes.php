<div id="overlay" style="display:none;"></div>
	
    
<!-- Downgrade Account -->
<div id="downgrade" class="lightbox" style="display:none;">

	<!-- CONFIRM -->
	<div class="view confirmation">
    	<h2>Are you sure you want to downgrade?</h2>
        <ul>
            <li>Your project and item limit will be decreased.</li>
            <li>You will not be charged again.</li>
        </ul>
        <a id="confirm-downgrade" href="javascript:void(0)" class="btn red">Downgrade Account</a><br />
    	<a onclick="lightbox('cancel')">Cancel</a>
    </div>
    
    <!-- PROCESSING -->
    <div class="view processing" style="display:none;">
    	<h2>Please wait,</h2>
        <p>While we process your downgrade.</p>
    	<img src="images/icon-unchecked.gif" />
    </div>
    
    <!-- RESULT -->
    <div class="view result" style="display:none;">
    	<!-- content found in subscribe function -->
    </div>
    
    
</div>


<!-- Cant Use Feature until UPgraded -->
<div id="pleaseUpgradeTemplate" class="lightbox small" style="display:none;">
	<center>
	<h2>Sorry!</h2>
	<br />
	<h3 class="center">This template is only available for <strong>Pro</strong> or <strong>Max</strong> users.</h3>
	<br />
    <a href="/admin/upgrade" class="btn blue">Upgrade</a> &nbsp;
    <a onclick="lightbox('cancel')" class="b tn gray">Got it</a>
	<div class="clr"></div>
	<!--
    <h3 style="font-size:24px;margin:0 0 30px;"><!-- check main.js for content </h3>
    <a class="btn gray" href="/admin/upgrade">Upgrade  &raquo;</a>-->
	<!--
    <span class="sep">|</span>
    <a onclick="lightbox('cancel')">No thanks.</a>
	<p>Free domain name included :)</p>
	-->
	</center>
</div>
<!-- Cant Use Feature until UPgraded -->
<div id="pleaseUpgrade" class="lightbox small" style="display:none;">
	<center>
	<h2>Sorry!</h2>
	<br />
	<h3 class="center">This option is only available for <strong>Pro</strong> or <strong>Max</strong> users.</h3>
	<br />
    <a href="/admin/upgrade" class="btn blue">Upgrade</a> &nbsp;
    <a onclick="lightbox('cancel')" class="bt n gray">Got it</a>
	<div class="clr"></div>
	<!--
    <h3 style="font-size:24px;margin:0 0 30px;"><!-- check main.js for content </h3>
    <a class="btn gray" href="/admin/upgrade">Upgrade  &raquo;</a>-->
	<!--
    <span class="sep">|</span>
    <a onclick="lightbox('cancel')">No thanks.</a>
	<p>Free domain name included :)</p>
	-->
	</center>
</div>

<!-- Cant Use Feature until UPgraded -->
<div id="paid-only" class="lightbox small" style="display:none;">
    <a href="/admin/upgrade" class="upgrade-thumb"></a>
    <h3 style="font-size:24px;margin:0 0 30px;"><!-- check main.js for content --></h3>
    <a class="btn gray" href="/admin/upgrade">Upgrade  &raquo;</a>
	<!--
    <span class="sep">|</span>
    <a onclick="lightbox('cancel')">No thanks.</a>
	<p>Free domain name included :)</p>
	-->
	<p>Free domain name included.</p>
</div>


<!-- Exceed Project limit -->
<div id="project-limit" class="lightbox" style="display:none;">
    <a href="/admin/upgrade" class="upgrade-thumb"></a>
    <h3 style="font-size:24px;margin:0 0 30px;"><!-- check main.js for content --></h3>
    <a class="btn gray" href="/admin/upgrade">Get More &raquo;</a>
	<!--
    <span class="sep">|</span>
    <a onclick="lightbox('cancel')">No thanks.</a>
	<p>Free domain name included :)</p>
	-->
	<p>Free domain name included.</p>
</div>

<!-- Exceed Item limit -->
<div id="item-limit" class="lightbox" style="display:none;">
    <h3><!-- check main.js for content --></h3>
    <a href="/admin/upgrade" class="upgrade-thumb"></a>
    <a class="btn gray" href="/admin/upgrade">Get More &raquo;</a>
	<!--
    <span class="sep">|</span>
    <a onclick="lightbox('cancel')">No thanks.</a>
	-->
	<p>You can also earn free items on the <a title="Earn free items" href="admin/account/rewards">Rewards page!</a></p>
</div>

<!-- Exceed Item limit (show Max) -->
<div id="item-limit" class="lightbox max" style="display:none;">
    <h3><!-- check main.js for content --></h3>
    <!--<p>We receive an enormous amount of uploaded items every day, and your help is greatly appreciated!</p>-->
    <a href="/admin/upgrade" class="upgrade-thumb max"></a>
    <a class="btn gray" href="/admin/upgrade">Check it out</a>
	<!--
    <span class="sep">|</span>
    <a onclick="lightbox('cancel')">No thanks.</a>
	-->
	<p>Free domain name included :)</p>
</div>


<!-- Errors -->
<div id="error" class="lightbox" style="display:none;">
    <h3></h3>
    <p></p>
    <a href="javascript:void(0)"  onclick="lightbox('cancel')">Try Again</a>
</div>


	
<!-- Upload Items -->
<div id="upload-items" class="lightbox" style="display:none;">
	<div class="message">
        <h3>Please wait.</h3>
        <em>Each image is being uploaded, duplicated, and resized to fit many different portfolio styles.</em>
    </div>
	<div id="progress">
		<div class="bar" style="width: 0%;"></div>
	</div>
</div>


	
	
<!-- Upload Profile Image -->
<div id="upload" class="lightbox" style="display:none;">
	<h3>Profile Image</h3>
	<br />
	<form id="profile-img" enctype="multipart/form-data" action="function/edit-profile-img.php?a=profile" method="POST">
		<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
		<input name="uploaded_image" type="file"  />
		<input type="submit" value="Upload File" />
	</form>
	<a href="javascript:void(0)"  onclick="lightbox('cancel');">Cancel</a>
</div>



<!-- Delete Project -->
<div id="confirm" class="lightbox confirm" style="display:none;">
	Are you sure you want to delete this project?
	<form id="delete" enctype="multipart/form-data" action="function/edit-projects.php?a=delete" method="POST">
		<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
		<input class="project_id" type="hidden" name="project_id" value="" />
		<input class="btn large red" type="submit" value="Yes, Delete this Project!" />
		<br />
		<a href="javascript:void(0)"  onclick="lightbox('cancel');">Cancel</a>
	</form>
</div>

<!-- Delete Item -->
<div id="deleteItem" class="lightbox confirm" style="display:none;">
	Are you sure you want to delete this item?
	<form id="delete" enctype="multipart/form-data" action="function/edit-items.php?a=delete" method="POST">
		<input class="item_id" type="hidden" name="item_id" value="" />
		<input type="hidden" name="project_id" value="<?php echo $_GET['x'];?>" />
		<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
		<input type="submit" class="btn red large" value="Yes, Delete this Item." />
		<br />
		<a href="javascript:void(0)"  onclick="lightbox('cancel');">Cancel</a>
	</form>
</div>



<!-- New Project -->
<div id="new-project" class="lightbox" style="display:none;">
	<form id="add-project" enctype="multipart/form-data" action="function/edit-projects.php?a=add" method="POST">
		<h2>Create a Project</h2>
		<br />
		<div class="row">
			<label>Project Title</label>
			<input class="input" id="project_title" maxlength="35" name="project_title" />
		</div>
		<div class="row">
			<!--<label>Category <em style="font-style:italic;font-size:10px">optional</em></label>	-->
			<select style="    margin: 0 27px 0 0;
    text-align: left;
    float: right;" name="category_id" ng-model="category_id">
					<option value="">Category (optional)</option>
				<?php $get_categories = $db->query("SELECT * FROM categories");
				foreach($get_categories->fetchAll() as $category){?>
					<option value="<?=$category['category_id']?>" <?php if(isset($project['category_id']) && $project['category_id']==$category['category_id']){ echo"selected";}?>><?=$category['category_name']?></option>
				<?}?>
			</select>
			<br>
			<br>
		</div>
        <!--
		<div class="row radios">
            <input name="project_type" value="image" type="radio" checked="checked" /><span>Images</span>
            <input name="project_type" value="video" type="radio" /><span>Video</span>
        </div>
        -->
        <div class="row">
			<label>Project Description <em style="font-style:italic;font-size:10px">optional</em></label>
			<textarea class="input" name="project_desc"></textarea>
		</div>
        
		<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
		<input class="btn" type="submit" value="Create Project!" />
		<div class="clear"></div>
		<a href="javascript:void(0)"  onclick="lightbox('cancel');">Cancel</a>
	</form>
</div>





<!-- Add a Video -->
<div id="add-video" class="lightbox" style="display:none;">
	<form id="add-project" enctype="multipart/form-data" action="function/edit-items.php?a=add-video" method="POST">
		<h2>Add a Vimeo Video</h2>
		<br />
		<div class="row">
			<label>Vimeo ID</label><br />
			<input name="video_id" />
		</div>
		<div class="row">
			<label>Video Title <em style="font-style:italic;font-size:10px">optional</em></label><br />
			<input maxlength="100" name="video_title" />
		</div>
        <div class="row">
			<label>Video Description <em style="font-style:italic;font-size:10px">optional</em></label>
			<textarea name="video_description"></textarea>
		</div>
        
		<input type="hidden" name="video_type" value="vimeo" />
		<input type="hidden" name="member_id" value="<?php echo $memberid;?>" />
		<input type="hidden" name="project_id" value="<?php echo $x;?>" />
        
		<input class="btn large" type="submit" value="Save Video" />
		<div class="clear"></div>
		<a href="javascript:void(0)"  onclick="lightbox('cancel');">Cancel</a>
	</form>
</div>



<!-- Change Cover -->
<div id="change-cover" class="lightbox" style="display:none;">
	<h3>Change Project Cover</h3>
	<hr />
	<div class="cover-thumbs">
	<?php // Get Items
	//$get_item_thumbs = mysql_query("SELECT * FROM items WHERE member_id = $memberid ORDER BY item_order") or die(mysql_error()); 
	$get_item_thumbs = $db->prepare("SELECT * FROM items WHERE member_id = :memberid ORDER BY item_order");
	$get_item_thumbs->bindValue(':memberid', $memberid);
	$get_item_thumbs->execute();
	$item_thumbs = $get_item_thumbs->fetchAll();
	//mysql_data_seek($get_item_thumbs,0);
	foreach($item_thumbs as $item_thumb):?>
		<a class="cover<?php echo $item_thumb["project_id"];?>" href="function/edit-projects.php?a=cover&project_id=<?php echo $item_thumb["project_id"];?>&cover_id=<?php echo $item_thumb["item_id"];?>&page_view=<?php echo $i;?>&e=<?php echo $x;?>" style="display:none;height:100px;width:100px;float:left;background:url(uploads/<?php echo $memberid.'/small/'.$item_thumb['item_filename'];?>) center center;"></a>
	<?php endforeach; ?>
	<?php
	/*
	 if (!$item_count){?>
		<div  style="text-align:center;">
			<h1 class="text-center" style="margin:0 0 8px;text-align:center;">Oops.</h1>
			<h2>Go back and upload some items first!</h2>
		</div>
	<?php } */?>
	</div>
	<div class="clear"></div>
	<hr />
	<a href="javascript:void(0)"  onclick="lightbox('cancel');">Cancel</a>
</div>

	
<!-- Upload Items -->
<div id="tips" class="lightbox" style="display:none;">
	<div class="inner">
		<div class="message">
			<h2>Upload Tips</h2>
			<div>
				<hr style="margin:20px 0" />
				<span>Small uploads may look stretched out. Use large images!</span>
				<hr style="margin:20px 0" />
				<span>Please keep file size(s) less than 3mb</span>
				<hr style="margin:20px 0" />
				<span>Please keep images less than 3000px x 3000px</span>
			</div>
		</div>
			<hr style="margin:20px 0" />
		<a href="javascript:void(0)"  onclick="lightbox('cancel');"><strong>Close</strong></a>
	</div>
</div>



<div id="privacy" class="lightbox" style="display:none;">
	<form class="smallform ng-pristine ng-valid" action="function/edit-projects.php?a=make-private-project&project_id=<?=$x?>" method="POST">
		<h2>Make Project Private</h2>
		<br>
		<div class="row">
			<label>Create a password</label>
			<input class="input" id="project_title" name="password2">
		</div>
		<input type="hidden" name="member_id" value="1">
		<input class="btn" type="submit" value="Save Password">
		<div class="clear"></div>
		<a href="javascript:void(0)" onclick="lightbox('cancel');">Cancel</a>
	</form>
</div>