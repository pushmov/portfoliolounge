<div class="center-title-block">
    <h2>Oops!</h2>
    <h3>Try logging in again.</h3>
    <?php if($p=='login-reset-password'){?>
    <h4>Your password has been reset.</h4>
    <?php }?>
</div>
<form id="signup-form" method="post" action="function/login.php">
	<?php if($i=='error'){?>
	
		<!-- Error Message -->
		<?php if($x=='deleted'){?>
			<p class="error show" style="text-align:center">Deleted account!</p>
		<?php } else {?>
			<p class="error show" style="text-align:center">Incorrect username and password combination. Please try again.</p>
		<?php } ?> 
	
	<br />
	<?php }?>
	<br />
	<div class="inputs pad-sm">
		<input class="username" id="username" name="username" placeholder="Username" />
		<input class="password" name="password" type="password" placeholder="Password" />
    </div>
    <input class="promobtn" type="submit" value="Login" />
	<br /><br /><br />
	<p class="cancel"><a href="/">Cancel</a> &nbsp; | &nbsp; <a href="help">Need help logging in?</a></p>
    <ul class="errors" style="position:absolute;color:#C30;font-weight:bold;"></ul>
    <input id="redirect" type="hidden" name="redirect" value="<?php echo $_SESSION["LOGIN_REDIRECT"];?>" />
</form>