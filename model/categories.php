<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/model.php';
class Categories extends Model {

	protected $_fields = array('category_id' => null, 'category_name' => '', 'category_url' => '');
	protected $_table_name = 'categories';

	public function __construct() {
		parent::__construct();
	}

	public function get_all() {
		$cat = $this->_db->prepare("SELECT * FROM {$this->_table_name} ORDER BY category_id ASC");
		$cat->execute();
		return $cat->fetchAll();
	}

	public function get_category_number($slug=null){
		if($slug === null) {
			return false;
		}
		$cat = $this->_db->prepare("SELECT * FROM {$this->_table_name} WHERE category_url = :url LIMIT 1");
		$cat->bindValue(':url', trim($slug));
		$cat->execute();
		return $cat->fetch();
	}


}

$categories = new Categories();