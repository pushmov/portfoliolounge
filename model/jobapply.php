<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/model.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/members.php';
class Jobapply extends Model {

	const S_APPLIED = 1;
	const S_REJECTED = 0;

	protected $_table_name = 'job_apply';
	protected $_members;
	protected $_status = array(self::S_REJECTED => 'Rejected', self::S_APPLIED => 'Applied');

	public function __construct () {
		global $members;
		parent::__construct();
		$this->_members =& $members;
	}

	public function applied($job_id){
		$applied = $this->_db->prepare("SELECT COUNT(id) AS total FROM {$this->_table_name} WHERE jobs_id = :jobid");
		$applied->bindValue(':jobid', $job_id);
		$applied->execute();
		$a = $applied->fetch();
		return $a['total'];
	}

	public function get_apply_status($id = null) {
		if ($id === null) {
			return $this->_status;
		}
		return array_key_exists($id, $this->_status) ? $this->_status[$id] : $this->_status;
	}

	public function is_applied($ref_id, $member_id) {
		$getjob = $this->_db->prepare("SELECT * FROM jobs WHERE reference_id =:refid LIMIT 1");
		$getjob->bindValue(':refid', $ref_id);
		$getjob->execute();
		$job = $getjob->fetch();

		$is_applied = $this->_db->prepare("SELECT COUNT(id) AS total FROM job_apply WHERE member_id = :mid AND jobs_id = :id");
		$is_applied->bindValue(':mid', $member_id);
		$is_applied->bindValue(':id', $job['job_id']);
		$is_applied->execute();
		$applied = $is_applied->fetch();
		if ($applied['total'] <= 0) {
			return false;
		} else {
			return true;
		}
	}

	public function dtt_applied($params) {
		$aColumns = array('email', 'username', 'first_name', 'last_name', 'account_type', 'last_login', 'date_applied','item_count', 'project_count', 'custom_domain');
		$data = array(
			'draw' => $params['draw'],
			'recordsTotal' => 1,
			'recordsFiltered' => 1
		);
		$orderby = $aColumns[$params['order'][0]['column']];
		$orderdir = strtoupper($params['order'][0]['dir']);
		
		$searchsql = "";
		if ($params['search']['value'] != '') {
			$searchsql = " WHERE (";
			$searchsql .= "email LIKE :search1";
			$searchsql .= " OR username LIKE :search2";
			$searchsql .= " OR first_name LIKE :search3";
			$searchsql .= " OR last_name LIKE :search4";
			$searchsql .= " OR account_type LIKE :search5";
			$searchsql .= " OR last_login LIKE :search6";
			$searchsql .= " OR date_applied LIKE :search7";
			$searchsql .= " OR item_count LIKE :search8";
			$searchsql .= " OR project_count LIKE :search9";
			$searchsql .= " OR custom_domain LIKE :search10";
			$searchsql .= ")";
		}

		$rows = array();
		$subsql = "(SELECT 
			job_apply.status, job_apply.date_updated, 
			members.email, members.username, members.first_name, members.last_name, 
			{$this->_members->get_account_type_sql('account_type')},
			DATE_FORMAT(FROM_UNIXTIME(members.last_login),'%b %e, %Y') AS last_login, 
			DATE_FORMAT(job_apply.date_applied, '%b %e, %Y') AS date_applied, 
			job_apply.id, members.item_count, members.project_count, members.custom_domain 
			FROM {$this->_table_name} INNER JOIN members 
			ON members.member_id = {$this->_table_name}.member_id 
			WHERE jobs_id = :id) AS tmp";
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM " . $subsql . $searchsql;
		$sql .= " ORDER BY {$orderby} {$orderdir} LIMIT :offset, :lim";
		$getmembers = $this->_db->prepare($sql);
		$getmembers->bindValue(':lim', $params['length']);
		$getmembers->bindValue(':offset', $params['start']);
		$getmembers->bindValue(':id', $params['job_id']);
		if ($params['search']['value'] != "") {
			$getmembers->bindValue(':search1', "%".$params['search']['value']."%");
			$getmembers->bindValue(':search2', "%".$params['search']['value']."%");
			$getmembers->bindValue(':search3', "%".$params['search']['value']."%");
			$getmembers->bindValue(':search4', "%".$params['search']['value']."%");
			$getmembers->bindValue(':search5', "%".$params['search']['value']."%");
			$getmembers->bindValue(':search6', "%".$params['search']['value']."%");
			$getmembers->bindValue(':search7', "%".$params['search']['value']."%");
			$getmembers->bindValue(':search8', "%".$params['search']['value']."%");
			$getmembers->bindValue(':search9', "%".$params['search']['value']."%");
			$getmembers->bindValue(':search10', "%".$params['search']['value']."%");
		}
		$getmembers->execute();
		$members = $getmembers->fetchAll();
		if(!empty($members)) {
			foreach($members as $row) {

				if ($row['status'] == self::S_APPLIED) {
					$act_reject = \Html::anchor('#', 'Reject', array('data-value' => $row['id'], 'class' => 'action-reject'));
				} else {
					$act_reject = $this->_status[$row['status']] . ' at ' . date_create($row['date_updated'])->format('F d, Y');
				}

				$values = array(
					'email' => $row['email'],
					'username' => \Html::anchor('https://' . $row['username'] . '.'.SITE_URL, $row['username']),
					'first_name' => $row['first_name'],
					'last_name' => $row['last_name'],
					'account_id' => $row['account_type'],
					'last_login' => $row['last_login'],
					'date_applied' => $row['date_applied'],
					'portfolio_item' => $row['item_count'],
					'project_count' => $row['project_count'],
					'external_url' => $row['custom_domain'],
					'action' => $act_reject
				);
				$rows[] = array_values($values);
			}
		}
		$data['data'] = $rows;
		return $data;
	}

	public function delete_job($job_id) {
		$del = $this->_db->prepare("DELETE FROM {$this->_table_name} WHERE jobs_id = :id");
		$del->bindValue(':id', $job_id);
		return $del->execute();
	}

	public function reject_apply($id){
		$response = array('status' => false, 'message' => 'DB Error');
		$reject = $this->_db->prepare("UPDATE {$this->_table_name} SET date_updated = :updated, status = :status WHERE id = :id");
		$reject->bindValue(':updated', date_create()->format('Y-m-d H:i:s'));
		$reject->bindValue(':status', self::S_REJECTED);
		$reject->bindValue(':id', $id);
		$executed = $reject->execute();
		if ($executed) {
			$response = array('status' => true, 'message' => 'Rejected');
		}
		return $response;
	}

	public function is_rejected($ref_id, $member_id) {
		$isrejected = $this->_db->prepare("SELECT COUNT(id) AS total FROM job_apply INNER JOIN jobs ON jobs.job_id = job_apply.jobs_id WHERE jobs.reference_id = :ref_id AND job_apply.member_id = :mem_id AND job_apply.status = :status LIMIT 1");
		$isrejected->bindValue(':ref_id', $ref_id);
		$isrejected->bindValue(':mem_id', $member_id);
		$isrejected->bindValue(':status', self::S_REJECTED);
		$isrejected->execute();
		$reject = $isrejected->fetch();
		if ($reject['total'] <= 0) {
			return false;
		} else {
			return true;
		}

	}

	public function get_rejected_date($ref_id, $member_id) {
		$isrejected = $this->_db->prepare("SELECT job_apply.date_updated FROM job_apply INNER JOIN jobs ON jobs.job_id = job_apply.jobs_id WHERE jobs.reference_id = :ref_id AND job_apply.member_id = :mem_id AND job_apply.status = :status LIMIT 1");
		$isrejected->bindValue(':ref_id', $ref_id);
		$isrejected->bindValue(':mem_id', $member_id);
		$isrejected->bindValue(':status', self::S_REJECTED);
		$isrejected->execute();
		$reject = $isrejected->fetch();
		if (!is_array($reject)) {
			return false;
		} else {
			return date_create($reject['date_updated'])->format('F d, Y');
		}
	}
	
}

$job_apply = new Jobapply();