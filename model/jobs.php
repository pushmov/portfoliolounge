<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/model.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/country.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobapply.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/html.php';
use Cocur\Slugify\Slugify;
use ZackKitzmiller\Tiny;

class Jobs extends Model {

	const PER_SEGMENT = 5;

	const REMOTEABLE_YES = 1;
	const REMOTEABLE_NO = 0;

	const EMPLTYPE_FULLTIME = 0;
	const EMPLTYPE_PARTTIME = 1;
	const EMPLTYPE_FREELANCE = 2;

	const PUBLISH_N = 0;
	const PUBLISH_Y = 1;

	const YEARS_OF_EXP_DEFAULT = 1;
	const YEARS_OF_EXP_RANGE = 10;

	const PORTFOLIO_ITEMS_DEFAULT = 1;
	const PORTFOLIO_ITEMS_RANGE = 10;

	protected $_table_name = 'jobs';
	protected $_fields = array( 'job_id' => null, 'member_id' => '', 'reference_id' => '',
		'years_of_exp' => self::YEARS_OF_EXP_DEFAULT, 'remoteable' => self::REMOTEABLE_NO, 'country' => \Country::DEFAULT_COUNTRY, 'prov_state' => '',
		'city' => '', 'postal_code' => '', 'salary' => '', 
		'employment_type' => self::EMPLTYPE_FULLTIME,
		'created_at' => '', 'updated_at' => '', 'title' => '', 'slug' => '',
		'description' => '', 'email' => '', 'cell' => '',
		'expired_at' => null, 'company_name' => '', 'publish' => self::PUBLISH_N, 'company_website' => null,
		'portfolio_items' => self::PORTFOLIO_ITEMS_DEFAULT,
		'stripe_charge_id' => ''
	);
	protected $_remoteable = array(self::REMOTEABLE_NO => 'Onsite', self::REMOTEABLE_YES => 'Remote');
	protected $_empltype = array(self::EMPLTYPE_FULLTIME => 'Full Time', self::EMPLTYPE_PARTTIME => 'Part Time', self::EMPLTYPE_FREELANCE => 'Freelance');
	protected $_publish = array(self::PUBLISH_N => 'Don\'t Publish', self::PUBLISH_Y => 'Publish');
	protected $_slugify;
	protected $_tiny;
	protected $_jobapply;

	public function __construct () {
		global $job_apply;
		parent::__construct();
		$this->_slugify = new Slugify();
		$this->_tiny = new Tiny('7WtvE3FTYBNwBrFRVYDTUPw8N9BhdumZ');
		$this->_jobapply =& $job_apply;
	}

	public function get_fields() {
		return $this->_fields;
	}

	public function insert($data) {
		$refid = $this->_tiny->to($data['member_id'].time());
		$data['reference_id'] = $refid;
		$data['slug'] = $this->_slugify->slugify($data['title'].' '.$refid);
		$data['created_at'] = $data['updated_at'] = date_create()->format('Y-m-d H:i:s');
		if ($data['expired_at'] == '') {
			unset($data['expired_at']);
		}
		return parent::insert($data);
	}

	public function get_remoteable_label($type = null) {
		if ($type === null) {
			return $this->_remoteable;
		}
		return array_key_exists($type, $this->_remoteable) ? $this->_remoteable[$type] : $this->_remoteable;
	}

	public function get_remoteable_sql($alias = 'tmp'){
		$sql = "(CASE ";
		foreach ($this->_remoteable as $val => $txt) {
			$sql .= " WHEN remoteable = '{$val}' THEN '{$txt}'";
		}
		$sql .= " END) AS ";
		$sql .= $alias;
		return $sql;
	}

	public function get_empltype_label($type = null) {
		if ($type === null) {
			return $this->_empltype;
		}
		return array_key_exists($type, $this->_empltype) ? $this->_empltype[$type] : $this->_empltype;
	}

	public function get_empltype_sql($alias = 'tmp') {
		$sql = "(CASE ";
		foreach ($this->_empltype as $val => $txt) {
			$sql .= " WHEN employment_type = '{$val}' THEN '{$txt}'";
		}
		$sql .= " END) AS ";
		$sql .= $alias;
		return $sql;
	}

	public function get_publish_label($publish = null) {
		if ($publish === null) {
			return $this->_publish;
		}
		return array_key_exists($publish, $this->_publish) ? $this->_publish[$publish] : $this->_publish;
	}

	public function get_years_of_exp_label($exp = null){
		$data = array();
		for ($i = 1; $i <= self::YEARS_OF_EXP_RANGE; $i++) {
			$data[$i] = $i . ' yrs.';
		}
		if ($exp === null) {
			return $data;
		}
		return array_key_exists($exp, $data) ? $data[$exp] : $data;
	}

	public function get_years_of_exp_sql($alias = 'tmp') {
		$sql = "(CASE ";
		foreach ($this->get_years_of_exp_label() as $val => $txt) {
			$sql .= " WHEN years_of_exp = '{$val}' THEN '{$txt}'";
		}
		$sql .= " END) AS ";
		$sql .= $alias;
		return $sql;
	}

	public function get_portfolio_items_label($items = null) {
		$data = array();
		for ($i = 1; $i <= self::PORTFOLIO_ITEMS_RANGE; $i++) {
			$data[$i] = $i . ' items';
		}
		if ($items === null) {
			return $data;
		}
		if ($items == 0) {
			return 0 .' items';
		}
		return array_key_exists($items, $data) ? $data[$items] : $data;
	}

	public function get_portfolio_items_sql($alias='tmp') {
		$sql = "(CASE ";
		foreach ($this->get_portfolio_items_label() as $val => $txt) {
			$sql .= " WHEN portfolio_items = '{$val}' THEN '{$txt}'";
		}
		$sql .= " END) AS ";
		$sql .= $alias;
		return $sql;
	}

	public function get_all($limit=null, $offset=null) {
		$lim = ($limit === null) ? self::PER_SEGMENT : $limit;
		$off = ($offset === null) ? 0 : $offset;
		$jobs = $this->_db->prepare("SELECT * FROM {$this->_table_name} WHERE publish = :publish AND (expired_at > CURDATE() OR expired_at IS NULL) ORDER BY created_at DESC, updated_at DESC LIMIT :offset, :lim");
		$jobs->bindValue(':publish', self::PUBLISH_Y);
		$jobs->bindValue(':lim', $lim);
		$jobs->bindValue(':offset', $off);
		$jobs->execute();
		return $jobs->fetchAll();
	}

	public function get_all_by_category($limit=null, $offset=null, $category_id='') {
		$lim = ($limit === null) ? self::PER_SEGMENT : $limit;
		$off = ($offset === null) ? 0 : $offset;
		$sql = "SELECT * FROM {$this->_table_name} INNER JOIN job_categories ON jobs.job_id = job_categories.jobs_id WHERE job_categories.categories_id = :catid AND (job_categories.categories_id IS NOT NULL) AND publish = :publish AND (expired_at > CURDATE() OR expired_at IS NULL) ORDER BY created_at DESC, updated_at DESC LIMIT :offset, :lim";
		$jobs = $this->_db->prepare($sql);
		$jobs->bindValue(':publish', self::PUBLISH_Y);
		$jobs->bindValue(':lim', $lim);
		$jobs->bindValue(':offset', $off);
		$jobs->bindValue(':catid', $category_id);
		$jobs->execute();
		return $jobs->fetchAll();
	}

	public function load_more($seq=1, $catid=''){
		$off = ($seq == 1) ? 0 : ($seq - 1) * self::PER_SEGMENT;
		if ($catid == '') {
			$sql = "SELECT * FROM {$this->_table_name} WHERE publish = :publish AND (expired_at > CURDATE() OR expired_at IS NULL) ORDER BY created_at DESC, updated_at DESC LIMIT :offset, :lim";
			$jobs = $this->_db->prepare($sql);
		} else {
			$sql = "SELECT * FROM {$this->_table_name} INNER JOIN job_categories ON jobs.job_id = job_categories.jobs_id WHERE job_categories.categories_id = :catid AND (job_categories.categories_id IS NOT NULL) AND publish = :publish AND (expired_at > CURDATE() OR expired_at IS NULL) ORDER BY created_at DESC, updated_at DESC LIMIT :offset, :lim";
			$jobs = $this->_db->prepare($sql);
			$jobs->bindValue(':catid', $catid);
		}
		$jobs->bindValue(':publish', self::PUBLISH_Y);
		$jobs->bindValue(':lim', self::PER_SEGMENT);
		$jobs->bindValue(':offset', $off);
		$jobs->execute();
		return $jobs->fetchAll();
	}

	public function get_by_member($member_id, $limit=null, $offset=null) {
		$lim = ($limit === null) ? self::PER_SEGMENT : $limit;
		$off = ($offset === null) ? 0 : $offset;
		$jobs = $this->_db->prepare("SELECT * FROM {$this->_table_name} WHERE member_id = :id ORDER BY created_at DESC, updated_at DESC LIMIT :offset, :lim");
		$jobs->bindValue(':lim', $lim);
		$jobs->bindValue(':offset', $off);
		$jobs->bindValue(':id', $member_id);
		$jobs->execute();
		return $jobs->fetchAll();
	}

	public function total(){
		$jobs = $this->_db->prepare("SELECT COUNT(job_id) AS total FROM {$this->_table_name} WHERE publish=:publish AND (expired_at > CURDATE() OR expired_at IS NULL)");
		$jobs->bindValue(':publish', self::PUBLISH_Y);
		$jobs->execute();
		$result = $jobs->fetch();
		return $result['total'];
	}

	public function total_by_category($category_id='') {
		$sql = "SELECT COUNT(jobs.job_id) AS total FROM {$this->_table_name} INNER JOIN job_categories ON jobs.job_id = job_categories.jobs_id WHERE job_categories.categories_id = :catid AND (job_categories.categories_id IS NOT NULL) AND publish = :publish AND (expired_at > CURDATE() OR expired_at IS NULL)";
		$jobs = $this->_db->prepare($sql);
		$jobs->bindValue(':catid', $category_id);
		$jobs->bindValue(':publish', self::PUBLISH_Y);
		$jobs->execute();
		$result = $jobs->fetch();
		return $result['total'];
	}

	public function total_by_member($member_id){
		$jobs = $this->_db->prepare("SELECT COUNT(job_id) AS total FROM {$this->_table_name} WHERE member_id = :id");
		$jobs->bindValue(':id', $member_id);
		$jobs->execute();
		$result = $jobs->fetch();
		return $result['total'];
	}

	public function get_by_reference_id($refid=null){
		$job = $this->_db->prepare("SELECT * FROM {$this->_table_name} WHERE reference_id = :rid LIMIT 1");
		$job->bindValue(':rid', $refid);
		$job->execute();
		return $job->fetch();
	}

	public function dtt_my_jobs($params){
		$aColumns = array('reference_id', 'title', 'company_name', 'employment_type_label', 
			'remoteable_label', 'years_of_exp_label', 'portfolio_items_label', 'publish');
		$data = array(
			'draw' => $params['draw'],
			'recordsTotal' => $this->total_by_member($params['member_id']),
			'recordsFiltered' => $this->total_by_member($params['member_id'])
		);
		$orderby = $aColumns[$params['order'][0]['column']];
		$orderdir = strtoupper($params['order'][0]['dir']);
		
		$searchsql = "";
		if ($params['search']['value'] != '') {
			$searchsql = " WHERE (reference_id LIKE :search1";
			$searchsql .= " OR title LIKE :search2";
			$searchsql .= " OR company_name LIKE :search3";
			$searchsql .= " OR employment_type_label LIKE :search4";
			$searchsql .= " OR remoteable_label LIKE :search5";
			$searchsql .= " OR years_of_exp_label LIKE :search6";
			$searchsql .= " OR portfolio_items_label LIKE :search7";
			$searchsql .= " OR publish LIKE :search8";
			$searchsql .= ")";
		}

		$rows = array();
		$subsql = "(SELECT job_id, reference_id, title, company_name, 
			{$this->get_empltype_sql('employment_type_label')}, 
			{$this->get_remoteable_sql('remoteable_label')}, 
			{$this->get_years_of_exp_sql('years_of_exp_label')}, 
			{$this->get_portfolio_items_sql('portfolio_items_label')}, 
			publish
			FROM {$this->_table_name} 
			WHERE member_id = :id) AS tmp";
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM " . $subsql . $searchsql;
		$sql .= " ORDER BY {$orderby} {$orderdir} LIMIT :offset, :lim";
		
		$jobs = $this->_db->prepare($sql);
		$jobs->bindValue(':lim', $params['length']);
		$jobs->bindValue(':offset', $params['start']);
		$jobs->bindValue(':id', $params['member_id']);
		if ($params['search']['value'] != "") {
			$jobs->bindValue(':search1', "%".$params['search']['value']."%");
			$jobs->bindValue(':search2', "%".$params['search']['value']."%");
			$jobs->bindValue(':search3', "%".$params['search']['value']."%");
			$jobs->bindValue(':search4', "%".$params['search']['value']."%");
			$jobs->bindValue(':search5', "%".$params['search']['value']."%");
			$jobs->bindValue(':search6', "%".$params['search']['value']."%");
			$jobs->bindValue(':search7', "%".$params['search']['value']."%");
			$jobs->bindValue(':search8', "%".$params['search']['value']."%");
		}
		$jobs->execute();
		$alljobs = $jobs->fetchAll();

		if(!empty($alljobs)) {
			foreach($alljobs as $row) {
				$count_apply = $this->_jobapply->applied($row['job_id']);
				$action1 = $count_apply . ' applied';
				if ($count_apply > 0) {
					$action1 = \Html::anchor('/admin/my-job-listing/applied/'.$row['reference_id'], $count_apply . ' applied');
				}
				$values = array(
					'ref_id' => $row['reference_id'],
					'title' => $row['title'],
					'company_name' => $row['company_name'],
					'employment_type_label' => $row['employment_type_label'],
					'remoteable_label' => $row['remoteable_label'],
					'years_of_exp_label' => $row['years_of_exp_label'],
					'portfolio_items_label' => $row['portfolio_items_label'],
					'publish' => $this->get_publish_label($row['publish']),
					'action1' => $action1,
					'action2' => \Html::anchor('/admin/my-job-listing/edit/'.$row['reference_id'], 'Edit'),
					'action3' => \Html::anchor('#', 'Delete', array('data-ref' => $row['reference_id'], 'class' => 'delete-job'))
				);
				$rows[] = array_values($values);
			}
		}
		$data['data'] = $rows;
		return $data;
	}

	public function update($params){
		$sql = "UPDATE {$this->_table_name} SET title=:title, description=:description, slug=:slug,
		email=:email, cell=:cell, country=:country, prov_state=:prov_state, 
		city=:city, postal_code=:postal_code, salary=:salary, 
		employment_type=:employment_type, company_name=:company_name, 
		company_website=:company_website, remoteable=:remoteable, 
		years_of_exp=:years_of_exp, portfolio_items=:portfolio_items, 
		expired_at=:expired_at, publish=:publish, updated_at=:updated_at WHERE reference_id = :ref_id";
		$update = $this->_db->prepare($sql);
		$update->bindValue(':slug', $this->_slugify->slugify($params['title'].' '.$params['reference_id']));
		$update->bindValue(':updated_at', date_create()->format('Y-m-d H:i:s'));
		$update->bindValue(':title', $params['title']);
		$update->bindValue(':description', $params['description']);
		$update->bindValue(':email', $params['email']);
		$update->bindValue(':cell', $params['cell']);
		$update->bindValue(':country', $params['country']);
		$update->bindValue(':prov_state', $params['prov_state']);
		$update->bindValue(':city', $params['city']);
		$update->bindValue(':postal_code', $params['postal_code']);
		$update->bindValue(':salary', $params['salary']);
		$update->bindValue(':employment_type', $params['employment_type']);
		$update->bindValue(':company_name', $params['company_name']);
		$update->bindValue(':company_website', $params['company_website']);
		$update->bindValue(':remoteable', $params['remoteable']);
		$update->bindValue(':years_of_exp', $params['years_of_exp']);
		$update->bindValue(':portfolio_items', $params['portfolio_items']);
		$update->bindValue(':expired_at', ($params['expired_at'] == '') ? null: $params['expired_at']);
		$update->bindValue(':publish', $params['publish']);
		$update->bindValue(':ref_id', $params['reference_id']);
		return $update->execute();
	}

	public function delete($params) {
		$return = array('status' => false, 'message' => 'Unknown error');
		$getjob = $this->_db->prepare("SELECT * FROM jobs WHERE reference_id = :refid LIMIT 1");
		$getjob->bindValue(':refid', $params['reference_id']);
		$getjob->execute();
		$job = $getjob->fetch();

		$del = $this->_db->prepare("DELETE FROM {$this->_table_name} WHERE reference_id = :refid");
		$del->bindValue(':refid', $params['reference_id']);
		$executed = $del->execute();
		if($executed) {
			$return['status'] = true;
			$return['message'] = 'Record deleted';
			///delete job_application
			$this->_jobapply->delete_job($job['job_id']);
		}

		return $return;
	}


}

$jobs = new Jobs();