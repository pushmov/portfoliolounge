<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/model.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/jobapply.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/html.php';

class Members extends Model {

	const ACCOUNT_BASIC = 1;
	const ACCOUNT_PRO = 2;
	const ACCOUNT_MAX = 3;

	const UT_USER = 0;
	const UT_ADMIN = 1;

	protected $_table_name = 'members';
	protected $_account_type = array(self::ACCOUNT_BASIC => 'Basic', self::ACCOUNT_PRO => 'Pro', self::ACCOUNT_MAX => 'Max');
	protected $_jobapply;
	protected $_user_type = array(self::UT_USER => 'User', self::UT_ADMIN => 'Admin');

	public function __construct () {
		global $job_apply;
		parent::__construct();
		$this->_jobapply =& $job_apply;
	}

	public function load($member_id) {
		$getmember = $this->_db->prepare("SELECT * FROM members WHERE member_id = :id LIMIT 1");
		$getmember->bindValue(':id', $member_id);
		$getmember->execute();
		return $getmember->fetch();
	}

	public function get_user_type($ut=null) {
		if ($ut === null) {
			return $this->_user_type;
		}
		return array_key_exists($ut, $this->_user_type) ? $this->_user_type[$ut] : $this->_user_type;
	}

	public function get_account_type($type=null){
		if($type === null) {
			return $this->_account_type;
		}
		return array_key_exists($type, $this->_account_type) ? $this->_account_type[$type] : $this->_account_type;
	}

	public function get_account_type_sql($alias='tmp') {
		$sql = "(CASE ";
		foreach ($this->get_account_type() as $val => $txt) {
			$sql .= "WHEN account_id = '{$val}' THEN '{$txt}' ";
		}
		$sql .= " END) AS ".$alias;
		return $sql;
	}

	
}

$members = new Members();