<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/function/config.php';

class Model {

	protected $_db;

	public function __construct(){
		global $db;
		$this->_db =& $db;
	}

	public function get_table_name () {
		return $this->_table_name;
	}

	public function insert($data){
		$new_data = array_merge($this->get_fields(), $data);
		$columns = array_keys($this->get_fields());
		$sql_cols = "(" . implode(', ', $columns) . ")";
		$sql_vals = "( :" . implode(', :', $columns) . " )";
		$sql = "INSERT INTO {$this->_table_name} {$sql_cols} VALUES {$sql_vals}";
		$insert = $this->_db->prepare($sql);
		foreach ($columns as $key) {
			$insert->bindValue(':'.$key, $new_data[$key]);
		}
		$insert->execute();
		return $this->_db->lastInsertId();
	}

	public function db_instance(){
		return $this->_db;
	}

	/**
	* $subsql = '(SELECT `id`, `date`, DATE_FORMAT(`date`, \'%M %d %Y %h:%i %p\') as date_name, `user_id`,`user_name`,`ip`,`message` FROM login_log) as tmp';
	* $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM ".$subsql;
	* 
	* SELECT SQL_CALC_FOUND_ROWS * FROM (SELECT job_id, reference_id, title, company_name, employment_type, 
	*	CASE
	*		WHEN employment_type = 0 THEN 'Full Time' 
	*		WHEN employment_type = 1 THEN 'Part Time' 
	*		ELSE 'Freelance'  
	*	END AS employment_type_label, remoteable, years_of_exp, portfolio_items, publish FROM jobs WHERE member_id = 27313 LIMIT 0, 5) AS tmp
	*/

}