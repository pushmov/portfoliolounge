<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/model.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/html.php';

class Transactions extends Model {

	const METHOD_STRIPE = 1;
	const METHOD_PAYPAL = 2;

	protected $_methods = array(self::METHOD_STRIPE => 'Stripe', self::METHOD_PAYPAL => 'PayPal');
	protected $_method;
	protected $_table_name = 'transactions';
	protected $_fields = array( 'trans_id' => null, 'member_id' => '', 'tx_return' => '', 'tx_ref_id' => '',
		'method' => 0, 'amount' => 0.00, 'date_created' => '', 'description' => '', 'currency' => ''
	);

	public function __construct(){
		parent::__construct();
	}

	public function get_method_label($method=null) {
		if ($method === null) {
			return $this->_methods;
		}
		return array_key_exists($method, $this->_methods) ? $this->_methods[$method] : $this->_methods;
	}

	public function get_method_sql($alias = 'tmp'){
		$sql = "(CASE ";
		foreach($this->_methods as $val => $txt) {
			$sql .= " WHEN method = '{$val}' THEN '{$txt}' ";
		}
		$sql .= " END) AS ".$alias;
		return $sql;
	}

	public function set_method($method){
		$this->_method = $method;
	}

	public function create($data) {
		$create = $this->_db->prepare("INSERT INTO {$this->_table_name} (member_id, tx_return, method, amount, description, currency, tx_ref_id) 
			VALUES (:mid, :tx, :method, :amount, :description, :currency, :tx_ref_id)");
		$create->bindValue(':mid', $data['mid']);
		$create->bindValue(':tx', $data['tx']);
		$create->bindValue(':method', $data['method']);
		$create->bindValue(':amount', $data['amount']);
		$create->bindValue(':description', $data['description']);
		$create->bindValue(':currency', $data['currency']);
		$create->bindValue(':tx_ref_id', $data['tx_ref_id']);
		$create->execute();
		return $this->_db->lastInsertId();
	}

	public function total_records(){
		$allrecords = $this->_db->prepare("SELECT COUNT(trans_id) AS total FROM {$this->_table_name}");
		$allrecords->execute();
		$records = $allrecords->fetch();
		return $records['total'];
	}

	public function dtt_all($params){
		$data = array(
			'draw' => $params['draw'],
			'recordsTotal' => $this->total_records(),
			'recordsFiltered' => $this->total_records()
		);
		$aColumns = array('reference_id', 'title', 'company_name', 'email', 'username', 'token', 'method', 'amount', 'currency', 'payment_date');
		$searchsql = "";
		$orderby = $aColumns[$params['order'][0]['column']];
		$orderdir = strtoupper($params['order'][0]['dir']);
		if ($params['search']['value'] != '') {
			$searchsql = " WHERE (reference_id LIKE :search1";
			$searchsql .= " OR title LIKE :search2";
			$searchsql .= " OR company_name LIKE :search3";
			$searchsql .= " OR email LIKE :search4";
			$searchsql .= " OR token LIKE :search5";
			$searchsql .= " OR method LIKE :search6";
			$searchsql .= " OR amount LIKE :search7";
			$searchsql .= " OR currency LIKE :search8";
			$searchsql .= " OR payment_date LIKE :search9";
			$searchsql .= " OR username LIKE :search10";
			$searchsql .= ")";
		}
		$subsql = "SELECT 
			jobs.reference_id, jobs.title, 
			jobs.company_name, jobs.company_website, 
			members.email, members.username, 
			transactions.tx_return AS token, 
			{$this->get_method_sql('method')}, 
			transactions.amount, transactions.currency, 
			DATE_FORMAT(transactions.date_created, '%b %e, %Y') AS payment_date 
			FROM transactions 
			INNER JOIN members ON members.member_id = transactions.member_id 
			INNER JOIN jobs ON jobs.reference_id = transactions.tx_ref_id";
		$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM (" . $subsql . ") AS tmp ".$searchsql;
		$sql .= " ORDER BY {$orderby} {$orderdir} LIMIT :offset, :lim";

		$trans = $this->_db->prepare($sql);
		$trans->bindValue(':offset', $params['start']);
		$trans->bindValue(':lim', $params['length']);
		if ($params['search']['value'] != "") {
			$trans->bindValue(':search1', "%".$params['search']['value']."%");
			$trans->bindValue(':search2', "%".$params['search']['value']."%");
			$trans->bindValue(':search3', "%".$params['search']['value']."%");
			$trans->bindValue(':search4', "%".$params['search']['value']."%");
			$trans->bindValue(':search5', "%".$params['search']['value']."%");
			$trans->bindValue(':search6', "%".$params['search']['value']."%");
			$trans->bindValue(':search7', "%".$params['search']['value']."%");
			$trans->bindValue(':search8', "%".$params['search']['value']."%");
			$trans->bindValue(':search9', "%".$params['search']['value']."%");
			$trans->bindValue(':search10', "%".$params['search']['value']."%");
		}
		$trans->execute();
		$transactions = $trans->fetchAll();
		$rows = array();
		if (!empty($transactions)) {
			foreach($transactions as $row) {
				$company = $row['company_name'];
				if ($row['company_website'] != '') {
					$company = \Html::anchor($row['company_website'], $row['company_name'], array('target' => '_blank'));
				}
				$values = array(
					'ref_id' => $row['reference_id'],
					'title' => $row['title'],
					'company_name' => $company,
					'email' => $row['email'],
					'username' => $row['username'],
					'token' => $row['token'],
					'method' => $row['method'],
					'amount' => $row['amount'],
					'currency' => strtoupper($row['currency']),
					'payment_date' => $row['payment_date']
				);
				$rows[] = array_values($values);
			}
		}
		$data['data'] = $rows;
		return $data;
	}

}

$transactions = new Transactions();