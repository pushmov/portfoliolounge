<link rel="stylesheet" type="text/css" href="css/owner.css" />
<?php if($i == 'job-transaction') : ?>
<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
<?php else :?>	
<script type="text/javascript" src="js/libs/jquery.dataTables.min.js"></script>
<?php endif; ?>

	<ul id="tabs"> 
		<li><a class="<?php if($i=='members'){echo 'cur';}?>" href="owner/members">Members</a></li>
		<li><a class="<?php if($i=='stats'){echo 'cur';}?>" href="owner/stats">Stats</a></li>
		<li><a class="<?php if($i=='top-pick'){echo 'cur';}?>" href="owner/top-pick">Editor's Pick</a></li>
		<li><a class="<?php if($i=='billing'){echo 'cur';}?>" href="owner/billing">Billing</a></li>
		<li><a class="<?php if($i=='paid'){echo 'cur';}?>" href="owner/paid">Paid</a></li>
		<li><a class="<?php if($i=='logo_img'){echo 'cur';}?>" href="owner/logo_img">Logos</a></li>
		<li><a class="<?php if($i=='profile_img'){echo 'cur';}?>" href="owner/profile_img">Profiles</a></li>
		<li><a class="<?php if($i=='items'){echo 'cur';}?>" href="owner/items">Items</a></li>
		<li><a class="<?php if($i=='commands'){echo 'cur';}?>" href="owner/commands">Commands</a></li>
		<li><a class="<?php if($i=='mail-list'){echo 'cur';}?>" href="owner/email-list">Email List</a></li>
		<li><a class="<?php if($i=='job-transaction'){echo 'cur';}?>" href="owner/job-transaction">Job Post Trans.</a></li>
	</ul>