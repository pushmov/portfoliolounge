<?php if($member['admin']==1){

require_once 'function/config.php';

$result = $db->prepare("SELECT COUNT(member_id) AS counts FROM members");
$result->execute();
$members = $result->fetch();

$result = $db->prepare("SELECT COUNT(project_id) AS counts FROM projects");
$result->execute();
$projects = $result->fetch();

$result = $db->prepare("SELECT COUNT(item_id) AS counts FROM items");
$result->execute();
$items = $result->fetch();

$result = $db->prepare("SELECT COUNT(member_id) AS counts FROM members WHERE account_profile_status = :status");
$result->bindValue(':status', 'ActiveProfile');
$result->execute();
$allrows = $result->fetch();
?>

<em>Members: <?=$members['counts'];?></em><br />
<em>Projects:  <?=$projects['counts']?></em><br />
<em>Items:  <?=$items['counts']?></em><br />
<em>Upgrades: <?=$allrows['counts']?></em>

<?php  /* Logos & Profile Picture
--------------------------------------------------*/
if($i=="items"){?>
				
	
	<div ng-controller="ownerCtrl">
		
		<?php			
		// Pagination Values
		$page = $x ? $x:0;
		$page_next = $page+1;
		$page_prev = $page ? $page-1 : 0; 
		$show_per_page = 100;
		$limit_start = $page * $show_per_page;
		$item_query = "SELECT * FROM items ORDER BY item_id DESC LIMIT $limit_start, $show_per_page";
		
		if($x=='search'){
			$item_query = "SELECT * FROM items WHERE $e = '$l' ORDER BY item_id DESC LIMIT $limit_start, $show_per_page";
		}
		
		if($x=='random'){
			$item_query = "SELECT * FROM items ORDER BY RAND() LIMIT $limit_start, $show_per_page";
		}
		
		if($x=='username'){
			
			$search_member = $db->prepare("SELECT member_id FROM members WHERE username = :username LIMIT 1");
			$search_member->bindValue(':username', $e);
			$search_member->execute();
			$searched_member = $search_member->fetch();
			
			$item_query = "SELECT * FROM items WHERE member_id = '".$searched_member['member_id']."' ORDER BY item_id";
		}
		
		
		try {
			$get_items = $db->query($item_query);
		} catch(PDOException $exception){
			error_log($exception->getMessage());
			die("Internal server error.");
			exit();
		}
		
		foreach($get_items as $item) :
			
			$get_member = $db->prepare("SELECT username FROM members WHERE member_id = :memberid LIMIT 1");
			$get_member->bindValue(':memberid', $item['member_id']);
			$get_member->execute();
			$member_username = $get_member->fetch();
			$member_username = $member_username['username'];?>
			
			<div class="home_thumb frame">
				<a style="position:absolute" class="check <?php if($item['featured']) {echo 'checked';}?>" ng-click="markItem(<?=$item['item_id'];?>,$event)"></a>
				<a target="_blank" class="" style="position:absolute;right:0px;z-index:9;" href="owner/items/username/<?=$member_username?>"><?=$member_username?></a>
				<a target="_blank" href="http://portfoliolounge.com/<?php echo $item['member_id'];?>">
					<div class="canvas" data-source="https://portfoliolounge.com/uploads/<?php echo $item['member_id'].'/medium/'.$item['item_filename'];?>"></div>                        
				</a>
			</div>
		<?php endforeach; ?>
		
		<div class="clr container">
			<a class="btn" href='<?php echo $p.'/'.$i.'/'.$page_prev;?>'>Prev Page</a>
			<a class="btn gray right" style="margin:0 auto;" href='owner/items/random'>Random</a>
			<a class="btn right" href='<?php echo $p.'/'.$i.'/'.$page_next;?>'>Next Page</a>
		</div>
	</div>
	<script>
		function ownerCtrl($scope, $http) {
			$scope.markItem = function(id, event){
				$http.post("/services/owner/featured-item/"+id).success(function(response) {		
					//$scope.account = angular.copy($scope.settingsCopy);
					$scope.respone = response;
					thisItem = $(event.target);
					if(thisItem.hasClass("checked")){
						thisItem.removeClass("checked")
					} else {
						thisItem.addClass("checked")
					}
					
				}).error(function(data, status) {
					$scope.data = data || "Request failed";
					$scope.status = status;			
				});
			}
		}
	</script>
	
	
<?php } ?>








<div class="container responsive owner-wrapper">

	<div id="tab-views">
	
	
	
	
		<?php  /* All
		--------------------------------------------------*/
		if($i=='members'){
			
			// How many members shown?
			if($x == 'all') {
				$limit = '';
			} else {
				$limit = 'LIMIT 1000';
			}
			$get_members = $db->query("SELECT 
				member_id, last_login, template_id, editors_pick, account_id, username, 
				disabled, email, item_count, project_count, time,	login_count, campaign_id
			FROM members ORDER BY member_id DESC $limit");

			$allmembers = $get_members->fetchAll();
		
			?>
			
			<?php if($x!='all'){?>
			<a class="right" href="<?php echo $p.'/'.$i.'/all';?>">See All</a>
			<?php } ?>
			
			
			<table class="data">
			<thead>
				<tr>
					<th>#</th>	
					<th>Signup</th>	
					<th>Last Login</th>
					<th>A</th>	
					<th>Username</th>
					<th>Email</th>
					<th>P</th>
					<th>I</th>	
					<th>T</th>
					<th>Top</th>
					<th>Ad</th>	
					<th>D</th>	
				</tr>
			</thead>
			<tbody>
			<?php foreach($allmembers as $member){?>
				<tr>
					<td><?php echo $member['member_id'];?></td>
					<td><?php echo date('m/d/Y', $member['time']);?></td>
					<td><?php echo date('m/d/Y', $member['last_login']);?></td>
					<td><?php echo $member['account_id'];?></td>
					<td><a target="_blank" href="http://<?php echo $member['username'];?>.portfoliolounge.com"><?php echo $member['username'];?></a></td>
					<td><?php echo $member['email'];?></td>
					<td><?php echo $member['project_count'];?></td>
					<td><?php echo $member['item_count'];?></td>
					<td><?php echo $member['template_id'];?></td>
					<td><?php echo $member['editors_pick'];?></td>
					<td><?php echo $member['campaign_id'];?></td>
					<td><?php if($member['disabled']) echo 'x';?></td>
				</tr>
			<?php }?>
			</tbody>
			</table>
		
		<?php } ?>
		
		
		
		
		
		
		
		
	
		<?php  /* Paid Members
		--------------------------------------------------*/
		if($i=='stats'){

			$get_template_zero = $db->prepare("SELECT * FROM members 
				WHERE account_profile_status = :status AND template_id = :id");
			$get_template_zero->bindValue(':status', 'ActiveProfile');
			$get_template_zero->bindValue(':id', 0);
			$get_template_zero->execute();
			$template_zero_all = $get_template_zero->fetchAll();
			$template_zero = count($template_zero_all);
            echo $template_zero."<br>";
			
			
			$get_template_one = $db->prepare("SELECT * FROM members 
				WHERE account_profile_status = :status AND template_id = :id");
			$get_template_one->bindValue(':status', 'ActiveProfile');
			$get_template_one->bindValue(':id', 1);
			$get_template_one->execute();
			$template_one_all = $get_template_one->fetchAll();
			$template_one = count($template_one_all);
            echo $template_one."<br>";
			
			
			$get_template_two = $db->prepare("SELECT * FROM members 
				WHERE account_profile_status = :status AND template_id = :id");
			$get_template_two->bindValue(':status', 'ActiveProfile');
			$get_template_two->bindValue(':id', 2);
			$get_template_two->execute();
			$template_two_all = $get_template_two->fetchAll();
			$template_two = count($template_two_all);
            echo $template_two."<br>";
			
			
			$get_template_three = $db->prepare("SELECT * FROM members 
				WHERE account_profile_status = :status AND template_id = :id");
			$get_template_three->bindValue(':status', 'ActiveProfile');
			$get_template_three->bindValue(':id', 3);
			$get_template_three->execute();
			$template_three_all = $get_template_three->fetchAll();
			$template_three = count($template_three_all);
            echo $template_three."<br>";
			
			
			$get_template_four = $db->prepare("SELECT * FROM members 
				WHERE account_profile_status = :status AND template_id = :id");
			$get_template_four->bindValue(':status', 'ActiveProfile');
			$get_template_four->bindValue(':id', 4);
			$get_template_four->execute();
			$template_four_all = $get_template_four->fetchAll();
			$template_four = count($template_four_all);
            echo $template_four."<br>";
			
			
			$get_template_five = $db->prepare("SELECT * FROM members 
				WHERE account_profile_status = :status AND template_id = :id");
			$get_template_five->bindValue(':status', 'ActiveProfile');
			$get_template_five->bindValue(':id', 5);
			$get_template_five->execute();
			$template_five_all = $get_template_five->fetchAll();
			$template_five = count($template_five_all);
            echo $template_five."<br>";
			
			
			$get_template_six = $db->prepare("SELECT * FROM members 
				WHERE account_profile_status = :status AND template_id = :id");
			$get_template_six->bindValue(':status', 'ActiveProfile');
			$get_template_six->bindValue(':id', 6);
			$get_template_six->execute();
			$template_six_all = $get_template_six->fetchAll();
			$template_six = count($template_six_all);
            echo $template_six."<br>";
			
			
			$get_template_seven = $db->prepare("SELECT * FROM members 
				WHERE account_profile_status = :status AND template_id = :id");
			$get_template_seven->bindValue(':status', 'ActiveProfile');
			$get_template_seven->bindValue(':id', 7);
			$get_template_seven->execute();
			$template_seven_all = $get_template_seven->fetchAll();
			$template_seven = count($template_seven_all);
            echo $template_seven."<br>";
			
			
			$get_template_eight = $db->prepare("SELECT * FROM members 
				WHERE account_profile_status = :status AND template_id = :id");
			$get_template_eight->bindValue(':status', 'ActiveProfile');
			$get_template_eight->bindValue(':id', 8);
			$get_template_eight->execute();
			$template_eight_all = $get_template_eight->fetchAll();
			$template_eight = count($template_eight_all);
            echo $template_eight."<br>";

		
			
			$get_template_zero = $db->prepare("SELECT * FROM members 
				WHERE project_count > :p AND template_id = :id");
			$get_template_zero->bindValue(':p', 3);
			$get_template_zero->bindValue(':id', 0);
			$get_template_zero->execute();
			$template_zero_all = $get_template_zero->fetchAll();
			$template_zero = count($template_zero_all);
            echo $template_zero."<br>";
			
			
			$get_template_one = $db->prepare("SELECT * FROM members 
				WHERE project_count > :p AND template_id = :id");
			$get_template_one->bindValue(':p', 3);
			$get_template_one->bindValue(':id', 1);
			$get_template_one->execute();
			$template_one_all = $get_template_one->fetchAll();
			$template_one = count($template_one_all);
            echo $template_one."<br>";
			
			$get_template_two = $db->prepare("SELECT * FROM members 
				WHERE project_count > :p AND template_id = :id");
			$get_template_two->bindValue(':p', 3);
			$get_template_two->bindValue(':id', 2);
			$get_template_two->execute();
			$template_two_all = $get_template_two->fetchAll();
			$template_two = count($template_two_all);
            echo $template_two."<br>";
			
			$get_template_three = $db->prepare("SELECT * FROM members 
				WHERE project_count > :p AND template_id = :id");
			$get_template_three->bindValue(':p', 3);
			$get_template_three->bindValue(':id', 3);
			$get_template_three->execute();
			$template_three_all = $get_template_three->fetchAll();
			$template_three = count($template_three_all);
            echo $template_three."<br>";
			
			$get_template_four = $db->prepare("SELECT * FROM members 
				WHERE project_count > :p AND template_id = :id");
			$get_template_four->bindValue(':p', 3);
			$get_template_four->bindValue(':id', 4);
			$get_template_four->execute();
			$template_four_all = $get_template_four->fetchAll();
			$template_four = count($template_four_all);
            echo $template_four."<br>";
			
			$get_template_five = $db->prepare("SELECT * FROM members 
				WHERE project_count > :p AND template_id = :id");
			$get_template_five->bindValue(':p', 3);
			$get_template_five->bindValue(':id', 5);
			$get_template_five->execute();
			$template_five_all = $get_template_five->fetchAll();
			$template_five = count($template_five_all);
            echo $template_five."<br>";
			
			$get_template_six = $db->prepare("SELECT * FROM members 
				WHERE project_count > :p AND template_id = :id");
			$get_template_six->bindValue(':p', 3);
			$get_template_six->bindValue(':id', 6);
			$get_template_six->execute();
            $template_six_all = $get_template_six->fetchAll();
            $template_six = count($template_six_all);
            echo $template_six."<br>";
			
			$get_template_seven = $db->prepare("SELECT * FROM members 
				WHERE project_count > :p AND template_id = :id");
			$get_template_seven->bindValue(':p', 3);
			$get_template_seven->bindValue(':id', 7);
			$get_template_seven->execute();
			$template_seven_all = $get_template_seven->fetchAll();
			$template_seven = count($template_seven_all);
            echo $template_seven."<br>";
			
			$get_template_eight = $db->prepare("SELECT * FROM members 
				WHERE project_count > :p AND template_id = :id");
			$get_template_eight->bindValue(':p', 3);
			$get_template_eight->bindValue(':id', 8);
			$get_template_eight->execute();
			$template_eight_all = $get_template_eight->fetchAll();
			$template_eight = count($template_eight_all);
            echo $template_eight."<br>";
			
		}
		?>
		
		
		
		
	
		<?php  /* Paid Members
		--------------------------------------------------*/
		if($i=='paid'){
			$get_members = $db->prepare("SELECT * FROM members 
				WHERE account_profile_status = :status ORDER BY account_start_date DESC");
			$get_members->bindValue(':status', 'ActiveProfile');
			$get_members->execute();
			$all_members = $get_members->fetchAll();
			?>

			
			<table class="data">
				<thead>
					<tr>
						<th>Id</th>	
						<th>A</th>	
						<th>Location</th>	
						<th>Signup</th>
						<th>Upgrade</th>
						<th>Last Login</th>			
						<th>Username</th>	
						<th>Email</th>	
						<th>PP</th>
						<th>P</th>
						<th>I</th>
						<th>T</th>
						<th>Ad</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($all_members as $member){?>
			
					<?php if($member['account_id']==2) $monthly_revenue = $monthly_revenue+7.46;?>
					<?php if($member['account_id']==3) $monthly_revenue = $monthly_revenue+16.20;?>
					
					<tr>
						<td><?php echo $member['member_id'];?></td>
						<td><?php echo $member['account_id'];?></td>
						<td><?php echo $member['location'];?></td>
						<td><?php echo date('m/d/Y', $member['time']);?></td>
						<td><?php echo date('m/d/Y', $member['account_start_date']);?></td>
						<td><?php echo date('m/d/Y', $member['last_login']);?></td>
						<td><a target="_blank" href="http://<?php echo $member['username'];?>.portfoliolounge.com"><?php echo $member['username'];?></a></td>
						<td><?php echo $member['email'];?></td>
						<td><?php echo $member['upgrade_method'];?></td>
						<td><?php echo $member['project_count'];?></td>
						<td><?php echo $member['item_count'];?></td>
						<td><?php echo $member['template_id']+1;?></td>
						<td><?php echo $member['campaign_id'];?></td>
					</tr>
				<?php }?>
				</tbody>
			</table>
				
			<h2 style="text-align:right;"><?php echo $monthly_revenue;?></h2>
			
		<?php }?>
		
		
		
		
		
		
		
		
	
		<?php  /* Logos & Profile Picture
		--------------------------------------------------*/
		if($i=="logo_img" || $i== 'profile_img'){
						
						
						
			// Pagination Values
			$page = $x ? $x:0;
			$page_next = $page+1;
			$page_prev = $page ? $page-1 : 0; 
			$show_per_page = 70;
			$limit_start = $page * $show_per_page;

			if($i == 'logo_img') {
				$get_members = $db->prepare("SELECT member_id, logo_img username FROM members 
					WHERE logo_img != :img ORDER BY member_id DESC LIMIT :limit_start, :show_per_page");
				$get_members->bindValue(':img', '');
				$get_members->bindValue(':limit_start', $limit_start);
				$get_members->bindValue(':show_per_page', $show_per_page);
				$get_members->execute();
				$all_members = $get_members->fetchAll();
			} elseif($i == 'profile_img') {
				$get_members = $db->prepare("SELECT member_id, profile_img, username FROM members 
					WHERE profile_img != :img ORDER BY member_id DESC LIMIT :limit_start, :show_per_page");
				$get_members->bindValue(':img', '');
				$get_members->bindValue(':limit_start', $limit_start);
				$get_members->bindValue(':show_per_page', $show_per_page);
				$get_members->execute();
				$all_members = $get_members->fetchAll();
			}
			
			foreach($all_members as $member) {?>
				<a class="img_canvas" target="_blank" href="http://<?php echo $member['username'];?>.portfoliolounge.com">
					<?php if($i=='profile_img'){?>
					<img src="http://portfoliolounge.com/uploads/<?php echo $member['member_id'].'/thumbnail/'.$member[$i];?>" />
					<?php } else { ?>
					<img src="http://portfoliolounge.com/uploads/<?php echo $member['member_id'].'/'.$member[$i];?>" />
					<?php }  ?>
				</a>
			<?php } ?>
			
			<div class="clr"></div>
			<a class="btn" href='<?php echo $p.'/'.$i.'/'.$page_prev;?>'>Prev Page</a>
			<a class="btn right" href='<?php echo $p.'/'.$i.'/'.$page_next;?>'>Next Page</a>
			
			
			
		<?php } ?>
		
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
	
		<?php  /* Commands
		--------------------------------------------------*/
		if($i=='commands'){?>
			
			
			<h2>Recount Items &amp; Projects</h2>
			<ul>
				<li>Reset item_count and project count to zero</li>
				<li>Loop through projects and tally up member project_count for each member</li>
				<li>Loop through items and tally up item_count for each member </li>
			</ul>
			<a class="btn red" href="owner/<?php echo $i;?>/recount">Recount Items &amp; Projects</a>
			
			<?php if($x=='recount'){
				$get_members = $db->query("SELECT member_id, account_id, username, 
					login_count, time FROM members ORDER BY member_id DESC");
				$member_arr=$get_members->fetchAll();
				
				$get_items = $db->query("SELECT member_id FROM items ORDER BY member_id");
				$item_arr=$get_items->fetchAll();

				$get_projects = $db->query("SELECT member_id FROM projects");
				$project_arr=$get_projects->fetchAll();
				?>
				
				<ul class="results">
					
					<?php // Reset Item Count
					foreach($member_arr as $member){ 
						$update = $db->prepare("UPDATE members SET item_count = :i, project_count = :p 
							WHERE member_id = :id");
						$update->bindValue(':i', 0);
						$update->bindValue(':p', 0);
						$update->bindValue(':id', $member['member_id']);
						$update->execute();
					}
					echo '<li>Item and Project count reset to zero.</li>';
					
					// Count Projects
					foreach($project_arr as $project){ 
						$db->query("UPDATE members SET project_count = project_count+1 
							WHERE member_id = ".$project['member_id']);
					} 
					echo '<li>Projects counted for each member.</li>';
					
					// Count Items
					foreach($item_arr as $item){ 
						$db->query("UPDATE members SET item_count = item_count+1 WHERE member_id = ".$item['member_id']);	
					} 
					echo '<li>Items counted for each member.</li>';?>
				
				</ul>
			<?php } ?>	
			
			
			
			
			
			
			<hr />
			
			<h2>Create Project Slugs</h2>
			<ul>
				<li>Loop through project and get the title</li>
				<li>Convert title to a slug title and store it.</li>
			</ul>
			<a class="btn red" href="owner/<?php echo $i;?>/slugs">Create Project Slugs</a>
			
			<br />
			<br />
			
			<?php if($x=='slugs'){
				$get_projects = $db->query("SELECT project_title, project_slug, project_id 
					FROM projects ORDER BY project_id DESC");
				$project_arr=$get_projects->fetchAll();
				
				function to_slug($string){
					return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
				}
				foreach($project_arr as $project){ 
					$project_slug = to_slug($project['project_title']);
					$project_slug = trim($project_slug,'-');
					$update = $db->prepare("UPDATE projects SET project_slug = :slug 
						WHERE project_id = :id");
					$update->bindValue(':slug', $project_slug);
					$update->bindValue(':id', $project['project_id']);
					$update->execute();
				}
				
				echo "Done!";
					
			} ?>	
			
			
				
			
		<?php } ?>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	
	
	
	
	
	
		<?php  /* Top Pick
		--------------------------------------------------*/
		if($i=='top-pick'){?>
		
		
			<h2>Add member to top pick</h2>
			<br />
			<form action="function/owner.php?a=add_top_pick" method="post">
				<label>Username</label><br />
				<input name="username" /><br /><br />
				<input class='btn' type="submit" />
			</form>
			
			
			<ul class="editors_pick">
			<?php 
			$get_members = $db->prepare("SELECT username, member_id, template_id 
				FROM members WHERE top_pick > :i");
			$get_members->bindValue(':i', 0);
			$get_members->execute();
			$all_members = $get_members->fetchAll();
			foreach($all_members as $member){?>
				<li>
					<a style="color:#C00;" href='function/owner.php?a=remove_top_pick&b=<?php echo $member['member_id'];?>'><strong>x</strong></a>
					<a target="_blank" href="http://<?php echo $member['username'];?>.portfoliolounge.com"><?php echo $member['username'];?> <?= $member['template_id']?></a>
				</li>
			<?php }?>	
				<div class="clr"></div>
			</ul>
		
		<hr>
			
			<h2>Add member to editor's pick.</h2>
			<br />
			<form action="function/owner.php?a=add_editors_pick" method="post">
				<label>Username</label><br />
				<input name="username" /><br /><br />
				<input class='btn' type="submit" />
			</form>
			
			
			<ul class="editors_pick">
			<?php 
			$get_members = $db->prepare("SELECT username, member_id, template_id 
				FROM members WHERE editors_pick > :e");
			$get_members->bindValue(':e', 0);
			$get_members->execute();
			$all_members = $get_members->fetchAll();
			foreach($all_members as $member){?>
				<li>
					<a style="color:#C00;" href='function/owner.php?a=remove_editors_pick&b=<?php echo $member['member_id'];?>'><strong>x</strong></a>
					<a target="_blank" href="http://<?php echo $member['username'];?>.portfoliolounge.com"><?php echo $member['username'];?> <?= $member['template_id']?></a>
				</li>
			<?php }?>	
				<div class="clr"></div>
			</ul>
		
		
		<?php } ?>
		
	
	
	
	
	
	
	
	
		<?php  /* Billing
		--------------------------------------------------*/
		if($i=='billing'){?>
			
			<h2>Payment Skipped</h2>
			<br />
			<form action="function/owner.php?a=suspend_acct" method="post">
				<label>Account Profile ID</label><br />
                <!-- js auto focus places cursor at end of value -->
				<input name="suspended_member_id" value="I-" autofocus onfocus="this.value = this.value;"/><br /><br />
				<input class='btn' type="submit" />
			</form>
			
			<hr />
			
			<ul class="editors_pick">
			<?php 
			$get_members = $db->prepare("SELECT username, member_id, email, account_profile_id 
				FROM members WHERE account_profile_status = :status ORDER BY member_id");
			$get_members->bindValue(':status', 'Suspended');
			$get_members->execute();
			$all_members = $get_members->fetchAll();
			foreach($all_members as $member){?>
				<li>
					<a style="font-size:10px;" target="_blank" href="http://<?php echo $member['username'];?>.portfoliolounge.com"><?=$member['member_id']?> <?php echo $member['account_profile_id'];?> <?php echo $member['email'];?></a>
				</li>
			<?php }?>	
				<div class="clr"></div>
			</ul>
		
		<?php } ?>
		
		
		
		
		
		
		
		
		
	
	
		<?php  /* Top Pick
		--------------------------------------------------*/
		if($i=='email-list'){
			
			$get_members = $db->prepare("SELECT email, username, first_name, last_name FROM members 
				WHERE item_count > :item");
			$get_members->bindValue(':item', 3);
			$get_members->execute(); 
			$members_all = $get_members->fetchAll();
			$member_count = count($members_all);

			?>
			
			<h2>Email List (<?php echo $member_count;?>)</h2>
			<p class="right"><?php echo $query;?></p>
			<hr>
			
			<?php foreach($members_all as $member){
				$formatted_first_name = ucfirst(strtolower($member['first_name'])); 
				echo $member['email'].', '.$member['username'].', '.$formatted_first_name.'<br>';
				//echo $member['email'].'<br>';
			
			}
		
			if($x=='send'){
			
					
				$mail = new PHPMailer();
				$mail->IsSMTP();           // set mailer to use SMTP
				$mail->Host = "mail.portfoliolounge.com";    // specify main and backup server
				$mail->SMTPAuth = true;           // turn on SMTP authentication
				$mail->Username = "mattvisk";   // SMTP username
				$mail->Password = "V1skM@tt!";      // SMTP password
				
				$emailFrom  = 'info@portfoliolounge.com';
				
				$mail->From = $emailFrom;
				$mail->IsHTML(true);    // set email format to HTML if needed
				
				$emailSubject = 'Test Loop Email';
				$emailBody  = "Whatever content you are sending.";
				
				$mail->Subject = $emailSubject;
				$mail->Body    = $emailBody;
					
				//foreach($emails => $email) {
				$get_recipients = $db->prepare("SELECT email, first_name FROM members 
					WHERE username = :u");
				$get_recipients->bindValue(':u', 'mattvisk');
				$get_recipients->execute();
				$all_recipients = $get_recipients->fetchAll();
				foreach($all_recipients as $recipient){
					
				
					$emailTo = $recipient['email'];
					$emailToName = $recipient['first_name'];
				
				// send an email to customer
					$mail->AddAddress($emailTo, $emailToName);
				
					if(!$mail->Send())
					{
						echo 'failed';
					}
				
					$mail->ClearAddresses();
				} 
			}
			
		}
		?>
		
		<?php
			if($i == 'job-transaction') {
				include $_SERVER['DOCUMENT_ROOT'] . '/owner.trans.php';
			}
		?>
		
		
	</div>
</div>


<script>
$(document).ready(function() {
	$('.data').dataTable({
		"aaSorting": [],
		"aLengthMenu" : [5, 10, 20, 30, 50, 100, 200, 500, 1000, 10000],
		"iDisplayLength": 30
	});
} );
</script>

<?php } ?>