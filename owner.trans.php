<link href="css/datatable.custom.css" rel="stylesheet">
<h3>Jobs Transaction</h3>
<table id="tb_trans" class="dataTable stripe display compact hover cell-border">
<thead>
	<tr>
		<th>Ref. ID</th>
		<th>Job Title</th>
		<th>Company</th>
		<th>Email</th>
		<th>Username</th>
		<th>Token</th>
		<th>Method</th>
		<th>Amount</th>
		<th>Currency</th>
		<th>Date</th>
	</tr>
</thead>
<tbody></tbody>
</table>
<script src="js/admin_jobs.js" type="text/javascript"></script>