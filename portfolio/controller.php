<script>
function portfolio($scope) {
	
	
	$scope.baseUrl = "http://portfoliolounge.com";
	$scope.p = "<?php echo $p;?>";
	$scope.i = "<?php echo $i;?>";
	$scope.x = "<?php echo $x;?>";
	$scope.currentYear = Date('Y');
	$scope.currentProject = $scope.i;
	
	
	$scope.showCurrentProject = function(project){
		return project;
	}
	
	
	
	$scope.loadItem = function(path){	
		$("<img />").attr("src", path).load(function() {
			marginLeft = this.width / 2 *-1+"px";
			marginTop = this.height / 2 *-1+"px";
			$('.lightbox .image').html('<img style="margin:'+marginTop+' 0 0 '+marginLeft+';" src="'+path+'">');
		});
	}
	
	
	
	// Member Constructor
	$scope.member = {
		memberId : 		"<?php echo str_replace('"',"'", $member['member_id']);?>",
		username : 		"<?php echo str_replace('"',"'", $member['username']);?>",
		email : 		"<?php echo str_replace('"',"'", $member['email']);?>",
		firstName : 	"<?php echo str_replace('"',"'", $member['first_name']);?>",
		lastName : 		"<?php echo str_replace('"',"'", $member['last_name']);?>",
		about : 		"<?php echo str_replace('"',"'", $member['about']);?>",
		siteTitle : 	"<?php echo str_replace('"',"'", $member['site_title']);?>",
		tagline : 		"<?php echo str_replace('"',"'", $member['tagline']);?>",
		location : 		"<?php echo str_replace('"',"'", $member['location']);?>",
		phone : 		"<?php echo str_replace('"',"'", $member['phone']);?>",
		school : 		"<?php echo str_replace('"',"'", $member['school']);?>",
		profileImg : 	"<?php echo str_replace('"',"'", $member['profile_img']);?>",
		fontId : 		"<?php echo str_replace('"',"'", $member['font_id']);?>",
		projectLayout : "<?php echo $member['layout_option'];?>",
		colorSchemeId : "<?php echo str_replace('"',"'", $member['color_scheme_id']);?>",
		titleOption : 	"<?php echo str_replace('"',"'", $member['title_option']);?>",
		logoImg : 		"<?php echo str_replace('"',"'", $member['logo_img']);?>",
		trackingId : 	"<?php echo str_replace('"',"'", $member['tracking_id']);?>",
		skills : [
		
			<?php // Get Member Skills
			$get_member_skills = $db->prepare("SELECT member_skills.member_id, member_skills.category_id, categories.category_name 
			FROM members_skills, categories WHERE member_skills.category_id = categories.category_id AND member_skills.member_id = :id");
			$get_member_skills->bindValue(':id', $memberid);
			$get_member_skills->execute();
			$all_skills = $get_member_skills->fetchAll();
			foreach($all_skills as $member_skills){?>	
				
				{title : "<?php echo $member_skills['category_name'];?>"},
				
			<?php } ?>
			
		],
		links : [
		
			<?php // Get Member Links
			$get_links = $db->prepare("SELECT * FROM member_links WHERE member_id = :id ORDER BY link_title");
			$get_links->bindValue(':id', $memberid);
			$get_links->execute();
			$all_links = $get_links->fetchAll();
			foreach($all_links as $link){?>	
				
				{
					title : "<?php echo $link['link_title'];?>",
					url : "<?php echo $link['link_url'];?>"
				},
				
				
			<?php } ?>
			
		],
		projects : [
		
			<?php 
			$get_projects = $db->prepare("SELECT * FROM projects WHERE member_id=:id ORDER BY project_order");
			$get_projects->bindValue(':id', $member['member_id']);
			$get_projects->execute();
			$all_projects = $get_projects->fetchAll();
			foreach ($all_projects as $project){
				$get_cover = $db->prepare("SELECT item_filename FROM items WHERE item_id=:itemid LIMIT 1");
				$get_cover->bindValue(':itemid', $project['cover_id']);
				$get_cover->execute();
				$cover = $get_cover->fetch();
				$cover_filename = $cover['item_filename'];
				if(!$cover_filename){
					$get_cover = $db->prepare("SELECT item_filename FROM items WHERE project_id = :pid LIMIT 1");
					$get_cover->bindValue(':pid', $project['project_id']);
					$get_cover->execute();
					$cover = $get_cover->fetch();
					$cover_filename = $cover['item_filename'];
					
				}?>
				
				// Loop Each Project
				{
					title : "<?php echo str_replace('"',"'", $project['project_title']) ?>",
					slug : "<?php echo $project['project_slug'] ?>",
					desc : "<?php echo str_replace('"',"'", $project['project_desc']) ?>",
					website : "<?php echo $project['project_website'] ?>",
					coverFilename : "<?php echo $cover_filename; ?>",
					projectId : "<?php echo $project['project_id'] ?>",
					projectOrder : "<?php echo $project['project_order'] ?>",
					items : [
					
						<?php 
						$get_items = $db->prepare("SELECT * FROM items WHERE project_id=:pid AND member_id=:memberid ORDER BY item_order");
						$get_items->bindValue(':pid', $project['project_id']);
						$get_items->bindValue(':memberid', $member['member_id']);
						$get_items->execute();
						$all_items = $get_items->fetchAll();
						foreach ($all_items as $item){?>	
							
							// Loop Each Item		
							{
								itemId : "<?php echo $item['item_id'] ?>",
								title : "<?php echo str_replace('"',"'", $item['item_title']) ?>",
								desc : "<?php echo str_replace('"',"'", $item['item_desc']) ?>",
								filename : "<?php echo $item['item_filename'] ?>",
								projectId : "<?php echo $item['project_id'] ?>"
							},	
									
						<?php } ?>
						
					]
				},	
					
			<?php } ?>
			
		],
	};	
}
</script>