<!doctype html><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>PortfolioLounge</title>
</head>

<style>

	body {
		text-align:center;	
		font-family:Arial, Helvetica, sans-serif;
		font-size:11px;
	}
	.middle {
		position:absolute;
		top:50%;
		left:50%;
		margin:-100px 0 0 -250px;
		width:500px;
	}

</style>
<body>


<div class="middle">
	
	<h2>This portfolio has been temporarily disabled.</h2>
	<p>If you are the owner of this portfolio, visit your <a href='http://portfoliolounge.com/admin/account/payment'>Payment Settings</a> to update payment method.</p>
	<br><br><br><br>
	<a href="http://portfoliolounge.com"><img src="../images/logo2-small.png"></a>

</div>


</body>
</html>