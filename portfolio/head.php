<?php

$meta_description_dash = $member['about'] && $member['tagline'] ? " | " : "";
$meta_description = ($member['tagline'] || $member['about']) ? $member['tagline'].$meta_description_dash.$member['about'] : 'Welcome to my portfolio website.';

?>
<!doctype html>
<head>
<base href="<?php echo $base_url;?>">
<meta name="viewport" content="initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="description" content="<?=$meta_description?>" />
<title><?php if($member['site_title']) {echo $member['site_title'];} else {echo "My portfolio website!";}?></title>

<link rel="stylesheet" type="text/css" href="/portfolio/css/portfolios.css">
<link rel="stylesheet" type="text/css" href="/portfolio/css/portfolio_v<?php echo $template_id;?>.css">
<?php if($template_id>=6){?><link rel="stylesheet" type="text/css" href="/portfolio/css/bootstrap.css"><?}?>
<?php if(isset($admin)){?><link rel="stylesheet" type="text/css" href="/portfolio/css/admin.css"><?}?>
<link href='<?=$protocol?><?php echo $font['url'];?>' rel='stylesheet' type='text/css'>
<script src="/js/libs/jquery-1.8.3.min.js"></script>
<script src="/js/libs/jquery.easing.1.3.js"></script>
<script src="/js/libs/jquery.cycle.all.min.js"></script>
<script src="/portfolio/js/portfolio.js"></script>
<script src="/js/libs/analytics.js"></script>
<?php include 'portfolio/js/tracking.php';?>
<?php include 'portfolio/css/style.php';?>
<?php 
if($member['first_name']){
	$last_name = $member['last_name'] ? " ".$member['last_name'] : "";
	$site_title = $member['site_title'] ? $member['site_title'] : $member['first_name'].$last_name."'s Portfolio";
} else {
	$site_title = $member['site_title'] ? $member['site_title'] : $member['username']."'s Portfolio";
}?>
<title><?=$site_title?></title>
</head>
<body class="<?php if($member['background']) echo $member['background'];?>">




<?php if(isset($_SESSION['AUTHENTICATED'])){?>

	<?php if($p=='portfolio'){
    	$get_project = $db->prepare("SELECT project_id FROM projects WHERE project_slug = :slug AND member_id = :id LIMIT 1");
		$get_project->bindValue(':slug', $i);
		$get_project->bindValue(':id', $member['member_id']);
		$get_project->execute();
		$project = $get_project->fetch();
		$project_path = 'project/'.$project['project_id'];
		
		
    } else {
		$project_path = 'projects';	
	}?>
	<div id="toolbar">
		<a href="https://portfoliolounge.com/admin/projects"><img src="images/icon-pencil-white.png"></a>
	</div>

<?php }?>

	
<?php if($p=='client') {?>
<div id="enter-password">
	<div class="inner">
		<h1>Password Required.</h1>
		<form action="portfolio/services.php">
			<input type="hidden" name="a" value="enter-password">
			<input type="hidden" name="project_id" value="<?=$i?>">
			<input name="password" type="password">
		<br>
		<br>
			<input type="submit" class="btn" value="Enter &raquo;">
		<br>
		<br>
			<a href="javascript:history.back()"><small>&larr; Back</small></a>
		</form>
	</div>
</div>
<div id="password-bg"></div>
<?php } ?>

	
<!-- Test <?=$admin?>-->
<?php if(isset($admin)) {?>
		<a style="position:fixed;z-index:99999;right:0;top:0;" href="http://portfoliolounge.com">&laquo; Back to PortfolioLounge</a>
		
<?php } ?>
	


<div id="overlay"></div>
<div class="lightbox">
	<div class="image"></div>
	<div class="info font"></div>
</div>	


<!-- pushes footer to bottom of page -->
<div id="wrapper">
	<div id="inner-wrapper">
		
		

	<div class="strategic">
		<h1>
			Create a portfolio website.
		</h1>
		<h2>
			Build your online portfolio quickly and easily.
		</h2>
	</div>
	