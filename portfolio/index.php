<?php

$host = $_SERVER['HTTP_HOST']; // username.portfoliolounge.com || www.custom.com 
if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
	$protocol = 'https://';
} else {
	$protocol = 'http://';
}
$base_url = $protocol. $host;

if(isset($_GET['debug']) && $_GET['debug'] == '1') {
	error_reporting(E_ALL);
ini_set('display_errors', 1);
}

/* Determine Member
---------------------------------------*/
if (strpos($host,'portfoliolounge') || strpos($host,'sxdmodelsandartists')) {
	// PL URL
	
	$expload_url = explode(".",$host);
	$url_username = $expload_url[0]=='www' ? $expload_url[1] : $expload_url[0];
	
	$stmt = $db->prepare("SELECT * FROM members WHERE username=:u");
	$stmt->bindValue(':u', $url_username);
	$stmt->execute();
	$result = $stmt->fetchAll();
	$result_count = count($result);
	
	
	/*
	// Forward portfolio to secure protool (HTTPS)
	// Only forward when the user is using a portfoliolounge.com subdomain
	// ensuring that it doesnt happen for user owned domains or template preview url's
	if((empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") && $expload_url[1]!='v'){
		$redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: ' . $redirect);
		exit();
	}
	*/
	
} else if ($p=='your-portfolio'){
	
	
	$url_username = $i;
	
	$stmt = $db->prepare("SELECT * FROM members WHERE username=:u");
	$stmt->bindValue(':u', $url_username);
	$stmt->execute();
	$result = $stmt->fetchAll();
	$result_count = count($result);
	//$base_url = 'http://'.$host.'/your-portfolio/$i';
	$base_url = $base_url."/your-portfolio/".$url_username."/";
	
	$p = $x;
	$i = $e;
	

} else {
	// CUSTOM URL
	
	$stmt = $db->prepare("SELECT * FROM members WHERE custom_domain=:c");
	$stmt->bindValue(':c', $host);
	$stmt->execute();
	$result = $stmt->fetchAll();
	$result_count = count($result);
	
	
}


/* Get Member
---------------------------------------*/
if ($result_count > 0){
	
	$member = $result[0];
	$memberid = $member['member_id'];
	
	
	/* Template URLs
	---------------------------------------*/
	$beta = $expload_url[1]=='v' ? true : false;
	if(isset($_GET['debug']) && $_GET['debug'] == '1') {
		//var_dump($beta);
		//exit();
	}
	
	
	//echo 'v';
	//exit();
	
	
	
	
	
	/* Pull Template
	---------------------------*/
	if($member['account_profile_status']!='Suspended' && !$member['disabled']){
		
		
			
		/* Authenticate PL user on portfolio subdomain.
		------------------------------------------------------*/
		if($p=='authenticate'){
			$_SESSION["AUTHENTICATED"] = true;
		} else {
		
			
			if(!$beta){	
				$template_id = $member['template_id']+1;
			} else {
				$template_id = $expload_url[2]+1;
			}
			
			if(!$member['under_constructionj']){
				include 'portfolio_ctrl.php';
				
				
				include 'portfolio_v'.$template_id.'.php';
			} else {
				include 'under_construction.php';
			}
			

		}
		
		
	} else {
	
		// Deleted Member
		if($member['disabled']){
			header ("location: http://portfoliolounge.com");
		}
		// Disabled Paid Member
		include 'disabled.php';
	
	}
	
	
} else {
	
	header ("location: http://portfoliolounge.com");
	
}





