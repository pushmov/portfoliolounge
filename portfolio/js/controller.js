
	
	
	/* Lightboxes
	-----------------------------------*/
	
	function lightbox(action, id){
		
		// Open
		if(action=="newItems"){
			$("#overlay").fadeIn();			
			$("#upload-items").fadeIn();				
		}
		if(action=="profileImg"){
			$("#overlay").fadeIn();			
			$("#upload").fadeIn();				
		}
		if(action=="delete"){
			$("#overlay").fadeIn();				
			$("#confirm").fadeIn();
			$("#confirm #delete .project_id").val(id);
		}
		if(action=="deleteItem"){
			$("#overlay").fadeIn();				
			$("#deleteItem").fadeIn();
			$("#deleteItem #delete .item_id").val(id);
		}
		if(action=="newProject"){
			$("#overlay").fadeIn();				
			$("#new-project").fadeIn();
		}
		if(action=="changeCover"){
			$("#overlay").fadeIn();
			// Hide all thumbs
			$(".cover-thumbs a").css({"display":"none"});			
			$("#change-cover").fadeIn();
			// Fade in thumbs pertaining to project
			$(".cover-thumbs .cover"+id).fadeIn();
		}
		
		// Cancel
		if(action=="cancel"){
			$("#overlay").fadeOut();			
			$(".lightbox").fadeOut();	
		}
	}	
	
	$("#overlay").on({
		click: function() {
			lightbox('cancel');
		}
	});
	
	/* Hovers
	-----------------------------------*/
	$(".item-thumbs").hover(function(){
		$(this).stop().css({'opacity':'.7'});
	}, function(){
		$(this).stop().animate({'opacity':'1'},400);
	});
	
	/* Edit to Preview transition
	-----------------------------------*/
	
	// Show Edit Mode
	function editMode(){
		$(this).parents(".data-view").hide().next().fadeIn();
	}	
	
	// Show Preview Mode
	function previewMode(field){
		if(field=="all"){
			$(".data-edit").hide();
			$(".data-view").fadeIn();	
		}
		$(this).parents(".data-edit").hide().prev().fadeIn();
	}	
	
	
	// Edit & Cacel Btns
	$(".edit").on("click", editMode);
	$(".cancel").on("click", previewMode);
	
	
	
	/* Ajax Calls
	-----------------------------------*/	
	
	$("#project-title").submit(function(e) {
		
		$.ajax({
			type: "POST",
			url: "function/edit-projects.php?a=title",
			data: $(e.target).serialize(),
			dataType: "json",
			beforeSend:function(){
				//$('.alert-error,.alert-success').hide();
			},
			error: function(jqXHR, textStatus, errorThrown){
				
				//$('.alert-error').fadeIn();
				//$('#error-output').html(errorThrown);
			},
			success: function(data){
				previewMode("all");
			}
		});
		return false;
	});
	
	$("#bio-name").submit(function(e) {
		$.ajax({
			type: "POST",
			url: "function/edit-bio.php?a=name",
			data: $(e.target).serialize(),
			dataType: "json",
			beforeSend:function(){
				//$('.alert-error,.alert-success').hide();
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('error');
				//$('.alert-error').fadeIn();
				//$('#error-output').html(errorThrown);
			},
			success: function(data){
				previewMode("all");
			}
		});
		return false;
	});
	
	
	$("#bio-phone").submit(function(e) {
		$.ajax({
			type: "POST",
			url: "function/edit-bio.php?a=phone",
			data: $(e.target).serialize(),
			dataType: "json",
			beforeSend:function(){
				//$('.alert-error,.alert-success').hide();
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('error');
				//$('.alert-error').fadeIn();
				//$('#error-output').html(errorThrown);
			},
			success: function(data){
				previewMode("all");
			}
		});
		return false;
	});
	
	
	$("#single-data").submit(function(e) {
		$.ajax({
			type: "POST",
			url: "function/edit-items.php?a=item-info",
			data: $(e.target).serialize(),
			dataType: "json",
			beforeSend:function(){
				//$('.alert-error,.alert-success').hide();
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert(errorThrown);
				//$('.alert-error').fadeIn();
				//$('#error-output').html(errorThrown);
			},
			success: function(data){
				previewMode("all");
			}
		});
		return false;
	});
	
	
	$("#bio-location").submit(function(e) {
		$.ajax({
			type: "POST",
			url: "function/edit-bio.php?a=location",
			data: $(e.target).serialize(),
			dataType: "json",
			beforeSend:function(){
				//$('.alert-error,.alert-success').hide();
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('error');
				//$('.alert-error').fadeIn();
				//$('#error-output').html(errorThrown);
			},
			success: function(data){
				previewMode("all");
			}
		});
		return false;
	});
	
	
	$("#bio-school").submit(function(e) {
		$.ajax({
			type: "POST",
			url: "function/edit-bio.php?a=school",
			data: $(e.target).serialize(),
			dataType: "json",
			beforeSend:function(){
				//$('.alert-error,.alert-success').hide();
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('error');
				//$('.alert-error').fadeIn();
				//$('#error-output').html(errorThrown);
			},
			success: function(data){
				previewMode("all");
			}
		});
		return false;
	});
	
	
	$("#bio-email").submit(function(e) {
		$.ajax({
			type: "POST",
			url: "function/edit-bio.php?a=email",
			data: $(e.target).serialize(),
			dataType: "json",
			beforeSend:function(){
				//$('.alert-error,.alert-success').hide();
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('error');
				//$('.alert-error').fadeIn();
				//$('#error-output').html(errorThrown);
			},
			success: function(data){
				previewMode("all");
			}
		});
		return false;
	});
	
	
	$("#bio-skills").submit(function(e) {
		$.ajax({
			type: "POST",
			url: "function/edit-bio.php?a=skills",
			data: $(e.target).serialize(),
			dataType: "json",
			beforeSend:function(){
				//$('.alert-error,.alert-success').hide();
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('error');
				//$('.alert-error').fadeIn();
				//$('#error-output').html(errorThrown);
			},
			success: function(data){
				previewMode("all");
				updateSkills();
				
			}
		});
		return false;
	});
	
	
	$("#bio-about").submit(function(e) {
		$.ajax({
			type: "POST",
			url: "function/edit-bio.php?a=about",
			data: $(e.target).serialize(),
			dataType: "json",
			beforeSend:function(){
				//$('.alert-error,.alert-success').hide();
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert('error');
				//$('.alert-error').fadeIn();
				//$('#error-output').html(errorThrown);
			},
			success: function(data){
				previewMode("all");
			}
		});
		return false;
	});
	