$(document).ready(function(){
	
	
	
	
	
	
	
	
	/* Canvas & Frame
	-------------------------------*/	
	function initCanvas(){
		$('.canvas').each(function(){

			var canvas = $(this);
			var canvasWidth = canvas.closest('.frame').width();
			var canvasHeight = canvas.closest('.frame').height();
			var path = canvas.attr('data-source');	
			if(document.location.protocol=='https:'){	
				path = path.replace("http:", document.location.protocol);
			}
			//document.location.protocol
			
			
			// Display Image on load
			$("<img />").attr("src",path).load(function() {	
					
					
				// Figure out how to center and resize
				// the new image to fit frame.	
				var gridRatio = canvasWidth / canvasHeight;
				var imgRatio = this.width / this.height;
				var imgWidthAttr, imgHeightAttr;
				
				
				if (imgRatio < gridRatio){
					imgWidthAttr = canvasWidth;
					imgHeightAttr = this.height * (canvasWidth / this.width);
				} else {
					imgWidthAttr = this.width * (canvasHeight / this.height);
					imgHeightAttr = canvasHeight;
				}
				
				imgWidthAttr = imgWidthAttr+2;
				imgHeightAttr = imgHeightAttr+2;
				
				
				// Position Image and Display
				var marginTop = imgHeightAttr / 2 * -1 + canvasHeight / 2 ;
				var marginLeft = imgWidthAttr / 2 * -1 + canvasWidth / 2 ;
				canvas.append('<img width="'+imgWidthAttr+'" height="'+imgHeightAttr+'" style="margin:'+marginTop+'px 0 0 '+marginLeft+'px;" src="'+path+'" />').fadeIn(800);
				
				
				// Animate Image Title
				canvas.children('.info')
					.delay(300)
					.css({'margin-top':-20})
					.animate({
						'marginTop' : 0,
						'opacity' : 1
					}, 800, 'easeOutQuint');
				
				
				
					
			});
		});
	}
	initCanvas();
	
	
	
	/* Canvas & Frame
	-------------------------------*/	
	function initFitCanvas(){
		$('.canvasFit').each(function(){

			var canvas = $(this);
			var canvasWidth = canvas.closest('.frame').width();
			var canvasHeight = canvas.closest('.frame').height();
			var path = canvas.attr('data-source');	
			
			
			// Display Image on load
			$("<img />").attr("src",path).load(function() {	
					
					
				// Figure out how to center and resize
				// the new image to fit frame.	
				var gridRatio = canvasWidth / canvasHeight;
				var imgRatio = this.width / this.height;
				var imgWidthAttr, imgHeightAttr;
				
				if (imgRatio > gridRatio){
					imgWidthAttr = canvasWidth;
					imgHeightAttr = this.height * (canvasWidth / this.width);
				} else {
					imgWidthAttr = this.width * (canvasHeight / this.height);
					imgHeightAttr = canvasHeight;
				}
				
				imgHeightAttr = Math.round(imgHeightAttr);
				imgWidthAttr = Math.round(imgWidthAttr);
				
				if(imgHeightAttr > this.height) imgHeightAttr = this.height;
				if(imgWidthAttr > this.width) imgWidthAttr = this.width;
				
				
				// Position Image and Display
				var marginTop = imgHeightAttr / 2 * -1 + canvasHeight / 2;
				var marginLeft = imgWidthAttr / 2 * -1 + canvasWidth / 2;
				var marginTop = Math.round(marginTop);
				var marginLeft = Math.round(marginLeft);
				canvas.append('<img width="'+imgWidthAttr+'" height="'+imgHeightAttr+'" style="margin:'+marginTop+'px 0 0 '+marginLeft+'px;" src="'+path+'" />').fadeIn(800);
				
				canvas.append('<div class="share"></div>');
				
				
				// Animate Image Title
				canvas.children('.info')
					.delay(1000)
					.css({'margin-top':-20})
					.animate({
						'marginTop' : 0,
						'opacity' : 1
					}, 800, 'easeOutQuint');
				
				
				
					
			});
		});
	}
	initFitCanvas();
	
	
	
	
	
	/* Private Projects
	----------------------*/	
	$('.private-project').click(function(){
	
		event.preventDefault();
		
		$('#enter-password').fadeIn();
		$('#overlay').fadeIn();
		
	
	});
	
	/* Banners
	-------------------------------*/	
	$("#banner .inner").cycle({
		
		speed:500,
		timeout:5000,
		next:   '.next', 
		prev:   '.prev',
    	pager:  '.bullets'
		
	});
	
	
	
	
	/* Canvas Lightbox
	----------------------*/	
	$('.lb').click(function(){
		
		event.preventDefault();
		
		// Reset lightbox
		$('.lightbox .image, .lightbox .info').html('').hide();
		$('.lightbox').fadeIn();
		
		// Set Margins
		var lightboxMargin = 0;
		var infoHeight = 0;
		
		
		var infoText = $(this).find('.info').html();
		
		if(infoText) {
			$('.lightbox .info').html(infoText);
			$('.lightbox .info').fadeIn(500);
		}
		
		var canvasWidth = $('#overlay').width() - lightboxMargin;
		var canvasHeight = $('#overlay').height() - lightboxMargin - infoHeight;
		
		
		var path = $(this).attr('data-source');	
		path = path.replace("medium","/");
		path = path.replace("small","/");
		path = path.replace("/large/","/");
		
		$("<img />").attr("src",path).load(function() {	
									
			var gridRatio = canvasWidth / canvasHeight;
			var imgRatio = this.width / this.height;
			
			if (imgRatio > gridRatio){
				var imgWidthAttr = canvasWidth;
				var imgHeightAttr = this.height * (canvasWidth / this.width);
			} else {
				var imgWidthAttr = this.width * (canvasHeight / this.height);
				var imgHeightAttr = canvasHeight;
			}
			
			if(imgHeightAttr > this.height) imgHeightAttr = this.height;
			if(imgWidthAttr > this.width) imgWidthAttr = this.width;
			
			
			var marginTop = imgHeightAttr / 2 * -1 + canvasHeight / 2 + lightboxMargin/2;
			var marginLeft = imgWidthAttr / 2 * -1 + canvasWidth / 2 + lightboxMargin/2;
			$('.lightbox .image').html('<img width="'+imgWidthAttr+'" height="'+imgHeightAttr+'" style="margin:'+marginTop+'px 0 0 '+marginLeft+'px;" src="'+path+'" />').fadeIn(800);
			
				
		});
	});
	
	
	
	
	/* Video Lightbox
	----------------------*/	
	$('.video').click(function(){
		
		event.preventDefault();
		
		// Reset lightbox
		$('.lightbox .image, .lightbox .info').html('').hide();
		$('.lightbox').fadeIn();
		
		// Set Margins
		var lightboxMargin = 0;
		var infoHeight = 0;
		
		var videoId = $(this).attr('video-id');	
		var videoType = $(this).attr('video-type');
		
		
		var width = 640;
		var height = 360;
		
		
		
		// Create iFrame Video Player
		var iframe;
		if(videoType=='vimeo'){		
			var iframe = '<iframe src="https://player.vimeo.com/video/'+videoId+'" width="'+width+'" height="'+height+'" frameborder="0" webkitallowfullscreen mozallowfullscreen autoplay allowfullscreen></iframe>';
		}
		if(videoType=='youtube'){
			var iframe = '<iframe width="'+width+'" height="'+height+'" src="https://www.youtube.com/embed/'+videoId+'?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>';
		}
		if(videoType=='soundcloud'){
			var frame = '<iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/207202432&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>';	
		}
		
		
		
		
		// Create Player
		$(".lightbox").html('<div style="position:absolute;left:50%;top:50%;margin:-180px 0 0 -320px;">'+iframe+'</div>');
		
		//<iframe width="640" height="360" src="https://www.youtube.com/embed/'+videoId +'?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe> not work
		//<iframe width="640" height="360" src="https://www.youtube.com/embed/VpatzFx_o4Y?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
		// <iframe width="1280" height="720" src="https://www.youtube.com/embed/MMaGzFuxq1A?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
		
		
	});
	
	/* Close Lightbox
	----------------------*/
	function closeLightbox(){
		$('.lightbox').fadeOut(600);
	}	
	$('.lightbox .close, .lightbox').click(function(){
		closeLightbox();	
	});


	
	
	
	
	/* Contact Form
	----------------------*/
	$("#contact").submit(function(){
		var str = $(this).serialize();
		$.ajax({
			type: "POST",
			url:"/portfolio/send.php",
			data: str,
			beforeSend: function(){
				$('.btn').val('...');
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert(errorThrown);
			},
			success:function(result) {
				$('.btn').hide();
				$('.thank_you').fadeIn();
			}
		});
		return false;
	});
	
	
	
	
	
	
	
	

	
	

						
});

		
		
		