/* Portfolio v3 JS */


/* Lightboxes
----------------------*/
function openLightbox(path){
	$('.lightbox').fadeIn();
	$('.lightbox .image').html('');
	
	$("<img />").attr("src", path).load(function() {
        marginLeft = this.width / 2 *-1+"px";
        marginTop = this.height / 2 *-1+"px";
		$('.lightbox .image').html('<img style="margin:'+marginTop+' 0 0 '+marginLeft+';" src="'+path+'">');
    });
}

function closeLightbox(){
	$('.lightbox').fadeOut();
}

$('.lightbox .close, .lightbox').click(function(){
	closeLightbox();	
});


/* Contact Form
----------------------*/
$("#contact").submit(function(){
	var str = $(this).serialize();
	$.ajax({
		type: "POST",
		url:"/portfolio/send.php",
		data: str,
		beforeSend: function(){
			$('.btn').val('...');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		},
		success:function(result) {
			$('.btn').hide();
			$('.thank_you').fadeIn();
		}
	});
	return false;
});


$('.phone-number').text(function(i, text) {
	return text.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');
});











