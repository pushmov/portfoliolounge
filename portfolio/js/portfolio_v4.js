$(document).ready(function(){
	
	
	
	
	/* Banners
	-------------------------------*/	
	$("#banner .inner").cycle({
		
		speed:500,
		timeout:5000,
		next:   '.next', 
		prev:   '.prev'
		
	});
	
	
	
	
	
	/* Canvas Loading
	-------------------------------*/	
	$('.canvas').each(function(){	
		
		var canvas = $(this);
		var canvasWidth = canvas.closest('.frame').width();
		var canvasHeight = canvas.closest('.frame').height();
		var path = canvas.attr('data-source');		
		
		$("<img />").attr("src",path).load(function() {	
									
			var gridRatio = canvasWidth / canvasHeight;
			var imgRatio = this.width / this.height;
			
			if (imgRatio < gridRatio){
				var imgWidthAttr = canvasWidth;
				var imgHeightAttr = this.height * (canvasWidth / this.width);
			} else {
				var imgWidthAttr = this.width * (canvasHeight / this.height);
				var imgHeightAttr = canvasHeight;
			}
			var marginTop = imgHeightAttr / 2 * -1 + canvasHeight / 2;
			var marginLeft = imgWidthAttr / 2 * -1 + canvasWidth / 2;
			canvas.html('<img width="'+imgWidthAttr+'" height="'+imgHeightAttr+'" style="margin:'+marginTop+'px 0 0 '+marginLeft+'px;" src="'+path+'" />').fadeIn();

		});
	});
	
		
	
	/* Canvas Lightbox
	----------------------*/
	$('.canvas.lb').click(function(){
		
		$('.lightbox').fadeIn();
		$('.lightbox .image').html('').hide();
		
		
		var canvasWidth = $('#overlay').width() - 40;
		var canvasHeight = $('#overlay').height() - 40;
		var path = $(this).attr('data-source');	
		path = path.replace("medium","large");
		
		$("<img />").attr("src",path).load(function() {	
									
			var gridRatio = canvasWidth / canvasHeight;
			var imgRatio = this.width / this.height;
			
			if (imgRatio > gridRatio){
				var imgWidthAttr = canvasWidth;
				var imgHeightAttr = this.height * (canvasWidth / this.width);
			} else {
				var imgWidthAttr = this.width * (canvasHeight / this.height);
				var imgHeightAttr = canvasHeight;
			}
			var marginTop = imgHeightAttr / 2 * -1 + canvasHeight / 2 + 20;
			var marginLeft = imgWidthAttr / 2 * -1 + canvasWidth / 2 + 20;
			$('.lightbox .image').html('<img width="'+imgWidthAttr+'" height="'+imgHeightAttr+'" style="margin:'+marginTop+'px 0 0 '+marginLeft+'px;" src="'+path+'" />').fadeIn();

		});
	});
	
	
	
	/* Close Lightbox
	----------------------*/
	function closeLightbox(){
		$('.lightbox').fadeOut();
	}	
	$('.lightbox .close, .lightbox').click(function(){
		closeLightbox();	
	});

	
	

						
});

		
		
		