
var defaultAlpha = 0.10;
var itemWidth = 750;
var itemMargin = 20;
var easetype = "easeOutExpo";
var easetypeB = "easeOutExpo";
var x = $(window).width();
var y = $(window).height();
var marginLeft = itemWidth + x/3.5;
if(x<992){
	itemWidth = 400;
	marginLeft = itemWidth + 20;
}
var paginationId = 1;



$(document).ready(function(){
var totalItems = $( ".item" ).length;

	transition = function(toItemNumber){
		if(toItemNumber <= 0){
			paginationId = 1;
			return false;
		}
		if(toItemNumber > totalItems) {
			paginationId = totalItems;
			return false;
		}
		paginationId = toItemNumber;
		currentItem = $('.item:nth-child('+(toItemNumber)+')');
		currentNavItem = $('#nav ul li:nth-child('+(toItemNumber)+')');
		$('*').stop();
		$("#nav li a").removeClass("active");
		currentNavItem.children("a").addClass("active");
		$(".item").stop().animate({opacity:defaultAlpha},800);
		currentItem.stop().animate({opacity:1}, 800);
		$("#slider").stop().animate({left:((itemWidth+itemMargin)*paginationId*-1)+marginLeft},800,easetype);
		$('*').animate({ scrollTop: "0px" }, 800,easetypeB);
	};
	transition(1);

	$("#nav li a").click(function(){
		$("#nav li a").removeClass("active");
		$(this).addClass("active");
		$("#about").fadeOut();
	});

	
	$(".openAbout").click(function(event){
		event.preventDefault();
		$("#about").fadeIn();
	});
	$(".close").click(function(event){
		event.preventDefault();
		$("#about").fadeOut();
	});

	$("#mobileHeader .right").click(function(event){
		event.preventDefault();
		paginationId = paginationId+1;
		transition(paginationId);
	});
	$("#mobileHeader .left").click(function(event){
		event.preventDefault();
		paginationId = paginationId-1;
		transition(paginationId);
	});

	
});

$(document).keydown(function(e) {
	switch(e.which) {
		case 37: // Left Arrow Press
			paginationId = paginationId-1;
			transition(paginationId);
			break;
		case 39:// Right Arrow Press
			paginationId = paginationId+1;
			transition(paginationId);
			break;
		default: return; // exit this handler for other keys
	}
	e.preventDefault(); // prevent the default action (scroll / move caret)
});





