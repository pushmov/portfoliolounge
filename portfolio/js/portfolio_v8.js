var customScripts = {

    swingScroll: function () {
		if(window.location.hash) { //If page loads with hash
			customScripts.scrollToID(window.location.hash);
		}
		
		$('a[href^="#"]').click(function(event){ //Only target links that start with #
		   event.preventDefault();
		   customScripts.scrollToID($(this).attr('href'));
		});
    },
    slider: function () {
        $('#da-slider').cslider({
            autoplay: true,
            bgincrement: 0
        });
    },
    owlSlider: function () {
		
		//Details sliders
		$(".work-detail>.owl-carousel").owlCarousel({
			singleItem : true,
			slideSpeed : 450,
			navigation: true,
			pagination:false,
			afterAction : syncPosition,
			responsiveRefreshRate : 200,
		  });

		//Carousel sliders
		$(".slider-container>.owl-carousel").owlCarousel({
			autoPlay: false,
			items : 3,
			itemsDesktop : [1199,3],
			itemsDesktopSmall : [979,3],
			pagination: false,
			navigation: true,
			slideSpeed: 250,
			responsiveRefreshRate : 100,
			navigationText: [
			  "<a class='sliderButtonLeft'></a>",
			  "<a class='sliderButtonRight'></a>"
			  ],
			  afterInit : function(el){
				  el.find(".owl-item").eq(0).addClass("synced");
				}
		  });

		  // Custom Navigation Events
		  $(".next").click(function () {
			owl.trigger('owl.next');
		  })
		  $(".prev").click(function () {
			owl.trigger('owl.prev');
		  })
		 
		  $('.slider-container .owl-carousel').on("click", ".owl-item", function(e){
			e.preventDefault();
			var number = $(this).data("owlItem");
			$(this).parents('.slider-container').siblings('.owl-carousel').trigger("owl.goTo",number);
		  });

		  function syncPosition(el){
			var current = this.currentItem;
			var syncedCarousel = $(el).siblings('.slider-container').children('.owl-carousel');
			syncedCarousel
			  .find(".owl-item")
			  .removeClass("synced")
			  .eq(current)
			  .addClass("synced");

			if($(el).data("owlCarousel") !== undefined){
				center(current, syncedCarousel)
			}
		  }
		 
		  function center(num, syncedCarousel){
			var sync2visible = syncedCarousel.data("owlCarousel").owl.visibleItems;

			var found = (sync2visible.indexOf(num) != -1);
		 
			if(found==false){
			  if(num>sync2visible[sync2visible.length-1]){
				syncedCarousel.trigger("owl.goTo", num - sync2visible.length+2)
			  }else{
				if(num - 1 === -1){
				  num = 0;
				}
				syncedCarousel.trigger("owl.goTo", num);
			  }
			} else if(num === sync2visible[sync2visible.length-1]){
			  syncedCarousel.trigger("owl.goTo", sync2visible[1])
			} else if(num === sync2visible[0]){
			  syncedCarousel.trigger("owl.goTo", num-1)
			}
			
		  }
		  

    },
	scrollToID: function(ID){
		var navOffset = ($(window).width() > 979) ? 93 : 0; //adjust this if you have a fixed header with responsive design
		$('html, body').animate({
			'scrollTop': $(ID).offset().top - navOffset
		}, 1000)
	},
    init: function () {
        customScripts.swingScroll();
        customScripts.slider();
        customScripts.owlSlider();
    }
}

//customScripts.owlSlider();
$('document').ready(function () {
    customScripts.init();
});

$("#contactForm").submit(function(){
	var str = $(this).serialize();
	$.ajax({
		type: "POST",
		url:"/portfolio/send.php",
		data: str,
		beforeSend: function(){
			$('.btn').val('...');
		},
		error: function(jqXHR, textStatus, errorThrown){
			alert(errorThrown);
		},
		success:function(result) {
			$('#formSubmit').hide();
			$('#thank_you').fadeIn();
		}
	});
	return false;
});