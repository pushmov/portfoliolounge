<?php 

/* GET Logo
------------------------------------*/
function getLogo(){	

	
	?> <a id="logo" href="/"> <?
	
		global $memberid, $member; // Global Vars
		
		if($member['logo_img']) {// Logo
			echo '<img src="http://portfoliolounge.com/uploads/'.$memberid.'/'.$member["logo_img"].'">';
		
		} else if($member['site_title']) {// Site Title
			echo '<h1 class="font">'.$member['site_title'].'</h1>';
			
		} else {// Username	
			echo '<h1 class="font">'.$member['username'].'</h1>';	
			
		}
		
	?> </a> <?

}

/* 	GET Navigation
-------------------------------------------------*/
function getProjectInfo(){ 
	require_once 'config.php';
	global $memberid, $member, $i;
	$get_project = $db->prepare("SELECT project_title, project_desc, project_website FROM projects WHERE project_slug=:slug LIMIT 1");
	$get_project->bindValue(':slug', $i);
	$get_project->execute();
	$allprojects = $get_project->fetchAll();
	foreach ($allprojects as $project){
		
		$project_website = $project['project_website'];
				
		if (strpos($project_website,'https')) {
			
		} else {
			$project_website = 'http://'.$project['project_website'];
		}?>
		
		
		<div class="projectInfo">
			<h1 class="font"><?=$project['project_title']?></h1>
			<p><?=$project['project_desc']?></p>
			<p><a target="_blank" href="<?=$project_website?>"><?=$project['project_website']?></a></p>
		</div>
		
	<?php }
		
}


/* 	GET Navigation
-------------------------------------------------*/
function getProjectNav(){ 
	require_once 'config.php';
	global $memberid, $member, $i;
	
	$get_project_nav = $db->prepare("SELECT project_slug, project_title FROM projects WHERE member_id=:mid ORDER BY project_order");
	$get_project_nav->bindValue(':mid', $member['member_id']);
	$get_project_nav->execute();
	$all_project_nav = $get_project_nav->fetchAll();
	foreach ($all_project_nav as $project_nav){
		
		$currentClass = '';
		
		
		if($i==$project_nav['project_slug']) {
			
			$currentClass =  'cur';
		
		} 
		
		echo "<li class='".$currentClass."'><a href='portfolio/".$project_nav['project_slug']."'>".$project_nav['project_title']."</a></li>";
		
		
	}
		
}




/* 	GET Projects
-------------------------------------------------*/
function getProjects($size, $col, $height, $gutter){ 

	require_once 'config.php';

	global $memberid, $member, $i; 
	
	// Gutter
	$gutterBottom = $gutter;
	$gutterSides = $gutter * 1.5;
	$gutterSides = ($col==1) ? 0 : $gutterSides;
	
	?>

	<div class="col-<?=$col?>">
	
		<?php 
		
		$get_projects = $db->prepare("SELECT * FROM projects WHERE member_id=:mid ORDER BY project_order");
		$get_projects->bindValue(':mid', $member['member_id']);
		$get_projects->execute();
		$all_projects = $get_projects->fetchAll();
		foreach ($all_projects as $project){
			$get_cover = $db->prepare("SELECT item_filename FROM items WHERE item_id=:itemid LIMIT 1");
			$get_cover->bindValue(':itemid', $project['cover_id']);
			$get_cover->execute();
			$cover = $get_cover->fetch();
			$cover_filename = $cover['item_filename'];
			
			if(!$cover_filename){
				$get_cover = $db->prepare("SELECT item_filename FROM items WHERE project_id=:pid LIMIT 1");
				$get_cover->bindValue(':pid', $project['project_id']);
				$get_cover->execute();
				$cover = $get_cover->fetch();
				$cover_filename = $cover['item_filename'];
			}
			$slide_item_path = 'http://portfoliolounge.com/uploads/'.$memberid.'/'.$size.'/'.$cover_filename;?>
	
			<!-- Frame&Canvas -->		
			<a class="frame font" href='portfolio/<?= $project['project_slug'] ?>' style="height:<?=$height?>%; margin:0 0 <?= $gutterBottom ?>% 0;">
				<div class="canvas" data-source="<?php echo $slide_item_path;?>" style="margin:0 <?= $gutterSides?>%;">
					<?php if($project['project_title'] || $project['project_desc']){?>
						<div class="info">
							<?php if($project['project_title']){?><h2><?= $project['project_title'] ?></h2><?php } ?>
							<?php if($project['project_desc']){?><p><?= $project['project_desc'] ?></p><?php } ?>
						</div>
					<?php } ?>
				</div>
			</a>
			
		<?php }?>
	
	</div>
	
	<?
}


/* 	Get Items
-------------------------------------------------*/
function getItems($size, $col, $height, $gutter){ 
	require_once 'config.php';
	global $memberid, $member, $i;
	
	// Get Project
	$get_project = $db->prepare("SELECT project_id FROM projects WHERE project_slug = :slug AND member_id = :id LIMIT 1");
	$get_project->bindValue(':slug', $i);
	$get_project->bindValue(':id', $memberid);
	$get_project->execute();
	$project = $get_project->fetch();
	// Get Items
	$get_items = $db->prepare("SELECT * FROM items WHERE project_id=:pid ORDER BY item_order");
	$get_items->bindValue(':pid', $project['project_id']);
	$get_items->execute();
	$all_items = $get_items->fetchAll();
		
	// Gutter
	$gutterBottom = $gutter;
	$gutterSides = $gutter * 1.5;
	$gutterSides = ($col==1) ? 0 : $gutterSides;
	
	?>
	
	<div class="col-<?=$col?>">
		

		<?php 
			foreach($all_items as $item){
			$path = 'http://portfoliolounge.com/uploads/'.$memberid.'/'.$size.'/'.$item['item_filename'];?>
			
			<!-- Frame&Canvas -->		
			<div class="frame font" style="height:<?=$height?>%; margin:0 0 <?=$gutterBottom?>% 0;">
				<div class="canvas lb" data-source="<?= $path ?>" style="margin:0 <?=$gutterSides?>%;">
					<?php if($item['item_title'] || $item['item_desc']){?>
						<div class="info">
							<?php if($item['item_title']){?><h2><?= $item['item_title'] ?></h2><?php } ?>
							<?php if($item['item_desc']){?><p><?= $item['item_desc'] ?></p><?php } ?>
						</div>
					<?php } ?>
				</div>
			</div>
		
		<?php } ?>
		
	</div>
	
<?php }




/* 	Get Skills
-------------------------------------------------*/
function getSkills(){
	require_once 'config.php';
	global $memberid, $member, $i;
	$get_member_skills = $db->prepare("SELECT member_skills.member_id, member_skills.category_id, categories.category_name 
		FROM member_skills, categories WHERE member_skills.category_id = categories.category_id AND member_skills.member_id = :id");
	$get_member_skills->bindValue(':id', $memberid);
	$get_member_skills->execute();
	$skills_all = $get_member_skills->fetchAll();
	$count = count($skills_all);	
	
	if($count > 0){?>

		<ul class="list">
			<li class="font"><h2>Skills</h2></li>
			
		
			
			<?php foreach($skills_all as $member_skills){?>	
			
			
			<li><?= $member_skills['category_name'] ?></li>
			
			
		
			<?php } ?>
		
		</ul>

	<?php } ?>

<?php } 



/* 	Get Skills
-------------------------------------------------*/
function getLinks(){
	require_once 'config.php';
	global $memberid, $member, $i;

	$get_links = $db->prepare("SELECT * FROM member_links WHERE member_id = :mid ORDER BY link_title");
	$get_links->bindValue(':mid', $memberid);
	$get_links->execute();
	$links_all = $get_links->fetchAll();
	$count = count($links_all);
	if($count > 0){?>	

	<ul class="list">
		<li class="font"><h2>Links</h2></li>	
		
		<?php foreach($links_all as $link){?>	
			
	
			<li><a target="_blank" href='<?= $link['link_url'] ?>'><?= $link['link_title'] ?></a></li>
	
	
		<?php } ?>
		
		
	</ul>
	
	<?php } ?>


<?php }

?>