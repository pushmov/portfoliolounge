<?php 

require_once 'config.php';
/* Set project for homepage
if(!$i){
	$get_project = $db->prepare("SELECT project_slug FROM projects WHERE member_id = :id ORDER BY project_order LIMIT 1");
	$get_project->bindValue(':id', $memberid);
	$get_project->execute();
	$project = $get_project->fetch();
	$i = $project['project_slug'];
}*/

$get_colors = $db->prepare("SELECT * FROM colors WHERE color_scheme_id=:scheme LIMIT 1");
$get_colors->bindValue(':scheme', $member['color_scheme_id']);
$get_colors->execute();
$color = $get_colors->fetch();

$get_fonts = $db->prepare("SELECT * FROM fonts WHERE font_id=:font");
$get_fonts->bindValue(':font', $member['font_id']);
$get_fonts->execute();
$font = $get_fonts->fetch();

function getItems($id){
	require_once 'config.php';
	global $memberid, $member, $i; 

	// Get Items
	$get_items = $db->prepare("SELECT * FROM items WHERE project_id=:id AND member_id=:mid ORDER BY item_order");
	$get_items->bindValue(':id', $id);
	$get_items->bindValue(':mid', $memberid);
	$get_items->execute();
	$all_items = $get_items->fetchAll();
	foreach ($all_items as $item){
		$slide_item_path = 'http://portfoliolounge.com/uploads/'.$memberid.'/small/'.$item['item_filename'];?>

			<a class="frame lb" data-source="<?= $slide_item_path ?>" href='portfolio/<?= $project['project_slug'] ?>'>
				<div class="canvas" data-source="<?= $slide_item_path ?>">
					<?php if($item['item_title'] || $item['item_desc']){?>
					<div class="info">
						<?php if($item['item_title']){?><h2 class="font"><?= $item['item_title'] ?></h2><?php } ?>
						<?php if($item['item_desc']){?><p><?= $item['item_desc'] ?></p><?php } ?>
					</div>
					<?php } ?>
				</div>
			</a>
<?php }
}


/* 	GET Projects
-------------------------------------------------*/
function getUploads($type, $size, $plain, $fit){ 
	require_once 'config.php';
	global $memberid, $member, $i, $protocol; 
		
		if($type=='projects'){		
			$get_projects =  $db->prepare("SELECT * FROM projects WHERE member_id=:mid ORDER BY project_order");
			$get_projects->bindValue(':mid', $member['member_id']);
			$get_projects->execute();
			$all_projects = $get_projects->fetchAll();
			foreach ($all_projects as $project){	
				$get_cover = $db->prepare("SELECT item_filename FROM items WHERE item_id=:itemid LIMIT 1");
				$get_cover->bindValue(':itemid', $project['cover_id']);
				$get_cover->execute();
				$cover = $get_cover->fetch();
				$cover_filename = $cover['item_filename'];
				
				if(!$cover_filename){
					
					$get_cover = $db->prepare("SELECT item_filename FROM items WHERE project_id=:pid LIMIT 1");
					$get_cover->bindValue(':pid', $project['project_id']);
					$get_cover->execute();
					$cover = $get_cover->fetch();
					$cover_filename = $cover['item_filename'];
				}
				
				if($size!='original'){
				
				$slide_item_path = $protocol.'portfoliolounge.com/uploads/'.$memberid.'/'.$size.'/'.$cover_filename;

				} else {
				
				$slide_item_path = $protocol.'portfoliolounge.com/uploads/'.$memberid.'/'.$cover_filename;
				
				
				}?>
				


					<?php 
					
					$project_url = $project['privacy_option']=='1' ? 'client/'.$project['project_id']  :  'portfolio/'.$project['project_slug']  ?>
	
						
					
					<a class="frame font" href='<?= $project_url ?>'>
					<div class="canvas" data-source="<?php echo $slide_item_path;?>">	
						<div class="vinnette"></div>
						<?php if($project['project_title'] || $project['project_desc']){?>
							<div class="info">
								<?php if($project['project_title']){?><h2 class="font"><?= $project['project_title'] ?> <?php if($project['privacy_option']=='1'){?><img src="http://portfoliolounge.com/images/icon-lock.png" /><?php }?></h2><?php } ?>
								<?php if($project['project_desc']){?><p><?= $project['project_desc'] ?></p><?php } ?>
							</div>
						<?php } ?>
					</div>
				</a>
				
			<?php }
		
		} else if ($type=='items'){
				
			// Get Project for normal project pages
			$get_project = $db->prepare("SELECT project_id FROM projects WHERE project_slug=:slug 
				AND member_id = :mid LIMIT 1");
			$get_project->bindValue(':slug', $i);
			$get_project->bindValue(':mid', $memberid);
			$get_project->execute();
			$project = $get_project->fetch();
				
			// Get Items
			$get_items = $db->prepare("SELECT * FROM items WHERE project_id=:pid AND member_id=:mid ORDER BY item_order");
			$get_items->bindValue(':pid', $project['project_id']);
			$get_items->bindValue(':mid', $memberid);
			$get_items->execute();
			$all_items = $get_items->fetchAll();
				
			?>
			
		
				<?php foreach ($all_items as $item){
					
					
					if($size!='original'){
						
						$path = $protocol.'portfoliolounge.com/uploads/'.$memberid.'/'.$size.'/'.$item['item_filename'];
		
						} else {
						
						$slide_item_path = $protocol.'portfoliolounge.com/uploads/'.$memberid.'/'.$cover_filename;
						
					
					}?>
						

					<?php if($plain){?>


					<img class="frame lb" src="<?= $path ?>" data-source="<?= $path ?>">
					<div class="plain-info">
						<?php if($item['item_title']){?><h2 class="font"><?= $item['item_title'] ?></h2><?php } ?>
						<?php if($item['item_desc']){?><p><?= $item['item_desc'] ?></p><?php } ?>
					</div>

					<?php } else {?>



					<a class="frame lb" data-source="<?= $path ?>" href='portfolio/<?= $project['project_slug'] ?>'>
						<div class="canvas<?=$fit?>" data-source="<?= $path ?>">
							<?php if($item['item_title'] || $item['item_desc']){?>
								<div class="info">
									<?php if($item['item_title']){?><h2 class="font"><?= $item['item_title'] ?></h2><?php } ?>
									<?php if($item['item_desc']){?><p><?= $item['item_desc'] ?></p><?php } ?>
								</div>
							<?php } ?>
						</div>
					</a>

					<?php }
				}
		}	
}


/* GET Logo
------------------------------------*/
function getLogo(){	
	require_once 'config.php';
	global $protocol;

	
	?> <a id="logo" href="/"> <?php
	
		global $memberid, $member; // Global Vars
		
		if($member['logo_img']) {// Logo
			echo '<img src="'.$protocol.'portfoliolounge.com/uploads/'.$memberid.'/'.$member["logo_img"].'">';
		
		} else if($member['site_title']) {// Site Title
			echo '<h1 class="font">'.$member['site_title'].'</h1>';
			
		} else {// Username	
			echo '<h1 class="font">'.$member['username'].'</h1>';	
			
		}
		
	?> </a> <?php

}




/* 	GET Navigation
-------------------------------------------------*/
function getProjectInfo(){ global $memberid, $member, $i;
	
	require_once 'config.php';
	$get_project = $db->prepare("SELECT project_title, project_desc, project_website FROM projects WHERE 
		project_slug = :slug AND member_id = :mid LIMIT 1");
	$get_project->bindValue(':slug', $i);
	$get_project->bindValue(':mid', $memberid);
	$get_project->execute();
	$all_projects = $get_project->fetchAll();
	foreach ($all_projects as $project){
		
		$project_website = $project['project_website'];
				
		if (strpos($project_website,'https')) {
			
		} else {
			$project_website = 'http://'.$project['project_website'];
		}?>
		
			<div class="projectInfo">
				<h1 class="font"><?=$project['project_title']?></h1>
				<?php if($project['project_desc']){?> <p><?=$project['project_desc']?></p> <?php } ?>
				<?php if($project['project_website']){?> <p><a target="_blank" href="<?=$project_website?>"><?=$project['project_website']?></a></p><?php } ?>
			</div>
		
	<?php }
		
}



/* 	Get Lists
-------------------------------------------------*/
function listItems($type){
	require_once 'config.php';
	global $memberid, $member, $i;
	
	
	if($type=='links'){
		$get_links = $db->prepare("SELECT * FROM member_links WHERE member_id = :mid ORDER BY link_title");
		$get_links->bindValue(':mid', $memberid);
		$get_links->execute();
		$links_all = $get_links->fetchAll();
		$count = count($links_all);
		if($count > 0){ ?>	
	
			<?php foreach($links_all as $link){?>	
		
				<li><a target="_blank" href='<?= $link['link_url'] ?>'><?= $link['link_title'] ?></a></li>
		
			<?php } 
		} 
	} 
}



/* 	Get Lists
-------------------------------------------------*/
function getList($type){
	require_once 'config.php';
	global $memberid, $member, $i;
	
	if($type=='projects'){
		$get_project_nav = $db->prepare("SELECT project_slug, project_title, project_order FROM projects WHERE member_id=:mid ORDER BY project_order");
		$get_project_nav->bindValue(':mid', $member['member_id']);
		$get_project_nav->execute();
		$all_project_nav = $get_project_nav->fetchAll();
		foreach ($all_project_nav as $project_nav){
			
			$currentClass = '';
			
			
			if($i==$project_nav['project_slug']) {
				
				$currentClass =  'cur';
			
			} 
			
			echo "<li class='".$currentClass."'><a href='portfolio/".$project_nav['project_slug']."'>".$project_nav['project_title']."</a></li>";
			
			
		}
	}
	
	if($type=='skills'){
		$get_member_skills = $db->prepare("SELECT member_skills.member_id, member_skills.category_id, categories.category_name 
			FROM member_skills, categories WHERE member_skills.category_id = categories.category_id AND member_skills.member_id = :mid");
		$get_member_skills->bindValue(':mid', $memberid);
		$get_member_skills->execute();
		$skills_all = $get_member_skills->fetchAll();
		$count = count($skills_all);	
		if($count > 0){?>

			<h2 class="font">Skills</h2>	
			<ul class="list">
				<?php foreach($skills_all as $member_skills){?>	
					<li><a href='http://portfoliolounge.com/browse-portfolios/<?= $member_skills['category_slug']?>'><?= $member_skills['category_name'] ?></a></li>
				<?php } ?>
			</ul>	
		<?php }	
	}
	
	
	
	if($type=='links'){
		
		$get_links = $db->prepare("SELECT * FROM member_links WHERE member_id=:mid ORDER BY link_title");
		$get_links->bindValue(':mid', $memberid);
		$get_links->execute();
		$links_all = $get_links->fetchAll();
		$count = count($links_all);
		if($count > 0){ ?>	

		<h2 class="font">Links</h2>	
		<ul class="list">	
			<?php foreach($links_all as $link){?>	
		
				<li><a target="_blank" href='<?= $link['link_url'] ?>'><?= $link['link_title'] ?></a></li>
		
			<?php } ?>
		</ul>
		<?php } 
	} 
	if($type=='fixed_links'){
	
		$get_links = $db->prepare("SELECT * FROM member_links WHERE member_id=:mid ORDER BY link_title");
		$get_links->bindValue(':mid', $memberid);
		$get_links->execute();
		$links_all = $get_links->fetchAll();
		$count = count($links_all);
		if($count > 0){
			foreach($links_all as $link){
				$links[$link['link_title']]= $link['link_url'];
			}
			return $links;
		} else {
			return false;
		}
		
 	}
}
	
	

/* 	GET Navigation
-------------------------------------------------*/
function getColors($value){ global $memberid, $member, $i;

		require_once 'config.php';
		$get_colors = $db->prepare("SELECT project_title, project_desc, project_website FROM projects 
			WHERE project_slug=:slug AND member_id=:mid LIMIT 1");
		$get_colors->bindValue(':slug', $i);
		$get_colors->bindValue(':mid', $memberid);
		$get_colors->execute();
		$all_colors = $get_colors->fetchAll();
		foreach ($all_colors as $project){

			$project_website = $project['project_website'];

			if (strpos($project_website,'https')) {

			} else {
				$project_website = 'http://'.$project['project_website'];
			}?>
				<h1 class="font"><?=$project['project_title']?></h1>
				<?php if($project['project_desc']){?> <p><?=$project['project_desc']?></p> <?php } ?>
				<?php if($project['project_website']){?> <p><a target="_blank" href="<?=$project_website?>"><?=$project['project_website']?></a></p><?php } ?>

	<?php }	
}?>


