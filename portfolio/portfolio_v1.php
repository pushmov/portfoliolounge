<?php include 'portfolio/head.php';?>
<div class="container">

	<div id="header">
		<?= getLogo();?>
		<ul id="nav" class="font">
			<li <?php if($p=="portfolio"||$p==""){echo'class="cur"';}?>><a href="/">portfolio</a></li>
			<li <?php if($p=="about"){echo'class="cur"';}?>><a href="about">about</a></li>
		</ul>
	</div>



	<?php if(!$p){?>

	<div id="projects">
		<?= getUploads('projects', 'medium');?>
	</div>

	<?php } ?>

	

	<?php if($i){?>

		<?php if($member['layout_option']=='list'){?>

		<div id="items">
			<?= getProjectInfo();?>
			<?= getUploads('items', 'large', true);?>

		</div>

		<?php } else if($member['layout_option']=='slideshow'){?>

		<?= getProjectInfo();?>
		<div class="bullets"></div>
		<div id="banner">
			<div class="inner">
				<?= getUploads('items', 'large', false, 'Fit');?>
			</div>
			<a class="pagination prev"><span></span></a>
			<a class="pagination next"><span></span></a>
		</div>

		<?php } ?>
	<?php } ?>
	
	
	<?php if($p=='about'){?>
	
		<?php /* if($member['profile_img']){?>
		<div class="picture frame">
			<a class="lb" data-source='<?= "http://portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>' href="http://portfoliolounge.com"><div class='canvas' data-source='<?= "http://portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>'></div></a>
		</div>
		<?php } */ ?>
	
		<?php if($member['profile_img']){?>
		<div class="picture">
			<a class="lb" href="http://portfoliolounge.com" data-source='<?= "http://portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>'><img style="width:100%" src='<?= "http://portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>'></a>
		</div>
		<?php } ?>
	
			<div id="about">
				<h1 class="font"><?php echo $member['first_name'].' '.$member['last_name'];?></h1>
				<em>
				<?php if($member['phone']){echo $member['phone'].'<br>';}?>
				<?php if($member['email']){echo $member['email'].'<br>';}?>
				<?php if($member['location']){echo $member['location'].'<br>';}?>
				<?php if($member['location']){echo $member['school'].'<br>';}?> 
				</em>

				<br>
				<?php echo $member['about'];?>

	
				<?=getList('skills');?>
				<?=getList('links');?>

			</div>
		<?php }?>

</div>
<?php include 'portfolio/footer.php';?>