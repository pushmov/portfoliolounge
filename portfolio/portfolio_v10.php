<?php $admin = $_COOKIE['COOKIE_MEMBER_ID'];?>
<!doctype html>
<head>
<base href="<?php echo $base_url;?>">
<meta name="viewport" content="initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="portfolio/css/portfolios.css">
<link rel="stylesheet" type="text/css" href="portfolio/css/portfolio_v<?php echo $template_id;?>.css">
<?php if($template_id>=6){?><link rel="stylesheet" type="text/css" href="portfolio/css/bootstrap.css"><?}?>
<?php if($admin){?><link rel="stylesheet" type="text/css" href="portfolio/css/admin.css"><?}?>
<link href="<?=$protocol?><?php echo $font['url'];?>" rel="stylesheet" type="text/css">
<?php include 'portfolio/css/style.php';?>
<script src="js/libs/jquery-1.8.3.min.js"></script>
<script src="js/libs/jquery.easing.1.3.js"></script>
<script src="js/libs/jquery.cycle.all.min.js"></script>
<script src="portfolio/js/portfolio.js"></script>
<script src="js/libs/analytics.js"></script>
<?php include 'portfolio/js/tracking.php';?>
<?php if($member['first_name']){
    $last_name = $member['last_name'] ? " ".$member['last_name'] : "";
    $site_title = $member['site_title'] ? $member['site_title'] : $member['first_name'].$last_name."'s Portfolio";
} else {
    $site_title = $member['site_title'] ? $member['site_title'] : $member['username']."'s Portfolio";
} ?>
<title><?=$site_title?></title>
</head>
<body>



<?php if($_SESSION['AUTHENTICATED']){?>
    
    <div style="position:fixed;top:0;left:50%;margin-left:-120px;z-index:99999999;">
        <a style="display:block;padding:2px;background:#eee;color:#888;border:1px solid #ddd;box-shadow:4px 8px 10px rgba(0,0,0);border-radius:0 0 3px 3px;font-size:10px;text-align:center;width:240px;" href="https://portfoliolounge.com/admin/projects">&laquo; Back to Editor</a>
    </div>

    
<?php }?>

<?php if($p=='client') {?>
<div id="enter-password">
    <div class="inner">
        <h1>Password Required.</h1>
        <form action="portfolio/services.php">
            <input type="hidden" name="a" value="enter-password">
            <input type="hidden" name="project_id" value="<?=$i?>">
            <input name="password" type="password">
        <br>
        <br>
            <input type="submit" class="btn" value="Enter &raquo;">
        <br>
        <br>
            <a href="javascript:history.back()"><small>&larr; Back</small></a>
        </form>
    </div>
</div>
<div id="password-bg"></div>
<?php } ?>

    
<!-- Test <?=$admin?>-->
<?php if($admin) {?>
        <a style="position:fixed;z-index:99999;right:0;top:0;" href="http://portfoliolounge.com">&laquo; Back to PortfolioLounge</a>    
<?php } ?>
    
<div id="overlay"></div>
<div class="lightbox">
    <div class="image"></div>
    <div class="info font"></div>
</div>  

<div class="strategic">
    <h1>
        Create a portfolio website.
    </h1>
    <h2>
        Build your online portfolio quickly and easily.
    </h2>
</div>


<?php /* Header
--------------------------*/ ?>
<header class="font <?php if($p){echo "tuck";}?>">
    

    <!-- LOGO -->
    <?= getLogo();?>
    <p><?php echo $member['tagline'];?></p>
    
    <!-- LOWER -->
    <div class="content">
    
        
        <ul class="nav">
            <li class="<?php if(!$p){echo 'cur';}?>"><a href="/">Projects</a></li>
            <ul>
                <?= getList('projects');?>
            </ul>
            <li class="section"><a href="about">About</a></li>
            <?= getList('linksNEW');?>
        </ul>
        <?php if($p=='portfolio'){ ?>
        <div class="pagination">
            <ul class="noselect">
                <li><a id="prev">&laquo; Prev</a> <a id="next">Next &raquo;</a></li>
            </ul>
            <div id="bullets"></div>
        </div>
        <?php } ?>
        
        
    </div>
</header>



<?php /* Homepage
--------------------------*/
if(!$p){ ?>
<div id="main">
    <div id="projects">
        <?= getUploads('projects','large');?>
        <div class='clr'></div>
    </div>
    <div class='clr'></div>
</div>
<?php } ?>





<?php /* Slideshow
--------------------------*/ ?>
<?php if($p=='portfolio'){ ?>
<div id="project">    
    <div id="slideshow">
        <div id="cool">
            <?= loopImages(); ?>
        </div>
    </div>
</div>


<script>
$(function() {
    // Cool Slideshow
    $('#cool').cycle({
        speed:500,
        timeout:0,
        prev:    '#prev',
        next:    '#next',
        pager:   '#bullets',
        //easing: 'easeOutExpo',
        //fx: 'scrollVert',
        //random:1
    });
    
    // Cool Slideshow - Prevent auto play when using any controls
    $("#cool, #prev, #next, #pag").click(function(){
        $('#cool').cycle('pause');
    });
    
    // Cool Slideshow - Arrow Keys
    $(document.documentElement).keyup(function (e) {
        if (e.keyCode == 39){        
            $('#cool').cycle('next');
            $('#cool').cycle('pause');
        }
        if (e.keyCode == 37){
            $('#cool').cycle('prev');
            $('#cool').cycle('pause');
        }
    });
});
</script>
<?php } ?>

<?php /* About Page
--------------------------*/
if($p=='about'){?>
<div id="main">
        <div id="about">
            <?php if($member['profile_img']){?>
            <img class="profile lb" data-source="<?php echo $protocol."portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>" src="<?php echo $protocol."portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>" />
            <br />
            <?php } ?>
            <h1 class="font"><?php echo $member['first_name'].' '.$member['last_name'];?></h1>
            <em>
            <?php if($member['location']){echo $member['location'].'<br>';}?>
            <?php if($member['phone']){echo $member['phone'].'<br>';}?>
            <?php if($member['email']){echo $member['email'].'<br>';}?>
            </em>
            <br>
            <?php echo $member['about'];?>
            <?=getList('skills');?>
            <?=getList('links');?>
        </div>
    <div class='clr'></div>
</div>
<?php }?>