<?php include 'portfolio/head.php';?>
<div class="container">

	<?= getLogo('large') ?>
	
	
	<nav class="mobile">
		<li><a class="<?php if(!$p||$p=='portfolio'){echo'cur';}?>" href="/">Projects</a></li>
		<li><a class="<?php if($p=='about'){echo'cur';}?>" href="about">About</a></li>
	</nav>
	
	
	<div class="main">
		
		
		<?php if (!$p) {?>			
			<div id="home">
			<?php getUploads('projects','medium'); ?>
			</div>
		<?php }
		
		
		
		
		if ($p=='portfolio'){?>
        
			<?php getProjectInfo();?>
            
			<div id="project">
				<?php getUploads('items','large', 1); ?>
			</div>
			
			<div class="moreProjects mobile">
				<h2>See more projects...</h2>
				<br />
				<?php getUploads('projects', 'small');?>
			</div>
		<?php }
		
		
		
		
		
		
		
		if ($p=='about'){
			
			if($member['profile_img']){?>
			<div class="picture fra me" style="margin-bottom:50px">
				<img src="<?= "http://portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>" />
			</div>
			<?php } ?>
				
					
			<div class="left-col">		
				<h1 class="font"><?php echo $member["first_name"]." ".$member["last_name"];?></h1>
				
				<hr />
				
				<?php if($member['phone']){?> <span class="phone-number"><?php echo $member["phone"];?></span> &nbsp;&nbsp; <?php } ?>
				<a class="email-address" href="<?php echo $base_url;?>/contact"><?php echo $member["email"];?></a><br />
				
				<hr />	
					
				<?php if($member['about']){?>
					<p><?php echo $member["about"];?></p>
					<hr />
				<?php } ?>
				
				<?php if($member['location']){?><strong>Location</strong> &nbsp; <span><?php echo $member["location"];?></span> <?php } ?> <br />
				<?php if($member['school']){?><strong>School</strong>  &nbsp; <span><?php echo $member["school"];?></span><?php } ?>
				
				<br />
				<br />
				
				<?= getList('skills') ?>
				<?= getList('links') ?>
				
			</div>
				
				
				
			<div class="right-col">
				<h1 class="font">Contact <?php echo $member['first_name'];?></h1>								
				<form class="font" id="contact" method="post">						
					<label>Name:</label><input name="name" /><br />
					<label>Email:</label><input name="email" /><br />
					<label>Textarea:</label><textarea name="message"></textarea>
					<input type="hidden" name="owner_email" value="<?php echo $member['email']?>" />
					<input class="btn" type="submit" value="Send Message">	
					<div class="thank_you">
						<h3>Message Sent!</h3>
					</div>					
				</form>	
			</div>		
		
			
		<?php } ?>
			
			
		<div class="clr"></div>
	</div><!-- main -->
	
	
	<?php /* Sidebar
	-----------------------------------*/ ?>
	<div id="sidebar" class="desktop">
		<?php if($member['tagline']){?>	<p class="tagline"><?php echo $member['tagline'];?></p>	<?php }?>
		<div class='font hide-this-for-mobile'>
			<ul id="nav">
				<?= getList('projects') ?>
				<hr />
				<li class="<?php if($p=='about') echo 'cur';?>"><a href="<?php echo $base_url;?>/about">About</a></li>
			</ul>
		</div>
	</div>

</div><!-- container -->
	
	
<?php include 'portfolio/footer.php';?>