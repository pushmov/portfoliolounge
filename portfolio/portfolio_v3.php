<?php include 'portfolio/head.php';?>
    
<div class="outline">
    <div class="container">
    
        <div id="top">
            <?php if($member['tagline']){?> <p class="tagline font"><?php echo $member['tagline'];?></p><?php }?>
        </div>
        <div id="header">
            <div id="logo">
                <?php echo getLogo();?>
            </div>
            <div id="nav">
                <ul class="font">
                    <li class="<?php if($p=='about'){echo 'cur';}?>"><a href="<?php echo $base_url;?>/about">About</a></li>
                    <li class="<?php if(!$p || $p=='project'){echo 'cur';}?>"><a href="<?php echo $base_url;?>/">Projects</a></li>
                </ul>
            </div>
            <div class="clr"></div>
        </div>
        
        
        <?php /* Homepage Grid
        -----------------------------------*/
        $get_projects = $db->prepare("SELECT * FROM projects WHERE member_id=:mid ORDER BY project_order");
        $get_projects->bindValue(':mid', $memberid);
        $get_projects->execute();
        $project_arr = $get_projects->fetchAll();
        
        if (!$p){?>
        
        <div id="grid_container">
            <div id="grid">
                <?php foreach ($project_arr as $project){   
                
                    
                    // Cover or First Item
                    if($project['cover_id']) {
                        $get_project_cover = $db->prepare("SELECT item_filename FROM items WHERE item_id=:itemid LIMIT 1");
                        $get_project_cover->bindValue(':itemid', $project['cover_id']);
                        $get_project_cover->execute();
                        $project_cover_row = $get_project_cover->fetch();
                    } else {
                        $get_project_cover = $db->prepare("SELECT item_filename FROM items WHERE project_id=:pid ORDER BY item_id LIMIT 1");
                        $get_project_cover->bindValue(':pid', $project['project_id']);
                        $get_project_cover->execute();
                        $project_cover_row = $get_project_cover->fetch();
                    }

                    $cover_path = $protocol."portfoliolounge.com/uploads/".$memberid."/medium/".$project_cover_row['item_filename'];
                    $set_size = ($cover_width <= $cover_height ? $set_size = 'width="'.$required_width.'"' : $set_size = 'height="'.$required_height.'"');
                    $slug = $project['project_slug'] ? $project['project_slug'] : $project['project_id']; ?>
                    
                        <?php 
                        
                        $project_url = $project['privacy_option']=='1' ? 'client/'.$project['project_id']  :  'portfolio/'.$project['project_slug']  ?>
        
        
        
                    <div class="grid_item">
                        <a href="<?=$project_url?>" class="project_cover">
                            <div class="grid_item_cover" id="cover_<?php echo $project['project_id'];?>"></div>
                            <h2 class="font"><?php echo $project["project_title"];?></h2>
                        </a>
                    </div>
                    
                    <script type="text/javascript">                 
                        var gridItemWidth = $('.grid_item_cover').width();  
                        var gridItemHeight = $('.grid_item_cover').height();                    
                        
                        $("<img />").attr("src", "<?php echo $cover_path;?>").load(function() {                     
                            var gridRatio = gridItemWidth / gridItemHeight;
                            var imgRatio = this.width / this.height;
                            
                            if (imgRatio < gridRatio){
                                var imgWidthAttr = gridItemWidth;
                                var imgHeightAttr = this.height * (gridItemWidth / this.width);
                            } else {
                                var imgWidthAttr = this.width * (gridItemHeight / this.height);
                                var imgHeightAttr = gridItemHeight;
                            }
                            
                            var marginTop = imgHeightAttr / 2 * -1 + gridItemHeight / 2;
                            var marginLeft = imgWidthAttr / 2 * -1 + gridItemWidth / 2;
                            
                            $('#cover_<?php echo $project['project_id'];?>').html('<img width="'+imgWidthAttr+'" height="'+imgHeightAttr+'" style="margin:'+marginTop+'px 0 0 '+marginLeft+'px;" src="<?php echo $cover_path;?>" />').children('img').fadeIn();
                        });
                    </script>               
                <?php } ?>
                <div class="clr"></div>
            </div>
        </div>
            
        <?php } ?>
        
        
        
        
        <?php /* Project Page
        -----------------------------------*/
        if($p=='portfolio'){
            $get_projects = $db->prepare("SELECT * FROM projects WHERE project_slug=:slug AND member_id=:id LIMIT 1");
            $get_projects->bindValue(':slug', $i);
            $get_projects->bindValue(':id', $memberid);
            $get_projects->execute();
            $project = $get_projects->fetch();
        ?>
            
            
            <div id="project">
                <?= getProjectInfo();?>
                <?= getUploads('items','large', true);?>
            </div>
            
            
        <?php } ?>
        
        
        <?php /* About Me
            -----------------------------------*/
        if ($p=='about' || $p=='contact'){?>
            <div id="about">
            
            
                <?php if($member['profile_img']){?>
                    <img class="lb" id="profile_img" width="100%;" data-source='<?= "http://portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>' src="<?php echo $protocol."portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'];?>" />
                <?php } ?>
                
                
                <div class="about_left">
                    
                    <div class="col">
                        <div class="inner">
                            <h1 class="font name"><?php echo $member["first_name"]." ".$member["last_name"];?></h1>
                            <p class="info">
                                <a class="email-address" href="<?php echo $base_url;?>/contact"><?php echo $member["email"];?></a><br />
                                <span class="phone-number"><?php echo $member["phone"];?></span><br />
                                <?php echo $member["location"];?>
                            </p>
                            
                            <h2 class="font">About</h2>
                            <p><?php echo $member["about"];?></p>
                        </div>
                    </div>
                    
                    <div class="col">
                        <div class="inner">
                            <?php if($member['school']){?>
                            <h2 class="font">School</h2>
                            <p><?php echo $member["school"];?></p>
                            <?php } ?>
                                    
                            <?=getList('skills');?>
                            <?= getList('links') ?>
                        </div>
                    </div>
                            
                
                </div>
                
                <div class="about_right">
                    <h2 class="font">Contact <?php echo $member['first_name'];?></h2>
                    <form class="font" id="contact" method="post">                      
                        <label>Name:</label><input name="name" /><br />
                        <label>Email:</label><input name="email" /><br />
                        <label><span style="white-space: nowrap;">Text Area:</span></label><textarea name="message"></textarea>
                        <input type="hidden" name="owner_email" value="<?php echo $member['email']?>" />
                        <input class="btn" type="submit" value="Send Message">  
                        <div class="thank_you">
                            <h3>Message Sent!</h3>
                        </div>                  
                    </form>     
                </div>
                <div class="clr"></div>
            </div>
        <?php } ?>
        
        
        <?php /* Footer
        -----------------------------------*/ ?>
    
    </div><!-- container -->
</div><!-- outline -->

<?php include 'portfolio/footer.php';?>