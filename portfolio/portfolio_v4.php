<?php include 'portfolio/head.php';?>
	
	
<div class="container">
	
	<div class="mobile center">
		<?= getLogo();?>
		<nav class="mobile font">
			<a class="<?php if($p=='portfolio'||!$p){echo 'cur';}?>" href="/">Projects</a>
			<a class="<?php if($p=='about'){echo 'cur';}?>" href="about">About</a>
		</nav>
	</div>
	
	<nav id="sidebar" class="desktop">
		<?= getLogo();?>
		<ul id="nav">
			<?= getList('projects');?>
		</ul>
		<ul>
			<li><a href='about'>About</a></li>
		</ul>
	</nav>
	
	
	<div id="main">
		
		<?php /* if($member['banner']==0){?>


			<?php if(!$p){ ?>		
			<div id="banner">
				<div class="inner">
					<?= getUploads('projects', 'large');?>
				</div>
				<a class="pagination prev"><span></span></a>
				<a class="pagination next"><span></span></a>
			</div>		
			<?php } ?>

	
		<?php } */ ?>
		
		

		<?php if(!$p){ ?>
		<br><br>
		<div id="projects">
			<?= getUploads('projects','large');?>
			<div class='clr'></div>
		</div>
		<?php } ?>


		

		<?php if($p=='portfolio'){ ?>
		<div id="items">
			<div class="projectInfo">
				<?= getProjectInfo();?>
			</div>
			<?= getUploads('items','large');?>
		</div>
		<?php } ?>
		
		
		
		<?php if($p=='about'){?>
	
			<div id="about">
				<?
				if($member['profile_img']){?>
				<img class="lb" data-source="<?php echo $protocol."portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>" style="max-width:100%;margin-bottom:14px;" src="<?php echo $protocol."portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>" />
				<br />
				<?php } ?>
				<h1 class="font"><?php echo $member['first_name'].' '.$member['last_name'];?></h1>
				<em>
				<?php if($member['email']){echo $member['email'].'<br>';}?>
				<?php if($member['phone']){echo $member['phone'].'<br>';}?>
				<?php if($member['location']){echo $member['location'].'<br>';}?>
				</em>

				<br>
				<?php echo $member['about'];?>


				<?=getList('skills');?>

				<?=getList('links');?>

			</div>
		<?php }?>
		
		<div class='clr'></div>
	</div>
	
</div>
<?php include 'portfolio/footer.php';?>