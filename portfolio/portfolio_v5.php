<?php include 'portfolio/head.php';?>
<div class="container">
	
	<!--
		<header class="center mob ile"><?php echo getLogo();?></header>
	-->
	<div id="sidebar">
		<header class="left"><?php echo getLogo();?></header>
		<p class="font right tagline"><?php echo $member['tagline'];?></p>
		<nav>
			<?php echo getList('projects');?>
		</nav>
		
        <?php // Hide about page for project pages when in mobile ?>
        <div <?php if($p){?>class="desktop"<?php }?>>
            
            <?
            if($member['profile_img']){?>
            <div class="frame">
                <div class='canvas lb' data-source='<?= "http://portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>'></div>
            </div>
            <?php } ?>
            <div class="about">
                <h2 style="margin:0;" class="font"><?php echo $member['first_name'].' '.$member['last_name'];?> <a style="background: #eee;
        display: inline-block;
        padding: 7px 8px 9px 8px;
        border-radius: 80px;
        float: left;
        margin: 9px 14px 0 -4px;
        <?php if(!$member['location']){?>margin-top:-2px;<?php } ?>" href="mailto:<?=$member['email']?>"><img width="15" src="images/icon-envelope.png" /></a></h2>
                <p style="margin:0 0 30px;">
                <?php if($member['phone']){echo $member['phone'].'<br>';}?>
                <?php if($member['location']){echo $member['location'];}?>
                </p>
                
                <br>
                <?php echo $member['about'];?>
                
                    
                <?=getList('skills');?>
                <?=getList('links');?>
			</div>
        </div>
	</div>
	
	
	<?php if(!$p) {?>
	
	
	<div id="main">		
		<div class="homepage">
			<?php echo getUploads('projects', 'large');?>
		</div>
		
	</div>
	
	<?php } ?>
	
	<?php if($p=='portfolio') {?>
	
	
	<div id="main">
		<div class="project-info">
	    	<?php echo getProjectInfo();?>
		</div>
		<?php echo getUploads('items', 'large', true);?>
		
	</div>
	
	<?php } ?>
	
	
	<!--
	<div class="inner mobile">
		
		
		<hr />
		<h1 class="font"><?php echo $member['first_name'].' '.$member['last_name'];?></h1>
		<?
		if($member['profile_img']){?>
		<div class="frame">
			<div class='canvas lb' data-source='<?= "http://portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>'></div>
		</div>
		<?php } ?>
		<em>
		<?php if($member['email']){echo $member['email'].'<br>';}?>
		<?php if($member['phone']){echo $member['phone'].'<br>';}?>
		<?php if($member['location']){echo $member['location'].'<br>';}?>
		</em>
		
		<br>
		<?php echo $member['about'];?>
		
			
		<?=getList('skills');?>
		<?=getList('links');?>	
	</div>
	-->
	

</div>

<?php include 'portfolio/footer.php';?>