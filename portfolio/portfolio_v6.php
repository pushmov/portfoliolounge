<?php include 'portfolio/head.php';?>

<?= getLogo();?>

<script src="portfolio/js/libs/packery.pkgd.min.js"></script>

	<?php 
	$get_project = $db->prepare("SELECT * FROM projects WHERE member_id=:id ORDER BY project_order");
	$get_project->bindValue(':id', $memberid);
	$get_project->execute();
	$projects_all = $get_project->fetchAll();
	foreach($projects_all as $project){?>
		
		<h1 class="project_title"><?=$project['project_title'];?></h1>

		<div class="brickContainer">

			<?php 
			$get_item = $db->prepare("SELECT * FROM items WHERE project_id=:pid ORDER BY item_order");
			$get_item->bindValue(':pid', $project['project_id']);
			$get_item->execute();
			$items_all = $get_item->fetchAll();
			foreach($items_all as $item){
				$upload_path = $protocol."portfoliolounge.com/uploads/".$memberid."/medium/".$item['item_filename'];
				$image    = getimagesize($upload_path);
				$width    = $image[0];
				$height   = $image[1];
				$ratio = $width/$height; ?>

			<div style="height:<?=$height/1.5?>px" class="brick <?php if($ratio>=2){echo 'w2';}?> lb frame" data-source="<?=$upload_path?>" data-ratio="<?=$ratio?>">
				<div class="canvas" data-source="<?=$upload_path?>">

					<?php if($item['item_title']){?>
					<div class="info">
						<?php if($item['item_title']){?><h2 class="font"><?= $item['item_title'] ?></h2><?php } ?>
						<?php if($item['item_desc']){?><p><?= $item['item_desc'] ?></p><?php } ?>
					</div>
					<?php } ?>
				</div>
			</div>


			<?php } ?>
			
		</div>

	<?php } ?>


<script>
	
	var roundBy = 25;
	
	$(".brick").each(function(){
		var ratio = $(this).attr('data-ratio');
		var thisWidth = $(this).width();
		var thisHeight = thisWidth/ratio;
        thisHeight =  Math.ceil(thisHeight/roundBy) * roundBy;
		
		$(this).height(thisHeight);
		
	})
	
	var $container = $('.brickContainer');
	// init
	$container.packery({
	  itemSelector: '.brick',
	  gutter: 0
	});
	
	
</script>



<h1 class="project_title">About</h1>
<div id="about" class="container">
	<h1 class="font"><?php echo $member['first_name'].' '.$member['last_name'];?></h1>
	<?
if($member['profile_img']){?>
	<div>
		<img class="lb" data-source="<?php echo $protocol."portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>" style="max-width:80%" src="<?php echo $protocol."portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>" />
	</div>
	<?php } ?>
	<em>
		<?php if($member['email']){echo $member['email'].'<br>';}?>
		<?php if($member['phone']){echo $member['phone'].'<br>';}?>
		<?php if($member['location']){echo $member['location'].'<br>';}?>
	</em>

	<br>
	<?php echo $member['about'];?>

	
	<?=getList('skills');?>

	<?=getList('links');?>

</div>

<?php include 'portfolio/footer.php';?>