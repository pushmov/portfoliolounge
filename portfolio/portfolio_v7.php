<?php include 'portfolio/head.php'; ?>
<script type="text/javascript" src="portfolio/js/portfolio_v7.js"></script>

<div id="mobileHeader">
	<a class="next right"><span></span></a>
	<a class="prev left"><span></span></a>
	<?= getLogo();?>
	<div class="clr">
		
	</div>
</div>


<div id="nav">
	<style>
		#logo h1 {
			color:<?=$color['body_text']?>;
		}
	</style>
	<?= getLogo();?>
	<ul>
	

		<?php 
		$i = 0;
		$get_project = $db->prepare("SELECT * FROM projects WHERE member_id=:id ORDER BY project_order");
		$get_project->bindValue(':id', $memberid);
		$get_project->execute();
		$projects_all = $get_project->fetchAll();
		foreach($projects_all as $project){?>

		<li><a class="<?php if($i==0){echo'active';}?>" onclick="transition(<?php $i++;echo $i; ?>)" href="#<?=$project['project_slug']?>"><?=$project['project_title']?></a></li>
		<?php } ?>
		<br />
		<li><a class="openAbout" href="/"><strong>About</strong></a></li>

		</ul>


	
	</ul>
</div>


<div id="content">
	<div id="about">
		<a style="margin:10px" class="close right"><img width="10" src="portfolio/images/close.png" /></a>
		<?php if($member['profile_img']){?>
			<img class="lb" data-source="<?php echo $protocol."portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>" style="margin-bottom:14px" src="<?php echo $protocol."portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>" />
			
		<?php } ?>
		
		<h1 class="font"><?php echo $member['first_name'].' '.$member['last_name'];?></h1>
		
		<em>
		<?php if($member['email']){echo $member['email'].'<br>';}?>
		<?php if($member['phone']){echo $member['phone'].'<br>';}?>
		<?php if($member['location']){echo $member['location'].'<br>';}?>
		</em>
		
		<br>
		<p style="font-size:11px"><?php echo $member['about'];?></p>
		
			
		<?=getList('skills');?>
		<?=getList('links');?>
		
		<form class="font" id="contact" method="post">	
			<label>Name:</label><input name="name" /><br />
			<label>Email:</label><input name="email" /><br />
			<label>Textarea:</label><textarea name="message" style="height:80px;"></textarea>
			<input type="hidden" name="owner_email" value="<?php echo $member['email']?>" />
			<input class="btn" type="submit" value="Send Message">	
			<div class="thank_you">
				<h3>Message Sent!</h3>
			</div>				
		</form>	
		<div class="clr"></div>
		<br />		<br />		<br />
		<hr />
	</div>
	
	<?php if($p!='portfolio'){?>
	<div id="slider">
		 
		<?php 
		
		$get_project = $db->prepare("SELECT * FROM projects WHERE member_id=:id ORDER BY project_order");
		$get_project->bindValue(':id', $memberid);
		$get_project->execute();
		$projects_all = $get_project->fetchAll();
		foreach($projects_all as $project){
			
			$get_cover = $db->prepare("SELECT item_filename FROM items WHERE item_id=:itemid LIMIT 1");
			$get_cover->bindValue(':itemid', $project['cover_id']);
			$get_cover->execute();
			$cover = $get_cover->fetch();
			$cover_filename = $cover['item_filename'];

			if(!$cover_filename){
				$get_cover = $db->prepare("SELECT item_filename FROM items WHERE project_id=:pid LIMIT 1");
				$get_cover->bindValue(':pid', $project['project_id']);
				$get_cover->execute();
				$cover = $get_cover->fetch();
				$cover_filename = $cover['item_filename'];
			}
			$slide_item_path = 'http://portfoliolounge.com/uploads/'.$memberid.'/medium/'.$cover_filename;?>
		
		<div class="item">
			<div class="inner">
				
				<?php if(!$project['privacy_option']){?>

					<div class="info font">
					<h2><?=$project['project_title']?></h2>
					</div>
                    <!--
					<a class="banner frame lb" data-source="<?=$slide_item_path?>">
						<div class="canvasFit" data-source="<?=$slide_item_path?>"></div>
					</a>
                    -->
					<?php
					$project_website = $project['project_website'];
	
					if (strpos($project_website,'https')) {
	
					} else {
						$project_website = 'http://'.$project['project_website'];
					}?>
					<p class="lang"><?=$project['project_desc']?></p>
					<?php if($project['project_website']){?> <p><a target="_blank" href="<?=$project_website?>"><?=$project['project_website']?></a></p><?php } ?>
					<div class="clr"></div>
	
	
					<div class="grid">
						<?=getItems($project['project_id'])?>
						<div class="clr"></div>
					</div>
				
				<?php } else { ?>
				
					<div class="info font">
					<h2><?=$project['project_title']?></h2>
					</div>
					<a class="banner frame lb" data-source="<?=$slide_item_path?>">
						<div class="canvas" data-source="<?=$slide_item_path?>"></div>
					</a>
					
					<form action="portfolio/services.php">
					<h1>Password Required.</h1>
						<input type="hidden" name="a" value="enter-password">
						<input type="hidden" name="project_id" value="<?=$project['project_id']?>">
						<input name="password" type="password">
						<input type="submit" class="btn" value="Enter &raquo;">
					<br>
					</form>
					
				
				<?php } ?>
			
			</div>
		</div>
		
		
		<?php }?>
	</div><!-- slider -->
	<?php } else { ?>
		<?php echo getUploads('items', 'large', true);?>
	<?php } ?>
</div>
<?php include 'portfolio/footer.php';?>