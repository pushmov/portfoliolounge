<?php
$links= getList('fixed_links');
?>
<!doctype html>
<html lang="en-gb">
<head>
    <meta charset="utf-8">
	<base href="<?php echo $base_url;?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--[if lt IE 9]> 
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <meta name="description" content="">
    <!--[if lt IE 9]>
        <script src="<?=$protocol?>html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="<?=$protocol?>portfoliolounge.com/portfolio/css/bootstrap.min.css" />
    <!-- Owl Carousel Assets -->
    <link href="<?=$protocol?>portfoliolounge.com/portfolio/css/owl.carousel.css" rel="stylesheet">
	<link href="<?=$protocol?>portfoliolounge.com/portfolio/css/owl.theme.css" rel="stylesheet">
	<!-- Custom styles -->
    <link rel="stylesheet" href="<?=$protocol?>portfoliolounge.com/portfolio/css/portfolio_v8.css" />
	<?php if($admin){?><link rel="stylesheet" type="text/css" href="<?=$protocol?>portfoliolounge.com/portfolio/css/admin.css"><?php }?>
	
	<link href='<?=$protocol?><?php echo $font['url'];?>' rel='stylesheet' type='text/css'>
	
	<!-- Dynamic Styles for color schemas -->
	<style>
		h1, h2, h3, .nav a {
			font-family:<?=$font['name'];?> ;
		}
		body {
			background:<?=$color['background'];?>;
			background: url(../images/bg.jpg) fixed;
		}
		h1{
			color: <?=$color['main_text']; ?>;
			color: #444;
		}
		.bg-parlex h2 {
			color:<?=$color['body_text']; ?>;
			color:#006f82;
			background-color:<?=$color['transparency_hard'];?> ;
			background-color:rgba(255,255,255,0.8);
		}
		.bg-parlex h3 {
			color:<?=$color['main_text']; ?>;
			color: #444;
			background-color:<?=$color['transparency_hard'];?> ;
			background-color:rgba(255,255,255,0.8);
		}
		
		.work-description h1 {
			color:<?=$color['body_text']?>;
			color:#006f82;
		}
		.work-description p {
			color:<?=$color['body_text']?>;
			color:#006f82;
		}
		
		.itemOverlay {
			background-color:<?=$color['borders'];?>;
			background-color:rgba(0,111,130,0.8);
		}
		
		.synced .item .activeBorder {
			border-color:<?=$color['body_text']; ?>;
			border-color:#006f82;
		}
		
	</style>
	
	<script src="<?=$protocol?>portfoliolounge.com/js/libs/analytics.js"></script>
	<?php // include 'portfolio/js/tracking.php';?>
	
	<?php 
	if($member['first_name']){
		$last_name = $member['last_name'] ? " ".$member['last_name'] : "";
		$site_title = $member['site_title'] ? $member['site_title'] : $member['first_name'].$last_name."'s Portfolio";
	} else {
		$site_title = $member['site_title'] ? $member['site_title'] : $member['username']."'s Portfolio";
	}?>
	<title><?=$site_title?></title>
</head>

<body>
	<div id="top"></div>
    <header class="header">
        <div class="inner">
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="navbar-header">
                    <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-brand scroll-top logo">
						<?= getLogo();?>
					</div>
                </div>
                <!--/.navbar-header-->
                <div id="main-nav" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav" id="mainNav">
                        <li><a href="#footer" class="scroll-link">About</a></li>
                        <li><a href="#work-0" class="scroll-link">Portfolio</a></li>
                        <li><a href="#footer" class="scroll-link">Contact</a></li>
        
							<?php
								if($links){?>
						<div id="social-buttons">
							<?php
								if($links){
									//print_r($links);
									if(!empty($links['linkedin'])){
										echo '<div id="social-linkedin" class="header-button"><a target="_blank" href="'.$links['linkedin'].'"></a></div>';
									}
									if(!empty($links['facebook'])){
										echo '<div id="social-facebook" class="header-button""><a target="_blank" href="'.$links['facebook'].'"></a></div>';
									}
									if(!empty($links['twitter'])){
										echo '<div id="social-twitter" class="header-button"><a target="_blank" href="'.$links['twitter'].'"></a></div>';
									}
								}
							?>
						</div>
						<?php } ?>
                    </ul>
                </div>
                <!--/.navbar-collapse-->
            </nav>
            <!--/.navbar-->
        </div>
        <!--/.container-->
    </header>
    <!--/.header-->
	
	<?php
	//GET ALL PROJECTS
	$project_number= 0;
	$get_projects = $db->prepare("SELECT * FROM projects WHERE member_id=:id ORDER BY project_order");
	$get_projects->bindValue(':id', $memberid);
	$get_projects->execute();
	$projects_all = $get_projects->fetchAll();
	foreach($projects_all as $project){
		
		//GET THE COVER IMAGE
		//if cover is not set, take the first item.
		$first_item=true;
		$project_id= $project['project_id'];
		$get_cover = $db->prepare("SELECT items.* FROM items LEFT JOIN projects ON projects.cover_id=items.item_id 
			WHERE items.project_id = :pid 
			ORDER BY projects.project_id DESC, items.item_order ASC LIMIT 0,1");
		$get_cover->bindValue(':pid', $project_id);
		$get_cover->execute();
		$cover_image = $get_cover->fetch();
		$cover_image= $protocol.'portfoliolounge.com/uploads/'.$memberid.'/'.$cover_image['item_filename'];
		?>
		
		
		<!-- Fullscreen Cover -->
		<section id="cover-<?=$project_number?>" class="bg-parlex" style="background-image: url('<?php echo $cover_image; ?>');">
			<div class="parlex-back">
				<div class="container secPad text-center">
					<h2><?php echo $project['project_title']; ?></h2>
					<?php if($project['project_desc']){ ?><h3><?php echo $project['project_desc']; ?></h3><?php } ?>
				</div>
			</div>
			<a href="#work-<?=$project_number?>" class="scroll-link scroll-down"></a>
		</section>

		<!-- Item Thumbnails -->
		<section class="work-detail" id="work-<?=$project_number?>">
			<div class="slider-container">
				<div id="slider-<?=$project_number?>" class="owl-carousel owl-carrouselCustom">
					<?php
					$db->query("SET SQL_BIG_SELECTS=1");
					$get_items = $db->query("SELECT items.* FROM items LEFT JOIN projects 
						ON projects.cover_id=items.item_id WHERE items.project_id = $project_id 
						ORDER BY items.item_order ASC");
					$items_all = $get_items->fetchAll();
					foreach($items_all as $item){?>
						<div class="item frame">
							<div class="canvas" data-source="<?=$protocol?>portfoliolounge.com/uploads/<?=$memberid?>/large/<?=$item['item_filename']?>"></div>
							<!--<img src="<?=$protocol?>portfoliolounge.com/uploads/'.$memberid.'/small/'.$item['item_filename'].'">-->
							<?php if($item['item_title']||$item['item_desc']){?>
							<div class="itemOverlay">
								<h3><?=$item['item_title']?></h3>
								<p><?=$item['item_desc']?></p>
							</div>
							<?php } ?>
							<div class="activeBorder"></div>
						</div>
					<?php }?>
				</div>
			</div>
		
			<!-- Item Slides -->
			<div id="detail-<?=$project_number?>" class="owl-carousel big-slider">
				<?php 
				$thumbs= '';
				$get_items = $db->prepare("SELECT items.* FROM items LEFT JOIN projects 
					ON projects.cover_id=items.item_id WHERE items.project_id = :pid 
					ORDER BY items.item_order ASC");
				$get_items->bindValue(':pid', $project_id);
				$get_items->execute();
				$items2_all = $get_items->fetchAll();
				foreach($items2_all as $item){?>
				
					<div class="item work-container">
						<div class="col-md-8 col-sm-8 main-image-container">
							<img src="<?=$protocol?>portfoliolounge.com/uploads/<?=$memberid?>/large/<?=$item['item_filename']?>" alt="<?=$item['item_title']?>">
						</div>
						<div class="col-md-4 col-sm-4 work-description">
							<h1><?=$item['item_title']?></h1>
							<hr>
							<p><?=$item['item_desc']?></p>
						</div>
					</div>
					
					<?php
					$thumbs .= '<div class="item frame">
									<div class="canvas" data-source="'.$protocol.'portfoliolounge.com/uploads/'.$memberid.'/large/'.$item['item_filename'].'"></div>
									<div class="itemOverlay">
										<h3>'.$item['item_title'].'</h3>
										<p>'.$item['item_desc'].'</p>
									</div>
									<div class="activeBorder"></div>
								</div>';
				} ?>
			</div>
		</section>
		
		
			
		<?php $project_number++; ?>
	<?php } ?>		
	
	
	<footer id="footer">
		<a href="#top" class="scrollToTop"></a>
		<div class="container">
		
			<div class="col-md-4 col-sm-6">
				<h2>About me</h2>
				<?
				if($member['profile_img']){?>
					<img class="lb artist-picture" data-source="<?=$protocol."portfoliolounge.com/uploads/".$memberid."/".$member["profile_img"] ?>" src="<?= $protocol.'portfoliolounge.com/uploads/'.$memberid.'/'.$member['profile_img'] ?>" alt="'.$member['first_name'].' '.$member['last_name'].'"  />
				<?php }
				?>
				<?php echo '<p>'.$member['about'].'</p>';?>
			</div>

			<div class="col-md-4 col-sm-6">
				<?php
				if($member['tagline']){
					echo '<h2>About my Work</h2>';
					echo '<p>'.$member['tagline'].'</p>';
				}
				?>					
			</div>
			
			<div class="col-md-4 col-sm-12 contact">
				<h2>Contact Me</h2>
				<form action="#footer" method="post" id="contactForm">
					<input type="text" name="name" placeholder="Name" required />
					<input type="email" name="email" placeholder="Email" required />
					<textarea name="message" placeholder="Message" rows="7" required></textarea>
					<input type="hidden" name="owner_email" value="<?php echo $member['email']?>" />
					<input type="submit" id="formSubmit" value="" />
					<span id="thank_you">Message Sent!</span>
				</form>
				<div style="clear:both"></div>
			</div>
			
		</div>
		<div style="clear:both"></div>
		<div class="container">
		
			<div class="col-md-4 col-sm-6">
				<div class="bottom-content">
					<?php if($member['phone']){ echo '<p><a class="contact-data">Phone '.$member['phone'].'</a></p>'; }?>
					<?php if($member['email']){ echo '<p><a href="mailto:'.$member['email'].'" class="contact-data">'.$member['email'].'</a></p>'; }?>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-6">
				<div class="bottom-content">
					<?php
						if($links){
							//print_r($links);
							if(!empty($links['linkedin'])){
								echo '<div class="social-icons" id="linked-in"><a href="'.$links['linkedin'].'"></a></div>';
							}
							if(!empty($links['facebook'])){
								echo '<div class="social-icons" id="facebook"><a href="'.$links['facebook'].'"></a></div>';
							}
							if(!empty($links['twitter'])){
								echo '<div class="social-icons" id="twitter"><a href="'.$links['twitter'].'"></a></div>';
							}
							if(!empty($links['dribbble'])){
								echo '<div class="social-icons" id="dribbble"><a href="'.$links['dribbble'].'"></a></div>';
							}
							if(!empty($links['vimeo'])){
								echo '<div class="social-icons" id="vimeo"><a href="'.$links['vimeo'].'"></a></div>';
							}
							if(!empty($links['flickr'])){
								echo '<div class="social-icons" id="flickr"><a href="'.$links['flickr'].'"></a></div>';
							}
							if(!empty($links['instagram'])){
								echo '<div class="social-icons" id="instagram"><a href="'.$links['instagram'].'"></a></div>';
							}
						}
					?>
				</div>
			</div>
			<div class="col-md-4 col-sm-0"></div>
			
			<img src="<?=$protocol?>portfoliolounge.com/portfolio/images/bottom-line.png" id="footer-bottom-line" />
		</div>
	</footer>

    <!--[if lte IE 8]><script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script><![endif]-->
<link href='<?=$protocol?><?php echo $font['url'];?>' rel='stylesheet' type='text/css'>
    <script src="<?=$protocol?>portfoliolounge.com/portfolio/js/libs/jquery-1.8.3.min.js"></script>
    <script src="<?=$protocol?>portfoliolounge.com/portfolio/js/libs/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?=$protocol?>portfoliolounge.com/portfolio/js/libs/jquery.cslider.js" type="text/javascript"></script>
	<script src="<?=$protocol?>portfoliolounge.com/portfolio/js/libs/owl-carousel/owl.carousel.js"></script>
    <script src="<?=$protocol?>portfoliolounge.com/portfolio/js/portfolio_v8.js" type="text/javascript"></script>
	<script src="<?=$protocol?>portfoliolounge.com/portfolio/js/portfolio.js"></script>
</body>
</html>