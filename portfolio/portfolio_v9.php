<?php $admin = $_COOKIE['COOKIE_MEMBER_ID'];?>
<!doctype html>
<head>
<base href="<?php echo $base_url;?>">
<meta name="viewport" content="initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="portfolio/css/portfolios.css">
<link rel="stylesheet" type="text/css" href="portfolio/css/portfolio_v9.css">
<?php if($template_id>=6){?><link rel="stylesheet" type="text/css" href="portfolio/css/bootstrap.css"><?}?>
<?php if($admin){?><link rel="stylesheet" type="text/css" href="portfolio/css/admin.css"><?}?>
<link href="<?=$protocol?><?php echo $font['url'];?>" rel="stylesheet" type="text/css">
<?php include 'portfolio/css/style.php';?>
<script src="js/libs/jquery-1.8.3.min.js"></script>
<script src="js/libs/jquery.easing.1.3.js"></script>
<script src="js/libs/jquery.cycle.all.min.js"></script>
<script src="portfolio/js/portfolio.js"></script>
<script src="portfolio/js/portfolio_v9.js"></script>
<script src="js/libs/analytics.js"></script>
<?php include 'portfolio/js/tracking.php';?>
<?php if($member['first_name']){
    $last_name = $member['last_name'] ? " ".$member['last_name'] : "";
    $site_title = $member['site_title'] ? $member['site_title'] : $member['first_name'].$last_name."'s Portfolio";
} else {
    $site_title = $member['site_title'] ? $member['site_title'] : $member['username']."'s Portfolio";
} ?>
<title><?=$site_title?></title>
</head>
<body>


<?php if($_SESSION['AUTHENTICATED']){?>
    

    <?php if($p=='portfolio'){
        $get_project = $db->prepare("SELECT project_id FROM projects WHERE project_slug = :slug AND member_id = :id LIMIT 1");
        $get_project->bindValue(':slug', $i);
        $get_project->bindValue(':id', $member['member_id']);
        $get_project->execute();
        $project = $get_project->fetch();
        $project_path = 'project/'.$project['project_id'];
                
    } else {
        $project_path = 'projects'; 
    }?>
    
    <div id="toolbar">
        <a href="https://portfoliolounge.com/admin/<?=$project_path?>"><img src="images/icon-pencil-white.png"></a>
    </div>

    
<?php }?>
<?php if($p=='client') {?>
<div id="enter-password">
    <div class="inner">
        <h1>Password Required.</h1>
        <form action="portfolio/services.php">
            <input type="hidden" name="a" value="enter-password">
            <input type="hidden" name="project_id" value="<?=$i?>">
            <input name="password" type="password">
        <br>
        <br>
            <input type="submit" class="btn" value="Enter &raquo;">
        <br>
        <br>
            <a href="javascript:history.back()"><small>&larr; Back</small></a>
        </form>
    </div>
</div>
<div id="password-bg"></div>
<?php } ?>

    
    
<div id="overlay"></div>
<div class="lightbox">
    <div class="image"></div>
    <div class="info font"></div>
</div>  



<!-- Header -->
<header>
    <div class="inner">
        <!-- LOGO -->
        <?= getLogo();?>
        <p><?php echo $member['tagline'];?></p>
        
        <!-- Nav -->
        <ul class="nav">
            <!--<li class="section <?php if(!$p){echo 'cur';}?>"><a href="/">Projects</a></li>-->
            <ul>
                <?= getList('projects');?>
            </ul>
            <li class="section"><a href="about">About</a></li>
            
            <?php if($p=='portfolio'){?>
                <div id="project_info">
                    <?php echo getProjectInfo();?>
                </div>
            <?php } ?>
        </ul>
        
        <?php if($p=='portfolio'){ ?>
        <!-- Slideshow Pagination -->
        <div class="pagination">
            <ul class="noselect">
                <!--<li><a id="prev">&laquo; Prev</a> <a id="next">Next &raquo;</a></li>-->
                <li><a id="prev">Previous</a> / <a id="next">Next</a></li>
            </ul>
            <div id="bullets"></div>
            <div class="clr"></div>
        </div>
        <?php } ?>
    </div>
</header>





<?php /* Homepage
--------------------------*/
if(!$p){ ?>
<div id="main">
    <div id="projects">
        <?= getUploads('projects','large');?>
        <div class='clr'></div>
    </div>
    <div class='clr'></div>
</div>
<?php } ?>





<?php /* Project Page (slideshow and text)
------------------------------------------*/ ?>
<?php if($p=='portfolio'){ ?>
<div id="project">    
    <div id="slideshow">
        <div id="cool">
            <?= loopImages(); ?>
        </div>
    </div>
</div>


<?php } ?>




<?php /* About Page
--------------------------*/
if($p=='about'){?>
<div id="main">
        <div id="about">
        
            <?php if($member['profile_img']){?>
            <img class="profile lb" data-source="<?php echo $protocol."portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>" src="<?php echo $protocol."portfoliolounge.com/uploads/".$memberid."/".$member['profile_img'] ?>" />
            <br />
            <?php } ?>
            
            <div id="aboutText">
            
                <h1 class="font"><?php echo $member['first_name'].' '.$member['last_name'];?></h1>
                <em>
                <?php if($member['location']){echo $member['location'].'<br>';}?>
                <?php if($member['phone']){echo $member['phone'].'<br>';}?>
                <?php if($member['email']){echo $member['email'].'<br>';}?>
                <?php if($member['school']){echo $member['school'].'<br>';}?>
                </em>
                <br>
                <?php echo $member['about'];?><br>
                <?=getList('skills');?>
                <?php if($memberid != 16226){
                
                    getList('linksNEW');
                    //getList('links');
                
                 } else { ?>
                <br>
                <a target="_blank" href="https://instagram.com/mattvisk" class="svg instagram"></a>
                <a target="_blank" href="https://www.linkedin.com/pub/matt-visk/11/b5b/5ab" class="svg linkedin"></a>
                <a target="_blank" title="Resume" href="https://docs.google.com/document/d/1i4UCoLnnSBBSYW-y8XgMOH_39gaQkd9tZWMkkDNfjjI" class="svg resume"></a>
                <?php } ?>
                
            </div>
            <div class='clr'></div>

        </div>
    <div class='clr'></div>
</div>
<?php }?>