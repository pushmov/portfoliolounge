<?php
/*

Use a members ID to redirect to their portfolio.
Use these 'permalinks' on facebook and other promotional sites.
www.portfoliolounge.com/12345

*/

$result = $db->prepare("SELECT username FROM members WHERE member_id = :p LIMIT 1");
$result->bindValue(':p', $p);
$result->execute();
$member = $result->fetch();
$member = $member['username'];
if ($member){
	header ("location: http://$member.portfoliolounge.com");
} else {
	header ("location: http://www.portfoliolounge.com");	
}
?>