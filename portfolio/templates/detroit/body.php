
<?php //-------------------------------------------------- Navigation -- ?>
<ul id="nav">
	<?= getLogo();?>
	
	<?php 
	$i = 0;
	$get_project = $db->prepare("SELECT * FROM projects WHERE member_id=:id ORDER BY project_order");
	$get_project->bindValue(':id', $memberid);
	$get_project->execute();
	$all_projects = $get_project->fetchAll();
	foreach($all_projects as $project){?>
	
	<li><a class="<?php if($i==0){echo'active';}?>" onclick="transition(<?php $i++;echo $i; ?>)" href="#<?=$project['project_slug']?>"><?=$project['project_title']?></a></li>
	<?php } ?>
	<li style="margin-top:4px;"><a class="contact" onclick="transition(<?= $i + 1?>)" href="#contact">Contact</a></li>
</ul>
<script>
	
	$("#nav li a").on("click",function(){
		$("#nav li a").removeClass("active");
		$(this).addClass("active");
	})
	
</script>



<?php //-------------------------------------------------- Content -- ?>
<div id="content">
	<div id="slider">
		 
		<?php 
		
		$get_project = $db->prepare("SELECT * FROM projects WHERE member_id=:id ORDER BY project_order");
		$get_project->bindValue(':id', $memberid);
		$get_project->execute();
		$all_projects = $get_project->fetchAll();
		foreach($all_projects as $project){
			
			$get_cover = $db->prepare("SELECT item_filename FROM items WHERE item_id=:item_id LIMIT 1");
			$get_cover->bindValue(':item_id', $project['cover_id']);
			$get_cover->execute();
			$cover = $get_cover->fetch();
			$cover_filename = $cover['item_filename'];

			if(!$cover_filename){
				
				$get_cover = $db->prepare("SELECT item_filename FROM items WHERE project_id=:pid LIMIT 1");
				$get_cover->bindValue(':pid', $project['project_id']);
				$get_cover->execute();
				$cover = $get_cover->fetch();
				$cover_filename = $cover['item_filename'];
			}

			if($size!='original'){

				$slide_item_path = 'http://portfoliolounge.com/uploads/'.$memberid.'/'.$size.'/'.$cover_filename;

			} else {

				$slide_item_path = 'http://portfoliolounge.com/uploads/'.$memberid.'/'.$cover_filename;

			}?>
		
		<div class="item <?php echo $id[$i]; ?>">
			<div class="info font">
            <h2><?=$project['project_title']?></h2>
            </div>
        	<a class="banner frame" href="portfolio/<?=$project["project_slug"]?>">
				<div class="canvas" data-source="<?=$slide_item_path?>"></div>
			</a>
            
			<div class="info">
				<div class="tools">
					<p class="type"><?php echo $type[$i]; ?></p>
					<p class="software"><?=$project['project_desc']?></p>
					<p class="lang"><?=$project['project_desc']?></p>
				</div>
				<div class="desc-wrapper">
					<p class="desc"><?=$project['project_desc']?></p>
				</div>
			</div>
			<div class="clr"></div>
		</div>
		
		
		<?php }?>
		
		
		
		<div class="item">
			<div class="right-col">
				<h1 class="font">Contact <?php echo $member['first_name'];?></h1>								
				<form class="font" id="contact" method="post">						
					<label>Name:</label><input name="name" /><br />
					<label>Email:</label><input name="email" /><br />
					<label>Textarea:</label><textarea name="message"></textarea>
					<input type="hidden" name="owner_email" value="<?php echo $member['email']?>" />
					<input class="btn" type="submit" value="Send Message">	
					<div class="thank_you">
						<h3>Message Sent!</h3>
					</div>					
				</form>	
			</div>	
		</div>
		
	</div>
</div>
</div>

</body>
</html>
	
	
	
	
<script type="text/javascript">
var currentItem;
var defaultAlpha = 0.15;
var itemWidth = 750;
var itemMargin = 20;
var easetype = "easeOutExpo";
var x = $(window).width();
var y = $(window).height();

function transition(itemNumber){
	currentItem = $('.item:nth-child('+(itemNumber)+')');
	$("#slider").animate({left:((itemWidth+itemMargin)*itemNumber*-1)+x/2+520},600,easetype);
	$(".item").stop().animate({opacity:defaultAlpha},900,easetype);
	currentItem.stop().animate({opacity:1},900,easetype);
}

$(document).ready(function(){
transition(1);
	$("#slider").css({width:itemWidth*itemNumber},600,easetype);
	$("#content").css({width:x-260},600,easetype);		
	$(".item").stop().animate({opacity:defaultAlpha},0);
	$(".item:first-child").stop().animate({opacity:defaultAlpha},0);
});
	

	
</script>
