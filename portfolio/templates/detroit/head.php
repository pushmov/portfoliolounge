<!doctype html>
<head>
<base href="<?php echo $base_url;?>">
<meta name="viewport" content="initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" type="text/css" href="portfolio/css/portfolios.css">
<link rel="stylesheet" type="text/css" href="portfolio/templates/detroit/style.css">
	
<link href='<?php echo $font['url'];?>' rel='stylesheet' type='text/css'>
<?php include 'portfolio/css/style.php';?>
<script src="js/libs/jquery-1.8.3.min.js"></script>
<script src="js/libs/jquery.easing.1.3.js"></script>
<script src="js/libs/jquery.cycle.all.min.js"></script>
<script src="portfolio/js/portfolio.js"></script>
<script src="js/libs/analytics.js"></script>
<?php include 'portfolio/js/tracking.php';?>
<?php 
if($member['first_name']){
	$last_name = $member['last_name'] ? " ".$member['last_name'] : "";
	$site_title = $member['site_title'] ? $member['site_title'] : $member['first_name'].$last_name."'s Portfolio";
} else {
	$site_title = $member['site_title'] ? $member['site_title'] : $member['username']."'s Portfolio";
}?>
<title><?=$site_title?></title>
</head>
<body>

<div id="container">
