<?php 

// List Categories for Sidebar
$get_categories_query = $db->prepare("SELECT * FROM categories ORDER BY category_name");
$get_categories_query->execute();
$get_categories = $get_categories_query->fetchAll(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT, 0);


// ------------------------------------------ //
//    Show all portfolios for each category   //
// ------------------------------------------ //
if($i && $i!='page'){
	
	// Pagination Values
	if(!$x){
		$x = 0;
	}
	$show_per_page = 12;
	$limit_start = $x * $show_per_page;
	$prev_page = $x-1;
	$next_page = $x+1;

	// Get Relevant Category
	$get_category = $db->prepare("SELECT * FROM categories WHERE category_url = :url");
	$get_category->bindValue(':url', $i);
	$get_category->execute();
	$category = $get_category->fetch();
	
	// Calculate Last Pagination Page
	$count_category_members = $db->prepare("SELECT member_id FROM member_skills WHERE category_id = :category_id");
	$count_category_members->bindValue(':category_id', $category['category_id']);
	$count_category_members->execute();
	$total_members = $count_category_members->fetchAll();
	$total_members_in_category = count($total_members);
	$last_page = ($total_members_in_category - $show_per_page) / $show_per_page;
	
	// Get Relevant Members
	$relivant_members = $db->prepare("SELECT member_id FROM member_skills 
		WHERE category_id = :category_id ORDER BY member_id DESC LIMIT :limit_start, :show_per_page");
	$relivant_members->bindValue(':category_id', $category['category_id']);
	$relivant_members->bindValue(':limit_start', $limit_start);
	$relivant_members->bindValue(':show_per_page', $show_per_page);
	$relivant_members->execute();

	
	// Determine Title
	$page_title = $category['category_name'];
	
// ------------------------------------------ //
//             Show ALL portfolios            //
// ------------------------------------------ //
} else {
	
	if(!$x){
		$x = 0;
	}
	$show_per_page = 20;
	$limit_start = $x * $show_per_page;
	$prev_page = $x-1;
	$next_page = $x+1;
	
	// Used for pagination links in place of category name
	$i = 'page';
	
	// Get All Members
	$relivant_members = $db->prepare("SELECT member_id FROM members
		ORDER BY profile_img_true DESC, member_id DESC LIMIT :limit_start, :show_per_page");
	$relivant_members->bindValue(':limit_start', $limit_start);
	$relivant_members->bindValue(':show_per_page', $show_per_page);
	$relivant_members->execute();
	
	// Calculate Last Pagination Page
	$count = $db->query("SELECT member_id FROM members");
	$count_members = $count->fetchAll();
	$total_members = count($count_members);
	$last_page = ($total_members - $show_per_page) / $show_per_page;
	
	// Determine Title
	$page_title = "Most Recent";
	
}?>

<div class="container">
	<div id="page">
    	<div id="page-head">
            <h2><?php echo $page_title;?> Portfolios</h2>
        </div>
        
		<div id="sidebar">
        	<h3>Categories</h3>
			<ul>
				<?php 
				foreach($get_categories as $category) : 
                    $get_category_count = $db->prepare("SELECT * FROM member_skills WHERE category_id=:cid");
                	$get_category_count->bindValue(':cid', $category['category_id']);
                	$categories_count = $get_category_count->fetchAll();
                    $category_count = count($categories_count);
                    ?>
                    <li><a title="View all <?php echo $category['category_name'];?> portfolios." <?php if($i== $category['category_url']){echo 'class="cur"';}?> href="/portfolios/<?php echo $category['category_url'];?>"><?php echo $category['category_name'];?> <span><?php echo $category_count;?></span></a></li>
            	<?php endforeach; ?>
           		<li><a title="View all portfolios." <?php if(!$i){echo 'class="cur"';}?> href="/portfolios">Most Recent<!--<span><?php echo $category_count;?></span>--></a></li>
			</ul>
		</div>
		
		
		<div id="main">
			<ul>
			<?php 
				$all_relivant_members = $relivant_members->fetchAll();
				foreach($all_relivant_members as $this_member) {
			
				// Get Member Data
				$get_member = $db->prepare("SELECT * FROM members WHERE member_id = :memberid");
				$get_member->bindValue(':memberid', $this_member['member_id']);
				$get_member->execute();
				$member = $get_member->fetch();
				
				// Get Cover
				$get_random_item = $db->prepare("SELECT item_filename FROM items WHERE member_id = :memberid ORDER BY RAND() LIMIT 1");
				$get_random_item->bindValue(':memberid', $this_member['member_id']);
				$get_random_item->execute();
				$get_random_items = $get_random_item->fetchAll();
				$item_count = count($get_random_items);
				$random_item = $get_random_items[0];
				
				// Determine Name
				$member_name = $member['first_name']." ".$member['last_name'];
				if(!$member['first_name']&&!$member['last_name']){$member_name = $member['username'];}
				
				// Get Thumbnail
				$member_cover = "uploads/".$member['member_id']."/medium/".$random_item['item_filename'];
				
				// If has items, file exists, has extension
				if($item_count!=0 && file_exists($member_cover) &&  strpos($member_cover, ".")){?>
					<li><a title="Check out <?php echo $member['username'];?>'s free portfolio." style="background-image:url(<?php echo $member_cover;?>);" href="http://<?php echo $member['username'];?>.portfoliolounge.com"><span><?php echo $member_name;?>'s Portfolio</span></a><h3><?php echo $page_title;?> Online Portfolio - <?php echo $member['username'];?></h3><p><?php echo $member['about'];?></p></li>
				
				
				<?php
				}
			} ?>
			</ul>
            <div class="clr"></div>
            <div class="pagination">
            
				<?php // Previous Btn
				if($x==1 && $i!='page'){?>
                
                    <a class="btn gray" href="/portfolios/<?php echo $i;?>">&laquo; Prev Page</a>
            
				<?php } else if($x==1 && $i=='page'){?>
                
                    <a class="btn gray" href="/portfolios">&laquo; Prev Page</a>
                    
				<?php } else if($x!=0){?>

                    <a class="btn gray" href="/portfolios/<?php echo $i."/".$prev_page;?>">&laquo; Prev Page</a>

                <?php }
				
                // Next Btn
                if($x <= $last_page){?>
                
                    <a class="btn gray right" href="/portfolios/<?php echo $i."/".$next_page;?>">Next Page &raquo;</a>

                <?php }  ?>
                
                
            </div>
		</div>
		
		
		
		
	</div>
</div>