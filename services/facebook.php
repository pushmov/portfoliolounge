<?php

require_once('function/config.php');

session_start();
$memberid = $_SESSION['SESS_MEMBER_ID'];
$result = $db->prepare("SELECT * FROM members WHERE member_id=:id LIMIT 1");
$result->bindValue(':id', $memberid);
$result->execute();
$member = $result->fetch();

if($x=='share_pl_url'){

	if(!$member['fb_pl_share']){
		
		$update = $db->prepare("UPDATE members SET fb_pl_share=:share, earned_items=:earned, fb_share_date=:time WHERE member_id=:id");
		$update->bindValue(':share', 1);
		$update->bindValue(':earned', $member['earned_items'] + 5);
		$update->bindValue(':time', time());
		$update->bindValue(':id', $memberid);
		$update->execute();
		echo 'Thank you! 5 items have been added to your account!';
		sendEmail($x, $memberid);
	} else {
		echo 'Thanks again!';
	}


} else if($x=='like_pl_url'){

	if(!$member['fb_pl_like']){
		$update = $db->prepare("UPDATE members SET fb_pl_like=:plike, earned_items=:earned, fb_share_date=:time WHERE member_id=:id");
		$update->bindValue(':plike', 1);
		$update->bindValue(':earned', $member['earned_items'] + 2);
		$update->bindValue(':time', time());
		$update->bindValue(':id', $memberid);
		$update->execute();
		echo 'Thank you! 2 items have been added to your account!';
		sendEmail($x, $memberid);
	} else {
		echo 'Thanks again!';
	}
	
} else if($x=='like_fb_page'){

	if(!$member['fb_page_like']){
		$update = $db->prepare("UPDATE members SET fb_page_like=:fblike, earned_items=:earned, fb_share_date=:time WHERE member_id=:id");
		$update->bindValue(':fblike', 1);
		$update->bindValue(':earned', $member['earned_items'] + 2);
		$update->bindValue(':time', time());
		$update->bindValue(':id', $memberid);
		$update->execute();
		echo 'Thank you! 2 items have been added to your account!';
		sendEmail($x, $memberid);
	} else {
		echo 'Thanks again!';
	}
}

function sendEmail($reward_type, $member){
		
	// Email Administrators
	$message = "Reward Earned! \r\n http://portfoliolounge.com/".$member."/";
	$to = "info@portfoliolounge.com";
	$subject = "Reward $reward_type";
	$from = $email;
	$headers = "From:" . $email;
	mail($to,$subject,$message,$headers);

}?>