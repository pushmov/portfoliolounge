<?php
//Include database connection details
require_once('function/config.php');
session_start();
$memberid = $_SESSION['SESS_MEMBER_ID'];


if($x=='featured-item'){

	//services/owner/?/{item_id}
	$get_item = $db->prepare("SELECT * FROM items WHERE item_id=:itemid LIMIT 1");
	$get_item->bindValue(':itemid', $e);
	$get_item->execute();
	$item = $get_item->fetch();
	
	if(!$item['featured']){
		$addClient = $db->prepare("UPDATE items SET featured=:featured WHERE item_id=:itemid");
		$addClient->bindValue(':featured', 1);
		$addClient->bindValue(':itemid', $e);
		$addClient->execute();

		$get_member = $db->prepare("SELECT * FROM members WHERE member_id=:id LIMIT 1");
		$get_member->bindValue(':id', $item['member_id']);
		$get_member->execute();
		$special_member = $get_member->fetch();

		function sendEmail($recipient, $item){
			// Email Administrators
			$message = "Congratulations!<br><br>";
			$message .= "We added your item to our favorites! Keep up the great work!<br>";
			$message .= "<img width='200' src='https://portfoliolounge.com/uploads/".$recipient['member_id']."/small/".$item['item_filename']."'><br><br>";
			$message .= "Matt Visk, Founder at Portfoliolounge";
			$to = $recipient['email'];
			$subject = "We like your work :)";
			$from = $email;
			$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= "From: Matt Visk <mattvisk@portfoliolounge.com>";
			mail($to,$subject,$message,$headers);
		}
		sendEmail($special_member, $item);


	} else {

		$addClient = $db->prepare("UPDATE items SET featured=:featured WHERE item_id=:item_id");
		$addClient->bindValue(':featured', 0);
		$addClient->bindValue(':item_id', $e);
		$addClient->execute();

	}
	
} else if ($x=='featured-item-home'){
		$addClient = $db->prepare("UPDATE items SET featured_home=:featured WHERE item_id=:item_id");
		$addClient->bindValue(':featured', 1 - $item['featured_home']);
		$addClient->bindValue(':item_id', $e);
		$addClient->execute();

}
?>