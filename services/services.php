<?php

global $memberid, $member; // Global Vars
global $p, $i, $x, $e, $l; // Global Vars


/* GET Logo
-----------------------------------*/
function getFavorites($type){	


	$get_members = $db->query("SELECT * FROM members WHERE top_pick = 1 ORDER BY RAND(".date('dmY').") DESC LIMIT 30");
	while($member = $get_members->fetchAll()){

			$get_cover = $db->prepare("SELECT cover_id FROM projects WHERE member_id=:id AND cover_id != :c ORDER BY RAND() LIMIT 1");
			$get_cover->bindValue(':id', $member['member_id']);
			$get_cover->bindValue(':c', '');
			$get_cover->execute();
			$cover = $get_cover->fetch();
			$cover_id = $cover['cover_id'];
							
			if($cover_id){					
				$get_cover = $db->prepare("SELECT item_filename FROM items WHERE item_id=:itemid LIMIT 1");
				$get_cover->bindValue(':itemid', $cover_id);
				$get_cover->execute();
				$cover = $get_cover->fetch();
			} else {				
				$get_cover = $db->prepare("SELECT item_filename FROM items WHERE member_id=:id ORDER BY RAND() LIMIT 1");
				$get_cover->bindValue(':id', $member['member_id']);
				$get_cover->execute();
				$cover = $get_cover->fetch();
			}			
			$cover_path = "https://portfoliolounge.com/uploads/".$member['member_id']."/medium/".$cover['item_filename'];	
			
			$site_title = $member['site_title'] ? $member['site_title'] : $member['username'];
?>
			<a class="frame font" target="blank" href='https://<?= $member['username'] ?>.portfoliolounge.com'>
				<div class="canvas" data-source="<?= $cover_path;?>">	
					<div class="info" style="text-align:center;">
						<h2 style="text-transform:uppercase;font-size:10px;letter-spacing:1px;" class="font capitalized"><?=$site_title?></h2>
					</div>
				</div>
			</a>		
		
	<?php }
}?>