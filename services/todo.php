<?php

if($x=='get'){	
	$get_products = $db->query("SELECT * FROM todo ORDER BY status DESC, created_at DESC");
	while($product = $get_products->fetchAll()) {
		//$product['created_at'] = time() - $product['created_at'];
		$product['elapsed_time'] = round(abs(time() - $product['created_at']) / 60,0)." min";
		if($product['elapsed_time'] > 1440){
			$product['elapsed_time'] = round(abs(time() - $product['created_at']) / 60 / 60 / 24, 1)." days";
		}
		$product['completed_in'] = round(abs($product['completed_at'] - $product['created_at']) / 60,0)." min";
		$rows[] = $product;
	}
	if($rows){
		print json_encode($rows);
	}
}

if($x=='add'||$x=='update'){
	$data = file_get_contents("php://input");
	$json = json_decode($data);
	$name = $json->name;
	$description = $json->description;
	$category_id = $json->category_id;
	
	if($x=='update'){
	
		$addClient = $db->prepare("UPDATE todo SET name=:name, description=:description, 
			created_at=:created WHERE id=:id AND member_id=:mid");
		$addClient->bindValue(':name', $name);
		$addClient->bindValue(':description', $description);
		$addClient->bindValue(':created', $created_at);
		$addClient->bindValue(':id', $id);
		$addClient->bindValue(':mid', $memberid);
		$addClient->execute();
		
	} else if($x=='add'){
		$add = $db->prepare("INSERT INTO todo(member_id, name, description, created_at) 
			VALUES (:id, :name, :description, :time)");
		$add->bindValue(':id', $memberid);
		$add->bindValue(':name', $name);
		$add->bindValue(':description', $description);
		$add->bindValue(':time', time());
		$add->execute();
	
	}

}


if($x=='delete'){
	$delete = $db->prepare("DELETE FROM todo WHERE id = :id");
	$delete->bindValue(':id', $e);
	$delete->execute();
}

else if($x=='toggle_status'){ 
	$addClient = $db->prepare("UPDATE todo SET status = !status, completed_at = :time, 
		WHERE id = :id");
	$addClient->bindValue(':time', time());
	$addClient->bindValue(':id', $e);
	$addClient->execute();
}

else if($x=='toggle_todo'){ 
	$addClient = $db->prepare("UPDATE members SET show_todo = !show_todo 
		WHERE member_id = :id");
	$addClient->bindValue(':id', $member_id);
	$addClient->execute();
}

?>