<script src='https://www.google.com/recaptcha/api.js'></script>

<script type="text/javascript" src="js/signup.js"></script>
<div class="container">
    


    <?php if(!$i){?>
        
        
        <div class="center-title-block">
            <h2>Choose a portfolio!</h2>
            <h3>We can't wait to see your work!</h3>
        </div>
        
        <div id="packages">
            <div class="package free">
                <h2>Free</h2>
                <a href="signup/free-online-portfolio"><img src="images/upgrade-circle-free.png" /></a>
                <ul>
                    <li>8 Projects</li>
                    <li>20 Uploads</li>
                    <li>Custom Domain</li>
                    <li> ~ </li>
                </ul>
                <p>Just starting out? We offer a free package for everyone to enjoy at no cost. Your portfolio will be stunning.</p>
                <a href="signup/free-online-portfolio" class="btn green">Create Portfolio</a>
            </div>
            
            <div class="package pro">
                <h2>Pro</h2>
                <a href="signup/professional-online-portfolio"><img src="images/upgrade-circle-pro.png" /></a>
                <ul>
                    <li>80 Projects</li>
                    <li>1,000 Uploads</li>
                    <li>Custom Domain</li>
                    <li>Private Projects</li>
                </ul>
                <p>Our Professional package is great for creatives who want to step up in the creative industry. Let's get started!</p>
                <a href="signup/professional-online-portfolio" class="btn">Create Portfolio</a>
            </div>
            
            <div class="package max">
                <h2>Max</h2>
                <a href="signup/maximum-online-portfolio"><img src="images/upgrade-circle-max.png" /></a>
                <ul>
                    <li>1,000 Projects</li>
                    <li>10,000 Uploads</li>
                    <li>Custom Domain</li>
                    <li>Private Projects</li>
                </ul>
                <p>If you are well into your career and need that extra space in your portfolio. Consider our Maximum package.</p>
                <a href="signup/maximum-online-portfolio" class="btn gray">Create Portfolio</a>
            </div>
            
        </div>
        
    
    <?php } else {?>
       
        <?php if ($i=='free-online-portfolio') $account = 1;?>
        <?php if ($i=='professional-online-portfolio') $account = 2;?>
        <?php if ($i=='maximum-online-portfolio') $account = 3;?>
    
        
        <div class="center-title-block">
            <h2>Awesome!</h2>
            <h3>We can't wait to see your work.</h3>
        </div>
        <form id="signup-form" method="post" action="function/signup.php" ng-controller="signup">
            <div class="inputs">
                <label>Your Email</label>
                <input id="email" class="email" name="email" placeholder="Your Email" />
                <div class="error email"></div>
                <label>Username</label>
                <input class="username" id="username" name="username" placeholder="Username" />
                <div class="error username"></div>
                <label>Password</label>
                <input class="password short" name="password" type="password" placeholder="Password" />
                <input class="cpassword short last" name="cpassword" type="password" placeholder="Verify Password" />
                <div class="error passwords"></div>
            </div>
            <div class="clr"></div>
            <br />
            <br />
            <!--
            <div class="g-recaptcha" data-sitekey="6LfAWgITAAAAAJ9l4h7RUJ9jt58sIabUDI5wOyrP"></div>
            -->
            <input type="hidden" name="account" value="<?php echo $account;?>" />
            <input class="promobtn" type="submit" value="Build Portfolio &raquo;" />
            <br />
            <br />
            <a class="cancel" href="signup">Back to Packages</a>
        </form>
        
    
    <?php } ?>
    
</div>