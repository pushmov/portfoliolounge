<style>
header {
  position:absolute;
    background:rgba(0,0,0,.5);
    z-index:999;
    width:100%;
}

</style><script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" src="js/signup.js"></script>
<div class="frame" style="top:0;height:100%;width:100%;position:fixed;z-index:-1;background:rgba(0,0,0,.75)"></div>
<div class="frame" style="top:0;height:100%;width:100%;position:fixed;z-index:-2;">
	<div class="canvas" data-source="img/photos/create-a-portfolio-jeff-sheldon.jpg">
		
	</div>
</div>
<div class="container" style="z-index:1;">
	<?php if($p == 'signup' && !$i){?>
        <div class="center-title-block" style="color:#fff;margin-top:200px;">
            <h2>Choose a portfolio!</h2>
            <h3 style="color:#eee">We look forward to seeing your work.</h3>
        </div>
        <div id="packages">
            <div class="package free">
							
							<h2>Starter</h2>
							<a style="color:#fff;font-weight:700" class="font cool-circle" title="Create a free portfolio website." href="signup/free-online-portfolio">Free!</a>
							<ul>
								<li>4 Projects</li>
								<li>15 Uploads</li>
								<li>Custom Subdomain*</li>
								<li> ~ </li>
								<li> ~ </li>
								<li> ~ </li>
							</ul>
							<a href="signup/free-online-portfolio" class="btn green">Choose <strong>Free</strong></a>
							<p>Just starting out? We offer a free package for everyone to enjoy at no cost. Your portfolio will be stunning.</p>
							<p>*For example, matt.portfoliolounge.com or kristy.portfoliolougne.com</p>
							
            </div>
            <div class="package pro" style="border:none">
                <h2>Pro</h2>
                <a title="Create Pro portfolio." href="signup/professional-online-portfolio"><img alt="Pro Portfolio Plan" height="168" src="images/upgrade-circle-pro_v2.png" /></a>
                <ul>
                    <li>80 Projects</li>
                    <li>1,000 Uploads</li>
                    <li>Free Domain &amp; Setup*</li>
                    <li>Private Projects</li>
                    <li>Premium Templates</li>
                    <li>~</li>
                </ul>
                <a href="signup/professional-online-portfolio" class="btn">Choose <strong>Pro</strong></a>
                <p>Our Professional package is great for creatives who want to step up in the creative industry. Let's get started!</p>
								<p>*For example, YourName.com or MyHardWork.com. Let us know a domain that works for you.</p>
            </div>
            <div class="package max">
                <h2>Max</h2>
                <a title="Create Max portfolio." href="signup/maximum-online-portfolio"><img alt="Max Portfolio Plan" height="168" src="images/upgrade-circle-max_v2.png" /></a>
                <ul>
                    <li>1,000 Projects</li>
                    <li>10,000 Uploads</li>
                    <li>Free Domain &amp; Setup*</li>
                    <li>Private Projects</li>
                    <li>Premium Templates</li>
                    <li>Custom Email</li>
                </ul>
                <a href="signup/maximum-online-portfolio" class="btn gray">Choose <strong>Max</strong></a>
                <p>If you are well into your career and need that extra space in your portfolio. Consider Maximum.</p>
								<p>*For example, AwesomeSauce.com or SweetPhotos.com. Let us know a domain that works for you.</p>
            </div>
        </div>
        
    
    <?php } else {
			if ($i=='free-online-portfolio' || !$i) $account = 1;
			if ($i=='professional-online-portfolio') $account = 2;
			if ($i=='maximum-online-portfolio') $account = 3;
			// affiliate
			//if($expload_url[1]!='portfoliolounge') $account = 1;?>
        
        <div class="center-title-block" style="color:#fff">
            <h2>Awesome!</h2>
            <h3 style="color:#eee">We can't wait to see your work.</h3>
        </div>
        <form id="signup-form" method="post" action="function/signup.php">
					<div class="inputs">
						<label style="color:#888">Your Email</label>
						<input id="email" class="email" name="email" placeholder="Your Email" />
						<div class="error email"></div>
						<label style="color:#888">Username</label>
						<input class="username" id="username" name="username" placeholder="Username" />
						<div class="error username"></div>
						<label style="color:#888">Password</label>
						<input class="password short" name="password" type="password" placeholder="Password" />
						<input class="cpassword short last" name="cpassword" type="password" placeholder="Verify Password" />
						<div class="error passwords"></div>
						<div class="strategic">
							<input name="first_name" value="" placeholder="First Name" />
							<input name="last_name" value="" placeholder="Last Name" />
						</div>
          </div>
					<div class="clr"></div>
					<br />
					<br />
					<!--
					<div class="g-recaptcha" data-sitekey="6LfAWgITAAAAAJ9l4h7RUJ9jt58sIabUDI5wOyrP"></div>
					-->
					<input type="hidden" name="account" value="<?php echo $account;?>" />
					<input class="promobtn" type="submit" value="Build Portfolio &raquo;" />
					<!-- <br />
					<br />
					<a class="cancel" href="signup">Back to Packages</a>-->
        </form>
	<?php } ?>
</div>