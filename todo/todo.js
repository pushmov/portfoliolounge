// Tasks
// ------------------------------------------------------------
function todo($scope, $http) {

  $scope.showTodo = true;
  $scope.format = 'M/d/yy h:mm:ss a';

  $scope.getTasks = function(){
    $http.get("/services/todo/get").success(function(response) {
      $scope.tasks = response;
    });
  }
  $scope.getTasks();

  $scope.addTask = function(data){
    $scope.newTask='';
    //$scope.data.created_at = new Date();
    $http.post("/services/todo/add",data).success(function(response) {
      $scope.getTasks();
    });
  };
  $scope.deleteTask = function(id){
    $http.post("/services/todo/delete/"+id).success(function(response) {
      $scope.getTasks();
    });
  }
  $scope.toggleTaskStatus = function(id){
    $http.post("/services/todo/toggle_status/"+id).success(function(response) {
      $scope.getTasks();
    });
  }

}
