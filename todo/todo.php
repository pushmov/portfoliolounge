<script src="todo/todo.js"></script>
<style>

#todo {
    position:fixed;
    right:0;
    bottom: 0;
    padding: 20px 18px 16px 22px;
    z-index:999999;
    background:#eee;
    border:1px solid #ddd;
    border-radius:6px 0 0 0;
}
    #todo form {
        margin:0 0 8px;
    }
#todo a {
    font-size:11px;
    line-height:1.35em;
    color:#888;
    font-weight:bold;
}
#todo a span {
    color:#aeaeae;
}
#todo a.complete {
    font-weight:;
    color:#aaa;
    font-weight:normal;
}
#todo input {
    font-size:10px;
}
#todo a.delete {
    display:inline-block;
    padding:3px 3px 0 0;
    font-size:9px;
    color:#aaa;
    margin-left:-5px;
}
#todo a.delete:hover {
    color:#888;
}
</style>
<div ng-controller="todo" id="todo" ng-init="taskCount=8" ng-cloak>
    <a class="left" ng-click="showTodo=!showTodo"><h5>Done™</h5></a>
    <div ng-show="showTodo">
        <a class="right" ng-click="taskCount=30">+</a>
        <form>
            <input ng-model="newTask.name" />
            <input ng-show="false" ng-click="addTask(newTask)" type="submit" value="Add Task" />
        </form>
        <ul  style="text-align:left;padding:0;margin:0;">
            <li ng-repeat="task in tasks | limitTo:taskCount" style="padding:0;list-style:none;">
                <a class="delete" ng-click="deleteTask(task.id)">-</a>
                <a ng-class="{'complete' : task.status==0}" ng-click="toggleTaskStatus(task.id)">
                    {{task.name}}
                    <span ng-show="task.status">{{task.elapsed_time}}</span>
                    <span ng-hide="task.status">{{task.completed_in}}</span>
                    
                </a>
            </li>
        </ul>
    </div>
</div>