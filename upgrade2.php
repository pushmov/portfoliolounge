<div class="full-bg ally"></div>


<div class="container bg-image">

	<div class="row container">
		<div class="col-md-4" style="color:#fff">
			<div id="upgrade-content">
				<h2>Choose a plan.</h2>
				<h3>Become founding member #18471</h3>
				
				<p>Portfoliolounge only exists with members like you. We appreciate all members, but you are the ones that make this possible. With much respect, thank you and enjoy your new account.</p>
			</div>
		</div>	
		<div class="col-md-8">
			<div class="upgrade-col max">
				<div class="most-popular"></div>
				<h2>Max</h2>
				<a href="http://portfoliolounge.com/checkout/maximum"><img src="images/upgrade-circle-max.png" /></a>
				<ul>
                    <li>1,000 Projects</li>
                    <li>10,000 Uploads</li>
                    <li>Custom Domain</li>
                    <li>Private Projects</li>
                    <li>Premium Templates</li>
                    <li>Custom Email</li>
				</ul>
				<p>If you are well into your career and need that extra space in your portfolio. Consider our Maximum package.</p>
				<a class="promobtn gray" href="http://portfoliolounge.com/checkout/maximum">Upgrade</a>
			</div>
			
			<div class="upgrade-col pro">
				<h2>Pro</h2>
				<a href="http://portfoliolounge.com/checkout/professional"><img src="images/upgrade-circle-pro.png" /></a>
				<ul>
                    <li>80 Projects</li>
                    <li>1,000 Uploads</li>
                    <li>Custom Domain</li>
                    <li>Private Projects</li>
                    <li>Premium Templates</li>
                    <li>~</li>
				</ul>
				<p>Our Professional package is great for creatives who want to step up in the creative industry. Let's get started.</p>
				<a class="promobtn" href="http://portfoliolounge.com/checkout/professional">Upgrade</a>
			</div>
			<div class="clr"></div>
		</div>
		
		
			
	</div>
</div>