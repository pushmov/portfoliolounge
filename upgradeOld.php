<div class="container">
<div id="page">
    
    
    <div id="upgrade-content">
        <h2>Choose a plan.</h2>
        <p>Take a moment to consider our packages. They both will allow for a much fuller portfolio to show off to potential employers and clients. </p>
        <h3>Why should I upgrade?</h3>
        <p>Here at PortfolioLounge, we receive an enormous amount of item uploads each and every day. This can become quite costly on our end, and with your help, we are be able to continue providing our portfolio services to creatives all around the world.</p>
        <h3>No hard feelings</h3>
        <p>If you are not interested in upgrading, please continue to update your work on your free account. We can't wait to see it!</p>
    </div>
    
    
    
    <div class="upgrade-col max">
        <div class="most-popular"></div>
        <h2>Upgrade to <br /><strong>Maximum</strong></h2>
        <a href="http://portfoliolounge.com/checkout/maximum"><img src="images/upgrade-circle-max.png" /></a>
        <ul>
            <li>1,000 Projects</li>
            <li>10,000 Items</li>
        </ul>
        <p>If you are well into your career and need that extra space in your portfolio. Consider our Maximum package.</p>
        <a class="btn gray" href="http://portfoliolounge.com/checkout/maximum">Upgrade to <strong>Max</strong></a>
    </div>
    
    <div class="upgrade-col pro">
        <h2>Upgrade to <br /><strong>Professional</strong></h2>
        <a href="http://portfoliolounge.com/checkout/professional"><img src="images/upgrade-circle-pro.png" /></a>
        <ul>
            <li>80 Projects</li>
            <li>1,000 Items</li>
        </ul>
        <p>Our Professional package is great for creatives who want to step up in the creative industry. Let's get started.</p>
        <a class="btn" href="http://portfoliolounge.com/checkout/professional">Upgrade to <strong>Pro</strong></a>
    </div>
</div>
</div>